# -*- coding: utf-8 -*-

#import scipy as sp
from scipy import optimize
#from numpy import *
import numpy as np

class Parameter:
    r"""
    Parameter class for optimization problems.
    
    Examples
    --------
    
    Insert here
    """
    def __init__(self, value):
            self.value = value

    def set(self, value):
            self.value = value

    def __call__(self):
            return self.value

def fit(function, parameters, y, x = None):
    r"""
    Wrapper for the fitting function in scipy.
    
    Examples
    --------
    
    >>> # giving initial parameters
    >>> mu = Parameter(7)
    >>> sigma = Parameter(3)
    >>> height = Parameter(5)
    
    >>> # define your function:
    >>> def f(x): return height() * exp(-((x-mu())/sigma())**2)
    
    >>> # fit! (given that data is an array with the data to fit)
    >>> fit(f, [mu, sigma, height], data)
    
    """
    def f(params):
        i = 0
        for p in parameters:
            p.set(params[i])
            i += 1
        return (y - function(x))

    if x is None: x = np.arange(y.shape[0])
    p = [param() for param in parameters]
    tmp = optimize.leastsq(f, p,full_output=True)
    return tmp
