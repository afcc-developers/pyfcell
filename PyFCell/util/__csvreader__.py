# -*- coding: utf-8 -*-
#! /usr/bin/env python

# Author: AFCC
# License: TBD
# Copyright (c) 2013

#from __future__ import print_function


from __baseclass__ import baseobject
import scipy as sp
import pylab as pl
import scipy.io
import scipy.interpolate
import scipy.optimize
from itertools import cycle

class csvreader(baseobject):
    r"""
    .. class::`PyFCell.BAS.cvsreader` -- Importer for CSV type data
    
    
    Astraction for scipy.genfromtext with some nice output.
    
    Parameters
    ----------    
    
    fname : string
        Name of the file to be imported
    delimiter : string (default="'")
        Delimiter argument for the textfile reader.

    Attributes
    ----------
    
    
    """
    def __init__(self,fname="test.csv",delimiter=",",comments="#",
                 names=True,dtype=None,**kwargs):
        super(csvreader,self).__init__(**kwargs)
        self._logger.info("Initialize cvsreader object")
        self._data=sp.genfromtxt(fname,delimiter=delimiter,names=names,
                                 dtype=dtype,deletechars='')
    def get_data(self):
        return self._data
        
    def print_all_column_info(self):
        print "="*50
        print "= Size", self._data.shape
        for name in self._data.dtype.names:
            print name, "\t\t", self._data[name].dtype
        print "="*50
    def print_column_types(self,column=None):
        temp=sp.unique(self._data[column])
        print "The unique entries of column ", column, "are:"  
        for tt in temp:
            print "\t ", tt
    def get_filtered_data(self,column=None,criterion=None):
        indices = sp.where(self._data[column]==criterion)
        return self._data[indices]
        

    
if __name__    =="__main__":    
    dreader = csvreader(fname='../../examples/rhe/Run1.csv')
    dreader.print_all_column_info()
    dreader.print_column_types(column="Action_Name")