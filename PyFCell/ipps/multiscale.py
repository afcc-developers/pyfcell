#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Mayank Sabharwal
# License: TBD
# Copyright (c) 2012, TBD
r"""
**************************************************************************************************************
:mod:`PyFCell.ipps.multiscale` -- Postprocessing and understanding the results obtained using image processing
**************************************************************************************************************


Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""

import numpy as np
from scipy import ndimage
import PyFCell 
import PyFCell.ips.analysis as an

class multiscale(PyFCell.util.baseobject):
    
    
    r"""
    Combining the information across scale, obtained from E-tomo & FIB-SEM data after image processing, to obtain a more generic catalyst layer structure with information about carbon, platinum & ionomer.
    Method is a volume based method which tries to replicate the information as is from the E-tomo into the FIB data. 
    The structure generated has a voxel size same as the FIB-SEM voxel size. 
    Outputs a VTI structure (default) upon successful execution.
    
    TEMfilename='tem.tif', FIBfilename='fib.tif', Cindex=98, Ptindex=92, SolidID=255
    
    Parameters
    ----------
    TEMfilename : string
        Filename for the segmented stack of Tomography data
    FIBfilename : string
        Filename for the segmented stack of FIB-SEM data
    temvoxel : tuple
        Pixel size in X,Y,Z direction for the tomography data
        To be specified as [2,2,2] for a pixel size of 2 in each direction
    fibvoxel : tuple
        Pixel size in X,Y,Z direction for the tomography data
        To be specified as [2,2,2] for a pixel size of 2 in each direction
    Cindex : int
        Material id for Carbon in the tomography dataset
    Ptindex : int
        Material id for Platinum in the tomography dataset
    SolidID : int
        Material id for solid phase in the FIB-SEM dataset
    

    
    
    Examples
    --------
    
    >>> import pylab as pl
    >>> import PyFCell as PFC
    >>> import PyFCell.ipps as post
    >>> crunch = post.multiscale(TEMfilename='../data/ETOMO/CL/MEA11/MEA11 surface cuts-with ionomer surr C- stack-grey values.tif',FIBfilename='../examples/IPS/fib.tif',Cindex=98,Ptindex=92,SolidID=255)
    >>> crunch.run()
    
        
    .. warning:: This documentation is not yet finished.

    .. todo:: Finish documentation; Ionomer placement algorithm to be developed
    
    """    
    def __init__(self, TEMfilename='test1.tif',FIBfilename='test2.tif',temvoxel=[2,2,2],fibvoxel=[5,5,5],Cindex=20,Ptindex=40,SolidID=255,**kwargs):
        
        super(multiscale,self).__init__(**kwargs)

        self.fibvoxel=fibvoxel
        self.temvoxel=temvoxel       
        crunch = PyFCell.ips.inout.reader(TEMfilename)
        temimage = crunch.getarray()
        crunch = an.distanceSD(temimage,voxelsize=temvoxel,material=Cindex)
        crunch.calcPSD()
        self.Csize = crunch.span
        self.Cweight = crunch.psd
        self.Csize = np.delete(self.Csize,[0])
        
        crunch = an.distanceSD(temimage,voxelsize=temvoxel,material=Ptindex)
        crunch.calcPSD()
        self.Ptsize = crunch.span
        self.Ptweight = crunch.psd
        self.Ptsize = np.delete(self.Ptsize,[0])
        
        crunch = PyFCell.ips.inout.reader(FIBfilename)
        fibimage = crunch.getarray()
        crunch = an.distanceSD(fibimage,material=SolidID)
        self.fibImage=crunch.image
        crunch.makeDistanceTransform()
        self.distMatrix = crunch.imDistanceTransform
        self.Csize=self.Csize/self.fibvoxel[0]
        self.Ptsize=self.Ptsize/self.fibvoxel[0]
        
        self.resultimage=self.fibImage
        solidfrac=np.size(np.where(self.resultimage==255)[0])
        self.Cweight=np.round(np.multiply(self.Cweight,solidfrac))
        self.Ptweight=np.round(np.multiply(self.Ptweight,solidfrac))
        

    def makeCarbon(self):
        
        for i,item in reversed(list(enumerate(self.Cweight))):
            
            self.__getStruct(self.Csize[i], item)
            self.distMatrix=ndimage.distance_transform_edt((self.resultimage==255))
        self.resultimage[self.resultimage==1]=20            
            
        
    
    def makePlatinum(self):
        
        for i,item in reversed(list(enumerate(self.Ptweight))):
            
            freq=item
            dim=self.Ptsize[i]
            print freq, dim
            rand=np.random
            rand.seed(None)
            arr=[]
            if dim < 1:
                tmp2=ndimage.distance_transform_edt(1-(self.resultimage==20))
                mp=1-(self.resultimage==255)
                tmp2[mp]=0
                freq=(dim-0.5)*freq*2 + freq
                x,y,z=np.where(tmp2==1)
                if np.size(x)<freq:
                    print "frequency changed from " , freq, " to " , np.size(x) 
                    freq=np.size(x)
                arr=rand.random_sample(freq)
                count=np.size(x)-1
                index=np.asarray(np.round(arr*count),dtype=np.uint32)
                i=x[index[:]]
                j=y[index[:]]
                k=z[index[:]]
                self.distMatrix[i[:],j[:],k[:]]=0
                self.resultimage[i[:],j[:],k[:]]=1
            
            else:
                tmp=ndimage.distance_transform_edt((self.resultimage>0))
                tmp2=ndimage.distance_transform_edt(1-(self.resultimage==20))
                mp=1-(self.resultimage==255)
                tmp[mp]=0
                tmp2[mp]=0
                index=(tmp>=dim)*(tmp2>=dim)
                counter = np.int(np.round(item))
                arr=rand.random_sample(counter)
                x,y,z=np.where(index>0)
                mapx,mapy,mapz = self.__generatemap(self.Ptsize[i])
                if np.size(x)<item:
                    print "there are not enough spaces to place particles of size = ",dim
                    counter = np.size(x)
                err=0
                part=0
                i=0
                while counter>0:
                    rnd = np.round(arr[i]*np.size(x))
                    try:
                        ind=[x[rnd],y[rnd],z[rnd]]
                    except:
                        err=err+1
                        continue
                    count=self.__placeMaterial(ind,dim,mapx,mapy,mapz)
                    counter=counter-count
                    i=i+1
                    part=part+count
                print "Size " +str(dim) + " completed in " +str(i) + " iterations with an error of " + str(err)
                print "Placed a total of " +str(part)+ " particles" 
        self.resultimage[self.resultimage==1]=40            
    
    
    def run(self):
        
        self.makeCarbon()
        self.makePlatinum()
        data={}
        data["carbon"]=(self.resultimage==20).astype('uint8')
        data["platinum"]=(self.resultimage==40).astype('uint8')
        data["SolidPore"]=(self.fibImage).astype('uint8')
        PyFCell.ips.inout.writer('MultiscaleResult',data)

                
        
        
    def __getStruct(self, dim, freq):
        
        rand=np.random
        rand.seed(None)
        arr=[]
        if dim < 1:
            freq=(dim-0.5)*freq*2 + freq
            x,y,z=np.where(self.distMatrix>=1)
            if np.size(x)<freq:
                print "frequency changed from " , freq, " to " , np.size(x) 
                freq=np.size(x)
            arr=rand.random_sample(freq)
            count=np.size(x)-1
            index=np.asarray(np.round(arr*count),dtype=np.uint32)
            i=x[index[:]]
            j=y[index[:]]
            k=z[index[:]]
            self.distMatrix[i[:],j[:],k[:]]=0
            self.resultimage[i[:],j[:],k[:]]=1
        else:
            counter = np.int(np.round(freq))
            arr=rand.random_sample(counter)
            x,y,z=np.where(self.distMatrix>=dim)
            mapx,mapy,mapz = self.__generatemap(dim)
            if np.size(x)<freq:
                print "there are not enough spaces to place particles of size = ",dim
                counter = np.size(x)
            err=0
            part=0
            i=0
            while counter>0:
                x,y,z=np.where(self.distMatrix>=dim)

                rnd = np.round(arr[i]*np.size(x))
                try:
                    ind=[x[rnd],y[rnd],z[rnd]]
                except:
                    err=err+1
                    continue
                count=self.__placeMaterial(ind,dim,mapx,mapy,mapz)
                counter=counter-count
                i=i+1
                part=part+count
                
                
            print "Size " +str(dim) + " completed in " +str(i) + " iterations with an error of " + str(err)
            print "Placed a total of " +str(part)+ " particles"
            
    def __generatemap(self,dim):
        
        sh=2*np.round(dim)+1
        test=np.ones((sh,sh,sh))
        mid=np.int((sh-1)/2)
        test[mid,mid,mid]=0
        tmp=ndimage.distance_transform_edt(test)
        x,y,z=np.where(tmp<=dim)
        x=x-mid
        y=y-mid
        z=z-mid
        return x,y,z
        
    def __correctindices(self,i,j,k):
        
                
        a,b,c = np.shape(self.resultimage)
        index=np.where(i<0)
        i=np.delete(i,index)
        j=np.delete(j,index)
        k=np.delete(k,index)
        
        index=np.where(j<0)
        i=np.delete(i,index)
        j=np.delete(j,index)
        k=np.delete(k,index)
        
        index=np.where(k<0)
        i=np.delete(i,index)
        j=np.delete(j,index)
        k=np.delete(k,index)
        
        index=np.where(k>=c)
        i=np.delete(i,index)
        j=np.delete(j,index)
        k=np.delete(k,index)
        
        index=np.where(j>=b)
        i=np.delete(i,index)
        j=np.delete(j,index)
        k=np.delete(k,index)
        
        index=np.where(i>=a)
        i=np.delete(i,index)
        j=np.delete(j,index)
        k=np.delete(k,index)
        
        return i,j,k
        

    def __placeMaterial(self,index,num,mapx,mapy,mapz):
        
        
        i=mapx+index[0]
        j=mapy+index[1]
        k=mapz+index[2]
        i,j,k = self.__correctindices(i,j,k)
        tmp=(np.ones(np.shape(self.resultimage)))*255
        tmp[i[:],j[:],k[:]]=num
        tmp[index[0],index[1],index[2]]=num
        temp=((tmp<=num)*(self.resultimage==255)).copy()
        self.resultimage[temp]=1
        self.distMatrix[temp]=0
        return np.count_nonzero(temp)