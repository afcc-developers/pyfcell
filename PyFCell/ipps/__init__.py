# Author: Mayank Sabharwal
# License: TBD
# Copyright (c) 2013, AFCC
r"""
*************************************************
:mod:`PyFCell.ipps` -- Image Postprocessing Suite
*************************************************



The module contains functions for the postprocessing and understanding of the structures from the image reconstructions.

The following features are currently available:
    
1. Multiscale combination of E-tomo & FIB-SEM data


"""

from multiscale import multiscale