# -*- coding: utf-8 -*-
# Author: Andreas Putz
# Copyright (c) 2013, OpenPNM
# License: TBD.

r"""

:mod:`PyFCell` --  A collection of classes scripts and executables for fuel cells
=================================================================================
.. module:: PyFCell
    :platform: Linux, Windows

Contents
--------

This package contains the bulk of all python codes for fuel cell releated applications.


Subpackages
-----------


.. list-table:: The :mod:`PyFCell` submodule structure.
   :widths: 10 70 10
   :header-rows: 1

   * - Name
     - Description
     - autoload
   * - :mod:`PyFCell.util`
     - common utilities and classes used by most of the of the modules
     - X
   * - :mod:`PyFCell.deg`
     - Prototype degradation models
       (BET,MIP,DLS,...)
     - X (future: blank)
   * - :mod:`PyFCell.por`
     - crunchers for varios pore and particle size distribution measurements
       (BET,MIP,DLS,...)
     - X (future: blank)
   * - :mod:`PyFCell.rhe`
     - crunchers for rheology measurements
     - X future: blank
   * - :mod:`PyFCell.ips`
     - image processing suite
     - X (future: blank)
   * - :mod:`PyFCell.ipps`
     - image post-processing suite
     - X (future: blank)


 
 
Utility tools
-------------
::

 TODO                --- Todo
 
 
Import
------
>>> import PyFCell as pfc

Inheritance Diagram
--------------------


.. inheritance-diagram:: PyFCell


"""

__version__ = '0.0.1'

__requires__ = [
    'scipy',
    'numpy',
]

__extras_require__ = {
    'app': [
        'envisage',
    ],
}

# __all__ = ['BAS']

# System Imports
import scipy as sp
import numpy as np
import pylab as pl

from itertools import cycle

# System Setup
def setup_pylab():
    r"""
    setup pylab environment
    """
    fig_width_pt        = 2*246.0         # Get this from LaTeX using \showthe\columnwidth
    inches_per_pt   = 1.0/72.27                 # Convert pt to inch
    fig_width_cm        = 30.0
    inches_per_cm   = 0.393
    golden_mean = (pl.sqrt(5)-1.0)/2.0             # Aesthetic ratio
    fig_width = fig_width_cm*inches_per_cm      # width in inches
    fig_height = fig_width*golden_mean          # height in inches
    fig_size =  [fig_width,fig_height]
    params = {
                'backend': 'ps',
    #           'backend': 'svg',
                'axes.labelsize': 16,
                'text.fontsize': 16,
                'axes.titlesize': 16,
                'legend.fontsize': 14,
                'xtick.labelsize': 14,
                'ytick.labelsize': 14,
                'text.usetex': True,
                'figure.figsize': fig_size
            }
    pl.rcParams.update(params)

setup_pylab()

# Imports for PyFCell
import util
import por

try:
  import ips
except:
  print "Image Processing Suite is not available."
  print "One or more dependencies not satisfied.Refer to installation notes for help."
#import ips
try:
    import ipps
except:
    print "Image Post Processing Suite not available."