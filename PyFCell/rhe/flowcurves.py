# -*- coding: utf-8 -*-

"""
:mod:`PyFCell.rhe.flowcurves` -- Flow Curve Analysis
====================================================

Contents
--------

This modlue contains all analysis tools for flowcurve analys.
 
Import
------

>>> import PyFCell as PFC


Submodules
----------

Classes
-------



"""

import PyFCell
import scipy as sp
import pylab as pl
import scipy.io
import scipy.interpolate
import scipy.optimize
from itertools import cycle


g_hatch=cycle([ '/', "\ ", '|', '-', '+', 'x', 'o', 'O', '.', '*' ])
g_color=cycle(['b','g','c','m'])

csvfields_kinexus={
        "Instrument":"MalvernKinexus",
        "ShearStress":"Shear_stress(Pa)",
        "ShearRate":"Shear_rate(s-¹)",
        "AngularVelocity":"Angular_velocity(rad/s)",
        "TimeSample":"Time_(sample)(s)",
        "TimeSequence":"Time_(sequence)(s)",
        "TimeAction":"Time_(action)(s)",
        "ActionName":"Action_Name",
        "NormalForce":"Normal_force(N)"
    }    


class Reader(PyFCell.util.csvreader):
    r"""
    
    Examples
    --------
    
    .. plot::
            
        import pylab as pl
        from PyFCell.rhe.flowcurves import Reader, Cruncher
        
        pl.ion()
    
        dreader = Reader(fname='../examples/rhe/Run1.csv')
        dreader.print_all_column_info()
        dreader.print_column_types(column="Action_Name")
        dreader.plot_history()
        
        pl.show()
    """
    def __init__(self,fname="test.csv",delimiter=",",comments="#",
                 names=True,dtype=None,csvsfields=csvfields_kinexus,**kwargs):
        super(Reader,self).__init__(fname=fname,delimiter=delimiter,comments=comments,
                 names=names,dtype=dtype,**kwargs)
        self._csvfields=csvsfields
    def plot_history(self,ax=None):
        
        from itertools import cycle
        
        if ax==None:
            figure = pl.figure()
            ax = figure.add_subplot(111)
        namex = self._csvfields["TimeSample"]
        namey = self._csvfields["TimeAction"]
        maxy = self._data[namey].max()
        
        name_action = self._csvfields["ActionName"]
        action_names=sp.unique(self._data[name_action])
        action_name_data = self._data[name_action]
        
        ax.plot(self._data[namex],self._data[namey],'o')
        pl.title("History of tests within this sequence")
        pl.xlabel("Sample time since loading [s]")
        pl.ylabel("Action time [s]")
        
        colors=cycle(["b","g"])
        for name in action_names:
            tmpx = self._data[namex][action_name_data==name]
            tmpy = self._data[namey][action_name_data==name]
            print ' ', name, ", ", tmpx.min(), ", ", tmpx.max()
            if (sp.isnan(tmpx.min()) or sp.isnan(tmpx.max()) ):
                # del self._csvfields["ActionName"][name]
                print "Error"
            else:
                ax.text(tmpx.min(),maxy,name, rotation=90,ha='left',va='top')
                ax.axvspan(tmpx.min(),tmpx.max(),facecolor=colors.next(),alpha=0.3)
        #ax.plot(self._data[namey],'o')        
        
    def print_history(self):
        name_action = self._csvfields["ActionName"]
        action_names=sp.unique(self._data[name_action])
        for name in action_names:
            print ' ', name

class Cruncher(PyFCell.util.baseobject):
    r"""
    Postprocessor for pore size distribution datafiles
    filename=None,dataset=None,label='',k=3,s=0
    
    Parameters
    ----------
    fname : string
        Filename to import.
        All occurences of # are interpreted as comments and will be ignored.
        The columns are interpeted as sequnces of (diameter,volume) pairs.
            
    
    
    Examples
    --------
    
    .. plot::
        :include-source:
            
        import pylab as pl
        from PyFCell.rhe.flowcurves import Reader, Cruncher
        
        pl.ion()
    
        dreader = Reader(fname='../examples/rhe/Run1.csv')
        
        dataset=dreader.get_filtered_data(column="Action_Name",criterion="Shear stress ramp up")
        curve1 = Cruncher(data=dataset,loggername="Run1 (Up)")
        fig1=curve1.plot_stress_strain_curve(scale="loglog",color="blue",marker="<",alpha=0.4)
        fig2=curve1.plot_flowcurve_elastic(color="blue",marker=None,alpha=0.4)
        
        dataset=dreader.get_filtered_data(column="Action_Name",criterion="Shear stress ramp down")
        curve1 = Cruncher(data=dataset,loggername="Run1 (Down)")
        fig1=curve1.plot_stress_strain_curve(ax=fig1,scale="loglog",color="blue",marker=">",alpha=0.6)
        fig2=curve1.plot_flowcurve_elastic(ax=fig2,color="blue",marker=None,alpha=0.4,linestyle="--")
        
        pl.show()
    
    
    
    .. warning:: This docuemtation is not yet finished.
    
    .. note:: Hallo

    .. todo:: Finish documentation
        
    """
    
    
    
    def __init__(self,data=None,label='Test',csvfields=csvfields_kinexus,**kwargs):
        r"""
        Constructor of the class
        - Reads and parses datafile
        """
        
        super(Cruncher,self).__init__(**kwargs)
        self._indent="\t"
        
        self._logger.info("="*4)
        self._logger.info("= Flowrate Analysis =")
        self._logger.info("-"*4)
        
        self._myData = {}
        if data != None:
            self._myData['raw']=data
            self.set_active_dataset()
        else:
            self._logger.error("No dataset given")
            
        self._logger.info("= - Shape of dataset:")
        self._logger.info("=   " +  str(self._myData["raw"].shape))
        self._label = label
        
        
        self._csvfields=csvfields
        
        if self._csvfields["Instrument"]=="MalvernKinexus":
            self._logger.info("Malvern Kinexus: Create signed shearrates")
            self._data[self._csvfields["ShearRate"]]= \
                self._data[self._csvfields["ShearRate"]]* \
                sp.sign(self._data[self._csvfields["AngularVelocity"]]).copy()
    
                
    def set_active_dataset(self,name="raw"):
        self._data=self._myData[name]
        
    def get_flowcurve(self):
        return [ self._data[self._csvfields["ShearStress"]], 
                 self._data[self._csvfields["ShearRate"]],
                 self._data[self._csvfields["AngularVelocity"]]
               ]
    def get_normalforce(self):
        return [ self._data[self._csvfields["ShearStress"]],
                 self._data[self._csvfields["NormalForce"]],                
                ]
                
                
    def plot_normal_force(self,ax=None,scale='default',color="green",marker=None,alpha=0.4,linestyle="-"):
        if ax==None:
            figure = pl.figure()
            ax = figure.add_subplot(111)
        x, y = self.get_normalforce()
        
        if scale == 'logy' or scale =='loglog':
            self._logger.info("Set logarithmic sacle for y, take absolute values")
            y=sp.absolute(y)
            ax.fill_between(x, y.min(), y, where=y<=0, facecolor=color, interpolate=True, alpha=0.6)
        else:
            ax.fill_between(x, 0., y, where=y<=0, facecolor=color, interpolate=True,alpha=alpha)
        ax.plot(x,y,marker=marker,linewidth=2,linestyle=linestyle,color=color)
        
        pl.title("Normal Force vs Shear Stress")
        pl.xlabel("Shear Stress [Pa]")
        pl.ylabel("Normal Force [Pa]")
        
        if scale == 'logx':
            #ax=pl.gca()
            ax.set_xscale('log')
        elif scale == 'logy':
            #ax=pl.gca()
            ax.set_yscale('log')     
        elif scale == 'loglog':
            #ax=pl.gca()
            ax.set_xscale('log')
            ax.set_yscale('log')  
        return ax
        
    
    def plot_stress_strain_curve(self,ax=None,scale='default',color="green",marker=None,alpha=0.4,linestyle="-",label=None):
        if ax==None:
            figure = pl.figure()
            ax = figure.add_subplot(111)
        x, y, z = self.get_flowcurve()
        
        if scale == 'logy' or scale =='loglog':
            self._logger.info("Set logarithmic sacle for y, take absolute values")
            y=sp.absolute(y)
            ax.fill_between(x, y.min(), y, where=z<=0, facecolor=color, interpolate=True, alpha=0.6)
        else:
            ax.fill_between(x, 0., y, where=z<=0, facecolor=color, interpolate=True,alpha=alpha)
        ax.plot(x,y,marker=marker,linewidth=2,linestyle=linestyle,color=color,label=label)
        
        pl.title("Flowcurve")
        pl.xlabel("Shear Stress [Pa]")
        pl.ylabel("Shear rates $[s^{-1}]$")
        
        if scale == 'logx':
            #ax=pl.gca()
            ax.set_xscale('log')
        elif scale == 'logy':
            #ax=pl.gca()
            ax.set_yscale('log')     
        elif scale == 'loglog':
            #ax=pl.gca()
            ax.set_xscale('log')
            ax.set_yscale('log')
        return ax
     
    def plot_flowcurve_elastic(self,ax=None,color="green",marker=None,linestyle="-",alpha=0.4):
        if ax==None:
            self._logger.info("Create new axis object")
            figure = pl.figure()
            ax = figure.add_subplot(111)
        x, y, z = self.get_flowcurve()
        
        index0=sp.where(y<=0)
        if sp.size(index0)!=0:
            miny=y[index0].min()
            index1=sp.where(y<=-miny)        
        
            index=sp.unique(sp.append(index0,index1))
        
            x=x[index]
            y=y[index]
            z=z[index]
            ax.fill_between(x, 0., y, where=z<=0, facecolor=color, interpolate=True,alpha=alpha)
            ax.plot(x,y,marker=marker,linestyle=linestyle,linewidth=2,color=color)
        pl.title("Flowcurve (elastic region)")
        return ax
         
        
if __name__    =="__main__":    
    
    pl.ion()
    
    dreader = Reader(fname='../../examples/rhe/Run1.csv')
    dreader.print_all_column_info()
    dreader.print_column_types(column="Action_Name")
    dreader.print_history()
    dreader.plot_history()
    

    dataset=dreader.get_filtered_data(column="Action_Name",criterion="Shear stress ramp up")
    curve1 = Cruncher(data=dataset,loggername="Run1 (Up)")
    fig1=curve1.plot_stress_strain_curve(scale="loglog",color="blue",marker="<",alpha=0.4)
    fig2=curve1.plot_flowcurve_elastic(color="blue",marker=None,alpha=0.4)
    
    dataset=dreader.get_filtered_data(column="Action_Name",criterion="Shear stress ramp down")
    curve1 = Cruncher(data=dataset,loggername="Run1 (Down)")
    fig1=curve1.plot_stress_strain_curve(ax=fig1,scale="loglog",color="blue",marker=">",alpha=0.6)
    fig2=curve1.plot_flowcurve_elastic(ax=fig2,color="blue",marker=None,alpha=0.4,linestyle="--")
    
    pl.show()
    
#    dreader = PyFCell.BAS.csvreader(fname='../../examples/RHE/Run2.csv')
#    dreader.print_all_column_info()
#    dreader.print_column_types(column="Action_Name")
#
#    dataset=dreader.get_filtered_data(column="Action_Name",criterion="Shear stress ramp up")
#    curve1 = CruncherFlowcurve(data=dataset)
#    fig=curve1.plot_stress_strain_curve(scale='loglog',color="green",marker="<",alpha=0.4)
#    
#    dataset=dreader.get_filtered_data(column="Action_Name",criterion="Shear stress ramp down")
#    curve1 = CruncherFlowcurve(data=dataset)
#    fig=curve1.plot_stress_strain_curve(ax=fig,scale='loglog',color="green",marker=">",alpha=0.6)
#    
#    
#    
#    dreader = PyFCell.BAS.csvreader(fname='../../examples/RHE/Run3_50degC.csv')
#    dreader.print_all_column_info()
#    dreader.print_column_types(column="Action_Name")
#
#    dataset=dreader.get_filtered_data(column="Action_Name",criterion="Shear stress ramp up")
#    curve1 = CruncherFlowcurve(data=dataset)
#    fig=curve1.plot_stress_strain_curve(scale='loglog',color="red",marker="<",alpha=0.4)
#    
#    dataset=dreader.get_filtered_data(column="Action_Name",criterion="Shear stress ramp down")
#    curve1 = CruncherFlowcurve(data=dataset)
#    fig=curve1.plot_stress_strain_curve(ax=fig,scale='loglog',color="red",marker=">",alpha=0.6)
#    pl.show()
#    