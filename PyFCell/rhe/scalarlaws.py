# -*- coding: utf-8 -*-

"""
=====================================================
:mod:`PyFCell.rhe.scalarlaws` -- Scalar material laws 
=====================================================

Contents
========

 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======



"""

import PyFCell
import scipy as sp
import pylab as pl
import scipy.io
import scipy.interpolate
import scipy.optimize
from PyFCell.util.opt import Parameter


class HershelBulkley(PyFCell.util.baseobject):
    r"""
    Class to define the linear hershel bulkley law    
    
    Parameters
    ----------
    tau0 : Parameter (0)
        Yield stress
            
    
    
    Examples
    --------
    """
    tau0 = Parameter(0)
    n = Parameter(1)
    K = Parameter(1)
    
    def __init__(self,tau0=0, n=1, K=1):
        self.tau0.set(tau0)
        self.K.set(K)
        self.n.set(n)
        
    def getParameters(self):
        return [self.tau0, self.K, self.n]
    
    def getParameterValues(self):
        return [self.tau0(), self.K(), self.n()]
    
    def gammadot(self,tau=0.0):
        tmp=tau*0
        indices = sp.where(tau>self.tau0())
        tmp[indices]=((tau[indices]-self.tau0())/self.K())**(1/self.n())
        return tmp
        
    def plot_tau(self,tau,ax=None,scale='default',color="green",marker=None,alpha=0.4,linestyle="-",label=None):
        if ax==None:
            #self._logger.info("Create new axis object")
            figure = pl.figure()
            ax = figure.add_subplot(111)
        
        label = label + ' ($tau_0 = ' + str(self.tau0()) + '$'
        label = label + ' , $K = ' + str(self.K()) + '$'
        label = label + ' , $n = ' + str(self.n()) + '$'
        label = label + ')'
        xdata=sp.unique(tau)
        xdata=xdata[sp.where(xdata>self.tau0())]
        ydata=self.gammadot(xdata)        
        ax.plot(xdata,ydata,marker=marker,linewidth=2,linestyle=linestyle,color=color,label=label)
        