# -*- coding: utf-8 -*-
# Author: Andreas Putz
# Copyright (c) 2013, OpenPNM
# License: TBD.
r"""
:mod:`PyFCell.rhe` -- Rheology Data Analysis
============================================

Contents
--------
This module contains all functionality for rheological data analysis.
 
Import
------

>>> from PyFCell import rhe






"""

import flowcurves, scalarlaws