# -*- coding: utf-8 -*-


"""
*************************************************************************
:mod:`PyFCell.por.io` -- IO for porosity data
*************************************************************************



Contents
========
The PyFCell package imports all the functions from the top level modules. 
 






"""

import PyFCell
import scipy as sp
import pylab as pl

class quantachrome(PyFCell.util.baseobject):
    r"""
    
    Parser for Qunatachrome BET measurement data.  
    
    Examples
    --------
    
    .. plot::
            
        import pylab as pl
        from PyFCell.por.io import quntachrome
        
        pl.show()
    """
    def __init__(self,fname="test.txt",**kwargs):
        super(quantachrome,self).__init__(**kwargs)
        self._logger.info("="*40)
        self._logger.info("= Parse Qunatchrome datafile =")
        self._logger.info("-"*40)
        self._datafile=open(fname)
        self._metadata={}
        self._parse_header()
        self._parse_data()
        
    def _parse_header(self):
        r"""
        Parse the header file. 
        """
        self._logger.info("-"*40)
        self._logger.info(" Parse header of datafile")
        line = self._datafile.readline()
        while line:
            line = line.lstrip().rstrip()
            line = line.split()
            #print line
            if "version" in line:
                self._metadata["version"]=line[1]
            if line ==['Sample', 'Desc:', 'Comment:']:
                line=self._datafile.readline().lstrip().rstrip().split()
                self._metadata["Sample Weight"]=[float(line[2]),line[3]]
                self._metadata["Sample Volume"]=[float(line[6]),line[7]]
                
                line=self._datafile.readline().lstrip().rstrip().split()
                self._metadata["Outgas Time"]=[float(line[2]),line[3]]
                self._metadata["Outgas Temperature"]=[float(line[5]),line[6]]
                
                line=self._datafile.readline().lstrip().rstrip().split()
                self._metadata["Gas"]=line[2]
                self._metadata["Bath Temperature"]=[float(line[5]),line[6]]
                
                line=self._datafile.readline().lstrip().rstrip()
                #print line
                #self._metadata["Pressure Tolerance [ads,des]"]=[line[1],line[2]]
                #self._metadata["Pressure Eq Time [ads/des]"]=[line[4],line[5]]
                
                line=self._datafile.readline().lstrip().rstrip()
                #print line
                #self._metadata["Pressure Tolerance [ads,des]"]=[line[1],line[2]]
                #self._metadata["Pressure Eq Time [ads/des]"]=[line[4],line[5]]
            
            if line == ['P/Po', 'Po', 'Volume', '@', 'STP']:
                self._datafile.readline()
                self._datafile.readline()
                self._datafile.readline()
                #self._datafile.readline()
                self._metadata["headers"] = ["P/P0","P0","Volume"]
                self._metadata["units"] = ["[1]","[mmHg]","cc"]
                break
            
            line = self._datafile.readline()
        self._logger.info("-"*40)
            
    def _parse_data(self):
        r"""
        Parse the data part of the file.
        """
        self._logger.info("-"*40)
        self._logger.info(" Parse data part of datafile")
        self._data=sp.genfromtxt(self._datafile,names=self._metadata["headers"])
        self._logger.info(" Shape of data: " + str(self._data.shape))
        self._logger.info(" Column names:  " + str(self._data.dtype.names))
        self._logger.info(" Parse data part of datafile")
        self._logger.info(" Remove " 
                            + str(sp.sum(sp.isnan(self.get_volume())))
                            + " NaN's from dataset"
                         )
        indices = sp.isfinite(self.get_volume())
        self._data=self._data[indices]
        self._logger.info("-"*40)
    
    def get_pp0(self):
        r"""
        Return the P/P0 data column
        """
        return self._data["PP0"]
    def get_p0(self):
        r"""
        Return the P0 data column
        """
        return self._data["P0"]
    def get_volume(self):
        r"""
        Return the volume data column
        """
        return self._data["Volume"]
        
    def get_weight(self,offset=0):
        r"""
        Return the sample weight modified by an offset
        
        Unit:   [g]
        """
        return self._metadata['Sample Weight'][0]
        
    def plot(self,title='',dataset='raw', scale='default'):
        fig=pl.figure()
        
        pl.subplot(2,2,1)
        pl.plot(self.get_pp0(),'o-')
        pl.xlabel('Measurement Point')
        pl.ylabel('$p/p_0$')
        pl.title(title)
        
        pl.subplot(2,2,2)
        pl.plot(self.get_p0(),'o-')
        pl.xlabel('Measurement Point')
        pl.ylabel('$p_0$ [mmHg]')
        
        pl.subplot(2,2,3)
        pl.plot(self.get_volume(),'o-')
        pl.xlabel('Measurement Point')
        pl.ylabel('Volume at STP [cc]')
        
        pl.subplot(2,2,4)
        pl.plot(self.get_pp0(),self.get_volume(),'o-')
        pl.xlabel('$p/p_0$')
        pl.ylabel('Volume at STP [cc]')
       
            
if __name__  == "__main__": 
    reader=quantachrome('../../data/BET/Quantachrom/test01.txt')
    reader.plot(title="Test")
    
    reader2=quantachrome('../../data/BET/Quantachrom/MB071913-3-4.txt')
    reader2.plot(title="MB071913-3-4")
    pl.show()
