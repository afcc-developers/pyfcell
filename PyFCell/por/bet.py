#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2012, TBD

r"""
*************************************************************************
:mod:`PyFCell.por.bet` -- BET data analysis
*************************************************************************


Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""

import PyFCell
import scipy as sp
import pylab as pl
import scipy.io
import scipy.interpolate
import scipy.optimize
from itertools import cycle


g_hatch=cycle([ '/', "\ ", '|', '-', '+', 'x', 'o', 'O', '.', '*' ])
g_color=cycle(['b','g','c','m'])


class CruncherProcessed(PyFCell.util.baseobject):
    r"""
    Postprocessor for pore size distribution datafiles
    filename=None,dataset=None,label='',k=3,s=0
    
    Parameters
    ----------
    filename : string
        Filename to import.
        The expected file format is as follows::
            
            # Dataset: F50
            # Date:
            # Units Diameter	[nm]
            # Units Volume	[cm^3/g_{Carbon}]
            # Data
            # Diameter		Volume		    Diameter		Volume		    Diameter		Volume
            1.4165340992	0.0208168657	1.3637629519	0.0316767376	1.4047205236	0.0206585166
            1.5810999725	0.0198791509	1.5536086754	0.0228929669	1.569555006  	0.0235514114
            1.8128894903	0.0192782879	1.8046820462	0.025856687	    1.8143708001	0.0214545809
            2.0575312653	0.0184822236	2.0599464551	0.0179654296	2.0714829865	0.0187977703
            2.3148335153	0.0172298259	2.3141917665	0.0193206103	2.334202489	    0.0199105329
            2.5487735458	0.0093866491	2.5441771194	0.012406799	    2.5576046258	0.0047663091
            2.7149207775	0.0071622755	2.7159479819	0.0077769581	2.706736024	    0.0092208143
            2.8439353774	0.0075978796	2.8443044015	0.0078474954	2.8417499185	0.0094488371
            2.9736465964	0.0066757118	2.9688711663	0.0081463434	2.9822413308	0.0077259985
            3.107992011	0.0076042136	3.100523746	   0.0080164971	    3.1162767628	0.0089234059
            3.4281686459	0.0297100413	3.4344945792	0.0395573424	3.44491681	0.0334738564
            # End Data 
        
        All occurences of # are interpreted as comments and will be ignored.
        The columns are interpeted as sequnces of (diameter,volume) pairs.
    dataset: ndarray
        Array form of the dataset
    label : string
        Label for the plots
    k : int
        Spline order (default=3)
    s : float 
        Smoothing (no smoothing=0 (default))
            
    
    
    Examples
    --------
    
    >>> import pylab as pl
    >>> import PyFCell as PFC
    >>> from PyFCell.POR.BET import CruncherProcessed
    >>> crunch = CruncherProcessed(filename='_examples/POR/BET.dat',label="BET",loglevel=40)
    >>> ax_cpsd=crunch.plot_cpsd()
    >>> ax_psd=crunch.plot_psd()
    >>> crunch.calc_average_cum()
    >>> crunch.calc_interpolation()
    >>> crunch.plot_cpsd(dataset='average',ax=ax_cpsd)
    >>> crunch.calc_integral_cum()
    >>> pl.show()
    
    .. plot::

       import pylab as pl
       import PyFCell as PFC
       from PyFCell.por.bet import CruncherProcessed
       crunch = CruncherProcessed(filename='_examples/POR/BET.dat',label="BET",loglevel=40)
       ax_cpsd=crunch.plot_cpsd()
       ax_psd=crunch.plot_psd()
       crunch.calc_average_cum()
       crunch.calc_interpolation()
       crunch.plot_cpsd(dataset='average',ax=ax_cpsd)
       crunch.calc_integral_cum()
       pl.show()
    
    .. warning:: This docuemtation is not yet finished.
    
    .. note:: Hallo

    .. todo:: Finish documentation
        
    """
    def __init__(self,filename=None,dataset=None,label='',k=3,s=0,**kwargs):
        r"""
        Constructor of the class
        - Reads and parses datafile
        """
        
        super(CruncherProcessed,self).__init__(**kwargs)
        self._indent="\t"
        
        self._k=k
        self._s=s
        
        self._logger.info("="*40)
        self._logger.info("= BET Analysis Cruncher =")
        self._logger.info("-"*40)
        
        self._logger.info("= - Parse body of the data file (1st row skipped)")
        self._logger.info("=   Filename: " + filename)
        #self._myData=sp.genfromtxt(fname=filename,names=True,comments='#')
        self._myData = {}
        if filename != None:
            self._myData["raw"]=sp.genfromtxt(fname=filename,comments='#')
        elif dataset != None :
            self._myData["raw"]=dataset.copy()
#        print indent,"= Insert zero pore size"
#        zeros = sp.zeros((1,self._myData["raw"].shape[1]))
#        self._myData["raw"]=sp.concatenate(
#                                (zeros,self._myData["raw"])
#                                )
        self._logger.info("= - Shape of dataset:")
        self._logger.info("=   " +  str(self._myData["raw"].shape))
        self._logger.info("= - Number of datasets")
        self._logger.info("=   " + str(self._myData["raw"].shape[1]/2))
        
        
        self._format_color = cycle(['b','g','m'])
        self._label = label
        
        self.calc_average_cum()        
    
    def calc_cPSD(self,xdata=None,ydata=None,direction=1):
        
        ycum = ydata.copy()
        istart = 1
        iend   = ycum.shape[0]
        iinc   = 1
        if direction == -1:
            istart = ycum.shape[0]-1
            iend   = -1
            iinc   = -1
            
        for ii in range(istart,iend,iinc):
            ycum[ii] = ycum[ii]+ycum[ii-iinc]
        return ycum.copy()
        
    def calc_spline(self,xdata=None,ydata=None):
        return sp.interpolate.UnivariateSpline(xdata,ydata,k=self._k,s=self._s)
    
    def calc_interpolation_data(self,xdata=None,ydata=None,npoints=5000,derivative=0):
        spline =sp.interpolate.UnivariateSpline(xdata,ydata,k=self._k,s=self._s)
        new_x = sp.linspace(xdata.min(),xdata.max(),npoints)
        new_x = sp.concatenate((new_x,xdata))
        new_x.sort()
        new_y = spline(new_x)
        new_yder = new_y.copy()
        if derivative > 0:
            for ii in range(0,len(new_x)):
                new_yder[ii] = spline.derivatives(new_x[ii])[derivative]
        return [new_x,new_y,spline,new_yder]
    
    
    def calc_interpolation(self,npoints=5000):
        if not(self._myData.has_key("average")):
            self.calc_average()
            
        data_x = self._myData["average"]["x"]
        data_y = self._myData["average"]["y_cum"]
        data_y_std = self._myData["average"]["y_cum_std"]
        
        spline_y = sp.interpolate.UnivariateSpline(data_x,data_y,k=self._k,s=self._s)
        spline_y_std = sp.interpolate.UnivariateSpline(data_x,data_y_std,k=self._k,s=self._s)
        
        data_x_int = sp.linspace(data_x.min(),data_x.max(),npoints)
        data_x_int = sp.concatenate((data_x_int,data_x))
        data_x_int.sort()
        
        tmp={}
        tmp["x"] = data_x_int.copy()
        tmp["y"] = spline_y(data_x_int).copy()
        tmp["y_std"] = spline_y_std(data_x_int).copy()
        tmp["y_spline"] = spline_y
        tmp["y_std_spline"] = spline_y_std
        self._myData["av_spline"]=tmp
        return tmp

    def calc_average_cum(self):
        print self._indent, "-"*30
        print self._indent, "= Average the cumulative PSD of the input data "
        cols_x = range(0,self._myData['raw'].shape[1],2)
        cols_y = range(1,self._myData['raw'].shape[1],2)
        
        print self._indent, "= - Calculate common list of x points"
        xdata = self._myData['raw'][:,cols_x].flatten()
        xdata = sp.sort(xdata)
        xdata = sp.unique(xdata)
        xmin = self._myData["raw"][0,cols_x].max()
        xmax = self._myData["raw"][-1,cols_x].min()
        minindices = sp.where(xdata >= xmin)
        xdata = xdata[minindices]
        maxindices = sp.where(xdata <= xmax)
        xdata = xdata[maxindices]
        ydata = sp.zeros((xdata.shape[0],len(cols_y)))
        
        print self._indent, "= - Calculate spline interpolation"
        for ii in range(0,len(cols_y)):
            x = self._myData['raw'][:,cols_x[ii]]
            y = self._myData['raw'][:,cols_y[ii]]
            ycum = self.calc_cPSD(xdata=x,ydata=y)
            spline=self.calc_spline( xdata=x, ydata=ycum )
            ydata[:,ii] = spline(xdata)
        
        print self._indent, "= - Calculate statistics"        
        
        y_cum = sp.mean(ydata,axis=1)
        y_std  = sp.std(ydata,axis=1)
        
                
        
        data={}
        data["x"]           = xdata
        data["y_cum"]       = y_cum
        data["y_cum_std"]   = y_std
        print self._indent, "= Datarange [xmin,xmax]: [", str(data["x"].min()), ", ",  str(data["x"].max()), "]"
        self._myData["average"]=data
        return data
    
    def calc_integral_cum(self,intervals=[0.,2.,20.,100.],ax=None):
        
        points=intervals
        print self._indent, "-"*30
        print self._indent, "= Integrate data over specified range"
        print self._indent, "= Datarange (average) [xmin,xmax]: [", str(self._myData["average"]["x"].min()), ", ",  str(self._myData["average"]["x"].max()), "]"        
        print self._indent, "= Datarange (spline)  [xmin,xmax]: [", str(self._myData["av_spline"]["x"].min()), ", ",  str(self._myData["av_spline"]["x"].max()), "]"         
        print self._indent, "= Points before truncation", points
        if points[0] < self._myData["av_spline"]["x"].min():
            points[0] = self._myData["av_spline"]["x"].min()
        if points[-1] > self._myData["av_spline"]["x"].max():
            points[-1] = self._myData["av_spline"]["x"].max()
        print self._indent, "= Points after truncation", points
        data_int=[]
        spline = self._myData["av_spline"]["y_spline"]
        spline_std = self._myData["av_spline"]["y_std_spline"]

        ax = self.plot_cpsd(dataset='average',scale='logx',ax=ax)
        pl.hold(True)
        tmp = sp.zeros((len(points)-1),
                       dtype={'names':['name','xmin','xmax','integral','error'],
                              'formats':['S10','f4','f4','f4','f4']})
        colors = cycle(['b','g','m'])
        for ii in range(0,len(points)-1):
            xmin=points[ii]
            xmax=points[ii+1]            
            integral = spline(xmax)-spline(xmin)
            error    = spline_std(xmax)-spline_std(xmin)
            print self._indent, "=   Integral:", xmin, xmax, integral, error
            pl.axvspan(xmin,xmax,alpha=0.5,color=next(colors))
            text="$\int \limits_{"+str(round(xmin,3))+"}^{"+str(xmax)+"} f(x)\mathrm{d}x = " + str(round(integral,3)) + " \pm " + str(round(error,3)) + "$"
            #posx=10**(sp.log(xmin)+0.5*(sp.log(xmax)-sp.log(xmin)))
            posx = xmin + 0.5 * (xmax-xmin)
            pl.text(posx,spline(posx)+spline_std(posx)
                    ,text
                    ,fontsize=20
                    ,horizontalalignment='center'
                    )
            tmp[ii][0]=self._label
            tmp[ii][1]=xmin
            tmp[ii][2]=xmax
            tmp[ii][3]=integral
            tmp[ii][4]=error
        return tmp
            
    def plot_cpsd(self,title='',dataset='raw',ax=None, scale='default'):
        colors = self._format_color
        if ax==None:
                print "TODO"
                figure = pl.figure()
                ax = figure.add_subplot(111)
        if title=='':
                title=self._label
        if dataset == 'raw' :
            dataset = self._myData["raw"]
            #pl.hold(True)
            for i in range(0,dataset.shape[1],2):
                color = next(colors)
                ycum = self.calc_cPSD( xdata=dataset[:,i]
                                     , ydata=dataset[:,i+1]
                                     )
                ax.plot(dataset[:,i],ycum,
                        color+'o--',
                        drawstyle='steps-post',
                        linestyle='--',
                        linewidth=2,
                        label="Dataset " + str(i/2)
                        )
                
                xint,yint,spline,yint_der = self.calc_interpolation_data( 
                                                        xdata=dataset[:,i]
                                                      , ydata=ycum
                                                      , derivative=1)                                                        
                ax.plot( xint,yint,color+':'
                         ,label = "(k="+str(self._k)+",s="+str(self._s)+")"
                        )
                        
            pl.title('Cumulative Pore Size Distribution of ' + title)
            pl.xlabel('Pore Diameter [nm]')
            pl.ylabel('Pore Volume $[cm^3/g_{Carbon}]$')
            pl.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        elif dataset == 'average':
            self.calc_average_cum()
            dataset = self._myData["average"]            
            ax.errorbar(dataset["x"], dataset["y_cum"]
                        , yerr = dataset["y_cum_std"]
                        , linewidth = 1, elinewidth = 1
                        , label = title
                        , color='red',marker='o',linestyle="-.")
            if self._myData.has_key("av_spline"):
                xdata = self._myData["av_spline"]["x"]
                ydata = self._myData["av_spline"]["y"]
                ydata_std = self._myData["av_spline"]["y_std"]
                ax.plot(xdata,ydata,'r',linewidth=2)
                ax.plot(xdata,ydata+ydata_std,'r--',linewidth=1)
                ax.plot(xdata,ydata-ydata_std,'r--',linewidth=1)
            pl.title('Discrete vs Continuous Pore Size Distribution')
            pl.xlabel('Pore Diameter [nm]')
            pl.ylabel('Pore Volume $[cm^3/g_{Carbon}]$')
            ax.legend()
            
        if scale == 'logx':
            ax=pl.gca()
            ax.set_xscale('log')
            
        return ax
            
    def plot_psd(self,title='',dataset='raw',ax=None, scale='default'):
        print self._indent,"=","-"*30
        print self._indent,"=","Print Pore Size Distributions"
        if ax==None:
                print "TODO"
                figure = pl.figure()
                ax = figure.add_subplot(111)
        pl.gca()
        if title=='':
                title=self._label
        if dataset == 'raw' :
            print self._indent,
            dataset = self._myData["raw"]
            #pl.hold(True)
            for i in range(0,dataset.shape[1],2):
                color = next(self._format_color)
                ax.step(dataset[:,i],dataset[:,i+1],
                        color+'o-',
                        linewidth=2,
                        where='post',
                        label="Dataset " + str(i/2)
                       )
                #x,y = self.calc_interpolation_data(xdata=dataset[:,i]
                #                                   , ydata=dataset[:,i+1])
                #ax.plot(x,y,color+'-.')
                ycum = self.calc_cPSD( xdata=dataset[:,i]
                                     , ydata=dataset[:,i+1]
                                     )
                
                xint,yint,spline,yint_der = self.calc_interpolation_data( 
                                                        xdata=dataset[:,i]
                                                      , ydata=ycum
                                                      , derivative=1)                                                        
                
                ax.plot( xint,yint_der,color+':'
                        ,label = "(k="+str(self._k)+",s="+str(self._s)+")"
                        )
                        
            pl.title('Pore Size Distribution of ' + title)
            pl.xlabel('Pore Diameter [nm]')
            pl.ylabel('Pore Volume $[cm^3/g_{Carbon}]$')
            pl.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        elif dataset == 'average':
            self.calc_average()
            dataset = self._myData["average"]            
            ax.errorbar(dataset["x"], dataset["y_cum"]
                        , yerr = dataset["y_cum_std"]
                        , linewidth = 1, elinewidth = 1
                        , label = title
                        , color='red',marker='o',linestyle="-.")
            if self._myData.has_key("av_spline"):
                xdata = self._myData["av_spline"]["x"]
                ydata = self._myData["av_spline"]["y"]
                ydata_std = self._myData["av_spline"]["y_std"]
                ax.step(xdata,ydata,'r',linewidth=2)
                ax.step(xdata,ydata+ydata_std,'r--',linewidth=1)
                ax.step(xdata,ydata-ydata_std,'r--',linewidth=1)
            pl.title('Pore Size Distribution')
            pl.xlabel('Pore Diameter [nm]')
            pl.ylabel('Pore Volume $[cm^3/g_{Carbon}]$')
            ax.legend()
            
        if scale == 'logx':
            ax=pl.gca()
            ax.set_xscale('log')
        return ax

class SurfaceArea(PyFCell.util.baseobject):
    r"""
    Calculates the surface area based on the BET equation.
    
    
    The BET equation:
    
    .. math::
    
        \frac{1}{V\left( \frac{p_0}{p} -1 \right)} = \frac{C-1}{V_m C} \frac{p}{p_0} + \frac{1}{V_m c}
        
        
    where
    
    * :math:`V` denotes the absorbed gas quantity (e.g. colume units, weight, ...)
    * :math:`V_m` denotes the monolayer absorbed gas quantity (volume, weight, ...)
    * :math:`C` denotes the BET constant (dimensionless)
    * :math:`p` denotes the quilibrium pressure
    * :math:`p_0` denotes the saturation pressure
    """
    
    def __init__(self,samplename='test',pp0=None,p0=None,vol=None,**kwargs):
        self._pp0=pp0
        self._p0=p0
        self._vol=vol
    
    def calcBETplot(self):
        ydata=1/(self._vol*(1/self._pp0)-1)
        xdata=self._pp0
        return [xdata,ydata]
    
    def plotBET(self,title='',ax=None):
        if ax==None:
                figure = pl.figure()
                ax = figure.add_subplot(111)
        pl.gca()
        [xdata,ydata]= self.calcBETplot()
        ax.plot(xdata,ydata,'-o')
        pl.title('The BET plot')
        pl.xlabel('$p/p_0$')
        pl.ylabel(r"$\frac{1}{V ( \frac{p_0}{p} -1 )}$")
        
        return ax
        
    def calcBETfit(self,xmin=0.05,xmax=0.35,ax=None):
        r"""
        Calculate the BET constants.
        
        The calculation of the BET constants involves a linear fit of the
        ad/desorption isotherm aainst :mat:`p/p_0` . This fit is valid in the
        linear regime, usually :math: 0.05 < p/p_0 < 0.35 .
        
        * Fit function: :math:`y = ax+b`
        * Calculate: :math:`V_m = \frac{1}{a+b}`
        * Calculate: :math:`C = 1+\frac{a}{b}`
        """
        
        indices=(self._pp0>xmin)*(self._pp0<xmax)
        xdata,ydata = self.calcBETplot()
        xdata=xdata[indices]
        ydata=ydata[indices]
        
        from PyFCell.util.opt import fit, Parameter
        
        a=Parameter(1)
        b=Parameter(0)
        
        def f(x): 
            return a()*x+b()
        
        fit(f,[a,b],y=ydata,x=xdata)
        
        C = 1+a() / b()
        V_m = a() / ( a()+b() )
        
        returns={'a':a(),'b':b(),'C':C,'V_m':V_m}
        print returns
        self._fitparams=returns
        
        if ax!=None:
            ax.plot(self._pp0,f(self._pp0),'r--')
            ax.plot(xdata,f(xdata),'r-',linewidth=2)
            ax.axvspan(xdata.min(), xdata.max(), facecolor='r', alpha=0.2)
            pl.figtext(ydata.min(),xdata.max() , 'Fit Function: $y=ax+b$',ha='left',fontsize=14,figure=ax)
        
        return returns
        
    def calc_surface_areas(self, A_ads=16.2, V_ads=1):
        r"""
        Calculate the total and the specific surface area:
        
        * :math:`S_\text{total}= \frac{V_m \ N_A \ A_\text{ads}}{V_\text{ads}}`
        * :math:`S_\text{BET} = \frac{S_\text{total}}{w}`
        """
        from scipy.constants import Avogadro
        
        S_total = self._fitparams["V_m"]*Avogadro
        
    

        
        
if __name__    =="__main__":    
#    crunch = CruncherProcessed(filename='../../examples/POR/BET.dat',label="BET",loglevel=10,k=1,s=0.0000)
#    #crunch = PoreSDcruncher(filename='DenkaOSAB.dat',label="Denka OSAB")
#    ax_cpsd=crunch.plot_cpsd()
#    ax_psd=crunch.plot_psd()
#    crunch.calc_average_cum()
#    crunch.calc_interpolation()
#    #crunch.plot_cpsd(dataset='average')
#    crunch.plot_cpsd(dataset='average',ax=ax_cpsd)
#    crunch.calc_integral_cum()
#    #crunch.plot_datasets()
#    #crunch.plot_datasets(scale='logx')
#    #crunch.calc_interpolation(npoints=5000,order=1,smoothing=0)
#    #crunch.plot_datasets(dataset='average')
#    #crunch.plot_datasets(dataset='average',scale='logx')
#    #intdata=crunch.calc_integral()
#    pl.show()
    import PyFCell as pfc
    import PyFCell.por as por
    
    reader = por.io.quantachrome('../../data/BET/Quantachrom/test01.txt')
    reader.plot(title="Test")
    
    BET = por.bet.SurfaceArea(pp0=reader.get_pp0(),p0=reader.get_p0(),vol=reader.get_volume() )
    fig = BET.plotBET()
    BET.calcBETfit(ax=fig)
    pl.show()
    
    
    