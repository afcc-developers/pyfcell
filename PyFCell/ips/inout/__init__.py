# Author: Mayank Sabharwal
# License: TBD
# Copyright (c) 2013, AFCC
"""
**************************************************************************
:mod:`PyFCell.ips.inout` -- Input/Output module for Image Processing Suite
**************************************************************************



This module provides the necessary input and output interfaces required for the Image Procesing suite

       
"""

from input import reader
from output import writer