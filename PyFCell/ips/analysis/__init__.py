# Author: Mayank Sabharwal
# License: TBD
# Copyright (c) 2013, AFCC
"""
*************************************************************************
:mod:`PyFCell.ips.analysis` -- Pore and Particle Size Data Analysis
*************************************************************************


The module contains functions for the postprocessing and analysis of Catalyst Layer data.
Current capabilities include:
    
1. Porosity statistics & Local Variations
2. Pore/Particle Size Distribution based on distance transform
3. Pore/Particle Size Distribution based on area counting
4. Tortuosity calculations based on tracer methods

"""

from basicStats import basicStats
from distanceSD import distanceSD
from areaSD import areaSD
from tortuosity import tortuosity