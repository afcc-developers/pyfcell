#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Mayank Sabharwal
# License: TBD
# Copyright (c) 2012, TBD

r"""
*************************************************************************
:mod:`PyFCell.ips.analysis` -- Postprocessing and analysis of CL data
*************************************************************************

.. module:: PyFCell.ips.analysis

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""
import numpy as np
import pylab as pl
import random
import scipy as sp
import math
import PyFCell
from itertools import cycle


class tortuosity(PyFCell.util.baseobject):
    r"""
        Calculates tortuosity of flow through the material id specified using tracer method.
        Number of tracers are randomly started from one face and the distance covered by it to exit from the other face is used to calculate tortuosity.
        The direction of flow is assumed to be Z direction.
        
        image, tracernum = 100, label='Pore', voxelsize=[1, 1, 1], voxelunit='nm',direction = 'Z', material=0
        
        Parameters
        ----------
        image : ndarray
            Input array for the 3D catalyst structure
        tracernum : int
            Number of tracers to use. Default value is 100.            
        label : string
            Specify the material or type of distribution
            Eg: For Pore Size Distribution label = 'Pore'
        voxelsize : tuple
            Pixel size in X,Y,Z direction
            To be specified as [2,2,2] for a pixel size of 2 in each direction
        voxelunit : string
            Unit of length for the voxelsize
        direction : string
            Direction in which tortuosity is needed. Default is Z=direction.
        material : int
            Material ID (in the input file) of the component for which the distribution is required
                
        
        
        Examples
        --------
        
        >>> import pylab as pl
        >>> import PyFCell as PFC
        >>> import PyFCell.ips.analysis as ipsanalysis
        >>> import PyFCell.ips.inout as io
        >>> obj=io.reader(fname='../examples/IPS/fib.tif')
        >>> image=obj.getarray()
        >>> crunch = ipsanalysis.tortuosity(image,voxelsize=[5,5,5],material=0,loglevel=40)
        >>> crunch.calc()
        >>> ax_bar=crunch.plot_tracer(label='fib')
        >>> pl.show()
        
        
        .. plot::
    
            import pylab as pl
            import PyFCell as PFC
            import PyFCell.ips.analysis as ipsanalysis
            import PyFCell.ips.inout as io
            obj=io.reader(fname='../examples/IPS/fib.tif')
            image=obj.getarray()
            crunch = ipsanalysis.tortuosity(image,voxelsize=[5,5,5],material=0,loglevel=40)
            crunch.calc()
            ax_bar=crunch.plot_tracer(label='fib')
            pl.show()
            
        .. warning:: This documentation is not yet finished.
        
    
        .. todo:: Finish documentation
            
        """
    def __init__(self,image, tracernum = 100, label='Pore', voxelsize=[1, 1, 1], voxelunit='nm',direction = 'Z', material=0, **kwargs):
                
        
        super(tortuosity,self).__init__(**kwargs)
        self.voxelsize=voxelsize
        self.__imageReorder(image,  material, direction)
        self.tracernum=tracernum    #number of tracers to be used
        self.thickness=np.shape(self._image)[2]*self.voxelsize[2]
        self._format_color = cycle(['b','g','m','r','y','o'])
        self._indent = '\t'
        self._direction=direction
        
        
    def __imageReorder(self, image, material, direction = 'Z'):
        
        tmp=[]        
        if direction == 'X':
            for i in range(np.shape(image)[0]):
                tmp.append(image[i,:,:])
            image = np.dstack(tmp)
            self.voxelsize = [self.voxelsize[1],self.voxelsize[2],self.voxelsize[0]]
                
        elif direction == 'Y':
            for i in range(np.shape(image)[1]):
                tmp.append(image[:,i,:])
            image=np.dstack(tmp)
            self.voxelsize = [self.voxelsize[0],self.voxelsize[2],self.voxelsize[1]]
        self._image=(image!=material).copy()
        self.slice_num = (np.shape(image)[2])
        
    def calc(self):
        
        print self._indent,"="*50
        Tortuosity=np.zeros(self.tracernum,dtype=float)
        for i in range(self.tracernum):
            temp1=self._image[:,:,0]
            m=np.size(self._image,0)
            n=np.size(self._image,1)
            x=np.zeros(self.slice_num)
            y=np.zeros(self.slice_num)
            step=np.zeros(self.slice_num)
            x[0]=random.randint(0,m-1)
            y[0]=random.randint(0,n-1)

            
            while temp1[x[0],y[0]]!=0:
                x[0]=random.randint(0,m-1)
                y[0]=random.randint(0,n-1)
            for j in range(self.slice_num-1):
                temp=self._image[:,:,j+1]
                
                #find tracer on next plane
                index_out=0
                x[j+1]=x[j]
                y[j+1]=y[j]
                step[j+1]=0
                while ((index_out==0) and (temp[x[j+1]+step[j+1],y[j+1]]*temp[x[j+1]-step[j+1], y[j+1]]*temp[x[j+1],y[j+1]+step[j+1]]*temp[x[j+1],y[j+1]-step[j+1]]*temp[x[j+1]+step[j+1],y[j+1]+step[j+1]]*temp[x[j+1]-step[j+1],y[j+1]-step[j+1]]*temp[x[j+1]+step[j+1],y[j+1]-step[j+1]]*temp[x[j+1]-step[j+1],y[j+1]+step[j+1]])!=0):
                    if (x[j+1]+step[j+1]==m-1 or x[j+1]-step[j+1]==0 or y[j+1]+step[j+1]==n-1 or y[j+1]-step[j+1]==0):
                        index_out=1
                    else:
                        step[j+1]+=1

                x_temp=np.zeros(8)
                y_temp=np.zeros(8)
                if temp[x[j+1]+step[j+1],y[j+1]]==0:
                    x_temp[0]=x[j+1]+step[j+1]
                    y_temp[0]=y[j+1]
                
                if temp[x[j+1]-step[j+1],y[j+1]]==0:
                    x_temp[1]=x[j+1]-step[j+1]
                    y_temp[1]=y[j+1]
                    
                if temp[x[j+1],y[j+1]+step[j+1]]==0:
                    x_temp[2]=x[j+1]
                    y_temp[2]=y[j+1]+step[j+1]
            
                if temp[x[j+1],y[j+1]-step[j+1]]==0:
                    x_temp[3]=x[j+1]
                    y_temp[3]=y[j+1]-step[j+1]
                
                if temp[x[j+1]+step[j+1],y[j+1]+step[j+1]]==0:
                    x_temp[4]=x[j+1]+step[j+1]
                    y_temp[4]=y[j+1]+step[j+1]
                
                if temp[x[j+1]-step[j+1],y[j+1]-step[j+1]]==0:
                    x_temp[5]=x[j+1]-step[j+1]
                    y_temp[5]=y[j+1]-step[j+1]
                
                if temp[x[j+1]+step[j+1],y[j+1]-step[j+1]]==0:
                    x_temp[6]=x[j+1]+step[j+1]
                    y_temp[6]=y[j+1]-step[j+1]
                
                if temp[x[j+1]-step[j+1],y[j+1]+step[j+1]]==0:
                    x_temp[7]=x[j+1]-step[j+1]
                    y_temp[7]=y[j+1]+step[j+1]
                
                for k in range(8):
                    if x_temp[7-k]>0:
                        x[j+1]=x_temp[7-k]
                        y[j+1]=y_temp[7-k]
                
            L2=0.0
            
            for a in range(self.slice_num-1):
                L2=L2+math.sqrt(math.pow((x[a+1]-x[a])*self.voxelsize[0],2)+math.pow((y[a+1]-y[a])*self.voxelsize[1],2))
            
            Tortuosity[i]=(L2+self.thickness)/self.thickness
            self.L2=L2
            if index_out==1:
                Tortuosity[i]=0.0
        
        self.avgTortuosity=0.0
        tmp=np.where(Tortuosity!=0)
        self.N_good_tracer=len(tmp[0])
        tmp2=np.cumsum(Tortuosity)    
        self.Tortuosity=Tortuosity
        if self.N_good_tracer !=0:
            self.avgTortuosity=tmp2[-1]/self.N_good_tracer
        
        print self._indent,"= Tortuosity Calculations complete - " + self._direction
        print self._indent,"= Average Tortuosity = ", self.avgTortuosity
        print self._indent,"= Failed tracer count = ", self.tracernum-self.N_good_tracer
        print self._indent,"= Effectively  ", np.round((self.N_good_tracer*1.0/self.tracernum),decimals=2)*100, "% of the tracers made it out"
        print self._indent,"="*50
        
        
    def plot_tracer(self,label='Sample1',ax=None, scale='default'):
        
        self._basename=label
        colors = self._format_color
        color=next(colors)
        ind=np.arange(self.tracernum)
        
        if ax==None:
            figure = pl.figure()
            ax = figure.add_subplot(111)
            ax.bar(ind,self.Tortuosity, color=color)
            ax.set_xlabel('Tracer Number')
            ax.set_ylabel('Tortuosity')
            tkw = dict(size=4, width=1.5)
            ax.tick_params(axis='y', colors=color, **tkw)
            ax.yaxis.label.set_color(color)
            
        else:
            ax2=ax.twinx()            
            ax2.bar(ind,self.Tortuosity, color=color)
            ax2.set_ylabel('Tortuosity')
            tkw = dict(size=4, width=1.5)
            ax2.tick_params(axis='y', colors=color, **tkw)
            ax2.yaxis.label.set_color(color)
        color=next(colors)
        ax.axhline(y=self.avgTortuosity,color=color,linewidth=2)
        rectdict={}
        rectdict["facecolor"]='white'
        ax.text(np.size(self.Tortuosity)-0.05*np.size(self.Tortuosity),self.avgTortuosity+0.05,str(np.round(self.avgTortuosity,3)),fontsize=12,color='red',bbox=rectdict)
        pl.title("Tortuosity - "+self._direction)
        
if __name__    =="__main__":    
    import PyFCell.ips.inout as io
    #obj=io.reader(fname='../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif')
    obj=io.reader(fname='/home/maysab/FIB/CCM100512-DH-22/Segmented_Data set 3.align.tif')
    image=obj.getarray()
    crunch = tortuosity(image,voxelsize=[5,5,5],material=0,loglevel=40,tracernum=1000)
    crunch.calc()
    ax_cpsd=crunch.plot_tracer(label='fib')
    pl.show()
        