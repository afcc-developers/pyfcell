#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Mayank Sabharwal
# License: TBD
# Copyright (c) 2012, TBD

r"""
*****************************************************************************
:mod:`PyFCell.ips.analysis.areaSD` -- Postprocessing and analysis of CL data
*****************************************************************************


Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""

import numpy as np
import pylab as pl
import scipy 
from scipy import ndimage
import mahotas
import PyFCell
from itertools import cycle


class areaSD(PyFCell.util.baseobject):
    r"""
    Post processor for calculation of pore/particle size distribution based on area counting of 2D slices
    
    image, label='Pore', voxelsize=[1, 1, 1], voxelunit='nm',material=0
    
    Parameters
    ----------
    image : ndarray
        Input array for the 3D catalyst structure
    label : string
        Specify the material or type of distribution
        Eg: For Pore Size Distribution label = 'Pore'
    voxelsize : tuple
        Pixel size in X,Y,Z direction
        To be specified as [2,2,2] for a pixel size of 2 in each direction
    voxelunit : string
        Unit of length for the voxelsize
    material : int
        Material ID (in the input file) of the component for which the distribution is required
    
    
    Examples
    --------
    
    >>> import pylab as pl
    >>> import PyFCell as PFC
    >>> import PyFCell.ips.analysis as ipsanalysis
    >>> import PyFCell.ips.inout as io
    >>> obj=io.reader(fname='../examples/IPS/fib.tif')
    >>> image=obj.getarray()
    >>> crunch = ipsanalysis.distanceSD(image,voxelsize=[5,5,5],material=0,loglevel=40)
    >>> crunch.calcPSD()
    >>> ax_cpsd=crunch.plot_distn(datatype='cpsd',label='fib')
    >>> ax_psd=crunch.plot_distn(datatype='psd',label='fib')
    >>> pl.show()
    >>> crunch.writeCSV()
    >>> crunch.write('result.vti', crunch.imPSD)
    
    
    .. plot::

        import pylab as pl
        import PyFCell as PFC
        import PyFCell.ips.analysis as ipsanalysis
        import PyFCell.ips.inout as io
        obj=io.reader(fname='../examples/IPS/fib.tif')
        image=obj.getarray()
        crunch = ipsanalysis.areaSD(image,voxelsize=[5,5,5],material=0,loglevel=40)
        crunch.calcPSD()
        ax_cpsd=crunch.plot_distn(datatype='cpsd',label='fib')
        ax_psd=crunch.plot_distn(datatype='psd',label='fib')
        pl.show()
        
    .. warning:: This documentation is not yet finished.

    .. todo:: Finish documentation
    
    """    
    def __init__(self, image, label='Pore', voxelsize=[1, 1, 1], voxelunit='nm',material=0,**kwargs):
            
        super(areaSD,self).__init__(**kwargs)
        indent='\t'        
        print indent, "="*50
        print indent, "Calculatiing Area Based Size Distribution " 
        print indent, "*"*50        
        print indent, "= Processing dataset " 
        print indent, "-"*50
        self._label=label
        
        self._spacing=voxelsize
        
        self._imSolidPore = 0
        self.imDistanceTransform = 0
        self.imPSD = 0
        self._indent=indent
        im = np.asarray(image)
        self._voxelsize = voxelsize
        self._format_color = cycle(['b','g','m','r','y','o'])
        self.scale=voxelsize[0]*voxelsize[1]
        
        self._voxelunit=voxelunit
        self.image=im
        t=self.image.shape
        if np.size(t)==2:
            self.image=self.image.reshape(t[0],t[1],1)
        self._imSolidPore=(self.image==material).copy()
        self._temp=[]
    
    def PoreCalc(self,im):

        T=mahotas.thresholding.otsu(im)
        structure=np.array([[1,1,1],[1,1,1],[1,1,1]])
        labeled,n_objects=ndimage.label(im>T,structure)

#        pl.figure()
#        pl.imshow(labeled)
#        pl.show()
        num=[]
        tmp=labeled.copy()
        
        for i in range(n_objects):
            #print 'Pore ',i
            cnt=labeled==i+1
            num.append(np.sum(cnt))
            tmp[labeled==i+1]=np.sum(cnt)*self.scale
        num=[self.scale*i for i in num]
        self._temp.append(tmp)
        return num
        
        
    def psd_func(self,n=603,por=0.345,no=20):
        

        col2=np.array(self.Psize)
        freq=[];val=[];arr={};cpore=[];cpore.append(por) #area porosity calculated based on test image
        
        for var1 in col2:
            a=0
            
            if (var1 in val):
                continue
            else:
                posn=np.where(col2==var1)
                count=np.size(posn[0])
                a+=count
                freq.append(a)
                val.append(var1)
        for i in range(len(val)):
            arr[val[i]]=freq[i]
        #sorting the values for graph
        
        val=sorted(arr)
        for i in range(len(val)):
            freq[i]=arr[val[i]]
            freq[i]=1.*freq[i]/n*por #dividing by no. of particles*area porosity
            cpore.append(cpore[i]-freq[i])
        cpore.pop()
        
        freqn=[];valn=[];cporen=[];cporen.append(por)
        num=1.*len(val)/no
        if int(num)==num:
            for i in range(int(num)):
                a1=no*i;a2=no*(i+1)-1
                freqn.append(sum(freq[a1:a2]))
                valn.append(sum(val[a1:a2])/no)
                cporen.append(cporen[i]-freqn[i])
            cporen.pop()
        else:
            for i in range(int(num)):
                a1=no*i;a2=no*(i+1)-1
                freqn.append(sum(freq[a1:a2]))
                valn.append(sum(val[a1:a2])/no)
                cporen.append(cporen[i]-freqn[i])
            freqn.append(sum(freq[no*(i+1):]))
            valn.append(sum(val[no*(i+1):])/len(val[no*(i+1):]))
            
        span=[item/(1E3) for item in valn]
        psd=[item*span[i] for i,item in enumerate(freqn)]
        total=sum(psd)
        psd=[item/total for item in psd]
        self.freqn=freqn
        self.freqn=[np.round(item*n/por) for item in freqn]
        self._imAreaSD=np.dstack(self._temp)
        return [span,cporen,psd]           
 #       return [span,cporen,freqn]    #cporen=cpsd,freqn=psd
        
    def calcPSD(self):
        
        Psize=[]
        for i in range(np.shape(self.image)[2]):
            img=self._imSolidPore[:,:,i]*255
            img=np.asarray(img,dtype=np.uint32)
            var=self.PoreCalc(img)
            Psize.extend(var)
        self.Psize=Psize
        [self.span,self.cpsd,self.psd]=self.psd_func(n=len(Psize),por=1.*sum(Psize)/(np.size(self.image)*self.scale),no=20)
        
    def plot_distn(self, label='Sample1',ax=None,datatype='cpsd', scale='default'):
        self._basename=label
        colors = self._format_color
        if ax==None:
            figure = pl.figure()
            ax = figure.add_subplot(111)
        color=next(colors)
        if datatype == 'cpsd' :
            p1, =ax.plot(self.span, self.cpsd,
                        color+'o--',
                        linestyle='--',
                        label=label + ' CPSD'
                        )
            pl.xlabel(self._label +' Area x $10^3 \ [' + self._voxelunit +'^2$]')
            pl.ylabel(self._label + ' Volume Fraction')
            pl.title('Area based Size Distribution')
            lines=[p1]
        
        elif datatype =='psd':
            
            p1, =ax.plot(self.span, self.psd,
                        color+'o--',
                        linestyle='--',
                        label=label + ' PSD'
                        )
            color=next(colors)
            ax2=ax.twinx()
            p2, =ax2.plot(self.span, self.freqn,color+'o--', linestyle='--',label=label + ' freq')
            ax.set_xlabel(self._label +' Area x $10^3 \ [' + self._voxelunit +'^2]$')
            ax.set_ylabel(' Normalized Area Fraction')
            ax2.set_ylabel(' Frequency')
            pl.title('Area based Size Distribution')
            lines=[p1,p2]
            ax.yaxis.label.set_color(p1.get_color())
            ax2.yaxis.label.set_color(p2.get_color())
            tkw = dict(size=4, width=1.5)
            ax.tick_params(axis='y', colors=p1.get_color(), **tkw)
            ax2.tick_params(axis='y', colors=p2.get_color(), **tkw)

    #        pl.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        
        ax.legend(lines, [l.get_label() for l in lines])

        return ax
    
    def writeCSV(self, filename):
        
        if filename == None:
            scipy.savetxt('Data_'+self._basename+'.csv',(self.span*self._rangeX[1], self.cpsd),delimiter=',')
        else: 
            scipy.savetxt(filename,(self.span, self.cpsd),delimiter=',')
 
    
    def write(self, data, filename = None):
        
        PyFCell.ips.inout.output.writer(filename, data, self._voxelsize)
        
        
if __name__    =="__main__":    
    import PyFCell.ips.inout as io
    obj=io.reader(fname='../../../data/FIBSEM/Segmented/Segmented-DH-16.tif')
    image=obj.getarray()
    crunch = areaSD(image,voxelsize=[5,5,10],material=255,loglevel=40)
    crunch.calcPSD()
    ax_cpsd=crunch.plot_distn(datatype='cpsd',label='fib')
    ax_psd=crunch.plot_distn(datatype='psd',label='fib')
    pl.show()
    res=np.dstack(crunch._temp)
    print res.shape
    data={}
    data["areaSD"]=res.astype('int16')
    from evtk.hl import imageToVTK
    imageToVTK('areaSD',cellData=data,spacing=[5,5,10])

    