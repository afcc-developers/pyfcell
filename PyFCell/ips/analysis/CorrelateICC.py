# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 15:32:24 2014

@author: Mayank Sabharwal
"""

import PyFCell.ips.inout as io
import PyFCell as pfc
import numpy as np
import scipy as sp
import pylab as pl

class CorrelateICC(pfc.util.baseobject):
    
    def __init__(self,names=[],icc=[],voxelsize=[5,5,5],step=10):
        
        self.names=names
        self.crunch={}
        for ii,name in enumerate(names):
            o=io.reader(name)
            im=o.getarray()
            self.crunch[name]=pfc.ips.analysis.distanceSD(image=im,voxelsize=voxelsize,material=0)
            self.crunch[name+"icc"]=icc[ii]
            self.crunch[name].calcPSD(step)
        self.fillascend()
        self.filldescend()
        
        
    def fillascend(self):
        
 
        step=np.arange(15,150,15)
        for s in step:
            x=[]
            y=[]
            tag='> ' + str(s)
            for ii,name in enumerate(self.names):
                psd=self.crunch[name].psd
                span=self.crunch[name].span[0:-1]
                x.append(sp.sum(psd[span>s])/sp.sum(psd)*100)
                y.append(self.crunch[name+"icc"])
            self.__plotdata(x,y,tag)
    
    
    def filldescend(self):
        
        step=np.arange(180,15,-15)
        for s in step:
            x=[]
            y=[]
            tag='<' + str(s)
            for ii,name in enumerate(self.names):
                psd=self.crunch[name].psd
                span=self.crunch[name].span[0:-1]
                x.append(sp.sum(psd[span<s])/sp.sum(psd)*100)
                y.append(self.crunch[name+"icc"])
            self.__plotdata(x,y,tag)    


    def __plotdata(self,x,y,tag):
        
#       z,residuals, rank, singular_values, rcond = np.polyfit(x, y, 1,full=True)
#       print residuals, rank, singular_values, rcond
#       f = np.poly1d(z)
        slope, intercept, r_value, p_value, std_err = sp.stats.linregress(x,y)
        x_new = np.linspace(x[0], x[-1]+0.1*(x[-1]-x[0]), 50)
        y_new = slope*(x_new)+intercept
        pl.figure()
        pl.plot(x,y,'ob',x_new,y_new,'--')
        s='R**2'+str(r_value)
        pl.text(50,30,s)
        pl.xlabel('Percentage of Porosity')
        pl.ylabel('MAT ICC Value (C/cm2)')
        pl.savefig('Correlation with pores '+ tag +'.jpg')
        
        
    def __func(self,x,a,b):
        return a*x+b
    