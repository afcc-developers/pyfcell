#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Mayank Sabharwal
# License: TBD
# Copyright (c) 2012, TBD

r"""
*************************************************************************
:mod:`PyFCell.ips.analysis` -- Postprocessing and analysis of CL data
*************************************************************************

.. module:: PyFCell.ips.analysis

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""

import numpy as np
import pylab as pl
import scipy 
import PyFCell as pfc
import PyFCell
from itertools import cycle

class basicStats(PyFCell.util.baseobject):
    
        r"""
        Calculates basic volume based statistics and local variations in the volume fractions.
        image, label='Pore', voxelsize=[1, 1, 1], voxelunit='nm',material=0
        
        Parameters
        ----------
        image : ndarray
            Input array for the 3D catalyst structure
        label : string
            Specify the material or type of distribution
            Eg: For Pore Size Distribution label = 'Pore'
        voxelsize : tuple
            Pixel size in X,Y,Z direction
            To be specified as [2,2,2] for a pixel size of 2 in each direction
        voxelunit : string
            Unit of length for the voxelsize
        material : int
            Material ID (in the input file) of the component for which the distribution is required
                
        
        
        Examples
        --------
        
        >>> import pylab as pl
        >>> import PyFCell as PFC
        >>> import PyFCell.ips.analysis as ipsanalysis
        >>> import PyFCell.ips.inout as io
        >>> obj=io.reader(fname='../examples/IPS/fib.tif')
        >>> image=obj.getarray()
        >>> crunch = ipsanalysis.basicStats(image,voxelsize=[5,5,5],material=0,loglevel=40)
        >>> crunch.makeStats()
        >>> ax_bar=crunch.plot_bar(label='fib')
        >>> ax_stat=crunch.plot_stat(label='fib')
        >>> pl.show()
        >>> crunch.write('result.vti', crunch._PorosityX)
        
        
        .. plot::
    
            import pylab as pl
            import PyFCell as PFC
            import PyFCell.ips.analysis as ipsanalysis
            import PyFCell.ips.inout as io
            obj=io.reader(fname='../examples/IPS/fib.tif')
            image=obj.getarray()
            crunch = ipsanalysis.basicStats(image,voxelsize=[5,5,5],material=0,loglevel=40)
            crunch.makeStats()
            ax_bar=crunch.plot_bar(label='fib')
            ax_stat=crunch.plot_stat(label='fib')
            pl.show()
            
        .. warning:: This documentation is not yet finished.
        
    
        .. todo:: Finish documentation
            
        """
        def __init__(self, image, label='Pore', voxelsize=[1, 1, 1], voxelunit='nm',material=0,**kwargs):
            
            super(basicStats,self).__init__(**kwargs)
            indent='\t'        
            print indent, "="*50
            if material ==  255:
                print indent," White : Pore"
                print indent," Black : Solid"
            elif material == 0:
                print indent," Black : Pore"
                print indent," White : Solid"
            print indent, "-"*50
            self._label=label
            print indent, "Image Dimensions (pixels):", np.shape(image)
            print indent, "Voxel Size (nm/pixel):", voxelsize
            print indent, "Image Dimensions (nm):", np.multiply(np.shape(image),voxelsize)
                        
            self._spacing=voxelsize
            self._imSolidPore = 0
            self.imDistanceTransform = 0
            self.imPSD = 0
            self._indent=indent
            im = np.asarray(image)
            self._voxelsize = voxelsize
            self._format_color = cycle(['b','g','m','r','y','o'])
            self.scale=np.shape(im)[0]*np.shape(im)[1]
            self._rangeX=scipy.linspace(0,im.shape[0]*voxelsize[0],im.shape[0],endpoint=False)
            self._rangeY=scipy.linspace(0,im.shape[1]*voxelsize[1],im.shape[1],endpoint=False)
            self._rangeZ=scipy.linspace(0,im.shape[2]*voxelsize[2],im.shape[2],endpoint=False)
            self._voxelunit=voxelunit
            self.image=im
            self._imSolidPore=(self.image==material).copy()
            print self._indent, "="*50
            print self._indent, "=\t  - Total Porosity =   ", scipy.mean(self._imSolidPore)
            
            
        def makeStats(self):
            
            print self._indent, "="*50
            print self._indent, "=\t  - X slice statistics"
            PorosityX=[]
            for ii in range(self._imSolidPore.shape[0]):
                PorosityX.append(scipy.mean(self._imSolidPore[ii,:,:]))
            print self._indent, "=\t    Mean = ", scipy.mean(PorosityX),", Variance = ", scipy.var(PorosityX)**0.5
            print self._indent, "-"*50
            
            print self._indent, "=\t  - Y slice statistics"
            PorosityY=[]
            for ii in range(self._imSolidPore.shape[1]):
                PorosityY.append(scipy.mean(self._imSolidPore[:,ii,:]))
            print self._indent, "=\t    Mean = ", scipy.mean(PorosityY),", Variance = ", scipy.var(PorosityY)**0.5
         
            print self._indent, "=\t  - Z slice statistics"
            PorosityZ=[]
            for ii in range(self._imSolidPore.shape[2]):
                PorosityZ.append(scipy.mean(self._imSolidPore[:,:,ii]))
            print self._indent, "=\t    Mean = ", scipy.mean(PorosityZ),", Variance = ", scipy.var(PorosityZ)**0.5
            
            print self._indent, "-"*50
            
            self.PorosityX = PorosityX
            self.PorosityY = PorosityY
            self.PorosityZ = PorosityZ


        def plot_bar(self,label='Sample1',ax=None, scale='default'):
            
            self._basename=label
            if ax==None:
                figure = pl.figure()
                ax = figure.add_subplot(111)
            N = 3
            Means = (scipy.mean(self.PorosityX),scipy.mean(self.PorosityY),scipy.mean(self.PorosityZ))
            Vars  = (scipy.std(self.PorosityX),scipy.std(self.PorosityY),scipy.std(self.PorosityZ))
            
            
            ind = np.arange(N)  
            width = 0.35       
            pl.bar(ind, Means, width, color=['b','g','m'], yerr=Vars)        
            pl.xticks(ind+width/2,['X','Y','Z'])
            pl.ylabel(self._label + ' Volume')
            return ax
            
            

        def plot_stat(self, label='Sample1',ax=None, scale='default'):
        
            self._basename='label'
            colors = self._format_color
            if ax==None:
                figure = pl.figure()
                ax = figure.add_subplot(111)
            color=next(colors)
              
            pl.subplot(1,3,1)
            
            pl.plot(self._rangeX,self.PorosityX, 'o' + color)
            pl.axhline(y=scipy.mean(self.PorosityX),color=color ,linewidth=2)
            pl.axhline(y=scipy.mean(self.PorosityX)+(scipy.var(self.PorosityX))**0.5,color=color )
            pl.axhline(y=scipy.mean(self.PorosityX)-(scipy.var(self.PorosityX))**0.5,color=color )
            pl.fill_between(self._rangeX,self._rangeX*0+scipy.mean(self.PorosityX)+(scipy.var(self.PorosityX))**0.5,self._rangeX*0+scipy.mean(self.PorosityX)-(scipy.var(self.PorosityX))**0.5,facecolor=color,alpha=0.2)
    
            pl.title(self._label + 'Volume (X slices)')
            pl.xlabel('X ['+self._voxelunit+']')
            pl.ylabel(self._label + ' Volume')
            pl.xlim((self._rangeX[0],self._rangeX[-1]))
            pl.ylim((0,1))
            color=next(colors)

            pl.subplot(1,3,2)
            
            pl.plot(self._rangeY,self.PorosityY,'o' + color )
            pl.axhline(y=scipy.mean(self.PorosityY),color=color ,linewidth=2)
            pl.axhline(y=scipy.mean(self.PorosityY)+(scipy.var(self.PorosityY))**0.5,color=color )
            pl.axhline(y=scipy.mean(self.PorosityY)-(scipy.var(self.PorosityY))**0.5,color=color )
            pl.fill_between(self._rangeY,self._rangeY*0+scipy.mean(self.PorosityY)+(scipy.var(self.PorosityY))**0.5,self._rangeY*0+scipy.mean(self.PorosityY)-(scipy.var(self.PorosityY))**0.5,facecolor=color,alpha=0.2)
    
            pl.title(self._label + 'Volume (Y slices)')
            pl.xlabel('Y ['+self._voxelunit+']')
            pl.ylabel(self._label + ' Volume')
            pl.xlim((self._rangeY[0],self._rangeY[-1]))
            pl.ylim((0,1))
            
            color=next(colors)

            pl.subplot(1,3,3)
            
            pl.plot(self._rangeZ,self.PorosityZ,'o' + color)
            pl.axhline(y=scipy.mean(self.PorosityZ),color=color ,linewidth=2)
            pl.axhline(y=scipy.mean(self.PorosityZ)+(scipy.var(self.PorosityZ))**0.5,color=color )
            pl.axhline(y=scipy.mean(self.PorosityZ)-(scipy.var(self.PorosityZ))**0.5,color=color )
            pl.fill_between(self._rangeZ,self._rangeZ*0+scipy.mean(self.PorosityZ)+(scipy.var(self.PorosityZ))**0.5,self._rangeZ*0+scipy.mean(self.PorosityZ)-(scipy.var(self.PorosityZ))**0.5,facecolor=color,alpha=0.2)
    
            pl.title(self._label + 'Volume (Z slices)')
            pl.xlabel('Z ['+self._voxelunit+']')
            pl.ylabel(self._label + ' Volume')
            pl.xlim((self._rangeZ[0],self._rangeZ[-1]))
            pl.ylim((0,1))

            return ax       
            
            
        def writeCSV(self, filename=None):
            if filename == None:
                scipy.savetxt('Data_'+self._basename+'.csv',(self.PorosityX),delimiter=',')
                scipy.savetxt('Data_'+self._basename+'.csv',(self.PorosityY),delimiter=',')
                scipy.savetxt('Data_'+self._basename+'.csv',(self.PorosityZ),delimiter=',')
            else: 
                scipy.savetxt(filename,(self.PorosityX),delimiter=',')
                scipy.savetxt(filename,(self.PorosityY),delimiter=',')
                scipy.savetxt(filename,(self.PorosityZ),delimiter=',')
         
         
        def write(self, data, filename):

            pfc.ips.inout.output.VoxelWriter(filename, data, self._voxelsize)
            
if __name__    =="__main__":    
    import PyFCell.ips.inout as io
    obj=io.reader(fname='../../../examples/IPS/fib.tif')
    image=obj.getarray()
    crunch = basicStats(image,voxelsize=[5,5,5],material=0,loglevel=40)
    crunch.makeStats()
    ax_bar=crunch.plot_bar(label='fib')
    ax_stat=crunch.plot_stat(label='fib')
    pl.show()