# -*- coding: utf-8 -*-
"""
Created on Mon Apr 28 12:53:03 2014

@author: Amit Bhatkal
"""




import SimpleITK as sitk

import numpy as np
from mahotas.thresholding import otsu
import time
import PyFCell

class regiongrow(PyFCell.util.baseobject):
    r"""
    Segmentation of FIBSEM samples using region growing method.
    
    Parameters
    ----------
    inputimage : string
        Filename of the input image
    seeds_num : int
        Number of seeds . Default is 100
    seed_slices : int
        The number of slices of the input image to be considered for generating seeds. Default is 1  
    
    Examples
    --------
    
    >>> import regiongrow as seg
    >>> obj=seg.segment3D('inputimage',seeds_num=100,seed_slices=1)
    >>> segmentedimage=obj.segmented_image       
        
    """
    
    
    def __init__(self,inputimage,seeds_num=100,seed_slices=1,**kwargs):
        io=PyFCell.ips.inout
        super(regiongrow,self).__init__(**kwargs)
        image=io.reader(inputimage).getarray()
        self.imsize=image.shape
        self.seed_slices=seed_slices # Number of slices from the image stack to be considered for extracting the seeds
        self.seeds_num=seeds_num # Prefereed number of seeds
        self.segmented_image=self.run_segmentation(image)
        io.writer(np.array(self.segmented_image,dtype='uint8'),"regionsegmented_"+inputimage+".tif")
    
    def run_segmentation(self,image):
        segmented_image=self.__grow(image)
        return segmented_image
    
    def __grow(self,orig_img):
        segment = PyFCell.ips.seg.threshold
        clahe = PyFCell.ips.util.clahe
        imtool = PyFCell.ips.util.ImageTools
        t0=time.time()
        seedimg=np.zeros((self.imsize[0],self.imsize[1],self.seed_slices)) # Initialise an image to represent the seedpoints
        print "seedimg",np.shape(seedimg)
        
        j=0
        for k in range(0,self.imsize[2]):
            # Image 
            orig_img[:,:,k]=(np.asarray(clahe.equalize_adapthist(orig_img[:,:,k],6,6,clip_limit = 0.05), dtype=np.uint8)) # 1
            orig_img[:,:,k]=(np.asarray(imtool.gaussian_blur(orig_img[:,:,k],theta=1),dtype=np.uint8)) # 2
            
            if k in range(self.seed_slices):
                print "j",j
                seedimg[:,:,j]=orig_img[:,:,k]
                threshimg=segment.sauvola(seedimg[:,:,j]) # Use of Sauvola 2D method to threshold the image slice and create a seed image.
                seedimg[:,:,j]=np.array(threshimg,dtype='uint8')
                j=j+1
        io.writer(seedimg,fname='seedimage.tif')
        
        temp=np.transpose(orig_img,axes=[2,0,1]) # Rearranging the array in to [slicenum,x,y] format in order the suit the requirements of sitk array.
        temp=sitk.GetImageFromArray(temp)
        img_T1=temp
        
        temp=None
        
        # A check to find the threshold even though seed image is a binary image obtained by sauvola segmentation.
        T=otsu(np.array(seedimg, dtype='uint8'))


        #Preparing the list of seed points in the image ie., the points with values > sauvola threshold.
        pt_list=np.where(seedimg>T)
        pt_list=np.array(pt_list)
        
        # If the number of point specified by the user is  greater than the number of seed points detected , limit the seeds_num        
        if np.shape(pt_list)[1]<= self.seeds_num :
            seeds_num=np.shape(pt_list)[1]
        else :
            seeds_num=self.seeds_num
        

        #Confidence connected  image filter is used to grow the region . Please refer http://www.itk.org/ItkSoftwareGuide page no.472-473 for more details
        #The criterion used by the ConfidenceConnectedImageFilter is based on simple statistics of the current region. 
        #The algorithm computes the mean and standard deviation of intensity values for all the pixels currently included in the region. 
        #A user-provided factor is used to multiply the standard deviation and define a range around the mean. 
        #Neighbor pixels whose intensity values fall inside the range are accepted and included in the region. 

        seg_filt=sitk.ConfidenceConnectedImageFilter()

    
        for i in range((seeds_num)): # Adding the seed to the seed list of the filter
            idx=(np.int(pt_list[1][i]),np.int(pt_list[0][i]),np.int(pt_list[2][i]))
            i=seg_filt.AddSeed(idx)

        seg_filt.SetNumberOfIterations(1)
        seg_filt.SetMultiplier(1)
        seg_image=seg_filt.Execute(img_T1)

        temp=sitk.GetArrayFromImage(seg_image) # Format of the returned array is => no. of slices x transpose(shape of original array)    

        temp=np.transpose(temp)
        temp=np.rot90(temp,k=1)
        temp=np.flipud(temp)

        seg_image=np.array(temp, dtype='uint8')
        
        
        seg_image[seg_image>0]=255
        seg_image[seg_image<=0]=0


        print seg_image.shape
    
        t1=time.time()
        print "Time taken by 3D region growth method" , (t1-t0)/60.0 , "mins"
    
        return np.array(seg_image,dtype='uint8')        

    



def main():
       
    obj=regiongrow('Segmented_Cropped_MB071913-3-4-R - 04.tif',100,1)
    print np.shape(obj.segmented_image)

   
    
if __name__    =="__main__":   
    main()