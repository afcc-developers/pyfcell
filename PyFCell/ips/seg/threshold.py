#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2012, TBD

r"""

*************************************************************************
:mod:`PyFCell.ips.seg.threshold` -- Segmentation module for Image Processing Suite
*************************************************************************

.. module:: PyFCell.ips.seg.threshold

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========
Classes
=======

"""

import numpy as np
import scipy
import scipy.io
import scipy.interpolate
import mahotas
from PIL import Image
import pylab as plt
        

def adaptive(image, block_size =20, method = 'gaussian', offset=0,mode='reflect',param=None,**kwargs):
    r""" 
    Computes the segmented image based on built-in scipy library using Adaptive Thresholding
    
    http://scipy-lectures.github.io/advanced/image_processing/
    
    image, block_size, method = 'gaussian', offset=0,mode='reflect',param=None, **kwargs
    
    Parameters
    ----------
    image : ndarray 2D
        Original image, NxM array
    
    block_size : int
        Size of neighbourhood in pixels which is used to calculate threshold, int
            
    method : string
        Adaptive thresholding method {'gaussian', 'mean', 'generic', 'median'}. Default is 'gaussian'
        
    offset : int
        Optional, constant subtracted from weighted mean, default = 0
            
    mode : string
        Optional, {'reflect', 'constant', 'nearest', 'mirror', 'wrap'},
        Determines how the array borders are handled, Default is reflect 
            
    param : float
        specifies sigma for gaussian method. Takes flat array of local
        neighbourhood as a single argument and returns the calculated 
        threshold for center pixel
        
    Examples
    --------
    >>> import PyFCell.ips.seg.threshold as threshold
    >>> import PyFCell.ips.inout as io
    >>> image=io.reader("../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
    >>> tmp=[]
    >>> tmp.append(image[:,:,0])
    >>> tmp.append(np.asarray(threshold.adaptive(image[:,:,0], 30, method = 'gaussian')))
    >>> res=np.asarray(np.dstack(tmp),dtype=np.int8)
    >>> io.writer(res,'Segmented.tif')
    
    .. plot::
            
        import pylab as pl
        import PyFCell.ips.seg.threshold as threshold
        import PyFCell.ips.inout as io
        import numpy as np
        image=io.reader("../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
        tmp=[]
        tmp.append(np.asarray(image[:,:,0],dtype=np.uint8))
        tmp.append(np.asarray(threshold.adaptive(image[:,:,0], 30, method = 'gaussian'),dtype=np.uint8))
        pl.subplot(1,2,1)       
        pl.imshow(tmp[0])
        pl.title("Original Image")
        pl.gray()
        pl.subplot(1,2,2)
        pl.imshow(tmp[1])
        pl.title("Segmented Image")
        pl.gray()
        pl.show()
            
    """
    thresh_image = np.zeros(image.shape, 'double')
    if method == 'gaussian':
        if param is None:
            #Automatically determine sigma which covers > 99% of distribution
            sigma = (block_size - 1) / 6.0
        else:
            sigma = param
        scipy.ndimage.gaussian_filter(image, sigma, output = thresh_image, mode=mode)
            
    elif method == 'mean':
        mask = 1. / block_size * np.ones((block_size,))
        # Separation of filters to speedup convolution
        scipy.ndimage.convolve1d(image, mask, axis=0, output = thresh_image, mode=mode)
        scipy.ndimage.convolve1d(thresh_image, mask, axis=1, output=thresh_image,mode=mode)
            
    elif method == 'median':
        scipy.ndimage.median_filter(image, block_size, output = thresh_image, mode=mode)
    
    return (image > (thresh_image - offset))*255

def __window(im,row,column,bs):
    
    """ creates a window around each pixel"""
    #  Inputs:
    #        im = Image with boarder reflection
    #        row = row corresponding to the pixel
    #        column = column corresponding to the pixel
    #        bs = block size
    row = row + bs
    column = column + bs  
        
    left = column - bs
    right = column + (bs + 1)
    up = row - bs
    down = row + (bs + 1)
    
    win = im[up:down,left:right]
    
    return np.std(win), np.mean(win)
    
def __reflect_frame(im, bs):
    
    """ Creates an artificial frame around the image"""
    #  Inputs:
    #        im = Original image
    #        bs = block size   
    
    n,m = im.shape
    s = (n + 2 * bs, m + 2 * bs)
    im2 = np.zeros(s)
    n2,m2 = im2.shape
    # copy image to the new frame
    im2[bs:n + bs,bs:m + bs] = im[:n,:m]
    
    # Creating reflection at the boundaries    
    for i in range(bs):
        im2[i,:]=im2[(2 * bs) - i, :]
        im2[:,i]=im2[:, (2 * bs) - i]
        im2[n2 - i - 1,:]=im2[n2 - (2 * bs) + i - 1, :]
        im2[:,m2 - i - 1]=im2[:, m2 - (2 * bs) + i - 1]        
        
    return im2
    
def sauvola(image, block_size = 20, k = 0.5, r = 128,**kwargs):
    r""" 
    Computes segmented image based on using Sauvola Thresholding algorithm
    
    http://fiji.sc/wiki/index.php/Auto_Local_Threshold
    
    image, block_size, k = 0.5, r = 128, **kwargs
    
    Parameters
    ----------
    image : ndarray 2D
        Original image in array format
    
    block_size : int
        size of the window surrounding each pixel, (Default = 30 pixels) 
            
    k, r : Sauvola constants, Default values are 0.5 and 128 respectively
    
    Examples
    --------
    >>> import PyFCell.ips.seg.threshold as threshold
    >>> import PyFCell.ips.inout as io
    >>> image=io.reader("../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
    >>> tmp=[]
    >>> tmp.append(image[:,:,0])
    >>> tmp.append(np.asarray(threshold.sauvola(image[:,:,0], 30, 0.5, 128)))
    >>> res=np.asarray(np.dstack(tmp),dtype=np.int8)
    >>> io.writer(res,'Segmented.tif')
    
    .. plot::
            
        import pylab as pl
        import PyFCell.ips.seg.threshold as threshold
        import PyFCell.ips.inout as io
        import numpy as np
        image=io.reader("../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
        tmp=[]
        tmp.append(np.asarray(image[:,:,0],dtype=np.uint8))
        tmp.append(np.asarray(threshold.sauvola(image[:,:,0], 30, 0.5, 128),dtype=np.uint8))
        pl.subplot(1,2,1)       
        pl.imshow(tmp[0])
        pl.title("Original Image")
        pl.gray()
        pl.subplot(1,2,2)
        pl.imshow(tmp[1])
        pl.title("Segmented Image")
        pl.gray()
        pl.show()
    
    """
    n, m  = image.shape
    thresh_image = np.zeros(image.shape, 'double')
    
    # creates a frame around image boundaries based on reflection method 
    # for boundary treatments
    framed_image = __reflect_frame(image, block_size)
    
    # iterates through each pixel and performs segmentation
    for i in range(n):
        for j in range(m): 
            pixel = [i,j]
                
            # returns the window surrounding the pixel based on block size
            sdev,mean_value = __window(framed_image,pixel[0],pixel[1], block_size)
            
            # Sauvola thresholding criterion
            if image[i,j] > mean_value * (1 + k * (sdev/r - 1)):
                thresh_image[i,j] = 255
                    
    return thresh_image

def otsu(im,**kwargs):
    r""" 
    Computes segmented image based on otsu algorithm
    
    http://en.wikipedia.org/wiki/Otsu's_method
        
    image
        
    Parameters
    ----------
    image : ndarray 2D
        Original image in array format
            
    Examples
    --------
    >>> import PyFCell.ips.seg.threshold as threshold
    >>> import PyFCell.ips.inout as io
    >>> image=io.reader("../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
    >>> tmp=[]
    >>> tmp.append(image[:,:,0])
    >>> tmp.append(np.asarray(threshold.otsu(image[:,:,0])))
    >>> res=np.asarray(np.dstack(tmp),dtype=np.int8)
    >>> io.writer(res,'Segmented.tif')
    
    .. plot::
            
        import pylab as pl
        import PyFCell.ips.seg.threshold as threshold
        import PyFCell.ips.inout as io
        import numpy as np
        image=io.reader("../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
        tmp=[]
        tmp.append(np.asarray(image[:,:,0],dtype=np.uint8))
        tmp.append(np.asarray(threshold.otsu(image[:,:,0]),dtype=np.uint8))
        pl.subplot(1,2,1)       
        pl.imshow(tmp[0])
        pl.title("Original Image")
        pl.gray()
        pl.subplot(1,2,2)
        pl.imshow(tmp[1])
        pl.title("Segmented Image")
        pl.gray()
        pl.show()
        
    """
    im_seg = np.zeros(im.shape)
    T_otsu = mahotas.otsu(im)
    im_seg[im>T_otsu]=255
    im_seg[im<T_otsu]=0
    return im_seg
        
def riddler_calvard(im,**kwargs):
    r""" 
    Computes segmented image based on Riddler Calvard
    
    http://ruraluniv.ac.in/papers%5C118.pdf
    
    image
    
    Parameters
    ----------
    image : ndarray 2D
        Original image in array format
        
    Examples
    --------
    >>> import PyFCell.ips.seg.threshold as threshold
    >>> import PyFCell.ips.inout as io
    >>> image=io.reader("../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
    >>> tmp=[]
    >>> tmp.append(image[:,:,0])
    >>> tmp.append(np.asarray(threshold.riddler_calvard(image[:,:,0])))
    >>> res=np.asarray(np.dstack(tmp),dtype=np.int8)
    >>> io.writer(res,'Segmented.tif')
    
    .. plot::
            
        import pylab as pl
        import PyFCell.ips.seg.threshold as threshold
        import PyFCell.ips.inout as io
        import numpy as np
        image=io.reader("../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
        tmp=[]
        tmp.append(np.asarray(image[:,:,0],dtype=np.uint8))
        tmp.append(np.asarray(threshold.riddler_calvard(image[:,:,0]),dtype=np.uint8))
        pl.subplot(1,2,1)       
        pl.imshow(tmp[0])
        pl.title("Original Image")
        pl.gray()
        pl.subplot(1,2,2)
        pl.imshow(tmp[1])
        pl.title("Segmented Image")
        pl.gray()
        pl.show()
            
    """
    im_seg = np.zeros(im.shape)    
    T_rc = mahotas.rc(im)
    im_seg[im>T_rc]=255
    im_seg[im<T_rc]=0
    return im_seg 

def rat(image, parts = 1,**kwargs):
    r""" 
    Computes segmented image based Robust Automatic Threshold algorithm
    
    http://fiji.sc/RATS:_Robust_Automatic_Threshold_Selection
    
    image, parts
    
    Parameters
    ----------
    image : ndarray 2D
        Original image in array format
            
    parts : 1 (Default)
    
    Examples
    --------
    >>> import PyFCell.ips.seg.threshold as threshold
    >>> import PyFCell.ips.inout as io
    >>> image=io.reader("../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
    >>> tmp=[]
    >>> tmp.append(image[:,:,0])
    >>> tmp.append(np.asarray(threshold.rat(image[:,:,0])))
    >>> res=np.asarray(np.dstack(tmp),dtype=np.int8)
    >>> io.writer(res,'Segmented.tif')
    
    .. plot::
            
        import pylab as pl
        import PyFCell.ips.seg.threshold as threshold
        import PyFCell.ips.inout as io
        import numpy as np
        image=io.reader("../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
        tmp=[]
        tmp.append(np.asarray(image[:,:,0],dtype=np.uint8))
        tmp.append(np.asarray(threshold.rat(image[:,:,0]),dtype=np.uint8))
        pl.subplot(1,2,1)       
        pl.imshow(tmp[0])
        pl.title("Original Image")
        pl.gray()
        pl.subplot(1,2,2)
        pl.imshow(tmp[1])
        pl.title("Segmented Image")
        pl.gray()
        pl.show()
        
    """
    # Creating the indices according to number of parts input by user
    size = image.shape
    x,y=size[0]/parts,size[1]/parts
    x1,y1= np.indices((parts,parts))
    x1=x1.flat
    y1=y1.flat
    ind=np.indices((x,y))
    thresh=x1                        # Initialise threshold array with size = square(number of parts) 
    image=Image.fromarray(image)
    image=np.array(image)
    grad = __calcgradient(image)

    for i1 in range(parts*parts):
        
        # Creating the range of indices in each part
        x2=np.array(x*x1[i1]+ind[0], dtype='uint16')                       
        y2=np.array(y*y1[i1]+ind[1], dtype='uint16')
        
        g=grad[x2,y2]                # Gradient at the coordinates [x2,y2]             
        img=image[x2,y2]             # Actual value at the coordinates [x2,y2]   
                  
        # Threshold calculation 
        num=np.sum((g**3)*img)
        den=np.sum((g**3))          
        thresh[i1]=num/den
               
        # Segementation implemented using the threshold
        img[np.where(img<thresh[i1])]=0
        img[np.where(img>=thresh[i1])]=255
        image[x2,y2]=img


    return image    

def __calcgradient(image) :
        
    r""" 
    Image gradient calculator
        
    """
        
    k1,k2=plt.gradient(image)
    grad=np.sqrt(np.square(k1)+np.square(k2))
    
    return grad
    
        
def hysteresis(image,**kwargs):
    r""" 
    Computes segmented image based on hysterisis algorithm
    
    http://imagejdocu.tudor.lu/doku.php?id=plugin:segmentation:hysteresis_thresholding:start
    
    image
    
    Parameters
    ----------
    image : ndarray 2D
        Original image in array format
        
    Examples
    --------
    >>> import PyFCell.ips.seg.threshold as threshold
    >>> import PyFCell.ips.inout as io
    >>> image=io.reader("../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
    >>> tmp=[]
    >>> tmp.append(image[:,:,0])
    >>> tmp.append(np.asarray(threshold.hysteresis(image[:,:,0])))
    >>> res=np.asarray(np.dstack(tmp),dtype=np.int8)
    >>> io.writer(res,'Segmented.tif')
    
    .. plot::
            
        import pylab as pl
        import PyFCell.ips.seg.threshold as threshold
        import PyFCell.ips.inout as io
        import numpy as np
        image=io.reader("../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
        tmp=[]
        tmp.append(np.asarray(image[:,:,0],dtype=np.uint8))
        tmp.append(np.asarray(threshold.hysteresis(image[:,:,0]),dtype=np.uint8))
        pl.subplot(1,2,1)       
        pl.imshow(tmp[0])
        pl.title("Original Image")
        pl.gray()
        pl.subplot(1,2,2)
        pl.imshow(tmp[1])
        pl.title("Segmented Image")
        pl.gray()
        pl.show()
        
            
    """
    im_seg = np.zeros(image.shape)
    
    size = image.shape
    histgrm,pixels=np.histogram(image, range(0,255))
    thresh1=np.where(histgrm==np.max(histgrm))[0]                                    # Obtaining the first highest threshold
    thresh2=np.where(histgrm==np.max(histgrm[np.where(histgrm<np.max(histgrm))]))[0] # Obtaining the second highest threshold 
    
    if thresh1>thresh2 :
        thresh1,thresh2=thresh2,thresh1
        
    im_seg[np.where(image<thresh1)]=0                                      # Background
    im_seg[np.where(image>thresh2)]=255                                    # Foreground    
    
    i,j=np.where((image>thresh1) & (image<thresh2))
    
    for x,y in zip(i,j) :
        if ((x<size[0]-1) and (y<size[1]-1)):
            if np.all(np.array([image[x-1,y-1],image[x-1,y],image[x-1,y+1],image[x,y-1],image[x,y+1],image[x+1,y-1],image[x+1,y],image[x+1,y+1]])==255) :
                im_seg[x,y]=255
            else :
                im_seg[x,y]=0
        
    return im_seg

def fastsauvola(image, parts = 1,  R = 128, k = 0.6,**kwargs):
    r""" 
    Computes segmented image based on sauvola algorithm
    
    http://fiji.sc/wiki/index.php/Auto_Local_Threshold
        
    image, parts = 1,  R = 128, k = 0.6
        
    Parameters
    ----------
    image : ndarray 2D
        Original image in array format
            
    parts : 
            
    R : Range of Std. Deviation which is usually chosen as 128 for 8 bit images
        
    k :  Adjustable scaling constant (usually 0.4 - 0.6)
    
    Examples
    --------
    >>> import PyFCell.ips.seg.threshold as threshold
    >>> import PyFCell.ips.inout as io
    >>> image=io.reader("../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
    >>> tmp=[]
    >>> tmp.append(image[:,:,0])
    >>> tmp.append(np.asarray(threshold.fastsauvola(image[:,:,0], 1, 128, 0.6)))
    >>> res=np.asarray(np.dstack(tmp),dtype=np.int8)
    >>> io.writer(res,'Segmented.tif')
    
    .. plot::
            
        import pylab as pl
        import PyFCell.ips.seg.threshold as threshold
        import PyFCell.ips.inout as io
        import numpy as np
        image=io.reader("../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
        tmp=[]
        tmp.append(np.asarray(image[:,:,0],dtype=np.uint8))
        tmp.append(np.asarray(threshold.fastsauvola(image[:,:,0], 1, 128, 0.6),dtype=np.uint8))
        pl.subplot(1,2,1)       
        pl.imshow(tmp[0])
        pl.title("Original Image")
        pl.gray()
        pl.subplot(1,2,2)
        pl.imshow(tmp[1])
        pl.title("Segmented Image")
        pl.gray()
        pl.show()
            
    """
    size = image.shape
    x,y=size[0]/parts,size[1]/parts
    x1,y1= np.indices((parts,parts))
    x1=x1.flat
    y1=y1.flat
    ind=np.indices((x,y))
    thresh=x1
    image=Image.fromarray(image)
    image=np.array(image)

    for i1 in range(parts**2):
            
        x2=np.array(x*x1[i1]+ind[0], dtype='uint16')
        y2=np.array(y*y1[i1]+ind[1], dtype='uint16')
        img=image[x2,y2]
        m=np.mean(img)                                                    # mean of the pixels under consideration
        s=np.std(img)                                                     # Std. Deviation of the pixels under consideration
        thresh[i1]=m*(1+k*((s/R)-1))                                      # Threshold calculation

            
        # Segementation implemented using the threshold
        img[np.where(img<thresh[i1])]=0
        img[np.where(img>=thresh[i1])]=255
        image[x2,y2]=img

    return image


        
#if __name__ == '__main__':
#    import PyFCell.ips.inout as io
#    image=io.reader("../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
#    crunch=threshold()
#    tmp=[]
#    tmp.append(np.asarray(image[:,:,0]))
#    tmp.append(np.asarray(crunch.adaptive(image[:,:,0],20)))
#    #tmp.append(np.asarray(crunch.sauvola(image[:,:,0],20))*255)
#    res=np.asarray(np.dstack(tmp),dtype=np.int16)
#    io.writer(res,'Segment_Test.tif')
