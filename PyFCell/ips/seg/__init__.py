# Author: Mayank Sabharwal
# License: TBD
# Copyright (c) 2013, AFCC
"""
**************************************************************************
:mod:`PyFCell.ips.seg` -- Segmentation module for Image Processing Suite
**************************************************************************

This module provides segmentation algorithms for various types of segmentation.
Currently Threshold based algorithms available.

"""

import threshold
from regiongrow import regiongrow


#.. autoclass:: threshold
#   :members:
#   :undoc-members:
#   :show-inheritance: