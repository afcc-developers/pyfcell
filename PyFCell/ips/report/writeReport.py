# -*- coding: utf-8 -*-
"""
Created on Tue Apr 22 11:46:31 2014

@author: Mayank Sabharwal
"""
from pptx import Presentation
from pptx.util import Pt,Inches,Px
import sys,os
import scipy as sp
import numpy as np
import pylab as pl
sys.path.append("../")
import PyFCell

class writeReport(PyFCell.util.baseobject):
    r"""
    Class for writing the report of sample analysis for the datasets 
    This class consists of functions which calculate the various properties in the analysis module and store the comparisons in the folder 'fig' created in the active working directory.
    
    samplename,paths,material,voxelsize,areastep=25,spherestep=15,interval=10, **kwargs
    
    Parameters
    ----------
    samplename : list
        Input array for the samplenames or tag names
    paths : list
        File locations of the samplenames corresponding to the previous list
    material : int
        Material ID (in the input file) of the component for which the distribution is required
    voxelsize : list of tuples
        Voxelsize for the different samples
        Pixel size in X,Y,Z direction
        To be specified as [2,2,2] for a pixel size of 2 in each direction
    areastep : int
        Area based histogram bin size
    spherestep : int
        Sphere based histogram bin size
    interval : int
        Number of intervals in the histogram
    filename : string
        Output report name. Default is "Report1.ppt"
        
    .. warning:: This documentation is not yet finished.
    

    .. todo:: 1. Finish documentation
              2. Add rst writer
        
    """
    def __init__(self,samplename,paths,material,voxelsize,areastep=25,spherestep=15,interval=10,**kwargs):
        
        super(writeReport,self).__init__(**kwargs)
        self.sample={}        
        if 'filename' in kwargs.keys():
            (path,filename) = os.path.split(kwargs['filename'])
            basename,extension = os.path.splitext(filename)              
            self.filename = basename
        else:
            self.filename='Report1'
            extension = '.ppt'
        try:
            os.mkdir('fig')
        except:
            t=[]                
        for i,item in enumerate(samplename):
            self.sample[item]={}
            self.sample[item]["filepath"]=paths[i]
            self.sample[item]["voxelsize"]=voxelsize[i]
            self.material = material
        self.crunch = PyFCell.ips.report.sampleAnalysis(samplename,paths,material,voxelsize,areastep=25,spherestep=15,interval=10,filename = self.filename)
        self.crunch.run()
        if extension == '.ppt':
            self.writePPT()

        
    def writePPT(self):
        r"""
        Function for writing the report of sample analysis for the datasets in .ppt format.
        The format for the presentation is predefined.
    
        .. todo:: 1. Finish documentation
                  2. Add AFCC Template to slides (Under development)
           
        """      
        #Slide 1 Title
        count = np.size(self.crunch.sample.keys())
        prs = Presentation()
        slide = prs.slides.add_slide(prs.slide_layouts[6])
        shapes = slide.shapes
        left = Inches(-2.5)
        top = Inches(0.1)
        height = Inches(2)
        width = Inches(9)
        title = shapes.add_textbox(left,top,width,height)
        tf = title.textframe
        p = tf.add_paragraph()
        p.text = 'Sample Description'
        p.font.bold = True
        p.font.size = Pt(24)
        
        #Slide 1 Table
        rows = 1+count  
        cols = 5
        top = Inches(1.5)
        left = Inches(0.2)
        width = Inches(9)
        height = Inches(0.8)
        tb1 = shapes.add_table(rows,cols,left,top,width,height)
        tb1.columns[0].width = Inches(1.75)
        tb1.columns[1].width = Inches(1.75)
        tb1.columns[2].width = Inches(1.5)
        tb1.columns[3].width = Inches(3)
        tb1.columns[4].width = Inches(1.5)
        tb1.cell(0,0).text = 'SAMPLE ID'
        tb1.cell(0,1).text = 'CATALYST'
        tb1.cell(0,2).text = 'IONOMER'
        tb1.cell(0,3).text = 'DESCRIPTION'
        tb1.cell(0,4).text = 'RN #'
        for i,name in enumerate(self.crunch.sample.iterkeys()):
            tb1.cell(i+1,0).text = name
        
        #Slide 1 Images
        
        if count == 2:
            width = Px(250)
            height = Px(250)
            top = Inches(4)
            left = Inches(0.5)    
            
            figname = 'fig/'+self.filename+'_'+self.sample.keys()[0]+'_'+'Slice1.png'
            pic = shapes.add_picture(figname,left,top,width,height)
            tag = shapes.add_textbox(left+Inches(1),top-Inches(0.3),Inches(0.4),Inches(0.4))
            tag.text=self.sample.keys()[0]
            figname = 'fig/'+self.filename+'_'+self.sample.keys()[1]+'_'+'Slice1.png'
            pic = shapes.add_picture(figname,left+Inches(4),top,width,height)
            tag = shapes.add_textbox(left+Inches(5),top-Inches(0.3),Inches(0.4),Inches(0.4))
            tag.text=self.sample.keys()[1]
            
        else:
            width = Px(250)*3/count
            height = Px(250)*3/count
            top = Inches(4)
            left = Inches(0)
            for i,name in enumerate(self.crunch.sample.iterkeys()):
                figname = 'fig/'+self.filename+name+'Slice1.png'                  
                pic = shapes.add_picture(figname,left,top,width,height)
                left = left + Inches(3.2*3/count)
                tag = shapes.add_textbox(left-Inches(1.5),top-Inches(0.3),Inches(0.4),Inches(0.4))
                tag.text = name
        
        #Slide 2 Title
        slide = prs.slides.add_slide(prs.slide_layouts[6])
        shapes = slide.shapes
        left = Inches(-1.75)
        top = Inches(0.1)
        height = Inches(2)
        width = Inches(9)
        title = shapes.add_textbox(left,top,width,height)
        tf = title.textframe
        p = tf.add_paragraph()
        p.text = 'Comparison of Parameters'
        p.font.bold = True
        p.font.size = Pt(24)
        
        #Slide 2 Table
        rows = 1+count  
        cols = 2
        top = Inches(1.5)
        left = Inches(0.2)
        width = Inches(2)
        height = Inches(0.8)
        tb2 = shapes.add_table(rows,cols,left,top,width,height)
        tb2.columns[0].width = Inches(1.75)
        tb2.columns[1].width = Inches(1.75)
        
        tb2.cell(0,0).text = 'SAMPLE ID'
        tb2.cell(0,1).text = 'POROSITY'
        for ii,name in enumerate(self.crunch.sample.iterkeys()):
            tb2.cell(ii+1,0).text = name
            tb2.cell(ii+1,1).text = str(self.crunch.sample[name]["Porosity"])
        
        #Slide 2 comparison graphs
        width = Px(320)
        height = Px(200)
        top = Inches(1)
        left = Inches(4.5)   
        figname = 'fig/'+self.filename+'_EffTracer.png'
        pic2 = shapes.add_picture(figname,left,top,width,height)
        left =  Inches(4.5)
        top = Inches(4)
        width = Px(320)
        height = Px(200)
        figname = 'fig/'+self.filename+'_SphereHist.png'
        pic3 = shapes.add_picture(figname,left,top,width,height)
        prs.save(self.filename+'.pptx')