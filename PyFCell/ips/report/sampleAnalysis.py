# -*- coding: utf-8 -*-
"""
Created on Tue Apr 22 11:46:31 2014

@author: Mayank Sabharwal
"""

import sys,os
import scipy as sp
import numpy as np
import pylab as pl
import PyFCell.ips.inout as io
import PyFCell.ips.analysis as a
sys.path.append("../")
import PyFCell

class sampleAnalysis(PyFCell.util.baseobject):
    r"""
    Class for the analysis of Segmented 3D Datasets. 
    This class consists of functions which calculate the various properties in the analysis module and store the comparisons in the folder 'fig' created in the active working directory.
    
    samplename,paths,material,voxelsize,areastep=25,spherestep=15,interval=10
    
    Parameters
    ----------
    samplename : list
        Input array for the samplenames or tag names
    paths : list
        File locations of the samplenames corresponding to the previous list
    material : int
        Material ID (in the input file) of the component for which the distribution is required
    voxelsize : list of tuples
        Voxelsize for the different samples
        Pixel size in X,Y,Z direction
        To be specified as [2,2,2] for a pixel size of 2 in each direction
    areastep : int
        Area based histogram bin size
    spherestep : int
        Sphere based histogram bin size
    interval : int
        Number of intervals in the histogram
    
    .. warning:: This documentation is not yet finished.
    

    .. todo:: 1. Finish documentation
              2. Add parallel python to improve speed
        
    """
    def __init__(self,samplename,paths,material,voxelsize,areastep=25,spherestep=15,interval=10,**kwargs):
        
        super(sampleAnalysis,self).__init__(**kwargs)
        self.sample={}        
        if 'filename' in kwargs.keys():
            self.filename = kwargs['filename']
        else:
            self.filename='Report1'
        self.interval = interval
        self.areastep = areastep
        self.spherestep = spherestep
        for i,item in enumerate(samplename):
            self.sample[item]={}
            self.sample[item]["filepath"]=paths[i]
            self.sample[item]["voxelsize"]=voxelsize[i]
            self.material = material
          
    def __getimage(self):
        for name in self.sample.iterkeys():
            crunch = io.reader(self.sample[name]["filepath"])
            self.sample[name]['image'] = crunch.getarray()
            fig = pl.figure()
            pl.gray()
            pl.imshow(self.sample[name]["image"][:,:,0])
            figname = 'fig/'+self.filename+'_'+name+'_'+'Slice1.png'   
            pl.savefig(figname)
            
    def __getBasicStats(self):
        self.basic={}
        for name in self.sample.iterkeys():
            crunch = a.basicStats(self.sample[name]["image"],voxelsize=self.sample[name]["voxelsize"],material=self.material)
            self.sample[name]["Porosity"] = sp.mean(crunch._imSolidPore)
            self.basic[name] = crunch
    
    def __getTortuosity(self):
        self.tortX = {}
        self.tortY = {}
        self.tortZ = {}
        for name in self.sample.iterkeys():
            crunch=a.tortuosity(self.sample[name]["image"],tracernum=1000,voxelsize=self.sample[name]["voxelsize"],material=self.material,direction='X')
            crunch.calc()
            self.tortX[name]=crunch
            self.sample[name]["tortX"]=crunch.avgTortuosity
            self.sample[name]["EffTX"]=np.round((crunch.N_good_tracer*1.0/crunch.tracernum),decimals=2)*100
            crunch=a.tortuosity(self.sample[name]["image"],tracernum=1000,voxelsize=self.sample[name]["voxelsize"],material=self.material,direction='Y')
            crunch.calc()
            self.tortY[name]=crunch
            self.sample[name]["tortY"]=crunch.avgTortuosity
            self.sample[name]["EffTY"]=np.round((crunch.N_good_tracer*1.0/crunch.tracernum),decimals=2)*100
            crunch=a.tortuosity(self.sample[name]["image"],tracernum=1000,voxelsize=self.sample[name]["voxelsize"],material=self.material,direction='Z')
            crunch.calc()
            self.tortZ[name]=crunch
            self.sample[name]["tortZ"]=crunch.avgTortuosity
            self.sample[name]["EffTZ"]=np.round((crunch.N_good_tracer*1.0/crunch.tracernum),decimals=2)*100
    
    def __getAreaSD(self):
        self.area={}
        for name in self.sample.iterkeys():
            crunch=a.areaSD(self.sample[name]["image"],label='Pore',voxelsize=self.sample[name]["voxelsize"],material=self.material)
            crunch.calcPSD()
            self.sample[name]["areapsd"]=crunch.psd
            self.sample[name]["areacpsd"]=crunch.cpsd
            self.sample[name]["areaspan"]=crunch.span
            self.area[name] = crunch
    
    def __getSphereSD(self):
        self.sphere={}
        for name in self.sample.iterkeys():
            crunch=a.distanceSD(self.sample[name]["image"],label='Pore',voxelsize=self.sample[name]["voxelsize"],material=self.material)
            crunch.calcPSD()
            self.sample[name]["spherepsd"]=crunch.psd
            self.sample[name]["spherecpsd"]=crunch.cpsd
            self.sample[name]["spherespan"]=crunch.span
            self.sphere[name] = crunch

    def run(self):
            
        self.__getimage()            
        self.__getBasicStats()
        self.__getTortuosity()
        self.__getAreaSD()
        self.__getSphereSD()
        self.__generateComparisons()
            
    def __generateComparisons(self):
        r"""
        Generating the histogram based on the sphere size calculations
        """
        
        num_samples=np.size(self.sample.keys())
        from itertools import cycle    
        width = 1.0/(num_samples+2)
        colors=cycle(['b','g','m','r','y'])
        rectlist = []
        label = []
        labelitem = []
        interval = sp.arange(self.interval)
        fig = pl.figure()
        ax = fig.add_subplot(111)
        for ii,name in enumerate(self.sample.iterkeys()):
            sum=[]
            ticknames=[]
            span=sp.asarray(self.sample[name]["spherespan"][0:-1])
            psd=sp.asarray(self.sample[name]["spherepsd"])
            total=sp.sum(psd)
            psd=sp.asarray([i/total for i in psd])
            
            for j in range(self.interval):
        	if j<self.interval-1:
        	    sum.append(sp.sum(psd[span<self.spherestep*(j+1)])-sp.sum(psd[span<self.spherestep*j]))
        	    ticknames.append( str(self.spherestep*j) + "-" + str(self.spherestep*(j+1)))
        
        	else:
        	    sum.append(sp.sum(psd[span>self.spherestep*(j)]))
        	    ticknames.append( str((j)*self.spherestep)+"-"+"above" )
        	    
            sum=[i*100 for i in sum]
            interval = interval + width
            color = next(colors)
            rect = ax.bar(interval,sum,width,color=color,ecolor='k')
            rectlist.append(rect)
            label.append(name)
            labelitem.append(rect[0])
        pl.title("Sphere Based Histogram")
        pl.ylabel('Percentage of Total Porosity')
        pl.xlabel("Void Radius (nm)")
        
        ax.set_xticks(sp.arange(self.interval)+0.5)
        ax.set_xticklabels( ticknames )
        
        ax.legend(labelitem,label, loc=1, borderaxespad=0.)
        pl.ylim(ymin=10**-4)
        figname = 'fig/'+self.filename+'_SphereHist.png'
        fig.savefig(figname)
        
        
        r"""
        Generating the effective tracer percentage graph
        """
        colors=cycle(['b','g','m','r','y'])
        num_samples=np.size(self.sample.keys())
        width = 1.0/(num_samples+2)
        rectlist = []
        label = []
        labelitem = []
        fig = pl.figure()
        ax = fig.add_subplot(111)
        ind=sp.arange(3)
        
        for ii,name in enumerate(self.sample.iterkeys()):
            y=[]
            ticknames=[]
            X=self.sample[name]["EffTX"]
            y.append(X)
            ticknames.append("X")
            Y=self.sample[name]["EffTY"]
            y.append(Y)
            ticknames.append("Y")
            Z=self.sample[name]["EffTZ"]
            y.append(Z)
            ticknames.append("Z")
            ind = ind + width
            color = next(colors)
            rect = ax.bar(ind,y,width,color=color,ecolor='k')
            rectlist.append(rect)
            label.append(name)
            labelitem.append(rect[0])
        
        pl.title("Effective Tracer Percentage")
        
        ax.set_xticks(sp.arange(3)+0.5)
        ax.set_xticklabels( ticknames )
        
        ax.legend(labelitem,label, loc=1, borderaxespad=0.)
        pl.ylim(ymin=10**-4)
        figname = 'fig/'+self.filename+'_EffTracer.png'
        fig.savefig(figname)
        
        
        r"""
        Generate a comparison of the basic statistics
        """        
        colors=cycle(['b','g','m','r','y'])
        width = 1.0/(num_samples+2)
        rectlist = []
        label = []
        labelitem = []
        fig = pl.figure()
        ax = fig.add_subplot(111)
        ind=sp.arange(4)
        
        
        for ii,name in enumerate(self.sample.iterkeys()):
            y=[]
            ticknames=[]
            porosity=self.sample[name]["Porosity"]
            y.append(porosity)
            ticknames.append("Porosity")
            tortX=self.sample[name]["tortX"]
            y.append(tortX)
            ticknames.append("Tortuosity-X")
            tortY=self.sample[name]["tortY"]
            y.append(tortY)
            ticknames.append("Tortuosity-Y")
            tortZ=self.sample[name]["tortZ"]
            y.append(tortZ)
            ticknames.append("Tortuosity-Z")
            ind = ind + width
            color = next(colors)
            rect = ax.bar(ind,y,width,color=color,ecolor='k')
            rectlist.append(rect)
            label.append(name)
            labelitem.append(rect[0])
        
        pl.title("Basic Statistics")
        
        ax.set_xticks(sp.arange(4)+0.5)
        ax.set_xticklabels( ticknames )
        
        ax.legend(labelitem,label, loc=1, borderaxespad=0.)
        pl.ylim(ymin=10**-4)
        figname = 'fig/'+self.filename+'_Basic.png'
        fig.savefig(figname)        
        
        colors=cycle(['b','g','m','r','y'])
        width = 1.0/(num_samples+2)
        
        rectlist = []
        label = []
        labelitem = []
        interval = sp.arange(self.interval)
        fig = pl.figure()
        ax = fig.add_subplot(111)
        
        
        
        for ii,name in enumerate(self.sample.iterkeys()):
            sum=[]
            ticknames=[]
            span=sp.asarray(self.sample[name]["areaspan"])
            psd=sp.asarray(self.sample[name]["areapsd"])
            for j in range(self.interval):
                if j<self.interval-1:
            	    sum.append(sp.sum(psd[span<self.areastep*(j+1)])-sp.sum(psd[span<self.areastep*j]))
            	    ticknames.append( str(self.areastep*j) + "-" + str(self.areastep*(j+1)))
        
                else:
                     sum.append(sp.sum(psd[span>self.areastep*(j)]))
                     ticknames.append( str((j)*self.areastep)+"-"+"above" )
        	    
            sum=[i*100 for i in sum]
            interval = interval + width
            color = next(colors)
            rect = ax.bar(interval,sum,width,color=color,ecolor='k')
            rectlist.append(rect)
            label.append(name)
            labelitem.append(rect[0])
        pl.title("Area Based Histogram")
        pl.ylabel('Percentage of Total Porosity')
        pl.xlabel("Void Area x $10^3$ ($nm^2$)")
        
        ax.set_xticks(sp.arange(self.interval)+0.5)
        ax.set_xticklabels( ticknames )
        
        ax.legend(labelitem,label, loc=1, borderaxespad=0.)
        pl.ylim(ymin=10**-4)
        figname = 'fig/'+self.filename+'_AreaSD.png'
        fig.savefig(figname)  