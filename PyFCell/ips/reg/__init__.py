# Author: Amit Bhatkal
# License: TBD
# Copyright (c) 2013, AFCC
    
"""
**************************************************************************
:mod:`PyFCell.ips.reg` -- Registration module for Image Processing Suite
**************************************************************************



This module provides the registration algorithms for registration of FIBSEM images
Currently Moving Least Squares based algorithm is available.

"""

from feature import mls
