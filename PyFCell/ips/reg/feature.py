#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2013, TBD

r"""

*************************************************************************
:mod:`PyFCell.ips.reg.feature` -- Registration module for Image Processing Suite
*************************************************************************

.. module:: PyFCell.ips.reg

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========
Classes
==========

"""


import numpy as np
import os
import shutil
import time
import Image
#import cv2
import itertools
import pickle
import PyFCell
 


class mls(PyFCell.util.baseobject):
    
     r"""
     
      Implementation of Moving Least Squares method for registration of FIBSEM images applying Rigid transformation
      Inherits input-reader from PyFCell.ips.inout module
      inputimage, reg_type=3 , output_folder , alpha=0 ,range_x=[0,0] ,range_y=[0,0]
      
      Parameters
      ----------
      inputimage    : string
          Filename of the FIBSEM image to import.
        
      reg_type      : int
          Type of registration {1, 2, 3}. Default is 3
          reg_type = 1 => Successive reference registration.
          reg_type = 2 => First image based registration.
          reg_type = 3 => Line-marker based registration.
            
      alpha         : int
          Falloff parameter. Default is 0 (for rigid registration)
      
      range_x       : int
          Range of x-coordinates within which the protective layer lies.
          
      range_y       : int
          Range of y-coordinates within which the protective layer lies.
      
      Examples
      --------
      >>> import PyFCell.ips.reg as reg
      >>> reg_img=reg.mls("../../../data/FIBSEM/CFCP/Sample1.tif",reg_type=3,alpha=0,range_x=[20,800],range_y=[0,180]) 
      >>> alignedimg=np.array(reg_img.aligned_image)
      
     """
     
     
     def __init__(self, inputimage=None , reg_type=3 ,range_x=[0,0], range_y=[0,0] , alpha=0 ,indent='\t',**kwargs): 
         
         super(mls,self).__init__(**kwargs)
         image=PyFCell.ips.reader(inputimage).getarray()
         image=np.array(image, dtype='uint8')                
         self.alpha=alpha                                                      # Default value is 0 
         self.extrashift=0
         res=list()
         diff=0
         self._indent=indent
         imnum=np.shape(image)[2]                                              # Number of images in the stack
         (path,output_folder)=os.path.split(inputimage)
         output_folder='Registered_'+str(os.path.splitext(output_folder)[0])
         
         if imnum==1 : #Simple 2D image
             
             refimage=np.array(image[:,:,0],dtype='uint8')                                                 # Eliminating the 3rd dimension index from the image read  
             res.append(refimage)
             self.imsize=np.shape(image)             
             x1,y1=np.indices((self.imsize[0],self.imsize[1]))                                             # Create global indices for future use
             x1=x1.flat
             y1=y1.flat
             self.indxpack=zip(x1,y1)                                                                      # Creating the tuple for indices to be used                 
             imagename = raw_input('Enter the file name of the Image to be aligned ( without quotes ) :' ) 
             self.imagetoreg = np.asarray((Image.open(imagename)) , dtype='uint8')                         # Register the image specified by the user 
             finalimage,diff=self.register(refimage,diff,range_x,range_y)                             
             finalimage=np.array(finalimage, dtype='uint8')
             cv2.imwrite('Aligned_'+imagename+'.jpg',finalimage)
             res.append(finalimage)
             
         elif imnum>1 : # Image stack
            
            if os.path.isdir(output_folder):       
                shutil.rmtree(output_folder)
                #os.system('rmdir /S /Q \"{}\"'.format(output_foldername))     # Removes the folder if present already
                
            while True:
                 try :                                                         # Exception handling module in case the file operation fails                                      
                     os.mkdir(output_folder)                                   # Creates a new folder with name "Aligned images"                                                     
                 except OSError :                
                     print "\nSame folder exists.Overwriting it ....."
                     time.sleep(1)                                             # A waiting time of 1 sec is provided in order to relax all the OS processes accessing the output_folder             
                 else :
                     os.chdir(output_folder)                                   # Adds the folder to python path
                     break     
            
            print "\n*----------------Stack of images detected------------------*\n"
                        
            self.imsize=np.shape(image[:,:,0])                                 # Shape of each image in the stack   
            x1,y1=np.indices((self.imsize[0],self.imsize[1]))                  # Create global indices for future use
            x1=x1.flat
            y1=y1.flat
            self.indxpack=zip(x1,y1)                                           # Creating the tuple for indices to be used
            
            refimage=np.array(image[:,:,0],dtype='uint8')  
            cv2.imwrite('Alignedimage'+str(0)+'.jpg', refimage)
            print "----------------------------------------------------------Image",0,"registered--------------------------------------------------------------------------------------\n"
            res.append(refimage)
            if (reg_type==1) : # Implement successive registration : Takes the calculated control points from the previously aligned image as reference
                
                print "=============Successive Alignment================\n"
                finalimage=refimage
                
                for index in range(1,imnum) :                                  # Passing the images to be registered  
                    self.extrashift=0 
                    self.imagetoreg=np.asarray(image[:,:,index],dtype='uint8')
                    finalimage,diff=self.register(finalimage,diff,range_x,range_y)
                    finalimage=np.array(finalimage, dtype='uint8')
                    res.append(finalimage)
                    print "image",index,"registered\n"
                    cv2.imwrite('Alignedimage'+str(index)+'.jpg',finalimage)
                    
   
            elif (reg_type==2): # Implement First image referenced registration : Takes the control points firstimage as reference throughout
                 
                 print "==========First Image Referenced Alignment=============\n" 
                 print "Testing if the feature detection range is valid for all images\n"
                 for i in range(1,imnum):
                     print "image",index,"\n"
                     self.getcontrolpoints(image[:,:,i],image[:,:,i-1],diff,range_x,range_y,mode='test',**kwargs)
                     os.system('cls')
                     print "Feature detection range is valid!! Registration is starting\n"
                 
                 for index in range(1,imnum) :
                    self.extrashift=0      
                    self.imagetoreg=np.asarray(image[:,:,index],dtype='uint8')
                    finalimage,diff=self.register(refimage,diff,range_x,range_y)
                    finalimage=np.array(finalimage, dtype='uint8')
                    res.append(finalimage)
                    print "image",index,"registered\n"                    
                    cv2.imwrite('Alignedimage'+str(index)+'.jpg',finalimage)
                    
            
            elif (reg_type==3): # Implements hybrid method to deal with marked images : Obtains the shift between 2 successive images as they are and then adds the previous shift.
                                
                     print "===========Marker Based Alignment===========\n"
                     print "Testing if the feature detection range is valid for all images\n"
                     for index in range(1,5):
                         print "image",index,"\n"
                         self.getcontrolpoints(image[:,:,index],image[:,:,index-1],diff,range_x,range_y,mode='test',**kwargs)
                     os.system('cls')
                     print "Feature detection range is valid!! Registration is starting\n"
                     
                     for index in range(1,5) :                      
                        refimage=np.array(image[:,:,index-1],dtype='uint8')
                        self.imagetoreg=np.asarray(image[:,:,index],dtype='uint8')                    
                        finalimage,diff=self.register(refimage,diff,range_x,range_y)
                        finalimage=np.array(finalimage, dtype='uint8')
                        res.append(finalimage)
                        print "image",index,"registered\n"                        
                        cv2.imwrite('Alignedimage'+str(index)+'.jpg',finalimage)
       
         print "\n-------------------Registration completed successfully------------------------ \n" , "Please check the following folder : " +str(output_folder) 
         
         self.aligned_image=np.asarray(np.dstack(res),dtype=np.int8)
         f=open(output_folder,'w')
         pickle.dump(self.aligned_image,f)
         f.close()         
         os.chdir('..')        
            
     
     def register(self,refimage,diff,range_x,range_y) :
         
         #Obtains the control points from the feature detection module and feeds them to the get_Transformedpoints() function to obtain the transformed coordinates.
         
         print "\n-------------------------------------------------------------"
         imagtoreg=np.array(self.imagetoreg , dtype='uint8')
         p,q,diff=self.getcontrolpoints(imagtoreg,refimage,diff,np.array(range_x),np.array(range_y))
  
         p[0]=p[0]+self.extrashift
                        
         # Registration of the image          

         transformedcoord,actcoord = self.get_Transformedpoints(p,q) # Obtain the shifted coordinates "coord" and original coordinates actcoord
         
         img=np.zeros((np.shape(self.imagetoreg)))                    
         i=None
         j=None
             
         for i,j in zip(transformedcoord,actcoord):
             
             if (np.shape(img)[0]>i[0]>=0 and np.shape(img)[1]>i[1]>=0 ):
                 img[(i[0]),(i[1])]=self.imagetoreg[j[0],j[1]]               # Mapping the pixel values to new coordinates
         
         del(transformedcoord)
         del(actcoord)
         del(imagtoreg)

         # Filtering the registered image and displaying it          
         #img=(Image.fromarray((img)))     
         #img=img.filter(ImageFilter.MedianFilter)                           # Median filter is used for smooth distribution of intensity values based on the neighbouring pixels 
         
         self.extrashift=self.extrashift+diff
         print "diff=",diff,"  and extrashift=",self.extrashift
         
         return np.array(img,dtype='uint8'),diff                             # Return the aligned image
     
    

     def feature_extractor(self,img, template, distance=100):
    
 
       # This function uses SIFT method to obtain the matching features which acts as a feed for the points_extractor
       # Ref article : http://courses.cs.washington.edu/courses/cse576/11sp/notes/SIFT_white2011.pdf 
    

        # Create instances of SIFT feature detector and feature descriptor 
        detector = cv2.FeatureDetector_create("SIFT")
        descriptor = cv2.DescriptorExtractor_create("SIFT")
    
        # skp and sd indicates the keypoints and detectors of the source(image to be registered) image      
        skp = detector.detect(img)
        skp, sd = descriptor.compute(img, skp)
        
        # tkp and td indicates the keypoints and detectors of the template (reference) image    
        tkp = detector.detect(template)
        tkp, td = descriptor.compute(template, tkp)
        
        # Searching for nearest neighbours using flann library implemented in opevcv . Flann library contains various algorithms for nearest neighbours search.     
        flann_params = dict(algorithm=1, trees=4)
        flann = cv2.flann_Index(sd, flann_params)
        idx, dist = flann.knnSearch(td, 1, params={}) # idx --> indices and dist--> distances corresponding to indices  
        del flann
    
        # Normalize the distances with respect to a large value (2500 in this case)
        dist = dist[:,0]/2500.0
        dist = dist.reshape(-1,).tolist()
        idx = idx.reshape(-1).tolist()
        indices = range(len(dist))
        indices.sort(key=lambda i: dist[i])
        dist = [dist[i] for i in indices]
        idx = [idx[i] for i in indices]
        skp_final = []
    
        # Obtain the distance filtered keypoints for source image
        for i, dis in itertools.izip(idx, dist):
            if dis < distance:
                skp_final.append(skp[i])
          
        # Searching for nearest neighbours for keypoints in reference image
        flann = cv2.flann_Index(td, flann_params)
        idx, dist = flann.knnSearch(sd, 1, params={})
        del flann
    
        # Normalize the distances with respect to a large value (2500 in this case)
        dist = dist[:,0]/2500.0
        dist = dist.reshape(-1,).tolist()
        idx = idx.reshape(-1).tolist()
        indices = range(len(dist))
        indices.sort(key=lambda i: dist[i])
        dist = [dist[i] for i in indices]
        idx = [idx[i] for i in indices]
        tkp_final = []
    
        # Obtain the distance filtered keypoints for reference image
        for i, dis in itertools.izip(idx, dist):
            if dis < distance:
                tkp_final.append(tkp[i])

        return skp_final, tkp_final


     def range_test(self,img,template,skp,tkp,num=2,**kwargs):
        
        # Tests whether the specified range of coordinates are suitable for point detection
        
        h1, w1 = img.shape[:2]
        h2, w2 = template.shape[:2]
        nWidth = w1+w2
        nHeight = max(h1, h2)
        hdif = (h1-h2)/2
        newimg = np.zeros((nHeight, nWidth), np.uint8)

        newimg[hdif:hdif+h2, :w2] = template
        newimg[:h1, w2:w1+w2] = img

        maxlen = min(len(skp), len(tkp)) #Indicates the number of similar points that can be shortlisted
        
        try :
            print "Number of detected points=",maxlen
            skp[num]
            tkp[num]
        except IndexError :
            os.chdir('..')
            raise SystemExit("The number of points detected are insufficient.Please increase the range or select a different registration type")
        
     
     def points_extractor(self,img , template , skp , tkp , num=2 , strt=0 , rang=0 , diff=0 , ideal_shft=-2 , moderate_shft=-5 , extrm_shft=-10 , **kwargs):   
        
        # Extracts suitable control points from images
        
        h1, w1 = img.shape[:2]
        h2, w2 = template.shape[:2]
        nWidth = w1+w2
        nHeight = max(h1, h2)
        hdif = (h1-h2)/2
        newimg = np.zeros((nHeight, nWidth), np.uint8)

        newimg[hdif:hdif+h2, :w2] = template
        newimg[:h1, w2:w1+w2] = img
        
        prev_diff=diff 
        p=np.zeros((2,num+1))
        q=np.zeros((2,num+1))
    
        for i in range(num+1):
#            if maxlen >= (num+1):
            pt_a = (int(tkp[i].pt[0]), int(tkp[i].pt[1]+hdif))
            pt_b = (int(skp[i].pt[0]+w2), int(skp[i].pt[1])) 
            cv2.line(newimg, pt_a, pt_b, (255, 0, 0))
                        
            #Reference image
            q[0,i]=pt_a[1]
            q[1,i]=pt_a[0]+strt
                        
            #Template image
            p[0,i]=pt_b[1]
            p[1,i]=pt_b[0]-rang+strt
        
        delta=p[1]-q[1] # The shifts in x coordinates
        if np.diff(delta,len(delta)-1) :
            print "x axis shift tracked"
        else :
            p[1]=q[1] #Since the shift in x-axis coordinates is non-uniform , x-axis coordinates are made constant 
        
        
        i=0
        delta=(p[0]-q[0]) # The shifts in y coordinates

        # Testing for wrong detection of points.  
        # Conditions :All shifts are different and most of the shifts are negative and the difference between the shifts is greater than 2 and the positive shift is lesser than abs(negative shifts) which indicates negative shift prominence 
        # Example : p=[11,12,13] , q=[3,19,45] , delta=p-q=[8,-7,-32] 
        if (1<np.size(delta[delta<0])<np.shape(delta)[0]) and np.abs(np.min(np.gradient(delta[delta<0]))) > 2 and np.max(delta[delta>=0])<np.abs(np.min(delta[delta<0])) :
            delta=np.ones(np.shape(delta))
            p[0]=q[0]+delta
        
        # If the difference between shifts is too high , the following condition is checked 
        if np.abs(delta[0]-delta[1])>np.abs(extrm_shft) and np.abs(delta[1]-delta[2])>np.abs(extrm_shft) and np.abs(delta[2]-delta[0])>np.abs(extrm_shft) : 
        
            if np.any(delta<0) and np.max(delta[delta<0])>-16 :
                delta=np.ones((3))*np.max(delta[delta<0])
                p[0]=q[0]+delta
    
            else:
                print "Because of ambiguity encountered shift is set to most common value = abs(ideal_shft) = 2"
                delta=np.ones((np.shape(delta)))*np.abs(ideal_shft)
                                                          

        # Setting the number of bins of the histogram for accurate detection of same shifts
        if np.all(np.gradient(delta)==0) : # If all shifts are same
            nbin=np.shape(p)[0] 
        
        elif np.min(delta)<0  : # If all shifts are not same and negative values are encountered 
            nbin=np.arange(np.min(delta),np.max(delta)+np.abs(ideal_shft))

        elif np.min(delta)>=0 : # If all shifts are positive  
            nbin=np.arange(0,np.max(delta)+np.abs(ideal_shft))
            
        h=np.histogram(delta,bins=nbin)    
#        print np.max(h[0]),h[1][np.where(h[0]==np.max(h[0]))]
        flag = 0 
        
        if np.max(h[0])>1: 
            print "\nMajority of points have same shifts"            
            if np.max(h[0])==np.shape(delta)[0] :
                print "All points have same shift. Good match\n"
                diff=delta[0]
                flag = 1
            else :
                flag=2
                ind=np.where(h[0]==np.max(h[0]))[0][0]
                diff_tmp=np.round_(h[1][ind])
                diff=diff_tmp   
        else :    
            flag=3
            print "\nPartially ambiguous condition (Each point chosen has distinct shifts)\n"
            if np.all(delta<=0)  :        
                 # Negative values (All points have shifted in opposite direction)
                 diff=np.max(delta)    # Minimum shift in -ve direction is considered
         
            elif  np.any(delta<0) and np.abs(np.max(delta[delta<0]))<np.min(delta[delta>0]) : # Condition is satisfied only if the least shift in negative direction is less than least shift in positive direction                 
                 # Some negative value encountered (Some points have shifted in opposite direction)
                 diff=np.max(delta[delta<0])             
                   
            elif np.size(delta[delta<0])>1  and np.max(delta[delta<0])>-6 :#If majority shifts are negative and lesser than 5 units in magnitude 
                 # More points have shifted in opposite direction and the negative shift is not more than -5",delta,"\n"
                 diff=np.max(delta[delta<0])
                      
         
            else : # A condition to compare the new shifts with older shifts
                
                newdelta=delta[delta>0]
                offst=newdelta-diff
             
                # n_key --> minimum most shift in terms of magnitude ; p_key --> Minimum most shift in positive direction
                n_key=np.min(np.abs(offst))
             
                if np.any(offst>=0): # Condition to check if any of the shifts are greater than older shifts
                    p_key=np.min(offst[offst>=0])
                
                else :
                    p_key=100 # Setting p_key to a high positive value for comparision  
             
                if n_key<p_key and p_key>np.abs(ideal_shft):
                    # -ve shift is < +ve shift and +ve shift> np.abs(ideal_shft=-2)  (shift will be in opposite direction) 
                    diff=newdelta[np.abs(offst)==n_key]
                
                else:
                    # Desired condition (positive shift)
                    diff=newdelta[offst==p_key]
                   
  
        if flag !=1 and np.any(delta>=0)and diff<0 and np.abs(np.min(delta[delta>=0])-np.abs(diff))<=1 :         
            #+ve and -ve shifts are very near to zero
            diff=0
            
        if diff in delta :
            p=p[:,delta==diff]
            q=q[:,delta==diff]
    
        else :
            p[0]=q[0]+diff
        
        # Checking for shift much less than required shift"
        
        if ((prev_diff>0) and ((diff-prev_diff)) > np.abs(extrm_shft) ) : # Note extrm_shft = 10 units in magnitude 
            diff=prev_diff+1
            p[0]=q[0]+diff

        elif np.size(delta[np.where(delta < extrm_shft)])>1 and diff< extrm_shft : 
            # Specific negative shift condition:
            delta=delta[delta<0]
            diff=np.min(delta)
            p[0]=q[0]+diff  
  
        elif diff < moderate_shft   : # Note that moderate_shft=5 units in magnitude    
            # A hapazard negative shift has appeared and hence truncating it to value of moderate_shft 
            diff=moderate_shft
            p[0]=q[0]+diff
    
        if diff==0 and flag==3 :
            # If the shift is selected as zero units , this condition cross checks and sets the correct shift
            if np.max(delta)>=3:
                diff=2
            else :
                diff=np.max(delta)
                p[0]=q[0]+diff
        
        temp_p=p[:,0:num]
        temp_q=q[:,0:num]
    
        del p,q
    
        p=temp_p
        q=temp_q
    
        del temp_p,temp_q
       
        if np.shape(p[0])[0]<num : #Creation of second control point if only one point is being shortlisted from above steps
            temp=np.ones((2,num))
            temq=np.ones((2,num))
            for i in range(0,num):
                temp[0][i] = p[0,i-1]+ i
                temq[0][i] = q[0,i-1]+ i
                temp[1][i] = p[1,i-1]
                temq[1][i] = q[1,i-1]
    
            del p , q
            p=temp
            q=temq
            del temp,temq     
    
        if np.max(p[0])==np.min(p[0]) or np.max(q[0])==np.min(q[0]) : #Condition to adjust the second point in case both points shortlisted are the same
            for i in range(1,num):
                p[0][i] = p[0][i-1]+ 1
                q[0][i] = q[0][i-1]+ 1
          
        if np.any(delta>0) and diff == 0 and flag < 1  :
            diff =1 
            p[0]=q[0]+diff
        
        return p,q, (p[0]-q[0])[0]



     def getcontrolpoints(self,img,temp,diff,range_x,range_y,num=2,mode='reg',**kwargs):
        
        # Returns the control points to the register() function
        
        offstx=np.array(range_x)
        offsty=np.array(range_y)
        
        strt= offstx[0]
        rang=offstx[1]-strt

        img=img[offsty[0]:offsty[1],offstx[0]:offstx[1]]
        temp=temp[offsty[0]:offsty[1],offstx[0]:offstx[1]]
        
        img=np.array(img,dtype='uint8')
        temp=np.array(temp,dtype='uint8')
        
        skp, tkp = self.feature_extractor(img,temp ,10)
        if mode=='test' :
            self.range_test(img,temp,skp,tkp,num)
            
        if mode=='reg':
            p,q,diff = self.points_extractor(img, temp, skp, tkp ,num,strt,rang,diff)
            return p,q,diff

     
     def get_Transformedpoints(self,cntrl_pt1,cntrl_pt2):  
         
         #Calculates the transformed points by determining the weights , centroids and the rigid transformation matrix  
         
         p=cntrl_pt1 # Control points of image to be registered
         q=cntrl_pt2 # Control points of reference image
         
         print "point p", p[0],p[1]                                              
         print "point q", q[0],q[1]
                  
         size1=np.shape(self.imagetoreg)
         size2=size1
         
         if(self.alpha != 0): # Executed condition if internal warping required in the registration ie., alpha>0
             k=np.zeros((size1[0],size1[1],np.shape(p)[0]))
             M=np.zeros((size1[0],size1[1],2,2))  # Initialise the transformation matrix M
         
             transformedcoord=list()  # List to hold the registered coordinates
             actcoord=list()          # List to hold the actual coordinates
         
             diffx=np.transpose(np.array(np.transpose(np.tile(p[0],(size1[0],1)))-range(size1[0])+0.0000000000001)) # 0.0000000000001 is added as offset to avoid zero values
             diffy=np.transpose(np.array(np.transpose(np.tile(p[1],(size1[1],1)))-range(size1[1])+0.0000000000001))
         
             weights= np.array(1.0/(np.power([np.hypot(diffx[i],diffy)for i in range(size1[0])],(2*self.alpha)))) 
             wsum=np.transpose((np.transpose(weights))[0]+(np.transpose(weights))[1])

             #pcentroid calculation             
             temp1=(weights)*p[0] 
             temp2=(weights)*p[1] 
         
             w0=np.transpose((np.transpose(temp1))[0]+(np.transpose(temp1))[1])
             w1=np.transpose((np.transpose(temp2))[0]+(np.transpose(temp2))[1])
             temp1=None
             temp2=None
         
             temp1= w0/wsum
             temp2= w1/wsum
         
             pcentroid=np.transpose(np.array([np.transpose(temp1),np.transpose(temp2)]))

             temp1=None
             temp2=None
         

             #qcentroid calculation         
             temp1=(weights)*q[0] 
             temp2=(weights)*q[1] 
         
             w0=np.transpose((np.transpose(temp1))[0]+(np.transpose(temp1))[1])
             w1=np.transpose((np.transpose(temp2))[0]+(np.transpose(temp2))[1])
         
             temp1=None
             temp2=None
         
             temp1= w0/wsum
             temp2= w1/wsum
         
             qcentroid=np.transpose(np.array([np.transpose(temp1),np.transpose(temp2)]))

             #pnew = p-pcentroid ie., Shifting the pcentroids to origin                  
             temp1=np.transpose(np.resize(p[0],(size1[0],size1[1],np.shape(p[0])[0])))
             temp1=np.transpose(np.array(temp1-np.transpose(pcentroid)[0]))
             
             temp2=np.transpose(np.resize(p[1],(size1[0],size1[1],np.shape(p[1])[0])))
             temp2=np.transpose(np.array(temp2-np.transpose(pcentroid)[1]))

             pnew= np.array([temp1,temp2])
         
             temp1=None
             temp2=None 
         
             #qnew = q-pcentroid ie., Shifting the qcentroids to origin                   
             temp1=np.transpose(np.resize(q[0],(size2[0],size2[1],np.shape(q[0])[0])))
             temp1=np.transpose(np.array(temp1-np.transpose(qcentroid)[0]))
         
             temp2=np.transpose(np.resize(q[1],(size2[0],size2[1],np.shape(q[1])[0])))
             temp2=np.transpose(np.array(temp2-np.transpose(qcentroid)[1]))
         
             qnew= np.array([temp1,temp2])
             
             pnewt=np.array([-1*pnew[1],pnew[0]])
             qnewt=np.array([-1*qnew[1],qnew[0]])
                 
             del(temp1)
             del(temp2)
                 
             #For Mu calculation         
             temp1=weights*((pnew[0]*qnew[0])+(pnew[1]*qnew[1]))
             musum1=np.transpose((np.transpose(temp1))[0]+(np.transpose(temp1))[1])
             musum1=musum1**2

             temp2=weights*((pnewt[0]*qnew[0])+(pnewt[1]*qnew[1]))
             musum2=np.transpose((np.transpose(temp2))[0]+(np.transpose(temp2))[1])
             musum2=musum2**2
             mu=(np.sqrt(musum1+musum2))+0.0000001                                           # 0.0000001 is added as offset incase mu falls to zero
 
             #Obtaining the Transform Matrix M 
             temp1= np.transpose((np.transpose(temp1))[0]+(np.transpose(temp1))[1]) 
             temp2=weights*(-1*pnew[0]*(qnewt[0]) - (pnew[1]*(qnewt[1])))
             temp2=-1*np.transpose((np.transpose(temp2))[0]+(np.transpose(temp2))[1])
             temp3=-1*temp2
           
             M= np.transpose(np.array([[(temp1)/mu,(temp2)/mu],[(temp3)/mu,(temp1)/mu]]))     
          
             del(temp1)
             del(temp2)
             del(temp3)

             x=0
             y=0 
             i=0
         
             controlpoints=np.zeros(np.shape(p))
         
             # Calculation of transformed coordinates 
             for x,y in self.indxpack:
                  
                 k[x,y]=np.dot(pcentroid[x,y],(M[y,x]))
                 actcoord.append([x,y])
                 offset=(np.dot([x,y],(M[y,x])))
                 transformedcoord.append(np.dot([x,y],(M[y,x])))
                 
                 if(x==p[0][0] and y==p[1][0]) or (x==p[0][1] and y==p[1][1]) :
                     
                     controlpoints[0][i]=[offset+qcentroid[x,y]-k[x,y]][0][0]
                     controlpoints[1][i]=[offset+qcentroid[x,y]-k[x,y]][0][1]
                     i=i+1
         
             translate= qcentroid-k         
             transformedcoord=transformedcoord+np.reshape(translate,np.shape(transformedcoord)) # Translation factor added 

             del(translate)
             del(offset)
             del(k)
         
         else: # When alpha=zero ie., we do not need any internal warping in the image , this condition is executed.
             dy=p[0]-q[0]
             dx=p[1]-q[1]
             ind=np.indices(size1)
             actcoord=np.reshape(np.transpose(ind),(size1[0]*size1[1],2))
             nind=ind
             nind[0]=ind[0]-dy[0]
             nind[1]=ind[1]-dx[0]
             transformedcoord=np.reshape(np.transpose(nind),(size1[0]*size1[1],2))
         
         return (transformedcoord), (actcoord)                                                  # Return the coordinates (both transformed and actual)
         
         





