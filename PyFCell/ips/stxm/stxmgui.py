#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Arash Ash, AFCC Development Team
# License: TBD
# Copyright (c) 2014, TBD

from __future__ import print_function
import os
from pprint import pprint as print

from sip import setapi
setapi('QString', 2)
setapi('QVariant', 2)

from PyQt4 import QtCore, QtGui

import stxmtools
from gui import *
from stxmio import *
import icons
from stxmclass import STXMData
import Register

AUTHORS = ['Arash Ash', 'Harold Day']
ORGANIZATION = 'AFCC'
APPNAME = 'stxmgui'
TITLE = 'XFCell STXM Data Processing Tool'
VERSION = '1.1'
LICENSE = 'MIT'

class StxmModel(QtGui.QStandardItemModel):
    enabledAndSelectable = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def __init__(self, parent=None):
        super(StxmModel, self).__init__(parent)
        self.I0Root = QtGui.QStandardItem("I0")
        self.I0Root.setData(QtGui.QIcon(':icons/error'), QtCore.Qt.DecorationRole)
        self.I0Root.setFlags(self.enabledAndSelectable)
        self.appendRow(self.I0Root)
        self.stringListToModel(self.I0Root, "Open File", "")
        self.stringListToModel(self.I0Root, "Registration")
        self.stringListToModel(self.I0Root, "Energy Shift")
        self.stringListToModel(self.I0Root, "I0 Selection")
        self.stringListToModel(self.I0Root, "Compare I0")
        self.sampleRoot = QtGui.QStandardItem("Sample")
        self.sampleRoot.setFlags(self.enabledAndSelectable)
        self.appendRow(self.sampleRoot)
        self.stringListToModel(self.sampleRoot, "Open File", "")
        self.stringListToModel(self.sampleRoot, "Registration")
        self.stringListToModel(self.sampleRoot, "Energy Shift")
        self.stringListToModel(self.sampleRoot, "Optical Density")
        self.stringListToModel(self.sampleRoot, "Spectrum Calculator")
#        self.stringListToModel(self.sampleRoot, "Component Mapping", )
        self.MappingRoot = QtGui.QStandardItem("Component Mapping")
        self.MappingRoot.setFlags(self.enabledAndSelectable)
        self.appendRow(self.MappingRoot)
        self.stringListToModel(self.MappingRoot, "Fit", )
        self.stringListToModel(self.MappingRoot, "Fit Quality Check", )
        self.stringListToModel(self.MappingRoot, "Scale", )
        self.stringListToModel(self.MappingRoot, "RGB", )
        self.stringListToModel(self.MappingRoot, "Image - Value Manipulation", )
        self.stringListToModel(self.MappingRoot, "Image - Image Manipulation", )

    def stringListToModel(self, parent, *columnValues):
        
        itemList = []
        for value in columnValues:
            item = QtGui.QStandardItem(value)
            item.setFlags(self.enabledAndSelectable)
            itemList.append(item)

        if itemList:
            parent.appendRow(itemList)

# Multi-Region Super Stack Selection
class RegionSelection(QtGui.QInputDialog):
    def __init__(self, regions, parent = None):
        super(RegionSelection, self).__init__(parent)
        self.setWindowTitle('Region Selection')
        self.setLabelText('Required Region?')
        RegionList = []
        for i in range(regions):
            RegionList.append('Region '+str(i + 1))
        self.setComboBoxItems(RegionList)
        self.accepted.connect(self.RegionSelected)
        self.exec_()
       
    def RegionSelected(self):
        text = str(self.textValue())
        txt, region = text.split()
        self.region = int(region) - 1
        
    def getRegion(self):
        return self.region

class StxmMain(QtGui.QMainWindow):

    def __init__(self, title, parent=None):
        super(StxmMain, self).__init__(parent)
        self.setWindowTitle(title)
        self.setWindowIcon(QtGui.QIcon(':icons/XFCell_icon2'))
        self.tabWidget = QtGui.QTabWidget()
        self.setCentralWidget(self.tabWidget)
        self.I0Analyzer = StackAnalyzer()       
        self.tabWidget.addTab(self.I0Analyzer, "I0")
        self.sampleAnalyzer = StackAnalyzer()
        self.tabWidget.addTab(self.sampleAnalyzer, "Sample")
        self.mapViewer = SimpleSlideView()
        self.tabWidget.addTab(self.mapViewer, "Map")
        self.model = StxmModel()
        self.tree = QtGui.QTreeView()
        self.tree.setHeaderHidden(True)
        self.tree.setModel(self.model)
        self.tree.expandAll()
        self.tree.selectionModel().selectionChanged.connect(self.selectionChanged)
        self.treeDock = QtGui.QDockWidget("Tree")
        self.treeDock.setWidget(self.tree)
        self.treeDock.setFeatures(QtGui.QDockWidget.NoDockWidgetFeatures)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.treeDock)
        self.propDock = QtGui.QDockWidget("Property Editor")
        self.propDock.setFeatures(QtGui.QDockWidget.NoDockWidgetFeatures)
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.propDock)
        self.setCorner(QtCore.Qt.BottomLeftCorner, QtCore.Qt.LeftDockWidgetArea)
        self.toolBar = QtGui.QToolBar("Top Level Tools")
        self.addToolBar(self.toolBar)
        self.toolBar.addAction(QtGui.QIcon(':icons/magnifier'), "Inspect", self.inspectSelectedIndex)
        self.toolBar.addAction(QtGui.QIcon(':icons/cog'), "Settings", lambda: os.system("gedit {}".format(QtCore.QSettings().fileName())))
        self.toolBar.addAction(QtGui.QIcon(':icons/information'), "About", lambda:
            QtGui.QMessageBox.about(self, TITLE, '\n'.join(AUTHORS)))
        self.toolBar.addAction(QtGui.QIcon(':icons/disk_multiple'), "Save Stack", self.saveStack)
        self.toolBar.addAction(QtGui.QIcon(':icons/disk'), "Save Image", self.saveImage)
        self.I0Analyzer.deletedSliceSig.connect(self.deleteSlice)
        self.sampleAnalyzer.deletedSliceSig.connect(self.deleteSlice)
        self.I0Analyzer.SaveSpectrumPressed.connect(self.saveSpectrum)
        self.sampleAnalyzer.SaveSpectrumPressed.connect(self.saveSpectrum)
        self.I0Analyzer.rotateSig.connect(self.dataRotationHandler)
        self.sampleAnalyzer.rotateSig.connect(self.dataRotationHandler)

    def dataRotationHandler(self):
        currentWidget = self.tabWidget.currentWidget()
        currentWidget.lastLoad['stack'] = currentWidget.rawImage
        
    def applyEnergyShift(self, value):
        currentWidget = self.tabWidget.currentWidget()
        currentWidget.reserveEnergy = currentWidget.lastLoad['energy']
        currentWidget.lastLoad['energy'] = np.add(currentWidget.lastLoad['energy'], value)
        currentWidget.setStack(currentWidget.lastLoad['stack'], currentWidget.lastLoad['energy'], currentWidget.stage)

    def revertEnergyShift(self):
        currentWidget = self.tabWidget.currentWidget()
        currentWidget.lastLoad['energy'] = currentWidget.reserveEnergy
        currentWidget.setStack(currentWidget.lastLoad['stack'], currentWidget.lastLoad['energy'], currentWidget.stage)

    def scaleBarSizeHandler(self, size):
        currentWidget = self.sampleAnalyzer
        self.mapViewer.scaleItem.setSize(size)
        self.mapViewer.setScale(scale=currentWidget.lastLoad['x_step'])

    def showHideScaleHandler(self):
        self.mapViewer.showHideInfo()

    def deleteSlice(self, ind):
        currentWidget = self.tabWidget.currentWidget()
        currentWidget.lastLoad['stack'] = currentWidget.visibleImage
        currentWidget.lastLoad['energy'] = currentWidget.depthAxis

    def saveStack(self, path=None):
        ''' if looking at either of the stacks, save the stacks. if looking
            at the images, save the images in sequence according to their
            component name

        '''
        try:
            currentWidget = self.tabWidget.currentWidget()
    #       Selected Widget
            if self.tabWidget.currentIndex() == 0:
                DataToSave = self.I0
            if self.tabWidget.currentIndex() == 1:
                DataToSave = self.Sample
                
            DataToSave.data = currentWidget.visibleImage
            
    #       Reading the header file of the current Widget
            header = DataToSave.header
#            Stackpath, FileName = os.path.split(DataToSave.path)
#            StackName, Ext = os.path.splitext(FileName)
            lastPath = QtCore.QSettings().value("PATH/{}".format(currentWidget.stage), os.getcwd())
            path = path or QtGui.QFileDialog.getSaveFileName(self, "Save Stack", directory=lastPath, filter='NCB Files (*.ncb);;XFCell Files(*.xfc)')
            ext = path.split('.')[1]
            if path:
                if ext == 'ncb':
                    ncb_writer(path, stack=DataToSave.data, x_axis=header['x_axis'], y_axis=header['y_axis'], energy=DataToSave.energy, path=DataToSave.path)
                            
    #       Adding Cheched ROIs to the STXMData to be saved     
            roiDict = currentWidget.checkedRois()
            if len(roiDict) > 0:        
                DataToSave.addROI(**roiDict)
    
    #       Adding additional Spectrums loaded from files
            spectrumDict = {}
            for label, (x,y) in currentWidget.checkedSpectrums():
                spectrumDict[label] = y   
                
            if len(spectrumDict) > len(roiDict):
                for keys, values in spectrumDict.iteritems():
                    try:
                        roiDict[keys]
                    except KeyError:
                        DataToSave.addSpectrum(label = keys, spectrum = spectrumDict[keys])                   
    
    #       Save STXMData to *.xfc file
            if ext == 'xfc':
                xfc_writer(path, data = DataToSave)
        except AttributeError:
            QtGui.QMessageBox.information(self, "Warning", "No stack is open!")           

    def saveImage(self, path=None):
        currentWidget = self.tabWidget.currentWidget()
        try:
            sliceList = currentWidget.sliceList            
        except AttributeError:
            sliceList = currentWidget.slideList
        
        if sliceList.count() > 0:
            currentIndex = sliceList.currentRow() % sliceList.count()
            lastPath = QtCore.QSettings().value("PATH/{}".format(currentWidget.stage), os.getcwd())
            path = path or QtGui.QFileDialog.getSaveFileName(self, "Save Image", directory=lastPath, filter='AXB Files (*.axb)')
            
            try:
                Image = currentWidget.visibleImage[:,:,currentIndex]                 
            except AttributeError:
                Image = sliceList.item(currentIndex).data(QtCore.Qt.UserRole)
            
            axb_writer(path, data=Image, **currentWidget.lastLoad)                

    def saveSpectrum(self):
        currentWidget = self.tabWidget.currentWidget()
        data = currentWidget.visibleImage  
        name  = currentWidget.name 
        comment = ['1d', name, data.shape[2]]
        currentWidget.saveSpectrum(comment)
            
    def loadStack(self, path, stage, append):
        self.model.I0Root.setData(QtGui.QIcon(':icons/error'), QtCore.Qt.DecorationRole)
        try:
            del(self.I0spectrum)
        except AttributeError:
            pass    
#        # initializing  stack for registration
        currentWidget = self.tabWidget.currentWidget()      
        if append:            
            self.oldStack = currentWidget.visibleImage
            self.oldEnergy = currentWidget.depthAxis

        ext = os.path.splitext(path)[1]
        if ext == '.hdr':
            currentWidget.lastLoad = hdr.load_stack(path, legacy=True)
            if currentWidget.lastLoad['region'] > 1:
                window = RegionSelection(currentWidget.lastLoad['region'])
                try:
                    currentWidget.lastLoad['stack'] = currentWidget.lastLoad['stack'][window.getRegion()]
                except AttributeError:
                    QtGui.QMessageBox.warning(self, 'Warning!', 'A Region Should be Selected for Multi-region Stacks')
#            else:
#                print ('im here')
                
        elif ext == '.ncb':
            currentWidget.lastLoad = ncb_reader(path)
            currentWidget.lastLoad['path'] = path
            
        elif ext == '.axb':
            currentWidget.lastLoad = axb_reader(path)
            currentWidget.lastLoad['stack'] = currentWidget.lastLoad['data']

        elif ext == '.xfc':
#           returns a dictionary and STXMData instance as obj 
            currentWidget.lastLoad, obj = xfc_reader(path) 
            
        elif ext == '.tif' or '.tiff':
            import PyFCell as pfc
            crunch = pfc.ips.inout.input.reader(path)
            currentWidget.lastLoad['stack'] = crunch.getarray()
            currentWidget.lastLoad['energy'] = np.arange(currentWidget.lastLoad['stack'].shape[2])
            currentWidget.lastLoad['path'] = path
            
        if append:
            try:
                self.newStack = currentWidget.lastLoad['stack']
                self.newEnergy = currentWidget.lastLoad['energy']
                currentWidget.lastLoad['stack'] = np.dstack((self.oldStack, self.newStack))
                currentWidget.lastLoad['energy'] = np.append(self.oldEnergy, self.newEnergy)
            except ValueError:
                QtGui.QMessageBox.warning(self, 'Error!', 'Array Sizes Must Agree for Stack Appending')
            
        ndarray = currentWidget.lastLoad['stack']
        depthAxis = np.asarray(currentWidget.lastLoad['energy'])
               
        if stage == 'I0':
            
            self.I0Analyzer.setStack(ndarray, depthAxis, stage)            
            self.model.I0Root.child(0,1).setData(path, 0)
            self.I0 = STXMData(**currentWidget.lastLoad)
            self.I0Analyzer.setName(self.I0.name)
            currentWidget = self.I0Analyzer
                           
        elif stage == 'Sample':
            self.sampleAnalyzer.setStack(ndarray, depthAxis, stage)   
            self.model.sampleRoot.child(0,1).setData(path, 0)
            self.Sample = STXMData(**currentWidget.lastLoad)
            self.sampleAnalyzer.setName(self.Sample.name)
            currentWidget = self.sampleAnalyzer
            
#       Extracting ROIs and Spectrums from the STXMData instance
        if ext == '.xfc':
            for keys, values in iter(sorted(obj.getROI().iteritems())):
                currentWidget.addRoi(values[0], values[1], keys, load = True)
            
#            if len(obj.getSpectrum()) > len(obj.getROI()):
#                for keys, values in iter(sorted(obj.getSpectrum().iteritems())):
#                    try:
#                        obj.getROI()[keys]
#                    except KeyError:
#                        currentWidget.loadSpectrum(fromfile = False, label = keys, xData = obj.energy, yData = obj.getSpectrum()[keys])
#                            
        QtCore.QSettings().setValue("PATH/{}".format(stage), path)

    def useI0Stack(self, stage):
        ndarray = self.I0Analyzer.lastLoad['stack']
        depthAxis = self.I0Analyzer.lastLoad['energy']
        self.sampleAnalyzer.lastLoad = self.I0Analyzer.lastLoad
        self.sampleAnalyzer.setStack(ndarray, depthAxis, stage)
        self.Sample = STXMData(self.I0Analyzer.lastLoad)
        self.sampleAnalyzer.setName(self.I0.name)
        self.sampleAnalyzer.stage = "I0"

    def SpectrumCalc(self, iA, iB, Op, valSupplied):
        currentWidget = self.tabWidget.currentWidget()
        OperationDict = {'/' : np.divide, '*' : np.multiply, '+' : np.add, '-' : np.subtract}
        curveA = currentWidget.roiList.item(iA).data(34)
        nameA = currentWidget.roiList.item(iA).data(0)
        if valSupplied:
            result = OperationDict[Op](curveA.yData, iB)
            nameB = str(iB)
        else:
            curveB = currentWidget.roiList.item(iB).data(34)
            nameB = currentWidget.roiList.item(iB).data(0)
            result = OperationDict[Op](curveA.yData, curveB.yData)
        dialog = PlotDialog(y = result,x = currentWidget.lastLoad['energy'],pname = nameA+Op+nameB, 
                            xlabel = 'energy (keV)', ylabel = 'Result', ppen='g', returnPlot = True) 
        dialog.returnPlotSig.connect(self.SpectrumCalcPlot)
        dialog.exec_()

    def SpectrumCalcPlot(self, plot, name):
        currentWidget = self.tabWidget.currentWidget()
        currentWidget.loadSpectrum(fromfile = False, label = name, 
                                   xData=plot.items[0].xData, yData=plot.items[0].yData)
                                
    def manipCalculator(self,mode,iA, iB, Op):
        OperationDict = {'/' : np.divide, '*' : np.multiply, '+' : np.add, '-' : np.subtract}
        bufferA = self.mapViewer.slideList.item(iA).data(QtCore.Qt.UserRole)
        bufferAName = self.mapViewer.slideList.item(iA).name
        bufferAUnit = self.mapViewer.slideList.item(iA).unit
        if mode == 'Image':
            bufferB = self.mapViewer.slideList.item(iB).data(QtCore.Qt.UserRole)
            bufferBName = self.mapViewer.slideList.item(iB).name
            bufferBUnit = self.mapViewer.slideList.item(iB).unit
            resultUnit = stxmtools.define_unit(bufferAUnit,bufferBUnit,Op)
        elif mode == 'Value':
            bufferB = iB
            bufferBName = str(iB)
            resultUnit = bufferAUnit
        result = OperationDict[Op](bufferA, bufferB)
        
        # nan and inf check
        isnan = np.isnan(result)
        isinf = np.isinf(result)
        indnan = [i for i, elem in np.ndenumerate(isnan) if elem]
        indinf = [i for i, elem in np.ndenumerate(isinf) if elem]
        if len(indnan) or len(indinf) > 0:
            # replacing nan and inf values with 0.
            result = np.nan_to_num(result)
            result[np.abs(result)>1e300]=0.
            
        self.mapViewer.addSlide(result, bufferAName+Op+bufferBName, unit = resultUnit)

    def setI0Spectrum(self, roiIndex, Line, direction, minV, maxV):
        self.LineI0 = Line
        currentWidget = self.tabWidget.currentWidget()
        item = self.I0Analyzer.roiList.item(roiIndex)
        if not Line:
            self.I0spectrum = item.data(34).yData # get the curve, and then get yData from the curve            
            try:
                roi = item.data(33)
                region = roi.getArrayRegion(data=currentWidget.visibleImage, img=self.I0Analyzer.imageItem, axes=(0,1))            
                stdvI0 = stxmtools.std_dev(region)
                plot = {'y' : self.I0spectrum, 'x' : item.data(34).xData, 'pname' : 'Selected I0', 
                        'y2' : stdvI0, 'pname2' : 'Std. Dev.', 'xlabel': 'Energy (Kev)', 
                        'ylabel' : 'Intensity', 'p2label' : 'Standard Deviation'}
            except AttributeError:
                plot = {'y' : self.I0spectrum, 'x' : item.data(34).xData, 'pname' : 'Selected I0', 
                        'xlabel': 'Energy (Kev)', 'ylabel' : 'Intensity'}
    
            dialog = PlotDialog(**plot) 
            dialog.exec_()
        elif Line:
#            roi = item.data(33)
#            self.I0spectrum = roi.getArrayRegion(data=currentWidget.visibleImage, img=self.I0Analyzer.imageItem, axes=(0,1))
            if direction == 'Vertical':
                self.I0spectrum = currentWidget.lastLoad['stack'][minV:maxV, :, :]
            elif direction == 'Horizontal':
                self.I0spectrum = currentWidget.lastLoad['stack'][:, minV:maxV, :]
                
        self.model.I0Root.setData(QtGui.QIcon(':icons/tick'), QtCore.Qt.DecorationRole)

    def saveLineI0Stack(self, path=None):
        currentWidget = self.tabWidget.currentWidget()
        header = self.I0.header
        lastPath = QtCore.QSettings().value("PATH/{}".format(currentWidget.stage), os.getcwd())
        path = path or QtGui.QFileDialog.getSaveFileName(self, "Save Line I0 Stack", directory=lastPath, filter='NCB Files (*.ncb)')
        if path:
            try:
                ncb_writer(path, stack=self.I0spectrum, x_axis=header['x_axis'],
                               y_axis=header['y_axis'], energy=self.I0Analyzer.lastLoad['energy'], path=path)
            except AttributeError:
                QtGui.QMessageBox.information(self, "Line I0 should be selected first!")

    def setVlineI0Line(self,minV,maxV,direction):        
        currentWidget = self.tabWidget.currentWidget()
        currentWidget.viewBoxInfLine([minV,maxV],direction)       

    def addRemoveVlineI0Line(self, show,direction):
        currentWidget = self.tabWidget.currentWidget()
        currentWidget.viewBoxInfLineAddRemove(add=show,direction=direction)
        
    def inspectSelectedIndex(self):
        indexList = self.tree.selectedIndexes()
        roleNames = self.model.roleNames()
        for index in indexList:
            print( "R{}C{}".format(index.row(), index.column()))
            for role, value in self.model.itemData(index).items():
                print("{:>10} : {}".format(roleNames.get(role, role), value))

    def opticalDensityFilter(self):
        filterFunction = lambda img: stxmtools.optical_density(
            self.I0spectrum, img, self.LineI0)
        self.sampleAnalyzer.applyFilter(filterFunction)
        self.sampleAnalyzer.lastLoad['stack'] = self.sampleAnalyzer.filteredImage

    def mapComponents(self): 
#        global fitted, labels, components        
#       Inheriting meta info from Sample 
        self.mapViewer.lastLoad['energy'] = self.sampleAnalyzer.lastLoad['energy']
        self.mapViewer.lastLoad['x_axis'] = self.sampleAnalyzer.lastLoad['x_axis']
        self.mapViewer.lastLoad['y_axis'] = self.sampleAnalyzer.lastLoad['y_axis']
        self.mapViewer.lastLoad['x_step'] = self.sampleAnalyzer.lastLoad['x_step']
        self.mapViewer.lastLoad['y_axis'] = self.sampleAnalyzer.lastLoad['y_axis']
        self.mapViewer.lastLoad['path'] = self.sampleAnalyzer.lastLoad['path']
        self.mapViewer.lastLoad['region'] = self.sampleAnalyzer.lastLoad['region']
        self.mapViewer.lastLoad['Dwell'] = self.sampleAnalyzer.lastLoad['Dwell']
        self.mapViewer.lastLoad['Dfocus'] = self.sampleAnalyzer.lastLoad['Dfocus']
        
        labels, components = zip(*[(label, y) for label, (x,y) in self.sampleAnalyzer.checkedSpectrums()])
        
        if not components:
            return
        self.mapViewer.clearslidelist()
        try:
            self.mapViewer.setScale(self.sampleAnalyzer.lastLoad['x_step'], self.sampleAnalyzer.lastLoad['Dwell'], self.sampleAnalyzer.lastLoad['Dfocus']) 
        except KeyError:
            self.mapViewer.setScale(self.sampleAnalyzer.lastLoad['scale'])      
        
        self.M , self.res, self.fitted, self.diff, self.sum_diff= stxmtools.fit_map(self.sampleAnalyzer.visibleImage, components)
        for i in range(self.M.shape[-1] - 1):
            if np.mean(self.M[:,:,i]) < 3: # assumed od is < 3 and nm scale is more
                unit = 'od'
            else:
                unit = 'nm'
            self.mapViewer.addSlide(self.M[:,:,i], labels[i], unit = unit)
        self.mapViewer.addSlide(self.M[:,:,-1], 'Constant', unit = 'od')            
        self.mapViewer.addSlide(self.res, 'residual', unit = 'od')
        self.mapViewer.addSlide(self.sum_diff, 'Sum diff', unit = 'od')
        self.mapViewer.stage = self.sampleAnalyzer.stage
        
    def fitQualityAssistant(self):
        labels, components = zip(*[(label, y) for label, (x,y) in self.sampleAnalyzer.checkedSpectrums()])
        self.qualityAssistantDialog = FitQualityCheckWindow(self.fitted, self.sampleAnalyzer.lastLoad['energy'] , 
                                                     'Fitted', self.sampleAnalyzer.visibleImage, 'Raw OD')
        for i in range(len(labels)):
            self.qualityAssistantDialog.addSpectrum(components[i], labels[i])
        self.qualityAssistantDialog.show()    
        
    def registerationHandler(self, stage):
        currentWidget = self.tabWidget.currentWidget() 
        regist = Register.Registration(currentWidget.lastLoad['stack'], 
                                        np.arange(currentWidget.lastLoad['stack'].shape[2]), 
                                        currentWidget.lastLoad['stack']) 
#        Reserve data for rejection
        self.reserveData = currentWidget.lastLoad['stack']                   
        currentWidget.lastLoad['stack'] = regist.cropped
        ndarray = currentWidget.lastLoad['stack']
        depthAxis = currentWidget.lastLoad['energy'] 
        currentWidget.setStack(ndarray, depthAxis, stage)

    def rejectRegistration(self, stage):        
        QtGui.QMessageBox.information(self, "Warning", "Registration was Rejected!")
        currentWidget = self.tabWidget.currentWidget()
        currentWidget.lastLoad['stack'] = self.reserveData
        ndarray = currentWidget.lastLoad['stack']
        depthAxis = currentWidget.lastLoad['energy'] 
        currentWidget.setStack(ndarray, depthAxis, stage)
        
    def registrationAssistant(self, stage):
        currentWidget = self.tabWidget.currentWidget()
        try:
            ndarray = currentWidget.lastLoad['stack']
            depthAxis = currentWidget.lastLoad['energy'] 
            self.registrationDialog = RegistrationAssistantWindow(ndarray, depthAxis, stage)
            self.registrationDialog.returnStack.connect(self.returnStackHandler)  
            self.registrationDialog.show()            
        except AttributeError:
            QtGui.QMessageBox.warning(self, "Error", "Stack Was not Found!")

    def returnStackHandler(self, stage):
        currentWidget = self.tabWidget.currentWidget()
        self.registrationDialog.close()
        currentWidget.lastLoad['stack'] = self.registrationDialog.registeredData
        ndarray = currentWidget.lastLoad['stack']
        depthAxis = currentWidget.lastLoad['energy'] 
        currentWidget = self.tabWidget.currentWidget()
        currentWidget.setStack(ndarray, depthAxis, stage)

    def RGBmap(self):
        RGBmap = stxmtools.RGB_Map(self.M)
        RGBimage = np.zeros((RGBmap.shape[1], RGBmap.shape[0], RGBmap.shape[2]))
        for i in range(RGBmap.shape[2]):
            RGBimage[:,:,i] = RGBmap[:,:,i].transpose()
        RGBimage = np.rollaxis(RGBimage, 0,2)
        self.mapViewer.addSlide(RGBimage, 'RGB', unit = 'Dimensionless')

    def odInterpolation(self, obj):
        currentWidget = self.tabWidget.currentWidget()
        obj.spectrumInterpolation(currentWidget.lastLoad['energy'])
            
    def selectionChanged(self, selected, deselected):
        if selected.isEmpty():
            self.propDock.setWidget(None)
            return

        index = self.tree.selectedIndexes()[0]
        item = self.model.itemFromIndex(index)
        typeString = item.data(QtCore.Qt.DisplayRole)
        self.changeVisibleWidget(index)

        if typeString == "Open File":
            tempWidget = OpenHeaderPropWidget(index)
            tempWidget.headerPathGiven.connect(self.loadStack)
            if index.parent().data(0) == 'Sample':
                tempWidget.useI0Sig.connect(self.useI0Stack)
                
        elif typeString == "Registration":
            currentWidget = self.tabWidget.currentWidget()
            tempWidget = RegistrationPropWidget(index, 
                model = currentWidget.sliceList.model())
            tempWidget.quickRegSig.connect(self.registerationHandler)
            tempWidget.rejectionSig.connect(self.rejectRegistration)
            tempWidget.assistantSig.connect(self.registrationAssistant)

        elif typeString == "I0 Selection":
            currentWidget = self.tabWidget.currentWidget()
            try:
                Shape = currentWidget.lastLoad['stack'].shape
            except KeyError:
                Shape = (0,0,0)
            tempWidget = SpectrumSelectionWidget(Shape)
            roiUnderlyingModel = self.I0Analyzer.roiList.model()
            tempWidget.selectRoi.setModel(roiUnderlyingModel)
            tempWidget.spectrumSelected.connect(self.setI0Spectrum)
            tempWidget.SaveLineI0Sig.connect(self.saveLineI0Stack)
            tempWidget.viewBoxVlinesig.connect(self.setVlineI0Line)
            tempWidget.winClosed.connect(self.addRemoveVlineI0Line)            
            
        elif typeString == "Compare I0":
            tempWidget = SpectrumComparisonWidget(
                model=self.I0Analyzer.roiList.model())
            tempWidget.dialogRequested.connect(self.SpectrumCalc)

        elif typeString == "Optical Density":
            tempWidget = QtGui.QPushButton("Apply OD")
            tempWidget.clicked.connect(self.opticalDensityFilter)

        elif typeString == "Spectrum Calculator":
            tempWidget = SpectrumComparisonWidget(
                model=self.sampleAnalyzer.roiList.model())
            tempWidget.dialogRequested.connect(self.SpectrumCalc)
            
        elif typeString == "Fit": 
            tempWidget = QtGui.QPushButton("Fit Checked Components")
            tempWidget.clicked.connect(self.mapComponents)

        elif typeString == "Fit Quality Check": 
            tempWidget = QtGui.QPushButton("Fit Quality Check Assistant")
            tempWidget.clicked.connect(self.fitQualityAssistant)

        elif typeString == "RGB": 
            tempWidget = QtGui.QPushButton("RGB Map")
            tempWidget.clicked.connect(self.RGBmap)
            
        elif typeString == "Image - Image Manipulation": 
            tempWidget = ImageManipulationWidget(
                model=self.mapViewer.slideList.model(), mode = 'Image - Image' )
            tempWidget.imageImageSig.connect(self.manipCalculator)

        elif typeString == "Image - Value Manipulation": 
            tempWidget = ImageManipulationWidget(
                model=self.mapViewer.slideList.model(), mode = 'Image - Value' )
            tempWidget.imageValueSig.connect(self.manipCalculator)
                       
        elif typeString == "Scale": 
            tempWidget = setScaleWidget()
            tempWidget.applyScaleSig.connect(self.scaleBarSizeHandler) 
            tempWidget.showHideSig.connect(self.showHideScaleHandler) 

        elif typeString == "Energy Shift": 
            tempWidget = EnergyShiftWidget()
            tempWidget.shiftSig.connect(self.applyEnergyShift) 
            tempWidget.revertSig.connect(self.revertEnergyShift)

        else:
            self.propDock.setWidget(None)
            return  
        self.propDock.setBaseSize(250,10)
        self.propDock.setMaximumWidth(250)        
        self.propDock.setWidget(tempWidget)

    def changeVisibleWidget(self, index):
        if index.parent().data(0) == 'Sample':
            self.tabWidget.setCurrentWidget(self.sampleAnalyzer)
        elif index.parent().data(0) == 'I0':
            self.tabWidget.setCurrentWidget(self.I0Analyzer)
        elif index.parent().data(0) == 'Component Mapping':
            self.tabWidget.setCurrentWidget(self.mapViewer)

    def showEvent(self, event):
        settings = QtCore.QSettings()
        self.move( settings.value("main/pos", QtCore.QPoint(100,100) ) )
        self.resize( settings.value("main/size", QtCore.QSize(1200, 600) ) )

    def closeEvent(self, event):
        settings = QtCore.QSettings()
        settings.setValue("main/pos", self.pos() )
        settings.setValue("main/size", self.size() )

def debug():
    import subprocess
    pathI0 = subprocess.check_output(["locate","-l","1","-r","532_130125101.hdr$"]).strip()
    main.loadStack(pathI0, "I0", False)    
    main.I0Analyzer.addRoi([50,300], [30,20], 'good area for I0')
    # SIMULATE MANUAL SELECTION
    # main.tree.setSelection(QtCore.QRect(20,60,1,1), QtGui.QItemSelectionModel.Select)
    main.setI0Spectrum(0)
    pathSample = subprocess.check_output(["locate","-l","1","-r","532_130125101.hdr$"]).strip()
    main.loadStack(pathSample, "Sample", False)
    main.tabWidget.setCurrentIndex(1)
    main.opticalDensityFilter()
    # main.sampleAnalyzer.addRoi([50,300], [30,20], 'empty space')
    main.sampleAnalyzer.addRoi([10,10], [30,20], 'epoxy')
    main.sampleAnalyzer.addRoi([50,200], [30,20], 'agglomerate')
    main.sampleAnalyzer.addRoi([50,100], [30,20], 'another thing')
    main.mapComponents()
    main.tabWidget.setCurrentIndex(2)
#    main.saveStack()
    
if __name__ == '__main__':
    import sys, os
    app = QtGui.QApplication(sys.argv)
    app.setOrganizationName(ORGANIZATION)
    app.setApplicationName(APPNAME)

    main = StxmMain("{} v{}".format(TITLE, VERSION))
    main.show()
    print(QtCore.QSettings.value)
    
#    debug()
    
    sys.exit(app.exec_())