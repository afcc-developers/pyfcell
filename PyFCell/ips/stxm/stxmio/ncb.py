#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Arash Ash
# License: TBD
# Copyright (c) 2014, TBD
r"""
*************************************************************************
:mod:`PyFCell.ips.stxm.stxmio.ncb` -- Input and Output for *.ncb format
*************************************************************************

.. module:: PyFCell.ips.stxm.stxmio

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""
from __future__ import division
import numpy as np
import pylab as plb
import scipy as sp
import os
import warnings

def ncb_writer(filename, param=None, stack=None, x_axis=None, y_axis=None, x_start=None, x_stop=None, y_start=None, y_stop=None,
    energy=None, path=None, align=None, **kwargs):
    r"""
    
    *.ncb data writer.Writes a scaled stack of images to a binary file. It also writes a *.dat file containing imaging information, energies, and location of *.xim files.
    
    Parameters
    ----------
    filename : string
        path AND filename of the *.ncb file to be written
        e.g. filename = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058.ncb'    
    
    param : dictionary, Default = None
       Dictionary with following keywords: stack, x_axis, y_axis, x_start, x_stop, y_start, y_stop, energy, path, align.
       if None, each key may be also entered individualy.
       
    stack : 3D array.
        Stack data to be written. 

    x_axis : array-like.
        x axis of the image data. 

    y_axis : array-like.
        y axis of the image data.
        
    x_start : float. Default = None
        starting point of the x axis. if None, 0. will be assigned. 

    x_stop : float. Default = None
        stopping point of the x axis with respect to x_start. if None, x axis will be used to calculate this value. 

    y_start : float. Default = None
        starting point of the y axis. if None, 0. will be assigned. 

    y_stop : float. Default = None
        stopping point of the y axis with respect to y_start. if None, y axis will be used to calculate this value. 

    energy : array-like.
        Energy range of the stack. 

    path : string. path of the original *.xim files. 
        
    align : float. Default = None
        Alignment flag. aligned = 1. else 0. 
    
    Examples
    ----------
    >>> fname = '/home/araash/PyFCell/trunk/data/STXM/Registration/47-1.ncb'
    >>> fname2 = '/home/araash/PyFCell/trunk/data/STXM/Registration/47-1_new.ncb'
    >>> par = ncb_reader(filename = fname)
    >>> ncb_writer(fname2, stack = par['stack'], path = filename, energy = par['energy'], x_stop = 49.96, y_stop = 7.96)
    >>> # or:
    >>> ncb_writer(fname2, param = par)
        
    """   
    
    if kwargs: warnings.warn("Unexpected extra keyword arguments found:"+str(kwargs))

#     Write the description file to filename.dat

#   Write param file for *.xim images
    fil,ext = filename.split('.')
    datfile = fil + '.dat'   
    if param == None:
        param = {}
        param['nx'], param['ny'], param['nimage'] = stack.shape
        
        if x_start == None:
            param['x_start'] = 0.
        else:
            param['x_start'] = x_start
        if y_start == None:
            param['y_start'] = 0.
        else:
            param['y_start'] = y_start
        
        if x_axis != None:
            param['x_stop'] = np.abs(np.abs(np.max(x_axis)) - np.abs(np.min(x_axis)))
        else:
            param['x_stop'] = x_stop
            
        if y_axis != None:
            param['y_stop'] = np.abs(np.abs(np.max(y_axis)) - np.abs(np.min(y_axis)))
        else:
            param['y_stop'] = y_stop
            
        factor = np.max([np.abs(np.min(stack)), np.abs(np.max(stack))])
        if factor >= 3e4 or factor < 1e3:
            param['scale'] = 10.**(3-np.uint16(np.log10(factor)))
        else:
            param['scale'] = 1.
            
        param['energy'] = energy
        param['stack'] = stack
        
        path,name = os.path.split(path)
        #Creates sorted list of files with extensions '.xim'
        imlist = [os.path.join(path,f) for f in sorted(os.listdir(path)) if f.endswith('.xim')]
        param['filelist'] = []
        
        if align  == None:
            align = np.ones(len(energy))
            
        for i in range(stack.shape[2]):
            p,n = os.path.split(imlist[i])
            param['filelist'].append(n+'\t'+str('%.2f'%energy[i])+'\t'+str('%.2f'%align[i]))
            
         
#   Writing the filename.dat file    
    with open(datfile, 'w') as df:
        df.write('\t'+str(param['nx'])+'\t'+str(param['ny'])+'\t'+str('%.4f'%param['scale'])+'\n')
        df.write('\t'+str('%.6f'%param['x_start'])+'\t'+str('%.5f'%param['x_stop'])+'\n')
        df.write('\t'+str('%.6f'%param['y_start'])+'\t'+str('%.5f'%param['y_stop'])+'\n')
        df.write('\t'+str(param['nimage'])+'\n')
        for i in range(param['nimage']):
            df.write('\t'+str('%.3f'%param['energy'][i])+'\n')
        for i in range(param['nimage']):
            df.write(param['filelist'][i]+'\n')

#   Writing the stack file to filename.ncb
#   Note: Data is multiplied by scale and is is written as int to reduce the size of the file 
#   Rearranging stack to AXIS2000 array format(z,y,x)   
    param['stack'] = sp.rollaxis(param['stack'],0,2) 
    param['stack'] = np.flipud(param['stack'])
    param['stack'] = sp.rollaxis(param['stack'],2,0)
    
    param['stack'] = sp.uint16(sp.multiply(param['stack'],param['scale']))
    f = open(filename, 'wb')   
    param['stack'].tofile(f)
    f.close()
    
def ncb_reader(filename = None):
    r"""
    
    *.ncb data reader. Returns stack of images and a dictionary with following keywords: stack, x_axis, y_axis, x_start, x_stop, y_start, y_stop, energy, path, align.
       
    
    Parameters
    ----------
    filename : string
        path AND filename of the *.ncb file to read.
        e.g. filename = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058.ncb'    
    
    
    Examples
    ----------
    >>> fname = '/home/araash/PyFCell/trunk/data/STXM/Registration/47-1.ncb'
    >>> par = ncb_reader(filename = fname)
        
    """ 
#    global params
    if filename != None:
        par = {}
#       Reading the *.dat file
        fil,ext = filename.split('.')
        datfile = fil + '.dat'
        
#       Open *.dat file
        params = open(datfile)
        line = params.readline()        
        line = line.lstrip().rstrip().split()
        nx = sp.uint16(line[0]) 
        ny = sp.uint16(line[1])
        scale = sp.float16(line[2])
        par['nx'], par['ny'], par['scale'] = nx, ny, scale
  
        line = params.readline()
        line = line.lstrip().rstrip().split()
        x_s = float(line[0])
        x_f = float(line[1])
        par['x_start'], par['x_stop'] = x_s, x_f

        line = params.readline()
        line = line.lstrip().rstrip().split()  
        y_s = float(line[0])
        y_f = float(line[1])
        par['y_start'], par['y_stop'] = y_s, y_f

        line = params.readline()
        line = line.lstrip().rstrip().split()  
        nimage = sp.uint16(line[0])
        par['nimage'] = nimage
        
        ev = []
        for i in range(nimage):
            line = params.readline()
            line = line.lstrip().rstrip().split()
            ev.append(sp.float16(line[0]))
        par['energy'] = ev
        
        filelist = []
        for i in range(nimage):
            line = params.readline()
            line = line.lstrip().rstrip()
            filelist.append(str(line))
        par['filelist'] = filelist
        
#       Read *.ncb file
        data = np.fromfile(filename,dtype = sp.uint16,count = -1)
        stack = np.divide(np.reshape(data,(nimage,ny,nx)), par['scale'])
        stack = np.rollaxis(stack, 0, 3)
        stack = np.flipud(stack)
        stack = np.rollaxis(stack, 0, 2)
        
        par['stack'] = stack
        par['Dwell'] = None
        par['Dfocus'] = None
        
        par['scale'] = (x_f - x_s) / nx

    return par

#axb_reader(fname = filename)
def main ():
    global data, par
    filename = '/home/araash/PyFCell/trunk/data/STXM/Registration/47-1.ncb'
#    filename2 = '/home/araash/Desktop/PyFCell/trunk/data/STXM/Sample Test/tatyana-f50-i45-c1s/532_130611043/test43.ncb'
#    filename3 = '/home/araash/Desktop/Image Processing/STXM/532_130710056/532_130710056_a000.xim'
    par = ncb_reader(filename = filename)
#    ncb_writer(filename2, stack = par['stack'], path = filename, energy = par['energy'], x_stop = 49.96, y_stop = 7.96)
#    ncb_writer(filename2, param = par)
    
    
    plb.imshow(par['stack'][:,:,0], cmap = 'gray')
    print(par['stack'].shape)
    plb.show()
    return par
    
if __name__ == "__main__":
    main()
#    from v2 import stxmtools

#    as_dict = stxmtools.load_stack('/home/harday/data/STXM/532_130607016/532_130607016.hdr')
#    ncb_writer('test.txt', **as_dict)