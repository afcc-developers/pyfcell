#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Arash Ash
# License: TBD
# Copyright (c) 2014, TBD
r"""
*************************************************************************
:mod:`PyFCell.ips.stxm.stxmio.axb` -- Input and Output for *.axb format
*************************************************************************

.. module:: PyFCell.ips.stxm.stxmio

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""
import xdrlib as xdr
import numpy as np
import warnings

def axb_writer(filename, param=None, data=None, x_axis=None, y_axis=None,
    data_type='2d', data_label='Data', x_label='X Axis', y_label='Y Axis',
    energy=None, dwell=None, **kwargs):
    r"""
    
    *.axb data writer
    
    Parameters
    ----------
    filename : string
        path AND filename of the *.axb file to be written    
    
    param : dictionary, Default = None
       Dictionary with following keywords: data, x_axis, y_axis, data_type, data_label, x_label, y_label.
       if None, each key may be also entered individualy.
       
    data : 2D array.
        Image data to be written. 

    x_axis : array-like, optional. Default = None
        x axis of the image data. if None, the number of pixels in x direction of the image data will be used. 

    y_axis : array-like, optional. Default = None
        y axis of the image data. if None, the number of pixels in y direction of the image data will be used. 
        
    data_type : string. Default = '2d'
        Data type. if not entered, data type will e '2d'. 

    data_label : string. Default = 'Data'
        Data label. if not entered, data label will e 'Data'. 

    x_label : string. Default = 'X Axis'
        X axis label. if not entered, X axis label will e 'X Axis'. 
        
    y_label : string. Default = 'Y Axis'
        Y axis label. if not entered, Y axis label will e 'Y Axis'. 
    
    Examples
    ----------
    >>> infile = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058.axb'
    >>> a = axb_reader(filename=fname)
    >>> outfile = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058_2.axb'
    >>> t = axb_writer(outfile, data=a['data'], x_axis = a['x_axis'], y_axis=a['y_axis'])
    >>> # or:
    >>> t = axb_writer(outfile, param = a)
    
    """  
    if kwargs:
        warnings.warn("Unexpected extra keyword arguments found:"+str(kwargs))
    
    t = xdr.Packer()
    if param == None:
        param = {}
        param['data_type'] = data_type        
        param['y_label'] = y_label + str(' (px)')             
        param['data_label'] = data_label 
        if energy is None:
            energy = 'Not Given'
        if dwell is None:
            dwell = 'Not Given'
        param['x_label'] = x_label+ str(' (px)') + 'E = ' + str(energy) +' eV' + ' dwell = ' + str(dwell) + ' ms'
        
        if data == None:
            warnings.warn('No data supplied!')
            param['data'] = np.zeros((2,2))
        else:
            param['data'] = data
        
        if x_axis == None:
            warnings.warn('No x axis supplied!')
            param['x_axis'] = np.arange(param['data'].shape[0])
        else:
            param['x_axis'] = x_axis

        if y_axis == None:
            warnings.warn('No y axis supplied!')
            param['y_axis'] = np.arange(param['data'].shape[1]) 
        else:
            param['y_axis'] = y_axis

#   Packing the data to filename.axb binary file        
    t.pack_int(len(param['data_type']))
    t.pack_string(param['data_type'])
    t.pack_int(len(param['data_label']))
    t.pack_string(param['data_label'])
    t.pack_int(len(param['x_label']))
    t.pack_string(param['x_label'])
    t.pack_int(len(param['y_label']))
    t.pack_string(param['y_label'])
    nx, ny = param['data'].shape    
    t.pack_uint(nx)
    t.pack_farray(nx,param['x_axis'],t.pack_float)
    t.pack_uint(ny)
    t.pack_farray(ny,param['y_axis'],t.pack_float)
    size = nx*ny
    
#    Rolling axis position and flipping the array so the image shows up correctly in AXIS2000
    param['data'] = np.rollaxis(param['data'], 1)
#    param['data'] = np.flipud(param['data'])
    
    t.pack_farray(size,param['data'].flatten(),t.pack_float)
    f = open(filename, 'wb')   
    f.write(t.get_buffer())
    f.close()
    
    return t


def axb_reader(filename = None):    
    r"""
    
    *.axb data reader. returns a dictionary with following keywords: data, x_axis, y_axis, data_type, data_label, x_label, y_label, energy and dwell.
    
    Parameters
    ----------
    filename : string
        path AND filename of the *.axb file to read   
    
    Examples
    ----------

    >>> fname = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058.axb'
    >>> a = axb_reader(filename=fname)
        
    """ 
    if filename != None:
        f = open(filename, 'rb').read()
        u = xdr.Unpacker(f)
        data = u.get_buffer()
#    else:
#        data = Input.get_buffer()
        
    t = xdr.Unpacker(data)
    t.unpack_int()    
    typ =  eval(repr(t.unpack_string()))
    t.unpack_int()
    tmp = eval(repr(t.unpack_string()))
    t.unpack_int()
    tmpx =  eval(repr(t.unpack_string()))
    t.unpack_int()
    tmpy =  eval(repr(t.unpack_string()))    
#    ny =  eval(repr(t.unpack_uint()))
#    y = np.zeros(ny)
#    y = t.unpack_farray(ny,t.unpack_float)  
    nx =  eval(repr(t.unpack_uint()))
    x = np.zeros(nx)
    x = t.unpack_farray(nx,t.unpack_float)
    
    ny =  eval(repr(t.unpack_uint()))
    y = np.zeros(ny)
    y = t.unpack_farray(ny,t.unpack_float)     
    
    size = nx*ny
    d = t.unpack_farray(size, t.unpack_float)
    d = np.reshape(d, (ny, nx))

#   Flip Image and roll axis: transiion from axis to gui, This is becuase of the different array structure of AXIS(z,y,x) and XFCell(x,y,z)    
    d = np.flipud(d)
    d = np.rollaxis(d, 1)
    
    try:
        Eind = tmpx.index('=')
        eVind = tmpx.index('eV')
        energy = [np.float(tmpx[Eind+1:eVind])]
        dwellind = tmpx.index('dwell')
        msind = tmpx.index('ms')
        dwell = [np.float(tmpx[dwellind+7:msind])]
    except ValueError:
        energy = [0.0]
        dwell = [0.0]
#   flipped x and y axis. This is becuase of the different array structure of AXIS(z,y,x) and XFCell(x,y,z)
    a = {'data_type':typ, 'x_axis': x, 'y_axis':y, 'x_label': tmpx,'y_label': tmpy, 'data':d, 'data_label':tmp, 'energy':energy, 'dwell':dwell}
    t.done()
    return a

#axb_reader(fname = filename)
def main():
    import os
    import pylab as plb
    global a

    infile = '/home/araash/PyFCell/trunk/data/STXM/Sample Test/tatyana-f50-i45-c1s/532_130611043/278od.axb'
#    infile = '/home/araash/Desktop/C.axb'
#    outfile = '/home/araash/PyFCell/trunk/data/STXM/Sample Test/tatyana-f50-i45-c1s/532_130611043/test278.axb'
    outfile = '/home/araash/Desktop/278.axb'
#    infile = '/home/araash/PyFCell/trunk/data/STXM/Sample Test/tatyana-f50-i45-c1s/532_130611043/test.axb'
    a = axb_reader(filename=infile)
    b = {}
#    b = dict(data= a['data'], y_axis = a['y_axis'])
#    b['x_axis']=a['x_axis']
#    b['y_axis']=a['y_axis']
    
#    b['data']=a['data']
#    print( a['data'].shape )
##    t = axb_writer(outfile, param = a)
#    
##    y = axb_reader(filename=outfile)
#     
#    plb.imshow(a['data'], cmap='gray')
#    plb.show()    
#    print( a['data'].shape )

    # remove outfile
#    os.system('rm '+outfile)


if __name__ == "__main__":
    main()



