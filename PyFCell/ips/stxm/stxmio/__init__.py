#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Arash Ash
# License: MIT?
# Copyright (c) 2014, TBD

"""
********************************************************************
:mod:`PyFCell.ips.stxm.stxmio` -- input/output module for STXM Suite
********************************************************************



This module provides the access to opening and saving stxm file formats such as
*.axb, *.ncb, *.xfc and *.hdr

"""
from axb import *
from ncb import *
from xfc import *
from hdr import *