#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Arash Ash, AFCC Development Team
# License: TBD
# Copyright (c) 2014, TBD

r"""
*************************************************************************
:mod:`PyFCell.ips.stxm.stxmio.hdr` -- Input for *.hdr format
*************************************************************************

.. module:: PyFCell.ips.stxm.stxmio

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""
from __future__ import print_function
import json, re, os
import numpy as np

def hdr2json(hdr_path):
    ''' uses regex to convert the syntax of .hdr header files
        produced at AFCC to JSON format, which easily converts
        to a Python dict and thus makes work easy
    '''
    with open(hdr_path) as f:
        as_str = f.read()

    as_str = re.sub(r'\s+', r' ', as_str) # replace any number of space-type characters (\n, etc.) with a single space
    as_str = re.sub(r'([\w_]+)\s?=', r'"\1":', as_str) # ensure all keys have quotes around them, and colon for equal sign
    as_str = re.sub(r'\(([-\d\., ]+)\);', r'[\1]', as_str) # format arrays right by replacing soft braces with square ones
    as_str = re.sub(r'\(\d+, ([^\)]+)\)', r'[\1]', as_str) # flatten those array tuples in the form (<count>, <content>)
    as_str = '{{{}}}'.format(as_str) # wrap the whole thing in curly braces. '{{' is Python formatter syntax for '{'
    as_str = re.sub(';',',',as_str) # get those commas right
    as_str = re.sub(r',(\s*})', r'\1', as_str) # remove any orphan commas left around

    as_dict = json.loads(as_str)
    return as_dict

def load_stack(hdr_path, legacy=True):
    r"""
    
    *.hdr data reader. returns a dictionary with following keywords: stack, 
        path, x_axis, y_axis, x_step, y_step and energy. first hdr -> json -> 
        python dict is optained, and then ndarray of all images is extracted.
    
    Parameters
    ----------
    hdr_path : string
        path AND filename of the *.hdr file to read
        e.g. filename = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058.hdr' 
        
    legacy : Boolean,
        if True the function produces a dictionary with the following enteries:
        
        stack: 3darray - format [x,y,z], stxm data
        x_axis: 1darray, physical x axis of the data
        y_axis: 1darray, physical y axis of the data
        x_step: float, physical resolution in x direction
        y_step: float, physical resolution in y direction
        energy: 1darray, energy points of the stxm data
        path: string, path of the .hdr file
        
        to interface with existing AXB/NCD data writers
        
    Examples
    ----------
    >>> fname = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058.hdr'
    >>> a = load_stack(fn_region)
        
        
    """
    ''' we first obtain hdr -> json -> python dict, and then we obtain
        an ndarray of all images.

        if legacy mode is set, this function instead produces another
        dictionary, with the following entries



        to interface with existing AXB/NCD data writers
    '''
    file_dir = os.path.dirname(hdr_path) 
    sample_id= os.path.basename(hdr_path).split('.')[0]
    meta = hdr2json(hdr_path)

    energy_levels= np.array(meta['ScanDefinition']['StackAxis']['Points'][1:])
    non_sorted = []
    # this list comprehension creates a the image 'data cube' from all the images
    # see: http://docs.scipy.org/doc/numpy/reference/generated/numpy.dstack.html
    if len(meta['ScanDefinition']['Regions']) == 1 :
        stack = np.dstack([np.genfromtxt(img_path).T for img_path in ( # load directly into array
            os.path.join(file_dir, img_fn) for img_fn in ( # obtain path by concatenation
                "{sid}_a{iid:0>3}.{ext}".format(sid=sample_id, iid=img_id, ext="xim") for img_id in (
                    range(len(energy_levels )) ) ) ) ] ) # pad zeros to create filename, and transpose

        for i in range(len(energy_levels)):
            non_sorted.append( (energy_levels[i], stack[:,:,i]) )

#        Sorting stack based on energy levels
        stack = np.dstack([y for (x,y) in sorted(non_sorted, key=lambda tup: tup[0])])
      
    else:
        super_stack = []
        sorted_stack =[]
        for i in range (len(meta['ScanDefinition']['Regions'])):
           super_stack.append(np.dstack([np.genfromtxt(img_path).T for img_path in ( # load directly into array
                            os.path.join(file_dir, img_fn) for img_fn in ( # obtain path by concatenation
                                "{sid}_a{iid:0>3}{region:0>1}.{ext}".format(sid=sample_id, iid=img_id , region = i, ext="xim") for img_id in (
                                    range(len(energy_levels )) ) ) ) ] )  )# pad zeros to create filename, and transpose
        
        for i in range(len(energy_levels)): 
            t = []
            t.append(energy_levels[i])
            for j in range (len(meta['ScanDefinition']['Regions'])):
                t.append(super_stack[j][:,:,i])                
            tup = tuple(t)
            non_sorted.append(tup)
        
        sorted_list = sorted(non_sorted, key=lambda tup: tup[0] )                  
        for j in range (len(meta['ScanDefinition']['Regions'])):
            t = []
            for i in range(len(energy_levels)):
                t.append( sorted_list[i][j+1] )
            sorted_stack.append(np.dstack(t) )
        
        super_stack = sorted_stack
        
    energy_levels = np.sort(energy_levels)
                
        
    if legacy:
        output = {}
        if len(meta['ScanDefinition']['Regions']) == 1 :
            output['stack'] = stack
        else:
            output['stack'] = super_stack               
        output['energy'] = energy_levels
        output['x_axis'] = meta['ScanDefinition']['Regions'][0]['PAxis']['Points'][1:]
        output['y_axis'] = meta['ScanDefinition']['Regions'][0]['QAxis']['Points'][1:]
        output['x_step'] = meta['ImageScan']['SpatialRegions'][0]['XStep']
        output['y_step'] = meta['ImageScan']['SpatialRegions'][0]['YStep']
        output['path'] = hdr_path
        output['region'] = len(meta['ScanDefinition']['Regions'])
        output['Dwell'] = meta['ImageScan']['EnergyRegions'][0]['DwellTime']
        output['Dfocus'] = meta['Defocus']
        return output

    return stack, energy_levels, meta
    
if __name__ == '__main__':
    fn_region = '/home/araash/PyFCell/trunk/data/STXM/Two-Regions/A140319029/A140319029.hdr'
    fn = '/home/araash/Desktop/Image Processing/STXM/Data/A140527059/A140527059.hdr'
    a= load_stack(fn_region) 
#    path, name  = a['path'].split()    
#    print (name)