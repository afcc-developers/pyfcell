#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Arash Ash
# License: TBD
# Copyright (c) 2014, TBD
r"""
*************************************************************************
:mod:`PyFCell.ips.stxm.stxmio.xfc` -- Input and Output for *.xfc format
*************************************************************************

.. module:: PyFCell.ips.stxm.stxmio

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""
import cPickle
import warnings


def xfc_writer(path, data=None, **kwargs):
    r"""
    
    *.xfc data writer
    
    Parameters
    ----------
    path : string
        path AND filename of the *.xfc file to be written
        e.g. filename = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058.xfc'    
       
    data : STXMData instance,
        created by PyFCell.ips.stxm.stxmclass. This data instance contains data, header, rois, spectrums and etc.
    
    Examples
    ----------  
    >>> fname = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058.xfc'
    >>> xfc_writer(fname, STXM_class_instance) 
        
    """  
    if kwargs:
        warnings.warn("Unexpected extra keyword arguments found:"+str(kwargs))
    
    xfc_filename = path[0:-4] + '.xfc'
    with open(xfc_filename, 'wb') as output:
        cPickle.dump(data, output, cPickle.HIGHEST_PROTOCOL)

def xfc_reader(path):    
    r"""
    
    *.xfc data reader. returns a dictionary and a STXMData instance. Dictionary 
        contains following keywords: stack, path, x_axis, y_axis, x_step, y_step and energy. STXMData instance
        contains data, header info, rois, sperctrums and etc.
    
    Parameters
    ----------
    path : string
        path AND filename of the *.xfc file to read
        e.g. filename = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058.xfc'    
    
    Examples
    ----------  
    >>> fname = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058.xfc'
    >>> dic, obj = xfc_reader(fname) 
        
    """
    with open(path, 'rb') as inp:
        obj = cPickle.load(inp)
    output = {}
    output['stack'] = obj.data
    output['path'] = obj.path
    header = obj.header
    output['x_axis'] = header['x_axis']
    output['y_axis'] = header['y_axis']
    output['x_step'] = header['x_step']
    output['y_step'] = header['y_step']
    output['Dwell'] = header['Dwell'] 
    output['Dfocus'] = header['Dfocus']
    output['energy'] = obj.energy  
    
    
    return output, obj

def main():
    pass

if __name__ == "__main__":
    main()



