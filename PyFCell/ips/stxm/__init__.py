# Author: Arash Ash
# License: TBD
# Copyright (c) 2013, AFCC
"""
**************************************************************************
:mod:`PyFCell.ips.stxm` -- STXM module for Image Processing Suite
**************************************************************************


This module provides image processgin algorithms for stxm imaging.


"""

import stxmtools
import stxmclass
import Register
import siftmatching