import os

TEMPLATE = '''<RCC>
<qresource prefix="/icons">
...
</qresource>
</RCC>'''.split('...\n')

CURRENT_DIR = os.path.dirname(__file__)
PNG_FILES = [i for i in os.listdir(CURRENT_DIR) if i.endswith('.png')]

form = '\t<file>{filename}</file>'
for fn in PNG_FILES:
	TEMPLATE.insert( 1, form.format(filename=fn) )

QRC = '\n'.join(TEMPLATE)
QRCSPEC = os.path.join(CURRENT_DIR, 'icons.qrc')
with open(QRCSPEC, 'w') as f:
	f.write(QRC)

# os.system('locate pyrcc4')
PYRCC4 = '/home/araash/PyFCell/trunk/contrib/eman2/EMAN2/Python-2.7-ucs4/bin/pyrcc4'
OUT = '../icons.py'
OUT2 = '../gui/icons.py'

os.system('{generator} "{qrcspec}" -o {outfile}'.format(generator=PYRCC4, qrcspec=QRCSPEC, outfile=OUT))
os.system('{generator} "{qrcspec}" -o {outfile}'.format(generator=PYRCC4, qrcspec=QRCSPEC, outfile=OUT2))

print( 'done!' )