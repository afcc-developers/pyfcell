from __future__ import print_function

from sip import setapi
setapi('QString', 2)
setapi('QVariant', 2)

from PyQt4 import QtCore, QtGui
import sys
import pyqtgraph as QtGraph
import icons
import os
import numpy as np


class OpenHeaderPropWidget(QtGui.QGroupBox):
    headerPathGiven = QtCore.pyqtSignal(str, str, bool)
    useI0Sig = QtCore.pyqtSignal(str)
#    appendStack = QtCore.pyqtSignal(bool)

    def __init__(self, index, parent=None):
        super(OpenHeaderPropWidget, self).__init__("Data File", parent)
        vbox = QtGui.QVBoxLayout(self)
        self.lineEdit = QtGui.QLineEdit()
        self.lineEdit.setReadOnly(True)
        openBtn = QtGui.QPushButton()
        openBtn.setIcon(QtGui.QIcon(':icons/folder'))
        openBtn.clicked.connect(self.getPath)
        self.appendCheck = QtGui.QCheckBox('Append to the Existing Stack')
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.lineEdit)
        hbox.addWidget(openBtn)        
        vbox.addLayout(hbox)
        vbox.addWidget(self.appendCheck)
        self.stage = index.parent().data(0)
        self.mapper = QtGui.QDataWidgetMapper(self)
        self.mapper.setModel(index.model())
        self.mapper.setRootIndex(index.parent())
        self.mapper.addMapping(self.lineEdit, 1)
        self.mapper.setCurrentIndex(index.row())
        if self.stage == 'Sample':
            useI0Btn = QtGui.QPushButton('Use I0 Stack')
            useI0Btn.clicked.connect(self.useI0)
            vbox.addWidget(useI0Btn)
        
    def useI0(self):
        self.useI0Sig.emit(self.stage)

    def getPath(self):
        

        lastPath = QtCore.QSettings().value("PATH/{}".format(self.stage), os.getcwd())

        path = QtGui.QFileDialog.getOpenFileName(
            parent=self,
            caption='',
            directory=lastPath,
            filter="All files(*.*);;HDR Files (*.hdr);;NCB Files (*.ncb);;AXB Files (*.axb);;Tiff Files (*.tif);;XFCell Files (*.xfc)")

        if path:
            if self.appendCheck.checkState() == 2:                
                self.headerPathGiven.emit(path, self.stage, True)
            else:
                self.headerPathGiven.emit(path, self.stage, False)
        
class SaveHeaderPropWidget(QtGui.QGroupBox):
    pass

class RegistrationPropWidget(QtGui.QGroupBox):
    quickRegSig = QtCore.pyqtSignal(str)
    rejectionSig = QtCore.pyqtSignal(str)
    assistantSig = QtCore.pyqtSignal(str)

    def __init__(self, index, model, parent=None):
        super(RegistrationPropWidget, self).__init__("Registration", parent)
        form = QtGui.QFormLayout(self)        
        registerBtn = QtGui.QPushButton("Quick Register")
        rejectBtn = QtGui.QPushButton("Reject")
        registerAssistBtn = QtGui.QPushButton("Registration Assistant")
        registerBtn.clicked.connect(self.Quick)
        rejectBtn.clicked.connect(self.rejected)
        registerAssistBtn.clicked.connect(self.assistant)
        form.addRow(registerBtn)
        form.addRow(rejectBtn)
        form.addRow(registerAssistBtn)
        
        self.stage = index.parent().data(0)
        
    def Quick(self):
        self.quickRegSig.emit(self.stage)
        
    def rejected(self):
        self.rejectionSig.emit(self.stage)
        
    def assistant(self):
        self.assistantSig.emit(self.stage)

class SpectrumSelectionWidget(QtGui.QGroupBox):
    spectrumSelected = QtCore.pyqtSignal(int,bool,str,int,int)
    SaveLineI0Sig = QtCore.pyqtSignal()
    viewBoxVlinesig = QtCore.pyqtSignal(int,int, str)
    winClosed = QtCore.pyqtSignal(bool,str)
    
    def __init__(self, Shape, parent=None):
        super(SpectrumSelectionWidget, self).__init__("Spectrum", parent)
        vbox = QtGui.QVBoxLayout(self)
        self.selectRoi = QtGui.QComboBox()
        vbox.addWidget(self.selectRoi)
        SelectBtn = QtGui.QPushButton("Select Spectrum for I0")
        LineI0Btn = QtGui.QPushButton("Line I0")
        LineI0Save = QtGui.QPushButton("Save Line I0 Stack")
        SelectBtn.clicked.connect(lambda: self.spectrumSelected.emit(self.selectRoi.currentIndex(), False, "No Line", 0, 0))
        LineI0Btn.clicked.connect(self.makeWin)
        LineI0Save.clicked.connect(lambda: self.SaveLineI0Sig.emit())
        vbox.addWidget(SelectBtn)
        vbox.addWidget(LineI0Btn)
        vbox.addWidget(LineI0Save)
        self.x = Shape[0]
        self.y = Shape[1] 
        
    def makeWin(self):
        self.winClosed.emit(True, 'Vertical')
        self.win = QtGui.QDialog()
        self.win.setWindowTitle('Line I0 Window')
        grid = QtGui.QGridLayout()
        directionTxt = QtGui.QLabel('Direction: ')
        self.directionCombo = QtGui.QComboBox()
        self.directionCombo.addItems(['Vertical', 'Horizontal'])
        self.directionCombo.currentIndexChanged.connect(self.dirChanged)
        grid.addWidget(directionTxt, 0, 0)
        grid.addWidget(self.directionCombo, 0, 1)
        self.minTxt = QtGui.QLabel("Min (px)")
        self.minVal = QtGraph.SpinBox(value=0, siPrefix=False, dec=False, step=1, bounds=[0, None], minStep=1)
        grid.addWidget(self.minTxt, 1, 0)
        grid.addWidget(self.minVal, 1, 1)
        self.maxTxt = QtGui.QLabel("Man (px)")
        self.maxVal = QtGraph.SpinBox(value=0, siPrefix=False, dec=False, step=1, bounds=[0, None], minStep=1)
        self.minVal.sigValueChanged.connect(self.ValChanged)
        self.maxVal.sigValueChanged.connect(self.ValChanged)
        grid.addWidget(self.maxTxt, 2, 0)
        grid.addWidget(self.maxVal, 2, 1)        
        okB = QtGui.QPushButton("Ok")
        cancelB = QtGui.QPushButton("Cancel")
        okB.clicked.connect(self.okeyed)
        cancelB.clicked.connect(self.canceled)
        grid.addWidget(okB, 3, 0)
        grid.addWidget(cancelB, 3, 1)
        grid.setColumnMinimumWidth(0, 100)
        grid.setColumnStretch(0,50)
        grid.setColumnMinimumWidth(1, 100)        
        self.win.setLayout(grid)
        self.win.exec_()

    def dirChanged(self, ind):
        minV = np.int(self.minVal.value())
        maxV = np.int(self.maxVal.value())
        if ind == 0:
            direction = 'Vertical'
            prev_dir = 'Horizontal'
        else:
            direction ='Horizontal'
            prev_dir = 'Vertical'
        self.winClosed.emit(False, prev_dir)
        self.winClosed.emit(True, direction)        
        self.viewBoxVlinesig.emit(minV,maxV, direction) 
        
    def okeyed(self):
        direction = self.directionCombo.currentText()
        minV = np.int(self.minVal.value())
        maxV = np.int(self.maxVal.value())
        self.spectrumSelected.emit(self.selectRoi.currentIndex(), True, direction, minV, maxV)
        self.win.close()
        self.winClosed.emit(False, direction)
                
    def canceled(self):
        direction = self.directionCombo.currentText()
        self.win.close()
        self.winClosed.emit(False,direction)
        
    def ValChanged(self):
        direction = self.directionCombo.currentText()
        minV = np.int(self.minVal.value())
        maxV = np.int(self.maxVal.value())
        if minV > maxV:
            minV = maxV
        elif minV < 0:
            minV = 0
        if maxV < minV:
            maxV = minV
        if direction == 'Vertical':
            if maxV > self.x:
                maxV = self.x
            elif minV > self.x:
                minV = self.x
        elif direction == 'Horizontal':
            if maxV > self.y:
                maxV = self.y
            elif minV > self.y:
                minV = self.y
        self.minVal.setValue(minV)   
        self.maxVal.setValue(maxV)                  
        self.viewBoxVlinesig.emit(minV,maxV, direction)        


class EnergyShiftWidget(QtGui.QGroupBox):
    shiftSig = QtCore.pyqtSignal(float)
    revertSig = QtCore.pyqtSignal()
    
    def __init__(self, parent=None):
        super(EnergyShiftWidget, self).__init__(parent)
        vbox = QtGui.QVBoxLayout(self)
        hbox = QtGui.QHBoxLayout()
        vbox.addLayout(hbox)
        self.shiftButton = QtGui.QPushButton("Shift Energy")
        self.shiftButton.clicked.connect(self.getValue)
        self.revertButton = QtGui.QPushButton("Revert")
        self.revertButton.clicked.connect(self.revertShift)
        vbox.addWidget(self.shiftButton)
        vbox.addWidget(self.revertButton)

    def revertShift(self):
        self.revertSig.emit()

    def getValue(self):
        value = QtGui.QInputDialog.getDouble(self, "Enter Value", "Value: ", decimals=6)
        if value[1]:
            if value[0] != 0.:
                self.applyShift(value[0])
            

    def applyShift(self, val = None):
        self.shiftSig.emit(val)
 

class SpectrumComparisonWidget(QtGui.QGroupBox):
    dialogRequested = QtCore.pyqtSignal(int,float,str,bool)
    
    def __init__(self, model, parent=None):
        super(SpectrumComparisonWidget, self).__init__(parent)
        vbox = QtGui.QVBoxLayout(self)
        hbox = QtGui.QHBoxLayout()
        vbox.addLayout(hbox)
        self.spectrumASelector = QtGui.QComboBox()
        self.spectrumASelector.setModel(model)
        hbox.addWidget(self.spectrumASelector)
        self.operationSelector = QtGui.QComboBox()
        self.operationSelector.addItems(['/', '*', '+', '-'])
        hbox.addWidget(self.operationSelector)
        self.spectrumBSelector = QtGui.QComboBox()
        self.spectrumBSelector.setModel(model)
        hbox.addWidget(self.spectrumBSelector)
        self.ComparisonButton = QtGui.QPushButton("Spectrum - Spectrum Manip")
        self.ValueButton = QtGui.QPushButton("Spectrum - Value Manip")
        self.ComparisonButton.clicked.connect(self.displayPlot)
        self.ValueButton.clicked.connect(self.getValue)
        vbox.addWidget(self.ComparisonButton)
        vbox.addWidget(self.ValueButton)

    def getValue(self):
        value = QtGui.QInputDialog.getDouble(self, "Enter Value", "Value: ", decimals=4)
        if value[1]:
            if value[0] != 0.:
                self.displayPlot(value[0])
            

    def displayPlot(self, val = None):
        idxA = self.spectrumASelector.currentIndex()
        idxB = self.spectrumBSelector.currentIndex()
        Op = self.operationSelector.currentText()
        if val:
            self.dialogRequested.emit(idxA, val, Op, True)
        else:
            self.dialogRequested.emit(idxA, idxB, Op, False)

class ImageManipulationWidget(QtGui.QGroupBox):
    imageImageSig = QtCore.pyqtSignal(str,int,int,str)
    imageValueSig = QtCore.pyqtSignal(str,int,float,str)
    
    def __init__(self, model, mode,  parent=None):
        super(ImageManipulationWidget, self).__init__(parent)
        vbox = QtGui.QVBoxLayout(self)
        hbox = QtGui.QHBoxLayout() 
        vbox.addLayout(hbox)
        self.mode = mode
        self.bufferASelector = QtGui.QComboBox()
        self.bufferASelector.setModel(model)
        hbox.addWidget(self.bufferASelector)
        self.operationSelector = QtGui.QComboBox()
        self.operationSelector.addItems(['/', '*', '+', '-'])
        hbox.addWidget(self.operationSelector)
        self.bufferBSelector = QtGui.QComboBox()
        self.bufferBSelector.setModel(model)
        self.value = QtGraph.SpinBox(value=0.0, siPrefix=False, dec=True, step=0.01, bounds=[0, None], minStep=0.001)
        if mode == 'Image - Image':                
            hbox.addWidget(self.bufferBSelector)
        elif mode == 'Image - Value':
            hbox.addWidget(self.value)
        self.showComparisonButton = QtGui.QPushButton("Calculate")
        self.showComparisonButton.clicked.connect(self.displayPlot)
        vbox.addWidget(self.showComparisonButton)

    def displayPlot(self):
        idxA = self.bufferASelector.currentIndex()
        idxB = self.bufferBSelector.currentIndex()
        Op = self.operationSelector.currentText()
        val = self.value.value()
        if self.mode == 'Image - Image':
            self.imageImageSig.emit('Image',idxA, idxB, Op)
        elif self.mode == 'Image - Value':
            self.imageValueSig.emit('Value',idxA, val, Op)

class setScaleWidget(QtGui.QGroupBox):
    applyScaleSig = QtCore.pyqtSignal(float)
    showHideSig = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(setScaleWidget, self).__init__("Set Scale", parent)
        form = QtGui.QFormLayout(self)
        self.scaleFactor = QtGraph.SpinBox(value=10, siPrefix=False, dec=True, step=0.01, bounds=[0, None], minStep=0.01)
        form.addRow("Set Scalebar size (px)", self.scaleFactor)
        scaleBtn = QtGui.QPushButton("Apply Scalebar size")
        showHideBtn = QtGui.QPushButton("Show/Hide Scale")
        showHideBtn.clicked.connect(self.showhide)
        scaleBtn.clicked.connect(self.applyscale)
        form.addRow(scaleBtn)
        form.addRow(showHideBtn)

    def showhide(self):
        self.showHideSig.emit()
        
    def applyscale(self):
        s = self.scaleFactor.value()
        self.applyScaleSig.emit(s)
        

        
if __name__=='__main__':
    app = QtGui.QApplication([])
    
#    model = QtGui.QStandardItemModel()
#    itemNames = "Vertical Horizontal".split()
#    for name in itemNames:
#        item = QtGui.QStandardItem(name)
#        item.setEditable(False)
#        item.setCheckable(True)
#        other = QtGui.QStandardItem("Other")
#        model.appendRow([item, other])
#        model.itemChanged.connect(print)
#
#    view = QtGui.QTableView()
#    view.setShowGrid(False)
#    view.horizontalHeader().hide()
#    view.verticalHeader().hide()
#    view.setModel(model)

    view = SpectrumSelectionWidget()
    view.show()
    
    app.exec_()