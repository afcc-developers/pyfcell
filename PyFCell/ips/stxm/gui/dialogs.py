from PyQt4 import QtGui, QtCore
import pyqtgraph as QtGraph

class PlotDialog(QtGui.QDialog):
    returnPlotSig = QtCore.pyqtSignal(object, str)
    def __init__(self, y = None, x = None, pname = None, y2 = None, pname2 = None, 
                     ppen = 'w', parent=None, p2label = None, xlabel = None, 
                     ylabel = None, returnPlot = False, **kwargs):
        super(PlotDialog, self).__init__(parent)
        vbox = QtGui.QVBoxLayout(self)
        self.plotWidget = QtGraph.PlotWidget()
        self.p1 = self.plotWidget.plotItem
        self.p2 = QtGraph.ViewBox()        
        self.p1.scene().addItem(self.p2)
        self.p2.setGeometry(self.p1.vb.sceneBoundingRect())
        self.p1.getAxis('right').linkToView(self.p2)
        self.p2.setXLink(self.p1)
        self.plotWidget.addLegend()
        self.plotWidget.setLabel('left', text = ylabel)        
        self.plotWidget.setLabel('bottom', text = xlabel)  
        self.returnBtn = QtGui.QPushButton("Return Plot")
        self.returnBtn.clicked.connect(self.returnPlot)
        vbox.addWidget(self.plotWidget)
        if returnPlot:
            vbox.addWidget(self.returnBtn)
        self.p2.enableAutoRange(axis = self.p2.XYAxes)
        self.pname = pname
        self.plotWidget.setMouseEnabled(x = False, y = False)
        self.p1.hideButtons()
        
        if y != None:      
            self.p1.setYRange(min(y), max(y))
            if x == None:
                self.p1.plot(y ,name = pname, pen = ppen, **kwargs)
            else:
                self.p1.setXRange(min(x), max(x))
                self.p1.plot(x, y ,name = pname, pen = ppen, **kwargs)
            
        if y2 != None:
            self.p1.showAxis('right')
            self.plotWidget.setLabel('right', text = p2label)
            self.plot2(y2, x, pname2)
       
    def plot2(self, y2, x = None, name = None):            
        if x == None:
            self.p2.addItem(self.p1.plot(y2, pen = 'b', name = name))
            self.p2.setYRange(min(y2), max(y2))
#            self.p2.setGeometry(self.p1.vb.sceneBoundingRect())                
        else:
            self.p2.addItem(self.p1.plot(x, y2, pen = 'b', name = name))
            self.p2.setYRange(min(y2), max(y2))
            self.p2.setXRange(min(x), max(x))
        self.p2.setGeometry(self.p1.vb.sceneBoundingRect())
#        self.p2.autoRange()
            
    def addplot(self, yToAdd, x = None, pname = None, ppen = 'w', **kwargs):
        if x == None:
            self.p1.plot(yToAdd ,name = pname, pen = ppen, **kwargs)
        else:
            self.p1.setXRange(min(x), max(x))
            self.p1.plot(x, yToAdd ,name = pname, pen = ppen, **kwargs) 
        self.plotWidget.autoRange()
        
    def clear(self):
        self.p1.clear()
        self.p1.legend.items = []

    def returnPlot(self):
        self.returnPlotSig.emit(self.p1, self.pname)
        self.close()
        
if __name__ == '__main__':
    import numpy as np
    import numpy.random as r
    app = QtGui.QApplication([])

    button = QtGui.QPushButton("PlotDialog")
    x1 = np.arange(10,20)
    y_1 = r.randint(10, size = 10)
    y_2 = r.randint(100, size = 10)
    y_3 = r.randint(10, size = 10)
    plot = {'y' : y_1, 'x' : x1, 'pname' : 'random', 'y2' : y_2, 'pname2' : 'More Random', 'ppen' : 'r', 'xlabel': 'x_axis', 'ylabel' : 'y_axis', 'p2label' : 'y2 axis'}
    w = PlotDialog(symbol = 'o', **plot)
    w.exec_()
#    button.clicked.connect(lambda: PlotDialog(symbol = 'o', **plot).exec_() )
#    w = QtGui.QWidget()
#    vbox = QtGui.QVBoxLayout(w)
#    vbox.addWidget(button)
#    w.show()

#    app.exec_()