from __future__ import print_function

from sip import setapi
setapi('QString', 2)
setapi('QVariant', 2)

from PyQt4 import QtCore, QtGui
import pyqtgraph as QtGraph
import numpy as np
import sys
# QtGraph.setConfigOption('background', 'w')
# QtGraph.setConfigOption('foreground', 'k')

import icons
from dialogs import PlotDialog
sys.path.append("../")
import Register
import stxmtools
import scipy as sp

from bisect import bisect
import re, os
from pyqtgraph import GraphicsObject, GraphicsWidgetAnchor, TextItem
import pyqtgraph.functions as fn

ROLE1, ROLE2 = range(32, 34)

class SimpleSlideView(QtGui.QMainWindow):
#class SimpleSlideView(QtGui.QWidget):
    def __init__(self, size = 200, parent=None,**kwargs):
        super(SimpleSlideView, self).__init__(parent)
        self.name = ''
        self.namelist = []
        self.linescanPoints = []
        self.lastLoad = {}
        self.opacity = 0.3
        self.ratioPlotCounter = 0
#        self.graphicsLayout = QtGraph.GraphicsLayoutWidget(border=(100,100,100))
        self.graphicsLayout = QtGraph.GraphicsLayoutWidget()       
        self.setCentralWidget(self.graphicsLayout) 
        
#        self.graphicsLayout.layout = QtGui.QGraphicsGridLayout()
#        self.graphicsLayout.setLayout(QtGui.QGraphicsGridLayout)
        
        
        self.imageItem = QtGraph.ImageItem() 
        self.imageoverlay = QtGraph.ImageItem() 
        
        """ only gradient """
        self.onlygradient = QtGraph.GradientEditorItem()  

        self.gradient = QtGraph.HistogramLUTItem(self.imageItem)

#        self.label = self.graphicsLayout.addLabel(str(self.name),col=1,rowspan=1,colspan=3)
#        self.graphicsLayout.nextRow()

#        self.viewBox = self.graphicsLayout.addViewBox(col=1, rowspan=3,colspan=3) 
        self.viewBox = self.graphicsLayout.addPlot(col=1, rowspan=3,colspan=3)
        
        self.graphicsLayout.nextRow()
        self.histogram = self.graphicsLayout.addPlot(row=4,col=0) 
        
        """ only gradient """        
#        self.graphicsLayout.addItem(self.onlygradient, row = 1, col = 4, rowspan=3)
        self.graphicsLayout.addItem(self.gradient, row = 1, col = 4, rowspan=3)
        
        self.verticalSection = self.graphicsLayout.addPlot(row=1,col=0)        
        self.horizontalSection = self.graphicsLayout.addPlot(row=4,col=1) 
        
        self.verticalSection.invertY()       
        self.viewBox.addItem(self.imageItem)
        self.viewBox.addItem(self.imageoverlay)
        self.viewBox.invertY()
        self.viewBox.setAspectLocked(True)         
        self.maxtextItem = TextItem(text='max =', anchor=(0,0))
        self.maxtextItem.setParentItem(self.imageItem) 
        self.mintextItem = TextItem(text='min =', anchor=(0,0))
        self.mintextItem.setParentItem(self.imageItem)
        font = QtGui.QFont()
        font.setBold(True)
        font.setPointSize(10)
        self.maxtextItem.setFont(font)
        self.mintextItem.setFont(font)        
        self.scaleItem = ScaleBarItem(10, brush='w', pen='w')
        
        self.scaleItemView = self.viewBox.getViewBox()
        self.scaleItem.setParentItem(self.scaleItemView)            
        self.SampleNametextItem = TextItem(text='', anchor=(0,0))
        self.SampleNametextItem.setParentItem(self.viewBox) 
        self.SampleNametextItem.setPos(35,0)
        self.SampleDwelltextItem = TextItem(text='', anchor=(0,-0.75))
        self.SampleDwelltextItem.setParentItem(self.viewBox) 
        self.SampleDwelltextItem.setPos(35,15)
        self.SampleFocustextItem = TextItem(text='', anchor=(0,-1.5))
        self.SampleFocustextItem.setParentItem(self.viewBox) 
        self.SampleFocustextItem.setPos(35,30)
        self.SampleNametextItem.setFont(font)        
        self.DocktoolBar = QtGui.QToolBar("Docks")
        self.slideList = QtGui.QListWidget()
        self.slideList.currentItemChanged.connect(self.changeSlide)
        self.slideList.currentTextChanged.connect(self.changeText)
        dock = QtGui.QDockWidget("Slide List")
        dock.setWidget(self.slideList)
        dock.setFeatures(QtGui.QDockWidget.DockWidgetClosable)
        dock.toggleViewAction().setIcon(QtGui.QIcon(':icons/layers'))
        self.DocktoolBar.addAction(dock.toggleViewAction())
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, dock)        
        self.RatioWidget = QtGraph.PlotWidget()
        self.RatioPlot = self.RatioWidget.plotItem
        dock_ratio = QtGui.QDockWidget("Ratio Plots")
        dock_ratio.setWidget(self.RatioWidget)
        dock_ratio.setFeatures(QtGui.QDockWidget.DockWidgetClosable)
        dock_ratio.toggleViewAction().setIcon(QtGui.QIcon(':icons/chart_curve'))
        self.DocktoolBar.addAction(dock_ratio.toggleViewAction())
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, dock_ratio) 
        dock_ratio.setFixedSize(400,300)        
        self.addToolBar(self.DocktoolBar)
        self.ThreshtoolBar = QtGui.QToolBar("Threshold toolbar")       
        self.addToolBar(self.ThreshtoolBar)
        self.ThresholdAction = self.ThreshtoolBar.addAction(QtGui.QIcon(':icons/threshold_icon'), "Threshold", self.thresholdHandler)
        self.resetRangeAction = self.ThreshtoolBar.addAction(QtGui.QIcon(':icons/arrow_refresh'), "Reset Range", self.resetRangeHandler)
        self.OpacityUpAction = self.ThreshtoolBar.addAction(QtGui.QIcon(':icons/resultset_previous'), "Opacity Up", self.opacityUpHandler)
        self.OpacityDownAction = self.ThreshtoolBar.addAction(QtGui.QIcon(':icons/resultset_next'), "Opacity Down", self.opacityDownHandler)        
        self.RatiotoolBar = QtGui.QToolBar("LineScans toolbar")       
        self.addToolBar(self.RatiotoolBar)
        self.HratioAction = self.RatiotoolBar.addAction(QtGui.QIcon(':icons/horizontal_scan'), "Horizontal Linescan", self.HLinescan)
        self.VratioAction = self.RatiotoolBar.addAction(QtGui.QIcon(':icons/vertical_scan'), "Vertical Linescan", self.VLinescan)
        self.clearRatioAction = self.RatiotoolBar.addAction(QtGui.QIcon(':icons/clear_plot'), "Clear Linescans", self.clearRatioPlot)
        self.SlidetoolBar = QtGui.QToolBar("Slide list toolbar")       
        self.addToolBar(self.SlidetoolBar)
        self.ClearSlidelistAction = self.SlidetoolBar.addAction(QtGui.QIcon(':icons/page_white'), "Clear Slide List", self.clearslidelist) 
        self.deleteSlideAction = self.SlidetoolBar.addAction(QtGui.QIcon(':icons/cross'), "Delete Slide", self.deleteslide) 
        self.imageToolBar = QtGui.QToolBar("Image toolbar")
        self.addToolBar(self.imageToolBar)
        self.showHideAction = self.imageToolBar.addAction(QtGui.QIcon(':icons/information'), "Hide info") 
        self.showHideAction.setCheckable(True)
        self.showHideAction.setChecked(True)
        self.showHideAction.setEnabled(True)
        self.showHideAction.toggled.connect(self.showHide)  
        self.addToolBar(self.imageToolBar) 
        self.backgroundColor = self.imageToolBar.addAction(QtGui.QIcon(':icons/whitebackground'), "White Background", self.whiteBackground)      
        self.imageToolBar.addAction(QtGui.QIcon(':icons/auto_range'), "Auto Range", self.viewBoxAutoRange)  
        self.white = False
        self.viewBox.sigRangeChanged.connect(self.viewChanged) 
        self.color = 'white'
         
        """ Only graqdient"""        
#        self.onlygradient.setOrientation('right')
#        self.onlygradient.loadPreset('grey')
#        self.onlygradient.setFlag(self.onlygradient.ItemStacksBehindParent)
#        self.viewBox.setFlag(self.onlygradient.ItemStacksBehindParent)
#        self.onlygradient.sigGradientChanged.connect(self.gradientChanged)
                 
        self.horizontalPlot = self.horizontalSection.plot([])
        self.verticalPlot = self.verticalSection.plot([])
        self.horizontalSection.setLabel('bottom', 'px')
        self.verticalSection.setLabel('left', 'px')
        self.histogramPlot = self.histogram.plot([])
        self.horizontalSection.setMouseEnabled(x = False, y = False)
        self.horizontalSection.enableAutoRange()
        self.verticalSection.setMouseEnabled(x = False, y = False)
        self.verticalSection.enableAutoRange()
        self.horizontalSection.hideButtons()
        self.verticalSection.hideButtons()
        self.histogram.setMouseEnabled(x = False, y = False)
        self.histogram.hideButtons()       
#        self.graphicsLayout.layout.setRowMaximumHeight(0, size)
#        self.graphicsLayout.layout.setColumnMaximumWidth(1, size+20)
#        
        self.hline = QtGraph.InfiniteLine(angle=0, movable=False)
        self.vline = QtGraph.InfiniteLine(angle=90, movable=False)        
        self.viewBox.addItem(self.hline, ignoreBounds=False)
        self.viewBox.addItem(self.vline, ignoreBounds=False)
        self.imageItem.scene().sigMouseClicked.connect(self.mouseClicked)

        self.hline_linescan = QtGraph.InfiniteLine(angle=0, movable=False, pen = 'g')
        self.hline_linescan2 = QtGraph.InfiniteLine(angle=0, movable=False, pen = 'g')
        self.vline_linescan = QtGraph.InfiniteLine(angle=90, movable=False, pen='r') 
        self.vline_linescan2 = QtGraph.InfiniteLine(angle=90, movable=False, pen='r') 
        self.clickCount = 0
#        self.hided = False


        self.threshold = QtGui.QPushButton('Threshold')
#        self.vbox.addWidget(self.threshold)
        self.hidden = False
        self.regionExist = False
        
    """ Only gradient"""
    def gradientChanged(self):
        if self.imageItem is not None:        
            if self.onlygradient.isLookupTrivial():        
                self.imageItem.setLookupTable(None) #lambda x: x.astype(np.uint8))        
            else:        
                self.imageItem.setLookupTable(self.getLookupTable) ## send function pointer, not the result  
        self.lut = None

    def viewBoxAutoRange(self):
        self.viewBox.autoRange()

    def getLookupTable(self, img=None, n=None, alpha=None):
        if n is None:
            if img.dtype == np.uint8:
                n = 256
            else:
                n = 512
        if self.lut is None:
            self.lut = self.onlygradient.getLookupTable(n, alpha=alpha)
        return self.lut
		 

    def whiteBackground(self):
        if not self.white:     
            self.white = True     
            self.graphicsLayout.setBackground(background = 'w')
            self.RatioWidget.setBackground(background = 'w')
            self.backgroundColor.setIcon(QtGui.QIcon(':icons/blackbackground'))
            self.color = 'black'
            self.changeSlide(self.slideList.currentItem())
            self.horizontalSection.getAxis('left').setPen('k')
            self.horizontalSection.getAxis('bottom').setPen('k')
            self.verticalSection.getAxis('left').setPen('k')
            self.verticalSection.getAxis('bottom').setPen('k')
            self.histogram.getAxis('left').setPen('k')
            self.histogram.getAxis('bottom').setPen('k')
            self.RatioPlot.getAxis('left').setPen('k')
            self.RatioPlot.getAxis('bottom').setPen('k')
            self.horizontalPlot.setPen('k')
            self.verticalPlot.setPen('k')
            self.histogramPlot.setPen('k')
            self.scaleItem.setPen('k', text = self.color)            
#           self.graphicsLayout.setForegroundBrush(fn.mkBrush(255,255,0))
        else:
            self.white = False     
            self.graphicsLayout.setBackground(background = 'default')
            self.RatioWidget.setBackground(background = 'default')
            self.backgroundColor.setIcon(QtGui.QIcon(':icons/whitebackground'))
            self.color = 'white'
            self.changeSlide(self.slideList.currentItem())
            self.horizontalSection.getAxis('left').setPen(QtGraph.mkPen(0.5))
            self.horizontalSection.getAxis('bottom').setPen(QtGraph.mkPen(0.5))
            self.verticalSection.getAxis('left').setPen(QtGraph.mkPen(0.5))
            self.verticalSection.getAxis('bottom').setPen(QtGraph.mkPen(0.5))
            self.histogram.getAxis('left').setPen(QtGraph.mkPen(0.5))
            self.histogram.getAxis('bottom').setPen(QtGraph.mkPen(0.5))
            self.RatioPlot.getAxis('left').setPen(QtGraph.mkPen(0.5))
            self.RatioPlot.getAxis('bottom').setPen(QtGraph.mkPen(0.5))
            self.horizontalPlot.setPen(QtGraph.mkPen(0.5))
            self.verticalPlot.setPen(QtGraph.mkPen(0.5))
            self.histogramPlot.setPen('w')
            self.scaleItem.setPen('w')
                 
    def showHide(self, show):
        if not show:     
            self.mintextItem.hide()
            self.maxtextItem.hide()
            self.SampleDwelltextItem.hide()
            self.SampleFocustextItem.hide()
            self.SampleNametextItem.hide()
        if show:          
            self.mintextItem.show()
            self.maxtextItem.show()
            self.SampleDwelltextItem.show()
            self.SampleFocustextItem.show()
            self.SampleNametextItem.show()
          
    def viewChanged(self):
        w = self.viewBox.rect().width()
        h = self.viewBox.rect().height()
        self.scaleItem.anchor((1,1), (1,1), offset = (-0.8*w,-0.1*h))
        try:   
            self.mintextItem.setPos( self.imageItem.image.shape[0], 0)  
            self.maxtextItem.setPos( self.imageItem.image.shape[0], 0.9*self.imageItem.image.shape[1]) 
        except AttributeError:
            self.mintextItem.setPos( 8*w/9, 0)  
            self.maxtextItem.setPos( 8*w/9, 8*h/9)  

    def showHideInfo(self):
        self.scaleItem.showHide()       
    
    def setScale(self, scale = None, dwell = None, dfocus = None):
        """
        set scale and infor for sample. scale should be provided in um
        """
        self.scale = scale
        self.dwell = dwell
        self.dfocus = dfocus
        try:
            self.scaleItem.text.setText(str(np.int(self.scaleItem.size*1000*self.scale))+' nm')
        except TypeError:
            self.scaleItem.text.setText('No Scale')

    def testlist(self):

        for i in range(self.slideList.count()):
            print (self.slideList.item(i).name)
#            print (self.slideList.item(i).data(QtCore.Qt.UserRole))

    def HLinescan(self):
        self.addLinescanHandler(direction='Horizontal')

    def VLinescan(self):
        self.addLinescanHandler(direction='Vertical')

    def addLinescanHandler(self, direction):
                
        if len(self.linescanPoints) == 2:
            self.ratioPlotCounter += 1
            point1 = self.linescanPoints[0][1]
            point2 = self.linescanPoints[1][1]
            if direction == 'Horizontal':
                self.RatioPlot.plot(np.mean(self.imageItem.image[:, min(point1,point2):max(point1,point2)], axis=1), pen =self.ratioPlotCounter-1 )
            elif direction =='Vertical':
                self.RatioPlot.plot(np.mean(self.imageItem.image[min(point1,point2):max(point1,point2), :], axis=0) , pen =self.ratioPlotCounter-1)
        else:
            QtGui.QMessageBox.warning(self, 'Warning!', 'Two Points Should be Selected for Linescan Calculations')

    def clearRatioPlot(self):
        self.RatioPlot.clear()
        self.ratioPlotCounter = 0

    def resetRangeHandler(self):
        hist = self.imageItem.getHistogram()
        self.region.setRegion([np.min(hist[0]), np.max(hist[0])])
    
    def opacityUpHandler(self):
        if self.opacity < 1.:
            self.opacity += 0.1
            self.overlay()
            
    def opacityDownHandler(self):
        if self.opacity > 0.:
            self.opacity -= 0.1
            self.overlay()
        
    def thresholdHandler(self):  
        name = self.name + '_masked'        
#        if name in self.namelist:
#            #could be btter
#            name = name + '0'
        self.maskImage = sp.ndimage.morphology.binary_fill_holes(self.maskImage)
        self.addSlide(self.maskImage, name, unit = 'Value')

    def overlay(self, n=512):
        image = self.imageItem.image
        self.maskImage = np.zeros(image.shape)
        for ind, val in np.ndenumerate(image):
            if val >= self.minHist and val <= self.maxHist:
                self.maskImage[ind] = 1.  

        tt = np.zeros((n,3))
        tt[:,0] = 0
        tt[:,1] = 0
        tt[:,2] = np.round(np.linspace(0,255,n))
        red = QtGraph.makeARGB(self.maskImage,lut = tt,levels = [np.min(self.maskImage), np.max(self.maskImage)])
        self.imageoverlay.setImage(red[0], opacity=self.opacity)

    def mouseClicked(self, evt):
        if evt.button() == 4:
            if not self.hidden:
                self.viewBox.removeItem(self.hline)
                self.viewBox.removeItem(self.vline)
                self.hidden = True
            elif self.hidden:
                self.viewBox.addItem(self.hline, ignoreBounds=False)
                self.viewBox.addItem(self.vline, ignoreBounds=False)
                self.hidden = False
        
        if evt.button() == 1:
            if self.imageItem.sceneBoundingRect().contains(evt.scenePos()):
                mouseCord = self.imageItem.getViewBox().mapSceneToView(evt.scenePos())                
                if self.clickCount == 0:
                    self.viewBox.addItem(self.hline_linescan, ignoreBounds=False)
                    self.viewBox.addItem(self.vline_linescan, ignoreBounds=False)
                    self.hline_linescan.setPos(mouseCord.y())
                    self.vline_linescan.setPos(mouseCord.x()) 
                    self.linescanPoints.append([int(mouseCord.x()), int(mouseCord.y())])
                if self.clickCount == 1:
                    self.viewBox.addItem(self.hline_linescan2, ignoreBounds=False)
                    self.viewBox.addItem(self.vline_linescan2, ignoreBounds=False)  
                    self.hline_linescan2.setPos(mouseCord.y())
                    self.vline_linescan2.setPos(mouseCord.x()) 
                    self.linescanPoints.append([int(mouseCord.x()), int(mouseCord.y())])
                if self.clickCount == 2:
                    self.viewBox.removeItem(self.hline_linescan)
                    self.viewBox.removeItem(self.vline_linescan)
                    self.viewBox.removeItem(self.hline_linescan2)
                    self.viewBox.removeItem(self.vline_linescan2)
                    self.linescanPoints = []
                self.clickCount = (self.clickCount + 1)%3

                
    def mouseMoved(self, evt):
        if self.imageItem.sceneBoundingRect().contains(evt):
            mouseCord = self.imageItem.getViewBox().mapSceneToView(evt)
            self.hline.setPos(mouseCord.y())
            self.vline.setPos(mouseCord.x())            
            max_x, max_y = self.imageItem.image.shape
            self.Xindex = max(min(int(mouseCord.x()), max_x-1), 0)
            self.Yindex = max(min(int(mouseCord.y()), max_y-1), 0)
            self.updatePlots()

    def getLinescanPoints(self):
        return self.linescanPoints
            
    def updatePlots(self):
        self.horizontalPlot.setData(self.imageItem.image[:, self.Yindex])
        self.verticalPlot.setData(self.imageItem.image[self.Xindex, :], range(self.imageItem.image.shape[1]))

    def clearslidelist(self):
        self.slideList.clear()

    def deleteslide(self):
        ind = self.slideList.currentIndex().row()
        self.slideList.takeItem(ind)

    def changeText(self, text):
        item = self.slideList.currentItem()
        item.name = text
        self.slideList.setCurrentItem(item)


    def changeSlide(self, item):
        if not item:
            return
        ndarray = item.data(QtCore.Qt.UserRole)
        self.imageItem.setImage(ndarray)
        
        self.levels = [self.imageItem.image.min(), self.imageItem.image.max()]
        self.gradient.setLevels(*self.levels)
        
        self.imageoverlay.setImage(ndarray, opacity = 0.)
        self.imageItem.scene().sigMouseMoved.connect(self.mouseMoved)
#        self.label.setText(item.name)  
        self.name = item.name      
        self.HistogramHandler()
        self.setTextinfo(name=item.name)
        self.maxtextItem.setHtml("<span style='color: %s'>max = %0.2f</span>" %(self.color, ndarray.max()))
        self.mintextItem.setHtml("<span style='color: %s'>min = %0.2f</span>" %(self.color, ndarray.min()))
        self.horizontalSection.setLabel('left', item.unit)
        self.verticalSection.setLabel('bottom', item.unit)

    def setTextinfo(self, name, dwell='', focus=''):
        self.SampleNametextItem.setHtml("<span style='color: %s'>%s</span>" %(self.color, name))
        try:
            self.SampleDwelltextItem.setHtml("<span style='color: %s'>%s</span>" %(self.color, 'DWell = '+str(self.dwell)))
            self.SampleFocustextItem.setHtml("<span style='color: %s'>%s</span>" %(self.color, 'Dfocus = '+str(self.dfocus)))
        except:
            pass
        
    def addSlide(self, ndarray, label, unit = 'od'):
        item = QtGui.QListWidgetItem(label, self.slideList)
        item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable)
        item.name = label
        item.unit = unit
        self.namelist.append(label)
        item.setData(QtCore.Qt.UserRole, ndarray)

    def HistogramHandler(self):        
        hist = self.imageItem.getHistogram()
        if self.regionExist:
            self.histogram.removeItem(self.region)
        self.minHist, self.maxHist = hist[0], hist[1]
        self.histogramPlot.setData(hist[0], hist[1])
        self.region = QtGraph.LinearRegionItem()
        self.region.setZValue(10)
        self.histogram.addItem(self.region)
        self.region.setRegion([np.min(hist[0]), np.max(hist[0])])
        self.region.sigRegionChanged.connect(self.updateRegion)  
        self.regionExist = True

    def updateRegion(self):
        self.region.setZValue(10)
        self.minHist, self.maxHist = self.region.getRegion()
        self.overlay()

class FitQualityCheckWindow(QtGui.QMainWindow):
    def __init__(self, ndarray1, energy, name1, ndarray2, name2, parent=None):
        super(FitQualityCheckWindow, self).__init__(parent)
        self.graphicsLayout = QtGraph.GraphicsLayoutWidget()
        self.setCentralWidget(self.graphicsLayout)  
        self.imageItem = QtGraph.ImageItem() 
        self.viewBox = self.graphicsLayout.addViewBox(row=1, col=0, rowspan=1, colspan=3)
        self.viewBox.addItem(self.imageItem)
        self.viewBox.invertY()
        self.viewBox.setAspectLocked(True)
        self.histogram = QtGraph.HistogramLUTItem(self.imageItem)
        self.graphicsLayout.addItem(self.histogram, row=1, col=3, rowspan=1, colspan=1) 
        self.plot = self.graphicsLayout.addPlot(row=2, col=0, rowspan=1, colspan=4)  
        self.plot.disableAutoRange()  
        try:  
            self.plot.enableAutoRange(y=True)  
        except TypeError:        
            self.plot.enableAutoRange(True)
        self.plot.setLabel('bottom', 'Energy Level','eV')
        self.plot.setLabel('left', 'OD')
        self.depthLine = QtGraph.InfiniteLine()  
        self.depthLineText = TextItem(text='', anchor=(0,1))  
        self.depthLine.sigPositionChanged.connect(self.depthLineMoved)
        self.plot.addItem(self.depthLine)
        self.dockToolBar = QtGui.QToolBar("Dock Visibility")
        self.addToolBar(self.dockToolBar)
        self.sliceList = QtGui.QListWidget()
        self.dockToolBar.addAction(QtGui.QIcon(':icons/auto_range'), "Auto Range", self.viewBoxAutoRange)
        self.showMarkersAction = self.dockToolBar.addAction(QtGui.QIcon(':icons/chart_line'), "Show Data Points")
        self.showMarkersAction.setCheckable(True)
        self.showMarkersAction.toggled.connect(self.showPoints)    
        self.energytextItem = QtGraph.TextItem(html="<span style='color: #FFF'></span>",
                                         anchor=(0,0), border=None, fill=None)
        self.energytextItem.setParentItem(self.viewBox) 
        self.textFont = QtGui.QFont()             
        self.textFont.setPointSize(10)  
        self.textFont.setBold(True)  
        self.energytextItem.setFont(self.textFont)   
        self.color = 'white'      
        self.fontSize = 9
        self.viewBox.sigRangeChanged.connect(self.viewChanged) 
        self.imageItem.scene().sigMouseMoved.connect(self.mouseMoved)
        self.hline = QtGraph.InfiniteLine(angle = 0)
        self.vline = QtGraph.InfiniteLine()
        self.viewBox.addItem(self.hline)
        self.viewBox.addItem(self.vline)
        # red for stack 1
        self.spectnum = 0
        self.name1 = name1
        self.curve1 = self.plot.plot(pen=self.spectnum)
        self.curvePointExists = False
        #yellow for stack 2
        self.spectnum = self.spectnum + 1        
        self.curve2 = self.plot.plot(pen=self.spectnum)
        self.name2 = name2
        self.spectrumSet = []
        self.legend = QtGraph.LegendItem(offset = [80,10])
        self.legend.setParentItem(self.plot)
        self.legend.addItem(self.curve1, self.name1)
        self.legend.addItem(self.curve2, self.name2)
        self.setStack(ndarray1, energy, name1, ndarray2, name2)
        self.PointShow = False
        self.toggleset1 = self.dockToolBar.addAction(QtGui.QIcon(':icons/1'), "Data set 1")
        self.toggleset1.setCheckable(True)        
        self.toggleset1.setChecked(True)
        self.toggleset2 = self.dockToolBar.addAction(QtGui.QIcon(':icons/2'), "Data set 2")
        self.toggleset2.setCheckable(True)
        self.toggleset2.setChecked(False)
        self.toggleset1.toggled.connect(self.toggleData1)
        self.toggleset2.toggled.connect(self.toggleData2)
        self.specshowhide = self.dockToolBar.addAction(QtGui.QIcon(':icons/chart_curve'), "Show/Hide Spectrums")
        self.specshowhide.setCheckable(True)
        self.specshowhide.setChecked(True)
        self.specshowhide.toggled.connect(self.showhideSpec)
        
    def showhideSpec(self, checkState):
#        show/hide spectrums added on plot
        if not checkState:
            self.legend.hide()
            for i in range(len(self.spectrumSet)):
                self.plot.removeItem(self.spectrumSet[i][0])
                self.legend.removeItem(self.spectrumSet[i][1])
        else:
            self.legend.show()
            for i in range(len(self.spectrumSet)):
                self.plot.addItem(self.spectrumSet[i][0])
                self.legend.addItem(self.spectrumSet[i][0], self.spectrumSet[i][1])
        
    def toggleData1(self, checkState):
#        swiches the view btw dataset1 and dataset2
        if checkState:        
            self.setVisibleImage(self.rawImage1, self.rawImage2)
            self.toggleset2.setChecked(False) 
        else:
            self.setVisibleImage(self.rawImage2, self.rawImage1)
            self.toggleset2.setChecked(True)             
            
    def toggleData2(self, checkState):        
#        swiches the view btw dataset1 and dataset2
        if checkState:        
            self.setVisibleImage(self.rawImage2, self.rawImage1)
            self.toggleset1.setChecked(False) 
        else:
            self.setVisibleImage(self.rawImage1, self.rawImage2)
            self.toggleset1.setChecked(True) 
        
    def mouseMoved(self, evt):
#        Monitors mouse hovering event and returns x, y index wrt image item
        if self.imageItem.sceneBoundingRect().contains(evt):
            mouseCord = self.imageItem.getViewBox().mapSceneToView(evt)
            self.hline.setPos(mouseCord.y())
            self.vline.setPos(mouseCord.x())            
            max_x, max_y = self.imageItem.image.shape
            self.Xindex = max(min(int(mouseCord.x()), max_x-1), 0)
            self.Yindex = max(min(int(mouseCord.y()), max_y-1), 0)
            self.updatePlots()

    def addSpectrum(self,spectrum, name=None):
        self.spectnum += 1
        curve = self.plot.plot(pen  = self.spectnum)
        self.spectrumSet.append(tuple([curve, name]))
        curve.setData(self.depthAxis, spectrum)
        self.legend.addItem(curve, name)

    def updatePlots(self):  
        yData = self.rawImage1[self.Xindex, self.Yindex, :]
        yData2 = self.rawImage2[self.Xindex, self.Yindex, :]
        if self.PointShow:
            self.curve1.setData(x=self.depthAxis, y=yData, symbol = 'o', symbolSize = 3, symbolPen = 0, symbolBrush = 0)
            self.curve2.setData(x=self.depthAxis, y=yData2, symbol = 'o', symbolSize = 3, symbolPen = 1, symbolBrush = 1)
        else:
            self.curve1.setData(self.depthAxis, yData, symbol = None) 
            self.curve2.setData(self.depthAxis, yData2, symbol = None)
        if not self.curvePointExists:
            self.curvePoint = QtGraph.CurvePoint(self.curve1) 
            self.plot.addItem(self.curvePoint)
            self.depthLineText.setParentItem(self.curvePoint)            
            self.curvePointExists = True              
        self.depthLineMoved()
                            
    def viewBoxAutoRange(self):
        self.viewBox.autoRange() 
                                   
    def viewChanged(self):
        w = self.viewBox.rect().width()
        self.energytextItem.setPos( w/2.-50, -3)       
                  
    def showPoints(self, checkState):
        self.PointShow = checkState
        for i in range(len(self.spectrumSet)):
            color = i+2
            curve = self.spectrumSet[i][0]
            if checkState:
                curve.setData(x=curve.xData, y=curve.yData, symbol = 'o', symbolSize = 3, symbolPen = color, symbolBrush = color)
                self.curve1.setData(x=self.curve1.xData, y=self.curve1.yData, symbol = 'o', symbolSize = 3, symbolPen = 0, symbolBrush = 0)
                self.curve2.setData(x=self.curve2.xData, y=self.curve2.yData, symbol = 'o', symbolSize = 3, symbolPen = 1, symbolBrush = 1)
            else:
                curve.setData(x=curve.xData, y=curve.yData, symbol = None)
                self.curve1.setData(x=self.curve1.xData, y=self.curve1.yData, symbol = None)
                self.curve2.setData(x=self.curve2.xData, y=self.curve2.yData, symbol = None)

    def addDockWidget(self, widget, label, area, icon=None):
        dock = QtGui.QDockWidget(label)
        dock.setWidget(widget)
        dock.setFeatures(QtGui.QDockWidget.DockWidgetClosable)
        if icon:
            dock.toggleViewAction().setIcon(icon)
        self.dockToolBar.addAction(dock.toggleViewAction())
        super(FitQualityCheckWindow, self).addDockWidget(area, dock)
	
    def setStack(self, ndarray1, depthAxis, name1, ndarray2=None, name2=None):
        self.rawImage1 = ndarray1
        self.rawImage2 = ndarray2
        self.filteredImage = None
        self.depthAxis = depthAxis
        self.setVisibleImage(self.rawImage1, self.rawImage2)
        top, bottom = depthAxis.min(), depthAxis.max()
        self.depthLine.maxRange = (top, bottom)
        self.plot.setXRange(top, bottom)
        self.depthLine.setValue(top)
        self.depthLine.setMovable(True)

    def setVisibleImage(self, stack, stack2):
        self.visibleImage = stack
        self.hiddenImage = stack2
        self.plot.setLabel('left', 'Intensity')   
        self.levels = [self.visibleImage.min(), self.visibleImage.max()]
        self.histogram.setLevels(*self.levels)
        self.depthLineMoved()

    def depthLineMoved(self):        
        x, y = self.depthLine.p
        index = bisect(self.depthAxis, x)-1
        self.sliceList.setCurrentRow(index)
        if self.curvePointExists:
            try:
            	self.curvePoint.setPos(float(index)/(len(self.depthAxis)-1))   		                         
            except AttributeError:
            	print ('Error in CurvePoint!')  
                    
        self.depthLineText.setHtml("<span style='color: %s; font-size: %ipt;'>%s</span>" %(self.color, self.fontSize, str(self.depthAxis[index]))) 
        self.energytextItem.setHtml("<span style='color: %s'>E = %0.3f</span>" %(self.color, self.depthAxis[index]))              
        currentSlice = self.visibleImage[:,:,index]
        self.levels = [currentSlice.min(), currentSlice.max()] 
        self.histogram.setLevels(*self.levels)      
        self.imageItem.setImage(currentSlice, levels=self.histogram.getLevels())


class ScaleBarItem(GraphicsObject, GraphicsWidgetAnchor):
    """
    Displays a rectangular bar to indicate the relative scale of objects on the view.
    """
    def __init__(self, size, width=5, brush=None, pen=None, suffix='nm'):
        GraphicsObject.__init__(self)
        GraphicsWidgetAnchor.__init__(self)
        self.setFlag(self.ItemHasNoContents)
        self.setAcceptedMouseButtons(QtCore.Qt.NoButton)
        
        if brush is None:
            brush = QtGraph.getConfigOption('foreground')
        self.brush = fn.mkBrush(brush)
        self.pen = fn.mkPen(pen)
        self._width = width
        self.size = size
        self.suffix = suffix
        
        self.bar = QtGui.QGraphicsRectItem()
        self.bar.setPen(self.pen)
        self.bar.setBrush(self.brush)
        self.bar.setParentItem(self)
        
        self.text = TextItem(text=fn.siFormat(size, suffix=self.suffix), anchor=(0.5,1))
        self.text.setParentItem(self)
        self.hide = False

    def setPen(self, pen, text = 'white'):
        self.brush = fn.mkBrush(pen)
        self.pen = fn.mkPen(pen)
        self.bar.setPen(self.pen)
        self.bar.setBrush(self.brush)
        self.text.setHtml("<span style='color: %s'>%s</span>" %(text, fn.siFormat(self.size, suffix=self.suffix)) )
#        self.updateBar()
        
    def setSize(self, s):
        self.size = s
#        self.text.setText(str(self.size*s))
        self.updateBar()
        
    def parentChanged(self):
        view = self.parentItem()
        if view is None:
            return
        view.sigRangeChanged.connect(self.updateBar)
        self.updateBar()

    def showHide(self):
        if not self.hide:
            self.hide = True
            self.bar.hide()
            self.text.hide()
        else:
            self.hide = False
            self.bar.show()
            self.text.show()
        
    def updateBar(self):
        view = self.parentItem()
        if view is None:
            return
        p1 = view.mapFromViewToItem(self, QtCore.QPointF(0,0))
        p2 = view.mapFromViewToItem(self, QtCore.QPointF(self.size,0))
        w = -(p2-p1).x()
        self.bar.setRect(QtCore.QRectF(-w, 0, w, self._width))
        self.text.setPos(-w/2., 0)

    def boundingRect(self):
        return QtCore.QRectF()
        
class StackAnalyzer(QtGui.QMainWindow):
	SaveSpectrumPressed = QtCore.pyqtSignal()
	addRoiPressed = QtCore.pyqtSignal(str, int)
	deletedSliceSig = QtCore.pyqtSignal(int) 
	rotateSig = QtCore.pyqtSignal()     
 
	def __init__(self, parent=None):
		super(StackAnalyzer, self).__init__(parent)
		self.lastLoad = {} 
		self.eLineTextlist = []
		self.eLineTextvalue = []
		self.eLineCurveItem = []  
		self.eLineItem = []
		self.graphicsLayout = QtGraph.GraphicsLayoutWidget()
		# self.graphicsToolBar = QtGui.QToolBar("Graphics")
		# self.addToolBar(self.graphicsToolBar)
		# self.lightsAction = self.graphicsToolBar.addAction(QtGui.QIcon(':icons/contrast'), "Lights On/Off")
		# self.lightsAction.setCheckable(True)
		self.setCentralWidget(self.graphicsLayout)  
		self.imageItem = QtGraph.ImageItem() 
#		self.viewBox = self.graphicsLayout.addViewBox(row=1, col=0, rowspan=1, colspan=3)
		self.viewBox = self.graphicsLayout.addPlot(row=1, col=0, rowspan=1, colspan=3)
		self.viewBox.addItem(self.imageItem)
		self.viewBox.invertY()

		self.viewBox.setAspectLocked(True)
		self.viewBoxVlineA = QtGraph.InfiniteLine() 
		self.viewBoxVlineB = QtGraph.InfiniteLine() 
		self.viewBoxHlineA = QtGraph.InfiniteLine(angle=0) 
		self.viewBoxHlineB = QtGraph.InfiniteLine(angle=0) 
  
		self.histogram = QtGraph.HistogramLUTItem(self.imageItem)
		self.graphicsLayout.addItem(self.histogram, row=1, col=3, rowspan=1, colspan=1)
		self.energytextItem = QtGraph.TextItem(html="<span style='color: #FFF'></span>",
                                         anchor=(0,0), border=None, fill=None)
		self.energytextItem.setParentItem(self.viewBox) 
		self.maxtextItem = TextItem(text='max =', anchor=(0,0))
		self.maxtextItem.setParentItem(self.imageItem) 
		self.mintextItem = TextItem(text='min =', anchor=(0,0))
		self.mintextItem.setParentItem(self.imageItem)
		self.textFont = QtGui.QFont()             
		self.textFont.setPointSize(10)  
		self.textFont.setBold(True)  
		self.energytextItem.setFont(self.textFont)  
		self.mintextItem.setFont(self.textFont)
		self.maxtextItem.setFont(self.textFont)    
		self.plot = self.graphicsLayout.addPlot(row=2, col=0, rowspan=1, colspan=4)  
		self.plot.disableAutoRange()
		try:  
                  self.plot.enableAutoRange(y=True)  
		except TypeError:        
                  self.plot.enableAutoRange(True)
		self.plot.setLabel('bottom', 'Energy Level','eV')
		self.plot.setLabel('left', 'Intensity')
		self.depthLine = QtGraph.InfiniteLine()  
		self.depthLineText = TextItem(text='', anchor=(0,1))  
		self.depthLine.sigPositionChanged.connect(self.depthLineMoved)
		self.plot.addItem(self.depthLine)
		self.dockToolBar = QtGui.QToolBar("Dock Visibility")
		self.addToolBar(self.dockToolBar)
		self.sliceList = QtGui.QListWidget()
		self.sliceList.currentTextChanged.connect(self.manualSliceSelection)
		self.addDockWidget(self.sliceList, "Slice List", QtCore.Qt.RightDockWidgetArea, QtGui.QIcon(':icons/layers'))
		self.roiList = QtGui.QListWidget()
		self.addDockWidget(self.roiList, "ROI List", QtCore.Qt.RightDockWidgetArea, QtGui.QIcon(':icons/shape_group'))
		self.filterAction = self.dockToolBar.addAction(QtGui.QIcon(':icons/contrast'), "Filter On/Off")
		self.filterAction.setCheckable(True)
		self.filterAction.setEnabled(False)
		self.filterAction.toggled.connect(self.setVisibleImage)
		self.updateEveryFrameSetting = self.dockToolBar.addAction(QtGui.QIcon(':icons/lightning'), "Update Levels Every Frame")
		self.updateEveryFrameSetting.setCheckable(True)
		self.updateEveryFrameSetting.setChecked(True)
#		self.updateEveryFrameSetting.setEnabled(False)
		self.updateEveryFrameSetting.toggled.connect(self.autoHistogramLevels)
		self.flipTimer = QtCore.QTimer()
		self.flipTimer.setInterval(10)
		self.flipTimer.timeout.connect(lambda: self.sliceList.setCurrentRow(self.sliceList.currentRow()+1 % self.sliceList.count()))
		self.flipThroughStackAction = self.dockToolBar.addAction(QtGui.QIcon(':icons/control_play_blue'), "Stack Movie")
		self.dockToolBar.addAction(QtGui.QIcon(':icons/auto_range'), "Auto Range", self.viewBoxAutoRange)
		self.flipThroughStackAction.setCheckable(True)
		self.flipThroughStackAction.setChecked(False)
		self.flipThroughStackAction.setEnabled(False)
		self.flipThroughStackAction.toggled.connect(lambda checkState: self.flipTimer.start() if checkState else self.flipTimer.stop())
		self.roiToolBar = QtGui.QToolBar("ROI toolbar")
		self.roiToolBar.setEnabled(False)
		self.roiToolBar.addAction(QtGui.QIcon(':icons/shape_square_add'), "Add ROI", self.addRoi)
		self.roiToolBar.addAction(QtGui.QIcon(':icons/shape_square_delete'), "Delete ROI", self.deleteRoi)
		self.colorbtn = QtGraph.ColorButton()
		self.colorbtn.sigColorChanged.connect(self.roiColorHandler)  
		self.roiToolBar.addAction(QtGui.QIcon(':icons/colors'), "ROI Color", self.ROIColor)
		self.spectrumToolBar = QtGui.QToolBar("Spectrum toolbar")
		self.spectrumToolBar.addAction(QtGui.QIcon(':icons/curve_load'), "Load Spectrum", self.loadSpectrum)
		self.spectrumToolBar.addAction(QtGui.QIcon(':icons/curve_save'), "Save Spectrum", self.saveSpectrumPressed)  
  		self.showMarkersAction = self.spectrumToolBar.addAction(QtGui.QIcon(':icons/chart_line'), "Show Data Points")
		self.showMarkersAction.setCheckable(True)
		self.showMarkersAction.toggled.connect(self.showPoints)    
		self.slidelistToolBar = QtGui.QToolBar("Slice toolbar")
		self.slidelistToolBar.addAction(QtGui.QIcon(':icons/cross'), "Delete Slice", self.deleteslice) 
		self.addToolBar(self.roiToolBar)
		self.addToolBar(self.spectrumToolBar)
  		self.addToolBar(self.slidelistToolBar)
		self.name = None   
		self.imageToolBar = QtGui.QToolBar("Image toolbar")
		self.imageToolBar.addAction(QtGui.QIcon(':icons/shape_rotate_clockwise'), "Rotate Clockwise", self.rotation)
		self.imageToolBar.addAction(QtGui.QIcon(':icons/shape_rotate_anticlockwise'), "Rotate Anti-Clockwise", self.antiRotation)
		self.imageToolBar.addAction(QtGui.QIcon(':icons/shape_flip_vertical'), "Flip Vertical", self.flipVertical)
		self.imageToolBar.addAction(QtGui.QIcon(':icons/shape_flip_horizontal'), "Flip Horizontal", self.flipHorizontal)
		self.showHideInfo = self.imageToolBar.addAction(QtGui.QIcon(':icons/information'), "Hide info") 
		self.showHideInfo.setCheckable(True)
		self.showHideInfo.setChecked(True)
		self.showHideInfo.setEnabled(True)
		self.showHideInfo.toggled.connect(self.showHide)  
		self.addToolBar(self.imageToolBar) 
		self.backgroundColor = self.imageToolBar.addAction(QtGui.QIcon(':icons/whitebackground'), "White Background", self.whiteBackground)      
        	self.white = False
        	self.color = 'white'
         
		self.reportToolBar = QtGui.QToolBar("Report toolbar")
		self.reportToolBar.addAction(QtGui.QIcon(':icons/addenergyline'), "Add Energy Line", self.depthLinePeak) 
		self.reportToolBar.addAction(QtGui.QIcon(':icons/page_white'), "Clear Energy Line", self.depthLineClear) 
		self.reportToolBar.addAction(QtGui.QIcon(':icons/font'), "Change Font Size", self.changeFontSize) 
		self.fontSize = 9
		self.addToolBar(self.reportToolBar)
		# self.label = self.graphicsLayout.addLabel("")
		# self.imageItem.hoverEvent = lambda event: self.label.setText("{x:.0f}, {y:.0f}".format(x=event.pos().x(), y=event.pos().y()), color='g')
		
		# fns related to poor man's peakTool feature
		self.plot.mouseDoubleClickEvent = self.peakTool
		self.peakToolState = 0
		self.viewBox.sigRangeChanged.connect(self.viewChanged) 

	def ROIColor(self):
        	self.colorbtn.click()

	def roiColorHandler(self):
        	self.roiColor = self.colorbtn.color()
		for item in self.roiList.selectedItems():
			roi = item.data(33)  
			curve = item.data(34)
			color	= self.roiColor	   
			curve.setData(x=curve.xData, y=curve.yData, pen = color)
			try:   
    				roi.setPen(color)
			except AttributeError:        
    				pass
			item.setData(QtCore.Qt.DecorationRole, color)
			item.color = color   

	def viewBoxInfLineAddRemove(self, add = True, direction = 'Horizontal'): 
        	if direction == 'Horizontal':
                 lineA = self.viewBoxHlineA 
                 lineB = self.viewBoxHlineB
        	elif direction == 'Vertical':            
                 lineA = self.viewBoxVlineA 
                 lineB = self.viewBoxVlineB
        	if add:
                 self.viewBox.addItem(lineA)
                 self.viewBox.addItem(lineB)
        	else:
                 self.viewBox.removeItem(lineA)
                 self.viewBox.removeItem(lineB)
                 self.viewBoxInfLine([0,0], direction)
                 
	def viewBoxInfLine(self, val, direction):
        	if direction == 'Horizontal':
                 lineA = self.viewBoxHlineA 
                 lineB = self.viewBoxHlineB
        	elif direction == 'Vertical':            
                 lineA = self.viewBoxVlineA 
                 lineB = self.viewBoxVlineB
        	lineA.setValue(val[0])
        	lineB.setValue(val[1])
                 
	def viewBoxAutoRange(self):
        	self.viewBox.autoRange() 
                         
	def whiteBackground(self):
        	if not self.white:     
                 self.white = True     
                 self.graphicsLayout.setBackground(background = 'w')
                 self.backgroundColor.setIcon(QtGui.QIcon(':icons/blackbackground'))
                 self.color = 'black'
                 self.depthLine.setPen('k')
                 self.plot.getAxis('left').setPen('k')
                 self.plot.getAxis('bottom').setPen('k')
#                 self.graphicsLayout.setForegroundBrush(fn.mkBrush(255,255,0))
        	else:
                 self.white = False     
                 self.graphicsLayout.setBackground(background = 'default')
                 self.backgroundColor.setIcon(QtGui.QIcon(':icons/whitebackground'))
                 self.color = 'white'
                 self.depthLine.setPen('y')
                 self.plot.getAxis('left').setPen(QtGraph.mkPen(0.5))
                 self.plot.getAxis('bottom').setPen(QtGraph.mkPen(0.5))
        	self.depthLineMoved()
        	self.depthLinePeak(updateColor = True)
                
	def showHide(self, show):
		if not show:     
        		self.energytextItem.hide()
        		self.mintextItem.hide()
        		self.maxtextItem.hide()
		if show:          
        		self.energytextItem.show()
        		self.mintextItem.show()
        		self.maxtextItem.show()
          
	def viewChanged(self):
		w = self.viewBox.rect().width()
		h = self.viewBox.rect().height()
		self.energytextItem.setPos( w/2 -20, -3) 
		try:      
        		self.mintextItem.setPos( self.imageItem.image.shape[0], 0)  
        		self.maxtextItem.setPos( self.imageItem.image.shape[0], 0.9*self.imageItem.image.shape[1]) 
		except AttributeError:
        		self.mintextItem.setPos( 8*w/9, 0)  
        		self.maxtextItem.setPos( 8*w/9, 8*h/9)           

	def spectrumInterpolation(self, xData, yData):
#		for row in range(self.roiList.count()):
#			item = self.roiList.item(row)
#			label = item.data(0)
#			curve = item.data(34)
#			if len(curve.xData) > len(energy):
#        			x, y = stxmtools.spectra_interp(curve.xData, curve.yData, energy)
#        			self.loadSpectrum(fromfile = False, label = label+'-interp-', xData=x, yData=y)   
		x, y = stxmtools.spectra_interp(xData, yData, self.lastLoad['energy'])
		return x, y  
#		self.loadSpectrum(fromfile = False, label = label+'-interp-', xData=x, yData=y)  

	def flipVertical(self):
        	self.visibleImage = np.fliplr(self.visibleImage)
        	self.setStack(self.visibleImage, self.depthAxis, self.stage, clear = False)
        	self.rotateSig.emit()         

	def flipHorizontal(self):
        	self.visibleImage = np.flipud(self.visibleImage)
        	self.setStack(self.visibleImage, self.depthAxis, self.stage, clear = False)
        	self.rotateSig.emit() 
         
	def rotation(self):
#		self.visibleImage = sp.ndimage.interpolation.rotate(self.visibleImage, 90, axes=(1,0), order = 0)
		rot = []
		for i in range(self.visibleImage.shape[2]): 
			rot.append(np.rot90(self.visibleImage[:,:,i]))
		self.visibleImage = np.dstack(rot)
		self.setStack(self.visibleImage, self.depthAxis, self.stage, clear = False)
        	self.rotateSig.emit() 
         
	def antiRotation(self):
#		self.visibleImage = sp.ndimage.interpolation.rotate(self.visibleImage, -90, axes=(1,0))
		rot = []
		for i in range(self.visibleImage.shape[2]): 
			rot.append(np.rot90(self.visibleImage[:,:,i], k = 3))
		self.visibleImage = np.dstack(rot)
		self.setStack(self.visibleImage, self.depthAxis, self.stage, clear = False)
        	self.rotateSig.emit() 
         
	def setName(self, name):
		self.name = name     

	def deleteslice(self):		       
		ind = self.sliceList.currentIndex().row()
		self.deletedSliceSig.emit(ind)  
		self.visibleImage = np.delete(self.visibleImage, ind, axis = 2) 
		self.rawImage = self.visibleImage
		self.depthAxis = np.delete(self.depthAxis, ind) 
		self.sliceList.takeItem(ind)
		self.setStack(self.visibleImage, self.depthAxis, self.stage, clear = False)  


	def changeFontSize(self):
        	if self.fontSize < 17:     
    			self.fontSize += 4
        	else:
    			self.fontSize = 9             
        	for i in range(len(self.eLineTextlist)):
    			self.eLineTextlist[i].setHtml("<span style='color: %s; font-size : %ipt;'>%s</span>" %(self.color, self.fontSize, self.eLineTextvalue[i])) 
        	self.depthLineMoved()
         
	def depthLineClear(self):
#		self.viewBox.autoRange()
		self.eLineTextlist = [] 
		self.eLineTextvalue = []
		for i in range(len(self.eLineItem)): 
        		self.plot.removeItem(self.eLineItem[i])  
        		self.plot.removeItem(self.eLineCurveItem[i])
          
          
	def depthLinePeak(self, updateColor = False):
		if updateColor:
        		for i in range(len(self.eLineTextlist)):
    				self.eLineTextlist[i].setHtml("<span style='color: %s; font-size: %ipt;'>%s</span>" %(self.color, self.fontSize, self.eLineTextvalue[i]))       
    				if self.white:
        				self.eLineItem[i].setPen('k')
    				else:
        				self.eLineItem[i].setPen('y')
		else:            
        		eLine = QtGraph.InfiniteLine()  
        		x = self.depthLine.value()
        		eLine.setValue(x)
        		index = bisect(self.depthAxis, x)-1
        		eLineText = TextItem(text='', anchor=(0,1))        
        		eLineText.setHtml("<span style='color: %s; font-size: %ipt;'>%s</span>" %(self.color, self.fontSize, str(self.depthAxis[index])+' eV'))
        		self.eLineTextlist.append(eLineText)  
        		self.eLineTextvalue.append(str(self.depthAxis[index])+' eV')
        		try:
    				item = self.roiList.item(self.roiCounter)
    				curve = item.data(34)
    				ecurvePoint = QtGraph.CurvePoint(curve)
    				self.plot.addItem(ecurvePoint) 
    				ecurvePoint.setPos(float(index)/(len(self.depthAxis)-1))
    				eLineText.setParentItem(ecurvePoint)  
    				self.plot.addItem(eLine) 
    				self.eLineCurveItem.append(ecurvePoint)		                         
    				self.eLineItem.append(eLine)
        		except AttributeError:
    				QtGui.QMessageBox.information(self,'warning','Roi or spectrum should be added') 
    
	def peakTool(self, event):
		''' dx, dy calculator '''
		point = self.plot.getViewBox().mapSceneToView(event.scenePos())
		if self.peakToolState == 0:
			self.vLineA = self.plot.addLine(x=point.x(), pen=QtGui.QColor('gray'))
			self.hLineA =self.plot.addLine(y=point.y(), pen=QtGui.QColor('gray'))
		if self.peakToolState == 1:
			self.vLineB = self.plot.addLine(x=point.x(), pen=QtGui.QColor('gray'))
			self.hLineB =self.plot.addLine(y=point.y(), pen=QtGui.QColor('gray'))
			xA = self.vLineA.value()
			xB = self.vLineB.value()
			yA = self.hLineA.value()
			yB = self.hLineB.value()
			self.dXLabel = QtGraph.TextItem("dx = {:.2f}".format(abs(xB-xA)))
			self.dXLabel.setPos(min(xA,xB), max(yA,yB))
			self.dYLabel = QtGraph.TextItem("dy = {:.2f}".format(abs(yB-yA)))
			self.dYLabel.setPos(max(xA,xB), min(yA,yB))
			self.plot.addItem(self.dXLabel)
			self.plot.addItem(self.dYLabel)
		if self.peakToolState == 2:
			self.plot.removeItem(self.vLineA)
			self.plot.removeItem(self.vLineB)
			self.plot.removeItem(self.hLineA)
			self.plot.removeItem(self.hLineB)
			self.plot.removeItem(self.dXLabel)
			self.plot.removeItem(self.dYLabel)

		self.peakToolState = (self.peakToolState+1)%3

	def addRoi(self, pos=[0,0], size=None, label=None, load = False):
		''' New Roles
		32 = QtCore.Qt.UserRole, the first usable role for app-specific purposes
		33 = Reference to QtGraph RoiItem
		34 = Reference to QtGraph CurveItem
		'''
#		Bounding the roi to the view
		if not load:              
                 	rect =self.imageItem.viewRect()          
                 	pos = [rect.x(), rect.y()]
                 	size = [rect.width(), rect.height()] 
                 	if pos[0] < 0:
        			size[0] = size[0] - np.abs(pos[0])
        			pos[0] = 0                  
                 	if pos[1] < 0:                   
        			size[1] = size[1] - np.abs(pos[1])
        			pos[1] = 0
                 	if pos[0] + size[0] > self.imageItem.image.shape[0]:
        			size[0] = self.imageItem.image.shape[0] - pos[0]
                 	if pos[1] + size[1] > self.imageItem.image.shape[1]:                   
        			size[1] = self.imageItem.image.shape[1] - pos[1] 
  
		roiNum = self.nextRoi()
		name = label if label else self.name + "-{:0>3}".format(roiNum) if self.name else "-{:0>3}".format(roiNum)
		color = QtGraph.intColor(roiNum)
		width, height = size if size else self.imageItem.image.shape
		item = QtGui.QListWidgetItem(name, self.roiList)
		item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsUserCheckable)
		item.setCheckState(2)
		item.setData(QtCore.Qt.DecorationRole, color)
		item.color = color  

		roi = QtGraph.RectROI(pos, [width, height], pen=color)
		roi.sigRegionChanged.connect(self.roiMoved)
		roi.item = item
		self.viewBox.addItem(roi)
  
		item.setData(33, roi)
		yData = self.roiSliceMeanArray(roi)  
		curve = self.plot.plot(pen=color)
		if self.showMarkersAction.isChecked():
			curve.setData(self.depthAxis, yData, symbol = 'o', symbolSize = 3, symbolPen = color, symbolBrush = color)
		else:
			curve.setData(self.depthAxis, yData, symbol = None)      
		item.setData(34, curve)

		self.curvePoint = QtGraph.CurvePoint(curve)
		self.plot.addItem(self.curvePoint)
        	self.depthLineText.setParentItem(self.curvePoint)
          
	def deleteRoi(self):
		for item in self.roiList.selectedItems():
			roi = item.data(33)
			if roi: self.viewBox.removeItem(roi)
			curve = item.data(34)
			if curve: self.plot.removeItem(curve)
			row = self.roiList.row(item)
			self.roiList.takeItem(row)
			self.roiCounter -= 1
#			print(self.roiCounter)

	def nextRoi(self):     
		try:
			self.roiCounter += 1
		except AttributeError:
			self.roiCounter = 0
#		print(self.roiCounter)   
		return self.roiCounter

	def saveSpectrumPressed(self):
		self.SaveSpectrumPressed.emit()
			 
	def saveSpectrum(self, comment):       
		if self.roiList.count() == 0:
			QtGui.QMessageBox.information(self, "Warning", "Desired Spectrums Should be Selected") 
			return                    
		lastpath = QtCore.QSettings().value("PATH/{}".format(self.stage), os.getcwd())
		path = QtGui.QFileDialog.getExistingDirectory(
			parent=self,
			caption='',
			directory=lastpath)
		if not path:
			return

		for label, (xData,yData) in self.checkedSpectrums():
        		with open(path + '/' + str(label) + '.txt', 'w') as f:
        			comments = [comment[0],comment[1],comment[2]]
        			f.write(''.join("# {}\n".format(entry) for entry in comments))
        			for x,y in zip(xData, yData):
        				f.write("\t{:.3f}\t\t{:.8f}\n".format(x,y))

	def loadSpectrum(self, fromfile = True, label = None, xData = None, yData = None):
		if fromfile: 
        		try:      
        			lastpath = QtCore.QSettings().value("PATH/{}".format(self.stage), os.getcwd()) 
        		except:
        			lastpath = None
        		path = QtGui.QFileDialog.getOpenFileName(
        			parent=self,
        			caption='',
        			directory=lastpath,
        			filter="Spectrum Files (*.txt)")        		
        		if not path:
        			return
           
        		xData, yData, comments = stxmtools.load_spectrum(path)

		roiNum = self.nextRoi()
		try:
        		name = comments['fname']  
		except:          
        		name = "#{:0>3}-From File-".format(roiNum)
		if not fromfile:
        		name = label 
          
		try:  
                 if len(xData) > len(self.lastLoad['energy']):
				xData, yData = self.spectrumInterpolation(xData, yData)
				name = name+'-interp-'   
		except KeyError:
        		self.depthAxis = xData
        		top, bottom = xData.min(), xData.max()
        		self.depthLine.maxRange = (top, bottom)
        		self.plot.setXRange(top, bottom)
          		self.roiToolBar.setEnabled(True)
            
		color = QtGraph.intColor(roiNum)
		item = QtGui.QListWidgetItem(name, self.roiList)
		item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsUserCheckable )
		item.setData(QtCore.Qt.DecorationRole, color)
		item.setCheckState(2)
		item.color = color  
		curve = self.plot.plot(pen=color)
        	curve.setData(xData, yData)        		              
		item.setData(34, curve)

		self.curvePoint = QtGraph.CurvePoint(curve)
		self.plot.addItem(self.curvePoint)
        	self.depthLineText.setParentItem(self.curvePoint)
                  
	def showPoints(self, checkState):
         for rowNumber in range(self.roiList.count()):
            item = self.roiList.item(rowNumber)
            try: 
                color = item.color
            except:                    
                color = QtGraph.intColor(rowNumber)
            curve = item.data(34)
            if checkState:
                curve.setData(x=curve.xData, y=curve.yData, symbol = 'o', symbolSize = 3, symbolPen = color, symbolBrush = color)
            else:
                curve.setData(x=curve.xData, y=curve.yData, symbol = None)

	def roiMoved(self, roi):
		''' hackish way of keeping it in bounds. could be improved by being
		aware of which specific boundary is being pushed
		'''
		if not self.imageItem.sceneBoundingRect().contains(roi.sceneBoundingRect()):
			roi.setState( roi.preMoveState )

		yData = self.roiSliceMeanArray(roi)
		item = roi.item
		curve = item.data(34)
		curve.setData(self.depthAxis, yData)
		self.roiList.setItemSelected(item, True)
		roi.preMoveState = roi.getState()

	def roiSliceMeanArray(self, roi):
		region = roi.getArrayRegion(data=self.visibleImage, img=self.imageItem, axes=(0,1))
		return [slc.mean() for slc in region.T]

	def addDockWidget(self, widget, label, area, icon=None):
		dock = QtGui.QDockWidget(label)
		dock.setWidget(widget)
		dock.setFeatures(QtGui.QDockWidget.DockWidgetClosable)
		if icon:
			dock.toggleViewAction().setIcon(icon)
		self.dockToolBar.addAction(dock.toggleViewAction())
		super(StackAnalyzer, self).addDockWidget(area, dock)
	
	def setStack(self, ndarray, depthAxis, stage, clear = True):
		if clear:     
        		self.roiCounter = -1
        		self.reset()
          		self.sliceList.clear()
          		self.sliceList.addItems(map(str,depthAxis))
		self.rawImage = ndarray
		self.filteredImage = None
		self.depthAxis = depthAxis
		self.setVisibleImage()
		top, bottom = depthAxis.min(), depthAxis.max()
		self.depthLine.maxRange = (top, bottom)
		self.plot.setXRange(top, bottom)
		self.depthLine.setValue(top)
		self.depthLine.setMovable(True)
		self.roiToolBar.setEnabled(True)
		self.filterAction.setEnabled(False)
		self.filterAction.setChecked(False)
		self.updateEveryFrameSetting.setEnabled(True)
		self.flipThroughStackAction.setEnabled(True)
		self.stage = stage 
#		print(self.roiCounter)

	def setVisibleImage(self, filterOn=False):
		if filterOn:
			self.visibleImage = self.filteredImage
			self.plot.setLabel('left', 'Optical Density')
		else:
			self.visibleImage = self.rawImage
			self.plot.setLabel('left', 'Intensity')
   
		self.levels = [self.visibleImage.min(), self.visibleImage.max()]
		self.histogram.setLevels(*self.levels)
		self.depthLineMoved()
		for row in range(self.roiList.count()):
			item = self.roiList.item(row)
			roi = item.data(33)
			if roi:
				self.roiMoved(roi)
		
	def applyFilter(self, filterFunction=lambda x: x):
		''' submit a function to transform the image array

		the function must take a single ndarray, and return
		another one with the same dimensions

		if the filter function in mind is too complex, look
		at `lambda` or `functools.partial` to pre-parametrize
		it before calling this method. for example, applying
		an empty filter simply resets the image
		'''
		self.filteredImage = filterFunction(self.rawImage.copy())
		assert(self.filteredImage.shape == self.rawImage.shape)
		self.filterAction.setEnabled(True)
		self.filterAction.setChecked(True)

	def autoHistogramLevels(self, autoChecked):
		if autoChecked:
			self.histogram.autoHistogramRange()
		else:
			self.histogram.setLevels(*self.levels)
		self.depthLineMoved()

	def depthLineMoved(self):
		''' this method is too important for such a specific name

		it is currently being called whenever the chosen depth is changed 
		'''    
		x, y = self.depthLine.p
		index = bisect(self.depthAxis, x)-1
		self.sliceList.setCurrentRow(index)
		try:
        		self.curvePoint.setPos(float(index)/(len(self.depthAxis)-1))   		                         
		except AttributeError:
        		pass  
#		try:  
#        		item = self.roiList.item(self.roiCounter)
#        		ycurve = item.data(34).yData     
#		except AttributeError:
#        		ycurve = np.zeros(self.depthAxis.shape) 
#		if index < 3:      
#                 self.depthLineText.anchor(0,0)
#		else:
#                 self.depthLineText.setPos(ycurve[index], 0.)
                    
		self.depthLineText.setHtml("<span style='color: %s; font-size: %ipt;'>%s</span>" %(self.color, self.fontSize, str(self.depthAxis[index]))) 
		self.energytextItem.setHtml("<span style='color: %s'>E = %0.3f</span>" %(self.color, self.depthAxis[index]))              
		try:            
			currentSlice = self.visibleImage[:,:,index]
		except IndexError:   
			currentSlice = self.visibleImage[:,:]

		self.maxtextItem.setHtml("<span style='color: %s'>max = %0.2f</span>" %(self.color, currentSlice.max()))
		self.mintextItem.setHtml("<span style='color: %s'>min = %0.2f</span>" %(self.color, currentSlice.min()))
		if self.updateEveryFrameSetting.isChecked():      
			self.histogram.setLevels(currentSlice.min(), currentSlice.max())
       
		self.imageItem.setImage(currentSlice, levels=self.histogram.getLevels())

	def manualSliceSelection(self, string):
		if not string:
			return
		value = float(string)
		self.depthLine.setValue(value)

	def checkedSpectrums(self):
		for row in range(self.roiList.count()):
			item = self.roiList.item(row)
			if item.checkState() != 2:
				continue
			label = item.data(0)
			curve = item.data(34)
			yield label, (curve.xData, curve.yData)
   
	def checkedRois(self):
           output = {}
           for rowNumber in range(self.roiList.count()):
               try:
                   item = self.roiList.item(rowNumber)
                   if item.checkState() != QtCore.Qt.Checked:
                       continue
                   label = item.data(0)
                   roi = item.data(33)
    
                   x, y = np.round(roi.pos().x()), np.round(roi.pos().y())
                   w, h = np.round(roi.size().x()), np.round(roi.size().y())
                   output[label] = [(x,y), (w,h)]
               except AttributeError:
                   pass
           return output

	def reset(self):
		for row in range(self.roiList.count()):
			self.roiList.setCurrentRow(row,
				QtGui.QItemSelectionModel.SelectCurrent)
			self.deleteRoi()

class RegistrationAssistantWindow(QtGui.QMainWindow):
    returnStack = QtCore.pyqtSignal(str)
    def __init__(self, ndarray, energy, stage, parent = None):
        super(RegistrationAssistantWindow, self).__init__(parent)        
        self.stage = stage        
        self.graphicsLayout = QtGraph.GraphicsLayoutWidget()
        self.setCentralWidget(self.graphicsLayout)
        self.viewBoxStack = self.graphicsLayout.addViewBox(row=0, col=0, rowspan=1, colspan=3)

#        scale = ScaleBarItem(10, brush = 'r', pen = 'r', suffix ='nm')
#        scale.setParentItem(self.viewBoxStack)
#        scale.anchor((1,1), (1,1), offset = (-50,-50))

        self.imageItemStack = QtGraph.ImageItem()
        self.viewBoxStack.addItem(self.imageItemStack)

        self.viewBoxStack.invertY()
        self.viewBoxStack.setAspectLocked(True)
        self.histogramStack = QtGraph.HistogramLUTItem(self.imageItemStack)
        self.graphicsLayout.addItem(self.histogramStack, row=0, col=3, rowspan=1, colspan=1)
        self.viewBoxReg = self.graphicsLayout.addViewBox(row=1, col=0, rowspan=1, colspan=3)
        self.imageItemReg = QtGraph.ImageItem()
        self.viewBoxReg.addItem(self.imageItemReg)
        self.histogramReg = QtGraph.HistogramLUTItem(self.imageItemReg)
        self.viewBoxReg.invertY()
        self.viewBoxReg.setAspectLocked(True)
        self.graphicsLayout.addItem(self.histogramReg, row=1,col=3,rowspan=1,colspan=1)
        self.dockToolBar = QtGui.QToolBar("Dock Visibility")
        self.addToolBar(self.dockToolBar)        
        self.sliceList = QtGui.QListWidget()
        self.sliceList.currentTextChanged.connect(self.manualSliceSelection)
        self.addDockWidget(self.sliceList, "Slice List", QtCore.Qt.RightDockWidgetArea, QtGui.QIcon(':icons/layers'))        
        self.regDock = RegistrationDockWidget(model = self.sliceList.model(), stage = self.stage)
        self.addDockWidget(self.regDock, "Properties", QtCore.Qt.LeftDockWidgetArea, QtGui.QIcon(':icons/calculator'))          
        plot = {'xlabel': 'Slice #', 'ylabel' : 'Pixels'}
        self.plotWidget = PlotDialog(**plot)        
        self.addDockWidget(self.plotWidget, "Plots", QtCore.Qt.LeftDockWidgetArea, QtGui.QIcon(':icons/chart_curve'), (400,300))
#        self.addDockWidget(self.roiList, "ROI List", QtCore.Qt.RightDockWidgetArea, QtGui.QIcon(':icons/shape_group'))  
        self.regDock.registrationMethod.connect(self.RegisterationHandler)
        self.regDock.rejection.connect(self.RejectRegistration)
        self.regDock.useRegistered.connect(self.useRegisteredStack)
        self.regDock.returnStackSig.connect(self.returnStackHandler)
        self.stackTimer = QtCore.QTimer()
        self.stackTimer.setInterval(10)
        self.stackTimer.timeout.connect(lambda: self.sliceList.setCurrentRow(self.sliceList.currentRow()+1 % self.sliceList.count()))
        self.flipThroughStackAction = self.dockToolBar.addAction(QtGui.QIcon(':icons/control_play_blue'), "Stack Movie")
        self.flipThroughStackAction.setCheckable(True)
        self.flipThroughStackAction.setChecked(False)
        self.flipThroughStackAction.toggled.connect(lambda checkState: self.stackTimer.start() if checkState else self.stackTimer.stop())
        self.regTimer = QtCore.QTimer()
        self.regTimer.setInterval(10)
        self.index = 0
        self.regTimer.timeout.connect(self.MovieFlipper)
        self.flipThroughRegAction = self.dockToolBar.addAction(QtGui.QIcon(':icons/control_repeat_blue'), "Registered Movie")
        self.flipThroughRegAction.setCheckable(True)
        self.flipThroughRegAction.setChecked(False)
        self.flipThroughRegAction.toggled.connect(lambda checkState: self.regTimer.start() if checkState else self.regTimer.stop())
        self.slidelistToolBar = QtGui.QToolBar("Slice toolbar")
        self.slidelistToolBar.addAction(QtGui.QIcon(':icons/cross'), "Delete Slice", self.deleteSlice) 
        self.slidelistToolBar.addAction(QtGui.QIcon(':icons/arrow_refresh'), "Refresh Stack", self.refreshStack) 
        self.slidelistToolBar.addAction(QtGui.QIcon(':icons/anchor'), "Anchor Slide", self.anchorSlice)
        
        self.addToolBar(self.slidelistToolBar)
        self.plotToolBar = QtGui.QToolBar("Plot toolbar")
        self.plotToolBar.addAction(QtGui.QIcon(':icons/clear_plot'), "Clear Plot", self.clearPlot)
        self.addToolBar(self.plotToolBar)
        self.roiAdded = False
        self.roiToolBar = QtGui.QToolBar("ROI toolbar")
        self.roiToolBar.addAction(QtGui.QIcon(':icons/shape_square_add'), "Add ROI", self.addRoi)
        self.roiToolBar.addAction(QtGui.QIcon(':icons/shape_square_delete'), "Delete ROI", self.deleteRoi)
        self.addToolBar(self.roiToolBar)
        self.imageToolBar = QtGui.QToolBar("Image toolbar")
        self.imageToolBar.addAction(QtGui.QIcon(':icons/shape_rotate_clockwise'), "Rotate Clockwise", self.rotation)
        self.imageToolBar.addAction(QtGui.QIcon(':icons/shape_rotate_anticlockwise'), "Rotate Anti-Clockwise", self.antiRotation)
        self.addToolBar(self.imageToolBar)
        
#        plot = {'xlabel': 'Slice #', 'ylabel' : 'Pixels'}
#        self.plotWidget = PlotDialog(**plot)        
#        self.addDockWidget(self.plotWidget, "Plots", QtCore.Qt.LeftDockWidgetArea, QtGui.QIcon(':icons/chart_curve'), (400,300))
        
        self.setData(ndarray, energy)

    def anchorSlice(self):
        ind = self.sliceList.currentRow()
        self.regDock.anchorSelect.setCurrentIndex(ind)

    def rotation(self):
        rot = []
        for i in range(self.visibleImage.shape[2]): 
            rot.append(np.rot90(self.visibleImage[:,:,i]))
        self.visibleImage = np.dstack(rot)  
#        self.visibleImage = sp.ndimage.interpolation.rotate(self.visibleImage, 90, axes=(0,1))
        self.setData(self.visibleImage, self.depthAxis)

    def antiRotation(self):
        rot = []
        for i in range(self.visibleImage.shape[2]): 
            rot.append(np.rot90(self.visibleImage[:,:,i], k = 3))
        self.visibleImage = np.dstack(rot)
#        self.visibleImage = sp.ndimage.interpolation.rotate(self.visibleImage, -90, axes=(0,1))
        self.setData(self.visibleImage, self.depthAxis)

    def clearPlot(self):
        self.plotWidget.clear()
        
    def refreshStack(self):
        self.setStack(self.rawdata, self.rawenergy)
        
    def deleteSlice(self):	
        global a	  
        ind = self.sliceList.currentRow() 
        self.visibleImage = np.delete(self.visibleImage, ind, axis = 2) 
        self.depthAxis = np.delete(self.depthAxis, ind)
        self.slicenum = np.delete(self.slicenum, ind)
        a = self.slicenum
        self.sliceList.takeItem(ind)
        self.deleteRoi()

    def RegisterationHandler(self, method, data, Filter, crop, anchor, k, s, distance, interp, ignore, stage):
        global dataToReg, reg
        self.clearPlot()
        if data == 'Whole Image':
            dataToReg = self.visibleImage
        elif data == 'Roi Data':
            try:
                dataToReg = self.roiRegion                
            except AttributeError:
                QtGui.QMessageBox.information(self, "Error", "Roi not selected")
        try:                   
            regist = Register.Registration(dataToReg, self.slicenum, 
                        self.rawdata, Anchor = anchor, Method = method, 
                        Filter = Filter, CurveDegree = k, SF = s, Distance = distance, Interpolation  = interp, ignore = ignore) 
            reg = regist.reg                   
            if crop == 'Auto Crop':
                self.registeredData = regist.cropped
            elif crop == 'No Crop':
                self.registeredData = reg['registered_fit']
                                
            self.setRegisteredImage(0)        
            self.plotWidget.addplot(reg['y_trans'], x = reg['slicenum'],pname='x translation', ppen='r')
            self.plotWidget.addplot(reg['x_trans'], x = reg['slicenum'],pname='y translation', ppen='g')
            self.plotWidget.addplot(reg['y_fit'], pname='x fit', ppen='m')
            self.plotWidget.addplot(reg['x_fit'], pname='y fit', ppen='c')
        except UnboundLocalError:
            QtGui.QMessageBox.information(self, "Warning", "Registration not completed")
        self.deleteRoi()

    def RejectRegistration(self):        
        QtGui.QMessageBox.information(self, "Warning", "Registration was Rejected!")
        self.registeredData = self.rawdata
        self.setRegisteredImage(0)  
        self.clearPlot()
        self.deleteRoi()                

    def useRegisteredStack(self):
        self.setData(self.registeredData, self.rawenergy)

    def returnStackHandler(self):
        self.returnStack.emit(self.stage) 
           
    def addDockWidget(self, widget, label, area, icon=None, size = None):
        dock = QtGui.QDockWidget(label)
        dock.setWidget(widget)
        dock.setFeatures(QtGui.QDockWidget.DockWidgetClosable)
        if icon:
            dock.toggleViewAction().setIcon(icon)
        self.dockToolBar.addAction(dock.toggleViewAction())
        if size:
            dock.setFixedSize(*size)
        super(RegistrationAssistantWindow, self).addDockWidget(area, dock)        

    def setData(self, ndarray, energy):
        self.rawdata = ndarray
        self.rawenergy = energy
        self.setStack(ndarray, energy) 
        self.registeredData = ndarray
        self.slicenum = np.arange(ndarray.shape[2])
        self.setRegisteredImage(0)
        
    def setStack(self, ndarray, energy):
        self.visibleImage = ndarray
        self.depthAxis = energy
        self.sliceList.clear()
        self.sliceList.addItems(map(str, self.depthAxis))
        currentSlice = self.visibleImage[:,:,0]
        levels = [currentSlice.min(), currentSlice.max()]
        self.histogramStack.setLevels(*levels)
        self.imageItemStack.setImage(currentSlice, levels = self.histogramStack.getLevels())
        self.slicenum = np.arange(ndarray.shape[2])
        
    def manualSliceSelection(self):        
        index = self.sliceList.currentRow()
        currentSlice = self.visibleImage[:,:,index] 
        self.histogramStack.setLevels(currentSlice.min(), currentSlice.max())
        self.imageItemStack.setImage(currentSlice, levels = self.histogramStack.getLevels())

    def MovieFlipper(self):
        self.index = (self.index + 1)%self.registeredData.shape[2]
        self.setRegisteredImage(self.index)
        
    def setRegisteredImage(self, ind):        
        currentSlice = self.registeredData[:,:,ind]
        levels = [currentSlice.min(), currentSlice.max()]
        self.histogramReg.setLevels(*levels)        
        self.imageItemReg.setImage(currentSlice, levels = self.histogramReg.getLevels())
        
    def addRoi(self, pos=[0,0], size=None, label=None):
        if not self.roiAdded:
#            Bounding Roi to the view
            rect =self.imageItemStack.viewRect()          
            pos = [rect.x(), rect.y()]
            size = [rect.width(), rect.height()] 
            if pos[0] < 0:
                 	size[0] = size[0] - np.abs(pos[0])
                 	pos[0] = 0                  
            if pos[1] < 0:                   
                 	size[1] = size[1] - np.abs(pos[1])
                 	pos[1] = 0
            if pos[0] + size[0] > self.imageItemStack.image.shape[0]:
                 	size[0] = self.imageItemStack.image.shape[0] - pos[0]
            if pos[1] + size[1] > self.imageItemStack.image.shape[1]:                   
                 	size[1] = self.imageItemStack.image.shape[1] - pos[1] 
            self.roiAdded = True
            width, height = size if size else self.imageItemStack.image.shape
            self.roi = QtGraph.RectROI(pos, [width, height], pen='r')
            self.roi.sigRegionChanged.connect(self.roiMoved)
            self.viewBoxStack.addItem(self.roi)

    def deleteRoi(self):
        if self.roiAdded: 
            self.viewBoxStack.removeItem(self.roi)
            self.roiAdded = False
            del self.roiRegion

    def roiMoved(self, roi):
        if not self.imageItemStack.sceneBoundingRect().contains(self.roi.sceneBoundingRect()):
            self.roi.setState( self.roi.preMoveState )

        self.roiArray(roi)
        roi.preMoveState = roi.getState()

    def roiArray(self, roi):
        self.roiRegion = roi.getArrayRegion(data=self.visibleImage, img=self.imageItemStack, axes=(0,1))

class ScaleBarItem(GraphicsObject, GraphicsWidgetAnchor):
    """
    Displays a rectangular bar to indicate the relative scale of objects on the view.
    """
    def __init__(self, size, width=5, brush=None, pen=None, suffix='nm'):
        GraphicsObject.__init__(self)
        GraphicsWidgetAnchor.__init__(self)
        self.setFlag(self.ItemHasNoContents)
        self.setAcceptedMouseButtons(QtCore.Qt.NoButton)
        
        if brush is None:
            brush = QtGraph.getConfigOption('foreground')
        self.brush = fn.mkBrush(brush)
        self.pen = fn.mkPen(pen)
        self._width = width
        self.size = size
        self.suffix = suffix
        
        self.bar = QtGui.QGraphicsRectItem()
        self.bar.setPen(self.pen)
        self.bar.setBrush(self.brush)
        self.bar.setParentItem(self)
        
        self.text = TextItem(text=fn.siFormat(size, suffix=self.suffix), anchor=(0.5,1))
        self.text.setParentItem(self)
        self.hide = False

    def setPen(self, pen, text = 'white'):
        self.brush = fn.mkBrush(pen)
        self.pen = fn.mkPen(pen)
        self.bar.setPen(self.pen)
        self.bar.setBrush(self.brush)
        self.text.setHtml("<span style='color: %s'>%s</span>" %(text, fn.siFormat(self.size, suffix=self.suffix)) )
#        self.updateBar()
        
    def setSize(self, s):
        self.size = s
#        self.text.setText(str(self.size*s))
        self.updateBar()
        
    def parentChanged(self):
        view = self.parentItem()
        if view is None:
            return
        view.sigRangeChanged.connect(self.updateBar)
        self.updateBar()

    def showHide(self):
        if not self.hide:
            self.hide = True
            self.bar.hide()
            self.text.hide()
        else:
            self.hide = False
            self.bar.show()
            self.text.show()
        
    def updateBar(self):
        view = self.parentItem()
        if view is None:
            return
        p1 = view.mapFromViewToItem(self, QtCore.QPointF(0,0))
        p2 = view.mapFromViewToItem(self, QtCore.QPointF(self.size,0))
        w = -(p2-p1).x()
        self.bar.setRect(QtCore.QRectF(-w, 0, w, self._width))
        self.text.setPos(-w/2., 0)

    def boundingRect(self):
        return QtCore.QRectF()
        
class RegistrationDockWidget(QtGui.QGroupBox):
    registrationMethod = QtCore.pyqtSignal(str, str, str, str, int, int, float, int, str, str, str)
    rejection = QtCore.pyqtSignal()
    useRegistered = QtCore.pyqtSignal()
    returnStackSig = QtCore.pyqtSignal()

    def __init__(self, model, stage, parent=None):
        super(RegistrationDockWidget, self).__init__("Registration", parent)
        size = 250
        BtnSize = 400
        form = QtGui.QFormLayout(self)
        self.methodSelect = QtGui.QComboBox()
        self.methodSelect.setMinimumWidth(size)
        self.methodSelect.addItems(["fft","sift"])
        self.dataSelect = QtGui.QComboBox()
        self.dataSelect.setMinimumWidth(size)
        self.dataSelect.addItems(["Whole Image","Roi Data"])
        self.filterSelect = QtGui.QComboBox()
        self.filterSelect.setMinimumWidth(size)
        self.filterSelect.addItems(["No Filter","Sobel"])
        self.cropSelect = QtGui.QComboBox()
        self.cropSelect.setMinimumWidth(size)
        self.cropSelect.addItems(["Auto Crop","No Crop"])
        self.interpSelect = QtGui.QComboBox()
        self.interpSelect.setMinimumWidth(size)
        self.interpSelect.addItems(["Stack Interpolation","Unchanged", "Linear Anchored on first/last image", "Extrapolated"])
        self.ignoreSelect = QtGui.QComboBox()
        self.ignoreSelect.setMinimumWidth(size)
        self.ignoreSelect.addItems(["None","x shift", "y shift"])
        self.anchorSelect = QtGui.QComboBox()
        self.anchorSelect.setMinimumWidth(size)
        self.anchorSelect.setModel(model)
        self.smoothValue = QtGraph.SpinBox(value=50, siPrefix=False, dec=True, step=10, bounds=[0, None], minStep=5)
        self.smoothValue.setMaximumWidth(size)
        self.curveDegreeValue = QtGraph.SpinBox(value=3, siPrefix=False, dec=False, step=1, bounds=[1, 5], minStep=1)
        self.curveDegreeValue.setMaximumWidth(size)
        self.distanceValue = QtGraph.SpinBox(value=20, siPrefix=False, dec=True, step=10, bounds=[0, None], minStep=5)
        self.distanceValue.setMaximumWidth(size)
        form.addRow("Method", self.methodSelect)
        form.addRow("Data", self.dataSelect)
        form.addRow("Filter", self.filterSelect)
        form.addRow("Crop", self.cropSelect)
        form.addRow("Anchor", self.anchorSelect)
        form.addRow("Stack Head/Tail Treatment", self.interpSelect)
        form.addRow("Ignore Shift in:", self.ignoreSelect)
        form.addRow("Curve Degree (1 to 5)", self.curveDegreeValue)
        form.addRow("Smoothing Factor", self.smoothValue)
        form.addRow("Distance (For Sift Only!)", self.distanceValue)
#        roiSelect = QtGui.QComboBox()
#        form.addRow("ROI", roiSelect)
        registerBtn = QtGui.QPushButton("Register")
        registerBtn.setMaximumWidth(BtnSize)
        rejectBtn = QtGui.QPushButton("Reject")
        rejectBtn.setMaximumWidth(BtnSize)
        useRegisteredBtn = QtGui.QPushButton("Use Registered Stack")
        useRegisteredBtn.setMaximumWidth(BtnSize)
        returnStackBtn = QtGui.QPushButton("Return Registered Stack")
        returnStackBtn.setMaximumWidth(BtnSize)
        registerBtn.clicked.connect(self.methodSelection)
        rejectBtn.clicked.connect(self.rejected)
        useRegisteredBtn.clicked.connect(self.reuse)
        returnStackBtn.clicked.connect(self.returnStack)
        form.addRow(registerBtn)
        form.addRow(rejectBtn)
        form.addRow(useRegisteredBtn)
        form.addRow(returnStackBtn)
        
#        self.stage = index.parent().data(0)
        self.stage = stage
        
    def methodSelection(self):
        Method  = self.methodSelect.currentText()
        Data  = self.dataSelect.currentText()
        Filter  = self.filterSelect.currentText()
        Crop  = self.cropSelect.currentText()
        Anchor = self.anchorSelect.currentIndex()
        s = self.smoothValue.value()
        k = self.curveDegreeValue.value()
        distance = self.distanceValue.value()
        interpolation = self.interpSelect.currentText()
        ignore = self.ignoreSelect.currentText()
        self.registrationMethod.emit(Method, Data, Filter, Crop, Anchor, k, s, distance, interpolation, ignore,self.stage)
        
    def rejected(self):
        self.rejection.emit()
        
    def reuse(self):
        self.useRegistered.emit()
        
    def returnStack(self):
        self.returnStackSig.emit()


def noise():
	import numpy as np
	import scipy.ndimage as ndim
	D = 100
	R = np.random.rand(30, 50, D)
	N = np.zeros_like(R)
	for i in range(6):
		N += ndim.filters.gaussian_filter(R, 2**i) * (2**i)**2
	N[:,:,D/2:] = N[:,:,D/2:] + 2
	Z = np.arange(753,753+D)
	return N, Z

def normalize(N):
	N -= N.min()
	N /= N.max()
	return N

def pg(item):
	print( '\n'.join(dir(item)) )

def testBig(ndarray, energy, stage):
    
    s = StackAnalyzer()
#	s = RegistrationAssistantWindow(ndarray, energy, stage)
#	N, Z = noise()
    s.setStack(ndarray, energy,'I0')
#	s.addRoi(label="calumnia")
#	s.addRoi([1,1],[5,5], label="gestura")
 
#	s.applyFilter(normalize)
#	 s.applyFilter(lambda x: -x)
#	s.checkedRois()
    s.show()
    return s

def testSmall():
	''' will get destroyed if not returned '''
	s = SimpleSlideView()
	
	N, Z = noise()
	s.addSlide(N[:,:,0], 'test')
	s.addSlide(N[:,:,3], 'test2')
	s.show()
	return s, N

def testFitcheck(array1,energy,name1,array2,name2):
    s = FitQualityCheckWindow(array1, energy, name1, array2, name2)
    s.show()
    return s
    
def debug():
    import sys
    sys.path.append("../")
    import stxmio

    filename = '/home/araash/PyFCell/trunk/data/STXM/Sample Test/printed-stack-with-i0-c1s/532_130125101/532_130125101.hdr'
    lastLoad = stxmio.hdr.load_stack(filename, legacy=True)
    
    return lastLoad['stack'], lastLoad['energy']

if __name__=='__main__':
    import sys
    import stxmio
    app = QtGui.QApplication(sys.argv)
    ndarray, energy = debug()
    filename = '/home/araash/PyFCell/trunk/data/STXM/Sample Test/printed-stack-with-i0-c1s/532_130125101/532_130125101od.ncb'
    load = stxmio.ncb.ncb_reader(filename)
#    t = testBig(ndarray, energy, 'I0')
    t = testFitcheck(ndarray, energy, 'I0',load['stack'],'New I0')
    spectrum = '/home/araash/PyFCell/trunk/data/STXM/Sample Test/printed-stack-with-i0-c1s/532_130125101/532_130125101-i0.txt'
    xData, yData, comments = stxmtools.load_spectrum(spectrum)
#    t.addSpectrum(yData, 'spec 1')
#    s, N = testSmall()

    sys.exit(app.exec_())