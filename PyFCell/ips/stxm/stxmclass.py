#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Arash Ash, AFCC Development Team
# License: TBD
# Copyright (c) 2014, TBD
import numpy as np
import numpy.ma as ma
import os
import stxmtools as stxmtools
#from PyFCell.util.__baseclass__ import baseobject

r"""

**************************************************************************
:mod:`PyFCell.ips.stxm.stxmclass` -- Stxm class for Image Processing Suite
**************************************************************************

.. module:: PyFCell.ips.stxm

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========
Classes
=======

"""
#class STXMData(baseobject):
class STXMData(object):    
    r"""
    Data Handling class for STXM data
    
    Parameters
    ----------
    stack : ndarray
       Numpy array with the data.
       2D or 3D array is expected as input.  

    path : string, optional. Default = None
        path AND filename of the STXM data. if None, no name will be assigened.
        e.g. path = '/home/user/PyFCell/trunk/data/STXM/532_130710058/532_130710058.hdr'
       
    x_axis : array-like, optional. Default = None
        x axis of the STXM data. if None, no value will be assigened. 

    y_axis : array-like, optional. Default = None
        y axis of the STXM data. if None, no value will be assigened. 

    x_step : int, optional. Default = None
        Value of physical increment in x direction. if None, no value will be assigened. 
           
    y_step : int, optional. Default = None
        Value of physical increment in y direction. if None, no value will be assigened.   

    energy : array-like, optional. Default = None
        Energy range of the STXM data. if None, no value will be assigened.
    
    Examples
    --------
    
    >>> # Single image example:
    >>> import scipy as sp
    >>> img = np.asarray(sp.lena())
    >>> T = STXMData(img)
    >>> # STXM data example:
    >>> import PyFCell.ips.stxm.stxmtools as stxm
    >>> path = '/home/user/PyFCell/trunk/data/STXM/532_130607016/532_130607016.hdr'
    >>> data = stxm.load_stack(path) # return a dictionary of the stxm data, containing 'stack', 'energy', 'path', 'x_axis', 'y_axis', 'x_step', 'y_step'
    >>> T = STXMData(**data) 
    
    .. warning:: This documentation is not yet finished.
    
    .. todo:: 1. Finish documentation
        
    """
    def __init__(self, stack = None, path = None, x_axis = None, y_axis = None,
                     x_step = None, y_step = None, energy = None, Dwell= None,
                     Dfocus = None, **kwarg):
        r"""
        Initializer: 
            Initializing the class, stxm data, manipulation result, ROI selections, spectrums        
        """           

#        super(STXMData, self).__init__(**kwargs)    
        super(STXMData, self).__init__() 
        self._data = stack
        self.result = []
        self._ROI = {}
        self._spectrum = {}
        self._path = path
        if path:
            path, name = os.path.split(path)
            name, ext = name.split('.')
#            self._path = path
            self._name = name
        self._energy = energy
        self._header(x_axis, y_axis, x_step, y_step, Dwell, Dfocus)    
        
    @property
    def data(self):
        r"""
        Data getter: 
            Returns the STXM data from the STXMData        
        """
        return self._data
    
    @data.setter    
    def data(self, val):
        r"""
        Data setter: 
            Sets the STXM data to the STXMData        
        
        Parameters
        ----------
        val : ndarray
            Data to be replaced by the data instance in STXMData. 
                   
        Examples
        --------
        >>> test_data = np.zeros((10,10))
        >>> T.data = test_data # Sets the STXM data to the test_data.
        """    
#        self._logger.debug("Call setter method for data property")
        self._data = val
    
    @data.deleter    
    def data(self):
        r"""
        Data deleter: 
            Deletes the STXM data from the STXMData       
        
        Examples
        --------
        >>> del T.data  # Sets the STXM data to an empty list.
        """
#        self._logger.debug("Call deleter method for data property")
        self._data = []
        
    @property
    def name(self):
        r"""
        Name getter: 
            Returns the STXM data name from the STXMData        
        """   
        return self._name
    
    @name.setter    
    def name(self, s):
        r"""
        Name setter: 
            Sets the STXM data name to the STXMData        
        
        Parameters
        ----------
        s : string
            Name to be replaced by the data name instance in STXMData. 
                   
        Examples
        --------
        >>> T.name = "Test Data" # Sets the STXM data name to "Test Data".
        """ 
        self._name = s       

    @property
    def energy(self):
        r"""
        Energy getter: 
            Returns the STXM data energy from the STXMData        
        """   
        return self._energy
        
    @property
    def path(self):
        r"""
        Path getter: 
            Returns the STXM data path from the STXMData        
        """   
        return self._path        
        
    def _header(self, x_axis, y_axis, x_step, y_step, Dwell, Dfocus):
        r"""
        Data header method, returns a dictionary of the header data, optional.
        
        Parameters
        ----------
        x_axis : array-like, optional. Default = None
           x axis of the STXM data. if None, no value will be assigened. 

        y_axis : array-like, optional. Default = None
           y axis of the STXM data. if None, no value will be assigened. 

        x_step : int, optional. Default = None
           Value of physical increment in x direction. if None, no value will be assigened. 
           
        y_step : int, optional. Default = None
           Value of physical increment in y direction. if None, no value will be assigened.   
        """ 
        self.header = dict([('x_axis', x_axis), ('y_axis', y_axis), 
                            ('x_step', x_step), ('y_step', y_step), 
                            ('Dwell', Dwell), ('Dfocus', Dfocus)])
        
    def manip(self, ops, value = None, operand = None, axis = None):
        r"""
        Data Manipulation method
        
        Parameters
        ----------
        operand: array_like or int,
            Object of the mathematical operation. If None, the STXM data will be used as the operand.
            
        ops : 'ave', 'divide', 'multiply', 'subtract', 'add'
           Desired Operation
           
        value: float or ndarray, Default = None.
           float or array depending on ops. This is a necessary value for 'divide', 'multiply', 'subtract', 'add'.   
        
        axis : int, optional
            Axis along which to average data. If None, averaging is done over teh flattened array. 
        
        Examples
        --------
        
        >>> # Average
        >>> T.manip('ave') # Calculated average of the flattened array
        >>> T.manip('ave', axis = 0) # calclates average along axis = 0
        >>> # Division, similarly for 'multiply', 'subtract', 'add'  
        >>> T.manip('divide', 2) # Elementwise Multiplication of data by a number. 
        >>> T.manip('divide', T.getdata())  # Elementwise array Multiplication of data by an another array. 
        >>> # Manipulating spectrums
        >>> result = T.manip('multiply', 2, T.getSpectrumdata(0)) # multoplying the first spectrum by 2.
        >>> # Calculating Spectrum from ROI
        >>> spectrum = T.manip('ave', operand = T.getROIdata(), axis = 1) 
        """ 
        
        calc = dict([('ave',np.average), ('divide',np.divide), ('multiply', np.multiply), ('subtract', np.subtract), ('add', np.add)])
        
        if operand == None:
            operand = self._data
        
        if ops == 'ave':
            try:
                self.result = calc[ops](operand, axis)
            except ValueError:
                self.result = calc[ops](operand)
        else:
            self.result = calc[ops](operand, value)
        return self.result
                
                               
    def addROI(self, label=None, pos=None, size=None, AutomaticSpectrumAdd = True, **ROIDict):
        r"""
        Adds Region of Interest(ROI) dictionary to the STXM data class. Selected ROIs are saved as masked array and are added to ROI dictionary with labels as keys.
        
        Parameters
        ----------
        label : string,
            label of the added ROI
        pos : point, 
            Upper left corner of the ROI 
        size : [width, height] of the ROI
        AutomaticSpectrumAdd = boolean,
            Automatic calculation and addition of the spectrum of the added ROI to the spectrum dictionary. Default = True.            
        **ROIDict : Dictionary of the ROIs. Labels are stored as keys. Values are lists in the following order: first element = Pos, second element  = size. 
            e.g. RoiDict['label'] = [[x,y],[width,height]]
                   
        Examples
        --------
        
        >>> T.addROI(label = 'I0', pos = [50,300], size = [30,20])
        >>> # Alternatively
        >>> roi = {}
        >>> roi['good'] = [[50,300], [30,20]]
        >>> T.addROI(**roi)
        """
        if ROIDict:
            for keys, values in ROIDict.iteritems():
                try:
                    mask = np.ones(self._data[:,:,0].shape)
                except IndexError:
                    mask = np.ones(self._data.shape)
                mask[values[0][0]:values[0][0] + values[1][0],values[0][1]:values[0][1]+values[1][1]] = 0.
                self._ROI[keys] = [values[0], values[1], mask]
                if AutomaticSpectrumAdd:
                    self.addSpectrum(label = keys, spectrum = self.manip('ave', operand = self.getROIdata(keys), axis = 1))
        else:            
            try:
                mask = np.ones(self._data[:,:,0].shape)
            except IndexError:
                mask = np.ones(self._data.shape)        
            mask[pos[0]:pos[0] + size[0],pos[1]:pos[1]+size[1]] = 0.  
            self._ROI[label] = [pos, size, mask]
            if AutomaticSpectrumAdd:
                self.addSpectrum(label = label, spectrum = self.manip('ave', operand = self.getROIdata(label), axis = 1))

    def getROI(self):
        r"""
        Returns a dictionary of Region of Interests(ROIs) as masks, ie. True = masked, Flase = unmasked(ROI region).
        Keys are labels of ROIs and Values are lists in the following order: first element = Pos, second element  = size and third element = mask.
        
        Examples
        --------
        
        >>> roi_Dict = T.ROI
        """
        return self._ROI   
        
    def removeROI(self, label):
        r"""
        Removes a ROI from the ROI dictionary
        
        Parameters
        ----------
        label : string
            label of the ROI to be removed.
        
        Examples
        --------
        
        >>> T.removeROI('I0') # Removes the 'I0' ROI from the ROI dictionary.
        """
        self._ROI.pop(label)
        
    def clearROI(self):
        r"""
        Resets the ROI dictionary. Please refer to getROI method for the ROI dictionary.
        
        Examples
        --------
        
        >>> T.clearROI()
        """
        self._ROI = {}

    def getROIdata(self, label):
        r"""
        Returns a flat array of the selected ROI from the data method.
        
        Parameters
        ----------
        label : string 
            label of the desired ROI in the ROI dictionary. Please refer to getROI method for ROI dictionary. 
                   
        Examples
        --------
        
        >>> roi_data = T.getROIdata('I0') # returns the data corresponding to 'I0'.
        """ 
        try:
            mask_data = []
            for i in range(self.data.shape[2]):
                masked = ma.masked_array(self._data[:,:,i], self._ROI[label][2])
                mask_data.append(masked.compressed())
        except IndexError:
            masked = ma.masked_array(self._data, self._ROI[label][2])
            mask_data = masked.compressed()
#        masked = ma.masked_array(self._data, self._ROI[number])
        return mask_data
        
    def addSpectrum(self, label = None, spectrum = None, **spectrumDict):
        r"""
        Adds spectrum and it's label to the STXM data class. Added spectrums are added to a Spectrum Dictionary. Please refer to getSpectrum method for the spectrum Dictionary.
        
        Parameters
        ----------
        label: string
            label of the added spectrum.
        spectrum: array-like            
            added spectrum.
        **spectrumDict : Dictionary,
            Dictionary of Spectrum labels as Keys and average pixels values of a desired region (or the whole image) at each energy point as Values.
                   
        Examples
        --------
        
        >>> import PyFCell.ips.stxm.stxmtools as stxm
        >>> path = '/home/user/PyFCell/trunk/data/STXM/532_130607016/532_130607016.hdr'
        >>> data = stxmtools.load_stack(path)
        >>> T = STXMData(**data)
        >>> T.addROI(label = 'I0', pos = [50,300], size = [30,20])
        >>> I0.addSpectrum(label = 'I0', spectrum = T.manip('ave', operand = T.getROIdata('I0'), axis = 1))
        >>> # alternatively
        >>> d = {}
        >>> d['I0 Spectrum'] = spectrum
        >>> T.addSpectrum(**d)        
        """ 
        if spectrumDict:
            self._spectrum = spectrumDict
        else:
            self._spectrum[label] = spectrum

    def getSpectrum(self):
        r"""
        Returns a dictionary of spectrums added to the STXM class. keys as labels and values as spectrums.

        Examples
        --------
        
        >>> spectrum_dict = T.getSpectrum()
        """
        return self._spectrum

    def removeSpectrum(self, label):
        r"""
        Removes a Spectrum from the Spectrum dictionary
        
        Parameters
        ----------
        label = string,
            label of the Spectrum to be removed.
        
        Examples
        --------
        
        >>> T.removeSpectrum('I0') # Removes the 'I0' Spectrum from the Spectrum dictionary
        """
        self._spectrum.pop(label)

    def clearSpectrum(self):
        r"""
        Resets the Spectrum dictionay. Please refer to getSpectrum method for the Spectrum dictionary.
        
        Examples
        --------
        
        >>> T.clearSpectrum() # Clears the Spectrumlist.
        """
        self._spectrum = {}

    def getSpectrumdata(self, label):
        r"""
        Returns a desired spectrum from the STXM Spectrumlist.
        
        Parameters
        ----------
        label : string 
            label of the desired Spectrum in the Spectrum dictionary. Please refer to getSpectrum method for Spectrum dictionary. 
                   
        Examples
        --------
        
        >>> spectrum_data = T.getSpectrumdata('I0') # returns the data corresponding to 'I0'.
        """ 
        return self._spectrum[label]


def debug():
    import numpy as np   
    import pylab as plb
    global I0, Sample, od_stack, data, odDict


    pathI0 = '/home/araash/PyFCell/trunk/data/STXM/Sample Test/printed-stack-with-i0-c1s/532_130125101/532_130125101.hdr'
    
    data = stxmtools.load_stack(pathI0)
#    data['stack'] = np.rollaxis(data['stack'], 0, 2)

    I0 = STXMData(**data)
#    I0.addROI(label = 'I0', pos = [50,300], size = [30,20])


#    pathSample ='/home/araash/PyFCell/trunk/data/STXM/Sample Test/printed-stack-with-i0-c1s/532_130125101/532_130125101.hdr'    
#    data_sample = stxmtools.load_stack(pathSample)
#    data_sample['stack'] = np.rollaxis(data_sample['stack'], 0, 2)
#    Sample = STXMData(**data_sample)    
##    
#    od = stxmtools.optical_density(I0.getSpectrumdata('I0'), Sample.data)
#    odDict = data_sample
#    odDict['stack'] = od
#    od_stack = STXMData(**odDict)
#    RoiDict = {}
#    RoiDict['empty space'] = [[50,300], [30,20]]
#    RoiDict['epoxy'] = [[10,10], [30,20]]
#    RoiDict['agglomerate'] = [[50,200], [30,20]]
#    RoiDict['another thing'] = [[50,100], [30,20]]   
#    od_stack.addROI(**RoiDict)
##    
#    plb.plot(od_stack.energy, od_stack.getSpectrumdata('empty space'), label = 'empty space')
#    plb.plot(od_stack.energy, od_stack.getSpectrumdata('epoxy'), label = 'epoxy')
#    plb.plot(od_stack.energy, od_stack.getSpectrumdata('agglomerate'), label = 'agglomerate')
#    plb.plot(od_stack.energy, od_stack.getSpectrumdata('another thing'),label = 'another thing')
#    plb.legend()
#    plb.ylim(-0.5,2.5)
#    plb.xlim(np.min(od_stack.energy), np.max(od_stack.energy))
#    plb.show()
    return I0

def main():
    import PyFCell.ips.stxm.stxmtools as stxm
    import pylab as plb
    import scipy as sp
    import cPickle
    global obj
    
#    img = np.asarray(sp.lena()) 
    I0 = debug() 
    
#   Saving class
#    filename = '/home/araash/Desktop/532_130125101.xfc'
#    with open(filename, 'wb') as output:
#        cPickle.dump(od_stack, output, cPickle.HIGHEST_PROTOCOL)
#   loading class        
#    with open(filename, 'rb') as inp:
#        obj = cPickle.load(inp)
        
#    filename = '/home/araash/Desktop/Image Processing/STXM/532_130710058/532_130710058.hdr'
#    header = open(filename).read()
#    raw_data, image_data = stxm.read_data(filename)
#    x_axis, y_axis, x_step, y_step, energy_range = stxm.read_header(header, image_data)
##    T = STXMData(img)
#    T = STXMData(raw_data, filename, x_axis, y_axis, x_step, y_step, energy_range) 
#    mask = np.ones(T.data[:,:,0].shape)
#    mask[0:10,0:10] = 0.
#    T.addROI(mask)
#    T.addSpectrum(T.manip('ave', operand = T.getROIdata(), axis = 1))
#   
if __name__ == "__main__":
    main()       
        
           