#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Arash Ash
# License: TBD
# Copyright (c) 2014, TBD

r"""

***************************************************************************************
:mod:`PyFCell.ips.stxm.Register` -- STXM Registration module for Image Processing Suite
***************************************************************************************

.. module:: PyFCell.ips.stxm

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========
Classes
=======

"""
from __future__ import print_function
import numpy as np
import pylab as plb
from numpy.fft import fft2, ifft2
import scipy.ndimage.interpolation as ni
import scipy.ndimage as ndimage
#from PyFCell.util.opt import fit, Parameter
#from opt import fit, Parameter
#from PyFCell.ips.util import sift_matching
from siftmatching import sift_matching
from scipy.interpolate import UnivariateSpline

class Registration():    
    r"""
    Registration class for stxm image processing. Returns an instance of the registration class. 
    Output data is stored in a method called "reg". "reg" dictionary contains following outputs:
        
    reg['x_trans']  : Translation in x direction;

    reg['y_trans']  : Translation in y direction; 
       
    reg['slicenum'] : Slices sequence used in resgistration;
    
    reg['x_fit'], reg['x_fit_param'], reg['fx'] : fitted x translation, fit parameters, fit function respectively;
    
    reg['y_fit'], reg['y_fit_param'], reg['fy'] = fitted y translation, fit parameters, fit function respectively;
    
    reg['registered_fit'] = fitted registered stack;

    Parameters
    ----------
    image : 3D array
        Stack of images to be registered. often in stxm imaging the raw stack will be manipulated to remove images with no fiducials and
        the remaining stack will be fed to the registration algorithm. Manipulated stack is addressed as image stack in this documentation. 
    
    slicenum : array-like, Default = None
       1D array of sequence of slice numbers presented in the image stack.
       
    originaldata : 3D array, Default = None
        Stack of raw images. This will be used in instances where the whole stack is not used for registration. 
        for instance in stxm analysis often images with no fiducials are deleted before feeding to registration algorithm.

    Anchor : int, Default = 0
        Index of the image to be used as anchor in registration algorithm. By default first image of the stack is used.

    Method : string. 'fft', 'sift', Default = 'fft'
        Name of the algorithm to be used for registration. currently 'fft' and 'sift' are available.
        
    Filter : string, 'No Filter' and 'Sobel', Default = 'No Filter'
        Filter to be used on the image before feeding to registration. Sobel option 
        performs image sharpenning based on Laplacian of Gaussian filter and sobel edge detection technique.

    CurveDegree : int. 1 to 5, must be <= 5. Default = 3.
        Degree of the smoothing spline in fittign routine.  

    SF : float. Default = 50.
        Positive smoothign factor used to choose the number of knots in spline curve fitting process. 
        if 0 spline will interpolate through all data points. SF > 10e4 returns a linear fit.

    Distance : int. Default = 20 (For SIFT algorithm only)
        Maximum pixel translation allowable for 'sift' algorithm. 

    Interpolation : string, 'Stack Interpolation', 'Unchanged', 'Linear Anchored on first/last image' and 'Extrapolated', Default = 'Stack Interpolation'. 
        Often in stxm image processing beginning or end of the stack is trimmed. 
        This method provides following options to introduce trimmed slices back to the output registered stack.
        
        'Stack Interpolation' :     
            Same fit function used for the registered stack will be used to find pixel translation for head/tail of the stack.
        
        'Unchanged' : 
            zero translation will be used for trimmed slices in head/tail of the stack.
        
        'Linear Anchored on first/last image' : 
            translation of the trimmed slices will be linearly interpolated based on zero translation on first/last
            slice. The rest of the slices will be linearly interpolated to match translation of the first/last slice of the image stack.
            
        'Extrapolated' : 
            Translations of the trimmed head/tail of the stack will be extrapolated based on 2 point slope of the head/tail of the image stack.

    ignore : 'None', 'x shift', 'y shift', Default = 'None'.
        This method is used to ignore translations in one direction. By default translations in both directions are considered.
    
    Examples
    --------
    >>> filename = '/home/araash/PyFCell/trunk/data/STXM/532_130607016/532_130607016.hdr'    
    >>> data = hdr.load_stack(filename, legacy=True)
    >>> regist = Registration(data['stack'])
    """
    def __init__(self, image, slicenum = None, originaldata = None, Anchor = 0, Method = 'fft',
                 Filter = 'No Filter', CurveDegree = 3,SF = 50, Distance = 20, 
                 Interpolation = 'Stack Interpolation', ignore = 'None'):
                     
        if slicenum is None:
            slicenum = np.arange(image.shape[2])
        if originaldata is None:
            originaldata = image
            
        self.originalImage = image
        self.Method = Method
        self.Filter = Filter
        self.Interpolation = Interpolation
        self.ignore = ignore
        self.rawdata = originaldata
        self.rawSlicenum = np.arange(np.shape(self.rawdata)[2])
        self.originalNumSlice = self.rawdata.shape[2]        
        self.Distance = Distance
        self.SmoothingFactor = SF
        self.curveDegree = CurveDegree
        self.y_trans, self.x_trans, self.image_reg = [0], [0], []
        print (self.Interpolation)
       
        if self.Filter == 'No Filter':
            self.data = image
        elif self.Filter == 'Sobel':
            self.data, output, logim = self.sobel(image)
            
        if self.Method == 'fft':
            self.fft_reg(Anchor)
            self.slicenum = slicenum

        elif self.Method == 'sift':
            self.sift_reg(self.data, distance=self.Distance)

                                             
        x_fit, self.x_fit_param, self.fx = self.fitter(self.x_trans, self.slicenum, self.curveDegree, self.SmoothingFactor)
        y_fit, self.y_fit_param, self.fy = self.fitter(self.y_trans, self.slicenum, self.curveDegree, self.SmoothingFactor)

        self.x_fit = np.zeros(self.rawdata.shape[2])
        self.y_fit = np.zeros(self.rawdata.shape[2])
        
        self.HeadTailTreatment()
        
        # Ignoring shifts in one direction
        if self.ignore == "x shift":
            print("x shift was ignored!")
            self.y_fit = np.zeros(self.rawdata.shape[2])
        elif self.ignore == "y shift":
            print("y shift was ignored!")
            self.x_fit = np.zeros(self.rawdata.shape[2])
        elif self.ignore == "None":
            pass
            
        self.registered_fit = self.translation_fit(self.rawdata, Anchor, self.x_fit, self.y_fit)
        
        self.output()
        self.crop()

    def HeadTailTreatment(self):
        r"""
        Provides methods to estimate translation of the slices ommited from the head or tail of the stack.
    
        """        
        def slope(y1,y2,x1,x2):
            return (y2-y1)/(x2-x1) 
        
        def line(x,y1,y2,x1,x2):
            return slope(y1,y2,x1,x2)*(x - x1) + y1
        
        for i in range(self.originalNumSlice):
            
            if i < self.slicenum[0] or i > self.slicenum[-1]: 
                
                if self.Interpolation == "Stack Interpolation":
#                    Same fit function found for the image stack will be used for the head/tail of the output registered stack
                    print ("Stack Interpolation - Slice# ", i)
                    self.x_fit[i] = self.fx(i)
                    self.y_fit[i] = self.fy(i)
                
                elif self.Interpolation == "Unchanged":  
#                    Zero translation will be used for slices omitted from head/tail of the image stack.
                    print ("Unchanged - Slice# ", i)
                    self.x_fit[i] = 0.
                    self.y_fit[i] = 0.  
                
                elif self.Interpolation == "Linear Anchored on first/last image":
#                    Linear interpolation. based on point 1: start/end slice of 
#                    the raw stack with zero translation and point 2 as translation 
#                    value of first/end slice of the image stack 
                    print ("Linear - Slice# ", i)
                    if i < self.slicenum[0]:
                        self.x_fit[i] = line(i, 0., self.fx(self.slicenum[0]), 0., self.slicenum[0])
                        self.y_fit[i] = line(i, 0., self.fy(self.slicenum[0]), 0., self.slicenum[0])                    
                    if i > self.slicenum[-1]:
                        self.x_fit[i] = line(i, self.fx(self.slicenum[-1]), 0., self.slicenum[-1], self.rawdata.shape[2] - 1)
                        self.y_fit[i] = line(i, self.fy(self.slicenum[-1]), 0., self.slicenum[-1], self.rawdata.shape[2] - 1)
                    
                elif self.Interpolation == "Extrapolated":
#                    translation of trimmed slices from head and tail of the 
#                    raw stack will be extrapolated based on staring/ending 2 points of the image stack
                    print ("Extrapolated - Slice# ", i)
                    if i < self.slicenum[0]:
                        self.x_fit[i] =  line(i, self.fx(self.slicenum[0]), self.fx(self.slicenum[1]), self.slicenum[0], self.slicenum[1])
                        self.y_fit[i] =  line(i, self.fy(self.slicenum[0]), self.fy(self.slicenum[1]), self.slicenum[0], self.slicenum[1])
                    if i > self.slicenum[-1]:
                        self.x_fit[i] =  line(i, self.fx(self.slicenum[-2]), self.fx(self.slicenum[-1]), self.slicenum[-2], self.slicenum[-1])
                        self.y_fit[i] =  line(i, self.fy(self.slicenum[-2]), self.fy(self.slicenum[-1]), self.slicenum[-2], self.slicenum[-1])                                    
            else:
                print ("Interpolate - Slice# ", i)
                self.x_fit[i] = self.fx(i)
                self.y_fit[i] = self.fy(i)             
            
    def output(self):
        r"""
        creates output dictionary of results
        
        """ 
        self.reg = {} 
        self.reg['x_trans']  = self.x_trans
        self.reg['y_trans']  = self.y_trans         
        self.reg['slicenum'] = self.slicenum
        self.reg['x_fit'], self.reg['x_fit_param'], self.reg['fx'] = self.x_fit, self.x_fit_param, self.fx
        self.reg['y_fit'], self.reg['y_fit_param'], self.reg['fy'] = self.y_fit, self.y_fit_param, self.fy
        self.reg['registered_fit'] = self.registered_fit        

    def fft_reg(self, Anchor): 
        r"""
        Runs fft translation algorithm based on the anchored image
    
        """           
        im0 = self.data[:,:,Anchor]
        for i in range(self.originalImage.shape[2]):
            if i != Anchor:
#            im0 = self.data[:,:,Anchor]
                im1 = self.data[:,:,i]
                im_reg, t0, t1 = self._translation(im0, im1)
                self.image_reg.append(im_reg)
                self.y_trans.append(t0)
                self.x_trans.append(t1)              

    def _translation(self, im0, im1):
        f0 = []
        """r
        Calculates translation between two images based on FFT
        
        im0, im1
        
        Parameters
        ----------
        im0 : 2D-array of first image (Will be used as anchor)
        im1 : 2D-array of misaligned image
        """
        shape = im0.shape
        f0 = fft2(im0)
        f1 = fft2(im1)
        ir = abs(ifft2((f0 * f1.conjugate()) / (abs(f0) * abs(f1))))
        t0, t1 = np.unravel_index(np.argmax(ir), shape)
        if t0 > shape[0] // 2:
            t0 -= shape[0]
        if t1 > shape[1] // 2:
            t1 -= shape[1]
        
        im2 = ni.shift(im1, [t0, t1])
        return im2, t0, t1

    def sift_reg(self, image, distance=20):
           """r
           Runs sift feature detection algorithm on image stack
            
           """    
           im=np.asarray(image,dtype=np.uint8)
           imshape=im.shape
    
           exception_list=[]
           slicenum=[] # List of slices in which the shifts have been detected. Numbering starts from 0 , Slicenum=1 refers to second slice of the image. 
           yshifts=[]
           xshifts=[]
           for i in range(im.shape[2]-1):
               orig_im=im[:,:,i]
               temp=im[:,:,i+1]  
    #           print("Iteration Number" , i )
               #cv2.imwrite('img'+str(i)+'.tif',orig_im)
    
               try:
                   matches=sift_matching(orig_im,temp,distance,save_slices='OFF',show_location='OFF',display_match='OFF',region_detect='OFF',tagname=str(i+1))
                   orig_pts=matches.orig_pts
                   template_pts=matches.template_pts
        
                   xshift,yshift=self._get_shifts(orig_pts,template_pts,imshape,xlimit=3,ylimit=3) # the xlimit and ylimit corresponds to the maximum shifts possible in terms of pixels.
                                
                   slicenum.append(i+1)
                   yshifts.append(yshift)
                   xshifts.append(xshift)
               except:
                    print ("exception encountered at iteration ",i)
                    exception_list.append(i)
                    continue
           
           slicenum=np.transpose(slicenum).astype(np.int)
           yshifts=np.transpose(yshifts).astype(np.int)
           xshifts=np.transpose(xshifts).astype(np.int)
    #       print (xshifts)
    #       print (yshifts)
           
           try: # An error handling block in case the number of xshifts and yshifts do not match 
               assert( xshifts.shape[0] == yshifts.shape[0] )
    #           print "================================"
    #           print  "slice no. | y shift | x shift |"
    #           print "================================"    
    #           for i in range(0,np.transpose(yshifts).shape[0]):
    #               print (slicenum)[i],"     |   ",(yshifts)[i] ,"  |   ",(xshifts)[i], "      |"
    #           print "================================"                
           except :
               pass
    #           if ( xshifts.shape[0] > yshifts.shape[0] ):
    #               print ("\n################################################################################")
    #               print ("x and y shifts are not equal in number. Please try increasing the y limits")
    #               print ("number of yshifts:",yshifts.shape[0])
    #               print ("number of xshifts:",xshift.shape[0])
    #               print ("################################################################################")
    #           elif ( xshifts.shape[0] < yshifts.shape[0] ):
    #               print ("\n################################################################################")          
    #               print ("x and y shifts are not equal in number. Please try increasing the x limits")
    #               print ("number of yshifts:",yshifts.shape[0])
    #               print ("number of xshifts:",xshift.shape[0])
    #               print ("################################################################################")
    #       
           self.x_trans = np.cumsum(xshifts)
           self.y_trans = np.cumsum(yshifts)
           self.slicenum = slicenum

    def _find_similarelements(self, array_a):
             
            # Function to search and return the most occuring value from the obtained array of shifts 
            inp=array_a
            siz=np.shape(inp)[0]
            inp=list(inp)
            ele=list()
            count=list()
        
            for i in range(siz,0,-1):
                for x in inp :
                    if (x not in ele) and (inp.count(x)==i):
                       ele.append(x)
                       count.append(i)            
            ele=np.asarray(ele)
            count=np.asarray(count)
            freq_ele=ele[np.where(count==np.max(count))]
            maxcount=np.max(count)        
            del ele 
            del count 
            
            return freq_ele,maxcount
    
    def _get_shifts(self, orig_pts,template_pts,imshape,xlimit=3,ylimit=3):
            
            #This module calculates the x and y shifts using the points detected in the original and template image.
        
            #The x_limit and y_limit are set by observing the shifts manually in fiji. 
            #The shifts dont appear to be more than 2 units hence the limit is set to 3. The shifts will lie between +3 to -3
                
            # rfx,rfy indicate the x and y coordinates of points detected in reference image ie., image[:,:,i]
            rfx=np.transpose(orig_pts)[0]-imshape[1] # The width of the image is subtracted in order to revert the xcoordinates to normal value 
            rfy=np.transpose(orig_pts)[1]
                
            # tempx,tempy indicate the x and y coordinates of points detected in template image ie., image[:,:,i+1]          
            tempx=np.transpose(template_pts)[0] 
            tempy=np.transpose(template_pts)[1]
             
            
            #diffx and diffy corresponds to shifts shown by the points detected in x and y direction respectively
            diffx=rfx-tempx
            diffy=rfy-tempy
                
            #Limit the shifts detected to fall between the x and y limit specified. This is done purely based on manual observation the shifts between succesive images using Fiji
            diffy=diffy[(np.abs(diffy)<ylimit)]
            diffy=diffy[(np.abs(diffy)>0)]
                
            diffx=diffx[(np.abs(diffx)<xlimit)]
            diffx=diffx[(np.abs(diffx)>0)]
                
            # Find the shift which is more consistent based on the frequency of its occurence 
            ele,count=self._find_similarelements(diffy)
            #print "highest repeated yshift is: ",ele, " with count: ",count
                
            # In case more than one shift have the same frequencies, the smallest shift is selected
            yshift=ele[np.abs(ele)==np.min(np.abs(ele))]
              
            #yshift=ele
                
            if yshift.shape[0] > 1: #If more than one shifts still have the same frequency of occurence , average shift is considered
            # For ex. if -1 and +1 both occur equal number of times , ambiguity aries. Hence we take the average =0 as the efective shift
            
                yshift=np.average(yshift)
                yshift=np.int(yshift)            
                
            
            ele,count=self._find_similarelements(diffx)
            
            #print "highest repeated xshift is: ",ele, " with count: ",count           
            
            xshift=ele[np.abs(ele)==np.min(np.abs(ele))]           
            
            #xshift=ele
            if xshift.shape[0] > 1:
                xshift=np.average(xshift)
                xshift=np.int(xshift)
                
            return xshift,yshift

    def fitter(self, y, ns, CD, SF):
        """r
        Fitting algorithm based on scipy.interpolation package. UnivariateSpline is used as the fitting package.
        
        """
        if CD < 1:
            CD = 1
        if CD > 5:
            CD = 5
        if SF < 0:
            SF = 0
        print ("Curve Degree", CD)
        print ("Smoothing Factor", SF)
        xdata = ns
        ydata = y
        f = UnivariateSpline(xdata, ydata, k=CD, s=SF)
        x = f(xdata)
        fit_param = None
        return x, fit_param, f  
        
#        def f(x): 
#            return a()*x**2+b()*x+c()  
#            
#        # fitting
#        # x transation fit
#        a=Parameter(1)
#        b=Parameter(0)
#        c=Parameter(0)
#        if fullStack:            
#            xdata = np.arange(ns)
#        else:
#            xdata = ns
#        ydata = y
#        fit(f,[a,b,c],y=ydata,x=xdata)            
#        returns={'a':a(),'b':b(),'c':c()}
#        fit_param = returns    
#        # Fit with a rounded to pixel value        
#    #    x = np.around(f(xdata))
#        # fit with actual value
#        x = f(xdata)
#        return x, fit_param, f

    def translation_fit(self, image, anchor, x, y):
        """r
        Shifts images in the stack based in the given x,y shift 
        """
        reg_fit = []
        for i in range(image.shape[2]):
            if i == anchor:
                reg_fit.append(image[:,:,i])
            else:
                shifted_image = ni.shift(image[:,:,i], [y[i], x[i]])
                reg_fit.append(shifted_image)
        reg_fit_stack = np.dstack(reg_fit)
        return reg_fit_stack
                        
    def sobel(self, image):  
        """r
        Performs edge sharpenning using Laplacian of Gaussian method on given image 
        based on sobel algorithm edge detection algorithm provided scipy.ndimage.
        """
        theta = 1.2
        size = 5
        output = np.zeros(image.shape)
        logim = np.zeros(image.shape)     
        mag = np.zeros(image.shape)
        
        def sobel_filter(image):
                
        #    global diff, delta, mag, enh, UM, sharpim,1 im, im2
        
            dx = ndimage.sobel(image, 0)
            dy = ndimage.sobel(image, 1)
            mag = np.hypot(dx, dy)
           
        #    mag *= 255.0 / np.max(mag)  
            
            return mag
            
        def image_sharpenning(image, theta = 1.4, size = 9, method ='log'):
            """r
            Implements image sharpenning based on Laplacian of Gaussian
            """
            def __GoL(theta, size = 9):
                global l, x, y
                x = np.linspace(-size//2 + 1, size//2, size)
                y = np.linspace(-size//2 + 1, size//2, size)
                [X, Y] = np.meshgrid(x,y)
                if method == 'log':
                    l = -1 / np.pi / theta**4 * (1 - ( np.hypot(X,Y)**2 / 2 / theta**2)) * np.exp(-np.hypot(X,Y)**2 / 2 / theta**2)
                if method == 'laplace':
                    l = np.multiply(np.ones((size,size)), -1)
                    l[np.ceil(size//2), np.ceil(size//2)] = len(l.flatten()) - 1
                return l  
                
            l = __GoL(theta, size) 
            
        #    LoGim = ndimage.filters.convolve(image, l, mode = 'constant', cval = 0.0)
            LoGim = ndimage.filters.convolve(image, l, mode = 'reflect', cval = 0.0)
            sharp_image = np.around(np.subtract(image, LoGim))
            return sharp_image, LoGim
            
        for i in range(image.shape[2]):
            output[:,:,i], logim[:,:,i] = image_sharpenning(image[:,:,i], theta, size, method = 'log')
    #        output.append(out)
    #        logim.append(log)
            mag[:,:,i] = sobel_filter(output[:,:,i])
            
        return np.round(mag), output, logim        

    def crop(self):
        """r
        crops the registered stack to eliminate areas with no data. Crops stack 
        borders to largest possible area containing data points.
        """
        n,m,ns = self.rawdata.shape
        if np.max(np.ceil(self.y_fit)) > 0.:
            up = np.max(np.ceil(self.y_fit))
        else:
            up = 0.
        if np.min(np.floor(self.y_fit)) < 0.:
            bottom = np.min(np.floor(self.y_fit))
        else:
            bottom = n
        if np.max(np.ceil(self.x_fit)) > 0.:
            left = np.max(np.ceil(self.x_fit))
        else:
            left = 0.
        if np.min(np.floor(self.x_fit)) < 0.:
            right = np.min(np.floor(self.x_fit))
        else:
            right = m
        self.cropped = self.registered_fit[up:(n + bottom),left:(m + right),:]
           
if __name__ == '__main__':
    from stxmio import *
    filename = '/home/araash/PyFCell/trunk/data/STXM/532_130607016/532_130607016.hdr'
    
    lastLoad = hdr.load_stack(filename, legacy=True)

#    regist = Registration(lastLoad['stack'])
    
    regist = Registration(lastLoad['stack'], Method = 'sift', Filter = 'No Filter')
    Registration()

#    print (regist.reg)
#    reg = sift(lastLoad['stack'])
#    
#    plb.figure()
#    plb.gray()
#    plb.subplot(221)
#    plb.imshow(raw_data[:,:,0])
#    plb.subplot(222)
#    plb.imshow(raw_data[:,:,90])
#    plb.subplot(223)
#    plb.imshow(reg_fit[0])
#    plb.subplot(224)
#    plb.imshow(reg_fit[90])
#
#
#    plb.show()  