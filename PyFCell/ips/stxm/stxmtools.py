r"""
*****************************************************************************
:mod:`PyFCell.ips.stxm.stxmtools` -- Graphical User Interface for stxm image processing
*****************************************************************************

.. module:: PyFCell.ips.stxm

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""

from __future__ import print_function
import re
import numpy as np
import pyqtgraph as pg
from scipy.interpolate import UnivariateSpline

def optical_density(I0, raw_image_stack, lineI0 = False):
    """r
    Converts pixel values from intensity to optical density [OD]
    
    I0, raw_image_stack, lineI0 
    
    Parameters
    ----------
    I0 : 3D array
        Array of the selected ROI to be used for OD calculation
        
    raw_image_stack : 3D array
        Array of raw images with intensity unit [I]
        
    lineI0 : Boolean, optional, default  = False
        If False, average of the selected ROI will be used. If True, each line
        of the raw data will be normalized based on the respected line in the selected ROI region. 
    """

    ''' the transposes accomodate some conventions regarding image display. we preserve [x, y, z] format.
    '''
    if not lineI0:
        norm = [I0_slice.mean()/raw_slice.T for I0_slice, raw_slice in zip(I0, raw_image_stack.T)]
        log_adjusted = np.log(norm)
        normalized_stack = np.dstack(log_adjusted)
        return normalized_stack
        
    else:
        norm =np.zeros(raw_image_stack.shape)
        if I0.shape[0] > I0.shape[1]:
            vstack = False
            if I0.shape[0] != raw_image_stack.shape[0]:
                print ('Should have the same horizontal size')
                return
        else:
            vstack = True
            if I0.shape[1] != raw_image_stack.shape[1]:
                print ('Should have the same vertical size')
                return
        for imnum in range(raw_image_stack.shape[2]):
            if vstack:
                for linenum in range(I0.shape[1]):
                    norm[:,linenum,imnum] = np.mean(I0[:,linenum,imnum])/raw_image_stack[:,linenum,imnum]
            else:
                for linenum in range(I0.shape[0]):
                    norm[linenum,:,imnum] = np.mean(I0[linenum,:,imnum])/raw_image_stack[linenum,:,imnum]                
        log_adjusted = np.log(norm)        
        return log_adjusted
#    log_adjusted = np.log(norm)
#    normalized_stack = np.dstack(log_adjusted)
#    return normalized_stack                
    
def explore(stack):
    import matplotlib.pyplot as plt
    import matplotlib.widgets as wdg
    from bisect import bisect
    plt.rcParams['image.cmap'] = 'copper'
    plt.rcParams['image.interpolation'] = 'nearest'

    try:
        img_data = stack['FilteredData']
    except KeyError:
        img_data = stack['ImageData']

    fig, ax = plt.subplots()
    img_ax = ax.imshow(img_data[:,:,0].T, vmin=img_data.min(), vmax=img_data.max())
    def update(x):
        i = bisect(stack['EnergyLevels'], x)-1
        img_ax.set_data(img_data[:,:,i].T)
        plt.draw()
    fig.subplots_adjust(bottom=0.2)
    sld_ax = fig.add_axes([0.3,0.1,0.4,0.05])
    sld = wdg.Slider(sld_ax, 'eV', stack['EnergyLevels'].min(), stack['EnergyLevels'].max(), stack['EnergyLevels'].min())
    sld.on_changed(update)
    plt.show()

def load_spectrum(path):
    """r
    Loads spectrum from *.txt files. returns x and y axes of the spectrum.txt 
    file together with comments stored in the file header. Comments are returned as 
    a dictionary with following keys:
    
    "dims" = dimesion of the file , 1D.
    "fname" = name of the sample
    "len" = number of data points
    "source" = user comments
    
    path 
    
    Parameters
    ----------
    path : string
        path of the spectrum to be opened
    """
    global as_lst, raw
    with open(path) as f:
        raw = f.read()

    comments = re.findall(r"%[ ]+(.*)\n", raw)
    meta = dict(zip(["dims", "fname", "len", "source"], [int(entry) if entry.isdigit() else entry for entry in comments ]))
    raw = '\n'.join([line for line in raw.split('\n') if not line.startswith('%')])
    as_lst = re.findall('-*\d+\.\d*(?:[Ee]-\d+)?', raw)
    
#    as_lst = re.findall(r"\t(.*)\t\t(.*)\n", raw)
    x = np.array(as_lst[::2], dtype=np.float)
    y = np.array(as_lst[1::2], dtype=np.float)
    return x, y, meta

def save_spectrum(path, arr, **meta):
    """r
    Saves spectrum as *.txt files. returns x and y axes of the spectrum.txt 
    file together with comments stored in the file header. Comments are returned as 
    a dictionary with following keys:
    
    "dims" = dimesion of the file , 1D.
    "fname" = name of the sample
    "len" = number of data points
    "source" = user comments
    
    path, arr, meta 
    
    Parameters
    ----------
    path : string
        path used to save the spectrum
        
    arr : array like
        2D array of spectrum x and y axes
        
    meta : comments to be written in the file header according to following keys
        "dims" = dimesion of the file , 1D.
        "fname" = name of the sample
        "len" = number of data points
        "source" = user comments
    """
    arr = np.vstack(arr).T

    with open(path, 'w') as f:
        f.write("% {}\n".format(meta.get('dims', "1d") ) )
        f.write("% {}\n".format(meta.get('fname', path)))
        f.write("% {}\n".format(meta.get('len', len(arr))))
        f.write("% {}\n".format(meta.get('source')))
        for x,y in arr:
            f.write("\t{:.3f}\t\t{:.3f}\n".format(x,y))

def spectra_interp(od1x, od1y, energy):
    """r
    Interpolates spectrum to a given energy range. returns x and y axes of the interpolated spectrum.
    scipy.interpolate.UnivariateSpline with 3rd order fit and zero smoothing factor is used for this purpose.
    
    od1x, od1y, energy
    
    Parameters
    ----------
    od1x : array like
        x axis of the original spectrum.
        
    od1y : array like
        x axis of the original spectrum.
        
    energy : array like
        energy array to be used for interpolation.
    """
    x = od1x
    y = od1y
    s = UnivariateSpline(x, y, s=0, k=3)
    xs = np.array(energy, dtype=np.float)
    ys = np.array(s(xs), dtype=np.float)
    return xs, ys
    
def fit_map(data, spectra):
#    global fitted
    """r
    Breaks data to constructing constituents based on given finger print spectras. 
    This is performed by computing least-squares solution to equation Ax=b such that
    |b - Ax| is minimized. A matrix is constructed based on given finger print spectras
    assuming that a constant spectra is also present in the solution (Pt).
    
    Returns 3D array of constituent maps. Each image in theis stack represents one component
    with the same sequence as the input spectra input. The final component present in the
    component map represents the constant spectra. This method also returns the residuals of the fit.
        
    numpy.linalg.lstsq package has been used to solve the minimization process.
    
    data, spectra
    
    Parameters
    ----------
    data : 3D array
        Data array containing the chemical based information in [OD] unit.
        
    spectra : array like
        finger print constituent spectras packed in a signle array. 
    """
#    ''' We do a least square fit to give us the expected weight
#    of each element at each pixel-point
#    '''
    A = np.vstack([spectra, np.ones(len(spectra[0]))] ).T
    nx, ny = data.shape[:2]
    nz = len(spectra) + 1
    M = np.zeros([nx, ny, nz]) # flat canvas for drawing
    res = np.zeros([nx, ny])
    fitted = np.zeros(data.shape)
    diff = np.zeros(data.shape)
    sum_diff = np.zeros([nx, ny])
    for (x,y), value in np.ndenumerate(M[:,:,0]):
        B = data[x, y, :]
        M[x, y, :] = np.linalg.lstsq(A, B)[0]
        res[x,y] = np.linalg.lstsq(A, B)[1]

#   calculate the fitted tsack using calculated fit components        
    for (x,y), value in np.ndenumerate(M[:,:,0]):
        fitted[x,y,:] = np.dot(M[x, y, :len(spectra)], spectra) + M[x, y, -1]
    
#   calculating the difference btw fitted and data stack
    for i in range(data.shape[2]):
        diff[:,:,i] = np.subtract(data[:,:,i], fitted[:,:,i])
        
#   sum of differences
    for (x,y), value in np.ndenumerate(data[:,:,0]):
        sum_diff[x,y] = np.sum(diff[x,y,:])
    
    return M, res, fitted, diff, sum_diff

def RGB_Map(comp_map):
    """r
    Constructs an RGB image based on fitted components. It currently only works 
    with 3 fitted components. Returns the RGB image.
    
    comp_map
    
    Parameters
    ----------
    comp_map : 3D array
        Array of constituent components. first 3 images will be used to construct the RGB image.
    """
    n = 512
    tt = np.zeros((n,3))
    
    #Red
    tt[:,0] = 0
    tt[:,1] = 0
    tt[:,2] = np.round(np.linspace(0,255,n))
    red = pg.makeARGB(comp_map[:,:,2],lut = tt,levels = [np.min(comp_map[:,:,2]), np.max(comp_map[:,:,2])])
    
    #Blue
    tt[:,0] = np.round(np.linspace(0,255,n))
    tt[:,1] = 0
    tt[:,2] = 0
    blue = pg.makeARGB(comp_map[:,:,1],lut = tt,levels = [np.min(comp_map[:,:,1]), np.max(comp_map[:,:,1])])
    
    #Green
    tt[:,0] = 0
    tt[:,1] = np.round(np.linspace(0,255,n))
    tt[:,2] = 0
    green = pg.makeARGB(comp_map[:,:,0],lut = tt,levels = [np.min(comp_map[:,:,0]), np.max(comp_map[:,:,0])])
    
    
    image = red[0]
    image[:,:,1] = green[0][:,:,1]
    image[:,:,2] = blue[0][:,:,2]
    return image

def std_dev(region):
    """r
    Calculates standard deviation of the given region. Returns Standard deviaion of the input.
    
    region
    
    Parameters
    ----------
    region : 3D array
        Calculate the standard deviation of this region. Standard deviation is 
        calculated for each image of the stack.
    """
    region_flatten = []
    for imnum in range(region.shape[2]):
        region_flatten.append(region[:,:,imnum].flatten())
    region_flatten = np.vstack(region_flatten).T
    stdv = np.std(region_flatten, axis = 0)  
    return stdv

def define_unit(unit1, unit2, op):
    """r
    Defines the unit of the output operation. Returns unit of the result.
    
    unit1, unit2, op
    
    Parameters
    ----------
    unit1 : String
        Unit of the first image
    
    unit2 : String
        unit of the second image
        
    op : String
        Operation. *, /, -, +
        
    """    
    if unit1==unit2:
        if unit1 == 'Value':
            runit = 'Value'
        else:
            if op == '*':
                runit = unit1+'*'+unit2
            elif op == '/':
                runit = 'Value'
            elif op == '+' or op == '-':           
                runit = unit1 
    else:
        if unit1 == 'Value':
            runit = unit2
        elif unit2 == 'Value':
            runit = unit1
        else:
            runit = unit1+op+unit2
    return runit
    
if __name__=='__main__':
    from stxmio import *    
    path='/home/araash/Desktop/Image Processing/STXM/Data/532_130710058/532_130710058.hdr'
    lastLoad = hdr.load_stack(path, legacy=True)
#    eV, i0 = load_spectrum(path='/home/harday/PyFCell/trunk/PyFCell/ips/stxm/v2/spectra/i0_printed-stack-with-i0-c1s.tda')
    I0_stack = lastLoad['stack'][0:1,:,:]
    
    filtered = optical_density(I0_stack, lastLoad['stack'],lineI0 = True)
#    explore(filtered)

    # load_stack(hdr_path='/home/harday/PyFCell/trunk/data/STXM/Sample Test/printed-stack-with-i0-c1s/532_130125101/532_130125101.hdr')