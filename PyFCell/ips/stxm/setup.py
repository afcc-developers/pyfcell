# -*- coding: utf-8 -*-
"""
Created on Mon Jul 07 17:00:37 2014

@author: araash
"""

from distutils.core import setup
import py2exe   

setup(scripts=['stxmgui.py'], options={"py2exe": {"includes":["sip", "PyQt4.QtGui"]}})