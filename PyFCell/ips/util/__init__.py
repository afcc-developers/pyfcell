# Author: Mayank Sabharwal
# License: TBD
# Copyright (c) 2013, AFCC
"""
**************************************************************************
:mod:`PyFCell.ips.util` -- Utility module for Image Processing Suite
**************************************************************************

This module consists of support libraries required for various IO and Image Processing Operations

"""



from ImageSequence import ImageSequence
from GridGenerator import GridGenerator
from ImageTools import *
import clahe
try:
    from sift_matching import sift_matching
except:
    print "SIFT Matching not available. Install OpenCV 2.4.6.*"
