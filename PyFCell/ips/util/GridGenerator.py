# -*- coding: utf-8 -*-
r"""
*************************************************************************
:mod:`PyFCell.ips.util.GridGenerator` -- Utilities for IPS Classes
*************************************************************************

Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""
import numpy as np
import PyFCell
try:
    from tvtk.api import tvtk
except:
    try:
        from enthought.tvtk.api import tvtk
    except:
        print "TVTK package not found"


class GridGenerator(PyFCell.util.baseobject):
    r"""
    Input reader for various file formats.
    Formats currently supported are tiff stacks, sequence of images (png,jpg,gif,tiff), MRC
    image,filename
    
    Parameters
    ----------
    image : ndarray
        3D image to be written to a VTK file
    
    filename : string
        Output filename for the VTK structure
       
    
    
    Examples
    --------
    
    >>> import PyFCell as PFC
    >>> import PyFCell.ips.util as util
    >>> crunch = util.GridGenerator(image,filename)
    >>> crunch.write()
    
    .. warning:: This documentation is not yet finished.
    
    .. todo:: 1. Finish documentation
        
    """
    def __init__(self,image, filename, **kwargs):
        
        super(GridGenerator,self).__init__(**kwargs)            
        self.image=image
        xdim,ydim,zdim=np.shape(image)
        self.filename=filename
        x,y,z=np.mgrid[0:xdim+1,0:ydim+1,0:zdim+1]        
        self.x=x.flatten()
        self.y=y.flatten()
        self.z=z.flatten()
        self._indent='\t'
        
    
    def generatePoints(self):

        points=np.array(np.dstack([self.x,self.y,self.z]),'f')
        points=points.reshape(self.x.shape[0],3)
        
        return points

    
    def generate3dCells(self,points):
        
        boundary =  np.max(self.z)
        count = self.x.shape[0]
        
        a=np.max(self.z)+1
        b=(1+np.max(self.z))*(np.max(self.y)+1)

        
        k = np.arange(count)
        i2=np.asarray([i for i in k if boundary not in points[i]])
        i1=np.ones(np.size(i2))*8
        i3=np.asarray([i+b for i in i2])
        i4=np.asarray([i+a+b for i in i2])
        i5=np.asarray([i+a for i in i2])
        i6=np.asarray([i+1 for i in i2])
        i7=np.asarray([i+b+1 for i in i2])
        i8=np.asarray([i+a+b+1 for i in i2])
        i9=np.asarray([i+a+1 for i in i2])
        
        cellcount=(np.max(self.x))*(np.max(self.y))*(np.max(self.z))
        
        cells=np.array(np.dstack([i1,i2,i3,i4,i5,i6,i7,i8,i9]))
        cells=cells.reshape(i1.shape[0],9)
        
        
        cells=cells.flatten()
        hex_type = tvtk.Hexahedron().cell_type # VTK_HEXAHEDRON == 12
        
        cell_types=np.ones(cellcount)*hex_type
        
        offset=np.arange(0,cells.shape[0],9)
        
        
        cell_data=self.image
        cell_data=cell_data.flatten()
        
        return [cell_types, offset, cellcount, cells, cell_data]
        
    
    def generate2dCells(self,points):
        
        boundary =  np.max(self.z)
        count = self.x.shape[0]
        k = np.arange(count)
        
        
        x=self.x.reshape(self.x.shape[0],1)
        y=self.y.reshape(self.y.shape[0],1)
        z=self.z.reshape(self.z.shape[0],1)
        
        a=np.max(z)+1
        b=(1+np.max(z))*(np.max(y)+1)        

        #Y-Z Plane boundaries
        
        
        ib2=np.asarray([i for i in k if 0 in x[i] and boundary not in points[i]])
        ib1=np.ones(np.size(ib2))*4
        ib3=np.asarray([i+1 for i in ib2])
        ib4=np.asarray([i+a+1 for i in ib2])
        ib5=np.asarray([i+a for i in ib2])
        data1=np.ones(ib1.shape[0])
        
        ob2=np.asarray([i for i in k if boundary in x[i] and boundary not in y[i] and boundary not in z[i]])
        ob1=np.ones(np.size(ob2))*4
        ob3=np.asarray([i+1 for i in ob2])
        ob4=np.asarray([i+a+1 for i in ob2])
        ob5=np.asarray([i+a for i in ob2])
        data2=np.ones(ob1.shape[0])*2
            
        
        innercellsX=np.array(np.dstack([ib1,ib2,ib3,ib4,ib5]))
        innercellsX=innercellsX.reshape(ib1.shape[0],5)
        
        outercellsX=np.array(np.dstack([ob1,ob2,ob3,ob4,ob5]))
        outercellsX=outercellsX.reshape(ob1.shape[0],5)
        
        #X-Z Plane boundaries
        
        
        ib2=np.asarray([i for i in k if 0 in y[i] and boundary not in points[i]])
        ib1=np.ones(np.size(ib2))*4
        ib3=np.asarray([i+1 for i in ib2])
        ib4=np.asarray([i+b+1 for i in ib2])
        ib5=np.asarray([i+b for i in ib2])
        data3=np.ones(ob1.shape[0])*3
        
        ob2=np.asarray([i for i in k if boundary in y[i] and boundary not in x[i] and boundary not in z[i]])
        ob1=np.ones(np.size(ob2))*4
        ob3=np.asarray([i+1 for i in ob2])
        ob4=np.asarray([i+b+1 for i in ob2])
        ob5=np.asarray([i+b for i in ob2])
        data4=np.ones(ob1.shape[0])*4      
            
        
        innercellsY=np.array(np.dstack([ib1,ib2,ib3,ib4,ib5]))
        innercellsY=innercellsY.reshape(ib1.shape[0],5)
        
        outercellsY=np.array(np.dstack([ob1,ob2,ob3,ob4,ob5]))
        outercellsY=outercellsY.reshape(ob1.shape[0],5)
        
        #X-Y Plane boundaries
        
        
        ib2=np.asarray([i for i in k if 0 in z[i] and boundary not in points[i]])
        ib1=np.ones(np.size(ib2))*4
        ib3=np.asarray([i+b for i in ib2])
        ib4=np.asarray([i+b+a for i in ib2])
        ib5=np.asarray([i+a for i in ib2])
        data5=np.ones(ob1.shape[0])*5

        
        ob2=np.asarray([i for i in k if boundary in z[i] and boundary not in x[i] and boundary not in y[i]])
        ob1=np.ones(np.size(ob2))*4
        ob3=np.asarray([i+b for i in ob2])
        ob4=np.asarray([i+b+a for i in ob2])
        ob5=np.asarray([i+a for i in ob2])
        data6=np.ones(ob1.shape[0])*6     
            
        
        innercellsZ=np.array(np.dstack([ib1,ib2,ib3,ib4,ib5]))
        innercellsZ=innercellsZ.reshape(ib1.shape[0],5)
        
        outercellsZ=np.array(np.dstack([ob1,ob2,ob3,ob4,ob5]))
        outercellsZ=outercellsZ.reshape(ob1.shape[0],5)
        
        cells=np.vstack([innercellsX,outercellsX,innercellsY,outercellsY,innercellsZ,outercellsZ])
        cellcount=cells.shape[0]
        cells=cells.flatten()
        
                
        quad_type=tvtk.Quad().cell_type
        offset=np.arange(0,cellcount,5)
        
        cell_types=np.ones(cellcount)*quad_type
               
        cell_data=np.vstack([data1,data2,data3,data4,data5,data6])
        cell_data=cell_data.flatten()
        
        return [cell_types, offset, cellcount, cells, cell_data]
    


    def writeUnstructuredGrid(self,points, cell_types, offset, cell_array, cellData):
        
        ug = tvtk.UnstructuredGrid(points=points)
        ug.set_cells(cell_types, offset, cell_array)
        ug.cell_data.scalars=cellData
        ug.cell_data.scalars.name='MaterialID'
        
        writer=tvtk.UnstructuredGridWriter()
        writer.file_name=self.filename
        writer.input=ug
        writer.update()
        writer.write()
    
    def write(self):
        print self._indent, "="*50
        print self._indent, "=  Writing a VTK file"
        points=self.generatePoints()
        [cell_types1, offset1, cellcount1, cells1, cellData1] = self.generate3dCells(points)
        [cell_types2, offset2, cellcount2, cells2, cellData2] = self.generate2dCells(points)
      
        cell_types=np.append(cell_types1, cell_types2)
        offset=np.append(offset1, offset2)
        cellcount= cellcount1 + cellcount2
        
        cells = np.append(cells1,cells2)
        cells = cells.flatten()
        
        cellData=np.append(cellData1, cellData2)
        cellData=cellData.flatten()
        
        cell_array = tvtk.CellArray()
        cell_array.set_cells(cellcount, cells)
    
        self.writeUnstructuredGrid(points, cell_types, offset, cell_array, cellData)
        print self._indent, "= The file has been successfully written!"
        print self._indent, "-"*50
