# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2012, AFCC
r"""
*************************************************************************
:mod:`PyFCell.ips.util.ImageSequence` -- Utilities for IPS Classes
*************************************************************************


Contents
========
The PyFCell package imports all the functions from the top level modules. 
 
Import
======
>>> import PyFCell as pfc


Submodules
==========

Classes
=======

"""

import PyFCell

class ImageSequence(PyFCell.util.baseobject):
    r"""
    Input reader for various file formats.
    Formats currently supported are tiff stacks, sequence of images (png,jpg,gif,tiff), MRC
    im
    
    Parameters
    ----------
    image : reference/handle to a image
        
    Examples
    --------
    
    >>> import PyFCell as PFC
    >>> import PyFCell.ips.util as util
    >>> im = Image.open('dummy.tiff')
    >>> for frame in util.ImageSequence(im):
    >>>     tmp.append(frame)
    
    .. warning:: This documentation is not yet finished.
    
    .. todo:: 1. Finish documentation
        
    """
    def __init__(self,im, **kwargs):
        
        super(ImageSequence,self).__init__(**kwargs)
        self.im=im
    def __getitem__(self, ix):
        try:
            if ix:
                self.im.seek(ix)
            return self.im
        except EOFError:
            raise IndexError 
            
            