#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2012, TBD

r"""
************************************************************************************
:mod:`PyFCell.ips.util.ImageTools` -- Image Tools module for Image Processing Suite
************************************************************************************


Contents
========
The PyFCell package imports all the functions from the top level modules. Module for some useful tools for Image Processing.
 
Import
======
>>> import PyFCell as pfc

Submodules
==========

Classes
=======

"""

__all__ = ["get_imlist", "imresize", "histeq", "gaussian_blur", "image_derivative", "number_objects", "save_image", "crop", "denoise"] 

import os
from PIL import Image
#import scipy.ndimage as sp
#import scipy.misc
from scipy.ndimage import filters, measurements
import numpy as np
import scipy
#import scipy.io
#import scipy.interpolate
#import PyFCell

 
r"""

Module for some useful tools for Image Processing
    
 
.. warning:: This documentation is not yet finished.

.. todo:: Finish documentation
        
"""    
        
   
def get_imlist(path):
    r"""
    return the list of filenames for all tif images in a directory
    
    path
    
    Parameters
    ----------
    path : string
    
    """
    return[os.path.join(path,f) for f in os.listdir(path) if f.endswith('.tif')]
        
def imresize(im, sz):
    r"""
    Reize and image array using PIL.
    
    im, sz
    
    Parameters
    ----------
    im : 2D array
    
    sz : [pixel,pixel]
    
    """
    
    pil_im = Image.fromarray(np.uint8(im))
    return np.array(pil_im.resize(sz))
    
def histeq(im, nbr_bins=256):
    r"""
    Returns histogram equalization of a grayscale image and cumulative distribution function      
    
    Parameters
    ----------
    im : 2D array
    
    nbr_bins : number of bins(optional)
    
    
    Examples
    --------
    >>> import PyFCell.ips.util.ImageTools as imtool
    >>> import PyFCell.ips.inout as io
    >>> image=io.reader("../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
    >>> tmp=[]
    >>> tmp.append(image[:,:,0])
    >>> im_histeq, cdf = imtool.histeq(tmp[0])
    >>> tmp.append(np.asarray(im_histeq, dtype = np.uint8))
        
    .. plot::
                
        import pylab as plb
        import numpy as np
        import PyFCell 
        import PyFCell.ips.util.ImageTools as imtool
        import PyFCell.ips.inout as io
        image=io.reader("../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
        tmp=[]
        tmp.append(np.asarray(image[:,:,0],dtype=np.uint8))
        im_histeq, cdf = imtool.histeq(tmp[0])
        tmp.append(np.asarray(im_histeq, dtype = np.uint8))
        
        plb.subplot(1,2,1)  
        plb.gray()    
        plb.imshow(tmp[0])
        plb.title("Original Image")
        plb.subplot(1,2,2)
        plb.imshow(tmp[1])
        plb.title("Histogram Equalization")
        plb.show()
    
    """
    # Get image histogram
    imhist,bins = np.histogram(im.flatten(),nbr_bins,normed=True)
    cdf = imhist.cumsum() # cumulative distribution function
    cdf = 255 * cdf / cdf[-1] # Normalize    
    
    # Use linear interpolation of cdf to find new pixel values
    im2 = np.interp(im.flatten(), bins[:-1],cdf)
    
    return im2.reshape(im.shape), cdf
        
def gaussian_blur(im, theta):
    r""" 
    Blurs the image using Gaussian filter with a given standard deviation (Theta)
    
    im, theta       
    
    Parameters
    ----------
    
    im : 2D array
    theta : float > 0      
    
    """
    return filters.gaussian_filter(im, theta)
    
        
def image_derivative(im):
    r"""
    Returns: image derivative in x and y direction and magnitude of the gradient
    
    im     
    
    Parameters
    ----------
    
    Im : 2D array
    
    Examples
    --------
    >>> import PyFCell.ips.util.ImageTools as imtool
    >>> import PyFCell.ips.inout as io
    >>> image=io.reader("../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
    >>> tmp=[]
    >>> tmp.append(image[:,:,0])
    >>> imx, imy, mag = imtool.image_derivative(tmp[0])
    >>> tmp.append(np.asarray(imx))

        
    .. plot::
                
        import pylab as plb
        import numpy as np
        import PyFCell 
        import PyFCell.ips.util.ImageTools as imtool
        import PyFCell.ips.inout as io
        image=io.reader("../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
        tmp=[]
        tmp.append(np.asarray(image[:,:,0],dtype=np.uint8))
        imx, imy, mag = imtool.image_derivative(tmp[0])
        tmp.append(np.asarray(imx))
        tmp.append(np.asarray(imy))
        tmp.append(np.asarray(mag))
    
        plb.subplot(2,2,1) 
        plb.gray()    
        plb.imshow(tmp[0])
        plb.title("Original Image")
        plb.subplot(2,2,2)
        plb.imshow(tmp[1])
        plb.title("x derivative")
        plb.subplot(2,2,3)
        plb.imshow(tmp[2])
        plb.title("y derivative")
        plb.subplot(2,2,4)
        plb.imshow(tmp[3])
        plb.title("Magnitude")
        plb.show()
        
    """
    # Sobel derivatives
    imx = np.zeros(im.shape)
    filters.sobel(im,1,imx)
    imy = np.zeros(im.shape)
    filters.sobel(im,0,imy)
    magnitude = np.sqrt(imx**2+imy**2)
    return imx, imy, magnitude
    
def number_objects(binaryimage):
    r""" 
    lables the pixels according to the object they belong to and counts the number of objects
    
    binaryimage     
    
    Parameters
    ----------
    binaryimage : 2D array
    
    """
    
    labels, nbr_objects = measurements.label(binaryimage)
    return labels, nbr_objects
    
def save_image(name, im):
    r""" 
    Saves arrays into image files in the working directory
    
    name, im       
    
    Parameters
    ----------
    name : string with desired image type
    
    im : 2D array        
    
    """
    scipy.misc.imsave(name, im)  
    return 

def crop(image):
    r""" 
    Automatically crops black boarders of stack of images. The size of the 
    cropped image will be such it could fit the smallest image of the stack. 
    
    image
    
    Parameters
    ----------
    
    image : 3D array
    
    """
    n, m, nslice = image.shape
    #Initializing
    up,bottom,left,right = list(),list(),list(),list()
    
    for sn in range(nslice):
    # y direction
        for i in range(2,n/2):
            # takes a 2 point neighbourhood to find boarders of the image
            if image[i,m/2,sn] != 0 and image[i+1,m/2,sn] > 0 and image[i+2,m/2,sn] > 0 and image[i-1,m/2,sn] == 0 and image[i-2,m/2,sn] == 0:
                up.append(i)
        for i in range(n/2,n-2):
            if image[i,m/2,sn] != 0 and image[i+1,m/2,sn] == 0 and image[i+2,m/2,sn] == 0 and image[i-1,m/2,sn] > 0 and image[i-2,m/2,sn] > 0:
                bottom.append(i)
        # x direction
        for i in range(2,m/2):
        # takes a 2 point neighbourhood to find boarders of the image
            if image[n/2,i,sn] != 0 and image[n/2,i+1,sn] > 0 and image[n/2,i+2,sn] > 0 and image[n/2,i-1,sn] == 0 and image[n/2,i-2,sn] == 0:
                left.append(i)
        for i in range(m/2,m-2):
            if image[n/2,i,sn] != 0 and image[n/2,i+1,sn] == 0 and image[n/2,i+2,sn] == 0 and image[n/2,i-1,sn] > 0 and image[n/2,i-2,sn] > 0:
                right.append(i)
                
    up_crop, bottom_crop, left_crop, right_crop = max(up), min(bottom), max(left), min(right)           
    
    return image[up_crop:bottom_crop,left_crop:right_crop,:]  
        
        
def denoise(im, U_init, tolerance = 0.1, tau = 0.125, tv_weight = 100):
    r"""
    An implementation of Rudin-Osher-Fatemi (ROF) denoising model using eq(11)
    in A. Chambolle (2005). Returns denoised image and texture residual. 
    
    im, U_init, tolerance = 0.1, tau = 0.125, tv_weight = 100
    
    Parameters
    ----------
    
    im : 2D array, noisy input image (grayscale)

    U_init : 2D array, initial guess for denoised image 

    tolerance : tolerance for stop criterion (optional)

    tau : steplength (optional)

    tv_weight : weight of the total variation regularizing term (optional)
    
    
    Examples
    --------
    >>> import PyFCell.ips.util.ImageTools as imtool
    >>> import PyFCell.ips.inout as io
    >>> image=io.reader("../../../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
    >>> tmp=[]
    >>> tmp.append(image[:,:,0])
    >>> tmp.append(np.asarray(imtool.gaussian_blur(image[:,:,0],2)))
    >>> de_im, U = imtool.denoise(image[:,:,0],blr_im)
    >>> tmp.append(np.asarray(de_im,dtype=np.int8))
    >>> res=np.asarray(np.dstack(tmp),dtype=np.int8)
    >>> io.writer(res,'denoise.tif')
        
    .. plot::
                
        import pylab as plb
        import numpy as np
        import PyFCell 
        import PyFCell.ips.util.ImageTools as imtool
        import PyFCell.ips.inout as io
        image=io.reader("../data/FIBSEM/Carmen/Sample14_Stack_cropped.tif").getarray()
        tmp=[]
        tmp.append(np.asarray(image[:,:,0],dtype=np.uint8))
        tmp.append(np.asarray(imtool.gaussian_blur(image[:,:,0],2),dtype = np.uint8))
        de_im, U = imtool.denoise(image[:,:,0],tmp[1])
        tmp.append(np.asarray(de_im))
        
        plb.subplot(1,2,1) 
        plb.gray()    
        plb.imshow(tmp[0])
        plb.title("Original Image")
        plb.subplot(1,2,2)
        plb.imshow(tmp[1])
        plb.title("Gaussian Blur")
        plb.figure()
        plb.imshow(tmp[2])
        plb.title("Denoised Image")
        plb.show()
        
    """
    m,n = im.shape # size of noisy image
    
    # initialize
    U = U_init
    Px = im #x-component of dual field
    Py = im #y-component of dual field
    error = 1
    
    while (error > tolerance):
        Uold = U
        
        # gradient of primal variable
        GradUx = np.roll(U, -1, axis=1)-U#x-component of U's gradient
        GradUy = np.roll(U, -1, axis=0)-U#y-component of U's gradient
        
        # update the dual variable
        PxNew = Px + (tau/tv_weight)*GradUx
        PyNew = Py + (tau/tv_weight)*GradUy
        NormNew = np.maximum(1, np.sqrt(PxNew**2+PyNew**2))
        
        Px = PxNew/NormNew # x-component update (dual)
        Py = PyNew/NormNew # y-component update (dual)
        
        # update the primal variable
        
        RxPx = np.roll(Px,1,axis=1)# right x-translation of x-component
        RyPy = np.roll(Py,1,axis=0)# right y-translation of y-component
        
        DivP = (Px-RxPx)+(Py-RyPy)# divergence of the dual field
        U = im + tv_weight * DivP # update of primal variable
    
        # update of error
        error = np.linalg.norm(U-Uold)/np.sqrt(n*m);
            
    return U, im-U # denoised image and texture residual
