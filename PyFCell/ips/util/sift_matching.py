# -*- coding: utf-8 -*-
"""
Created on Wed May 22 10:10:28 2013

@author: abhatka
"""

import cv2
import numpy as np
import itertools
#import sys
#import Image
import matplotlib.pyplot as plt

class sift_matching() :
    r"""
     
      sif_matching(self,orig_img,template_img, distace=10 ,save_slices='OFF',show_location='ON' ,display_match='OFF' , region_detect='OFF') 
        
      Template matching in the original image using SIFT feature recognition
 
      Inherits input-reader from PyFCell.ips.inout module
      
      Parameters
      ----------
      orig_img    : string
        Filename of the source/original image to import.

      template_img    : string
        Filename of the source/original image to import.
      
      distance    : integer
        The minimum distance between the descriptors to be shortlisted.Default is 10   
      
      save_slices    : string
        If 'ON' saves individual slices with lines joining matchin points . Default is 'OFF'

      show_location    : string
        If 'ON' shows a masked white region in the original image where the template is located. Default is 'ON'     
      
      display_match    : string
        If 'ON' displays the images with lines joining the matched points. Default is 'OFF'

      region_detect    : string
        If 'ON' displays the images with points marked in the region where template resides in the image. Default is 'OFF'       
      
      Examples
      --------
      >>> match=template_match('DataSet1.tif','DataSet2.tif',distance=10)
      >>> print match.orig_pts , match.template_pts
     """
     
     
    def __init__(self, orig_img,template_img,distance=10,save_slices='OFF',show_location='ON' ,display_match='OFF' , region_detect='OFF',tagname='default',**kwargs):
        
        orig_img=orig_img
        self.tag = tagname
        template_img=template_img
        self.distance=distance
        self.orig_pts,self.template_pts,self.rangex,self.rangey=self.run_SIFTmatch(orig_img,template_img, save_slices ,show_location ,display_match , region_detect)
        
        print "\n\nThe template image is located in the original image in the following range of coordinates : \n"
        print "X start : ",self.rangex[0]," to", " X end : ",self.rangex[1]
        print "\nY start : ",self.rangey[0]," to", " Y end : ",self.rangey[1]

        
    def run_SIFTmatch(self,orig_img,template_img, save_slices, show_location, display_match, region_detect):
        
    
        image1=orig_img
        image2=template_img
        try:
            imnum=image1.shape[2] # Just set the value of imnum = 1 if there is need to run only one slice
        except(IndexError):
            imnum=1
        #imnum=3
        
        minx=[]
        maxx=[]
        miny=[]
        maxy=[]
        image1 = image1.reshape([image1.shape[0],image1.shape[1],imnum])
        image2 = image2.reshape([image2.shape[0],image2.shape[1],imnum])
        for i in range(0,imnum):
            
            
            
            origimg=image1[:,:,i]
            template=image2[:,:,i]
            
            #origimg=origimg[0:500,0:2048]
            #template=template[0:500,0:2048]
            
            skp,tkp=self._findKeyPoints(origimg, template, self.distance)
            newimg, x1, x2, y1, y2, orig_pts, template_pts = self._getregion(origimg, template, skp, tkp, display_match, region_detect) 
            minx.append(x1)
            maxx.append(x2)
            miny.append(y1)
            maxy.append(y2)
            
            if save_slices.upper()=='ON':
                cv2.imwrite('newimg'+str(i)+'.tif',newimg)
        
        #print np.average(minx),np.average(maxx),np.average(miny),np.average(maxy)
        
        rangey=[np.average(miny),np.average(maxy)] 
        rangex=[np.average(minx),np.average(maxx)]
        
        if show_location.upper()=='ON' : # Masks the area (colours black) where the template is supposed to be located in the original image
            
            finalimage=np.array(image1[:,:,0])
            
            plt.figure()
            #finalimage[rangey[0]:rangey[1],rangex[0]:rangex[1]]=255
            plt.gray()    
            x=rangex[0]
            width=rangex[1]-rangex[0]
            height=rangey[1]-rangey[0]
            y=rangey[0]
            plt.imshow(finalimage)
            currentAxis = plt.gca()
            currentAxis.add_patch(plt.Rectangle((x,y), width, height, facecolor='none',fill='False',linestyle='solid',linewidth=1, ec='red'))
        
        return orig_pts,template_pts , rangex , rangey
    
    
    
    def _getkeypts(self,orig_pts,template_pts):
        return self.orig_pts , self.template_pts , self.rangex, self.rangey
    
    def _findKeyPoints(self,img, template, distance):
    
        # Obtains keypoints by comparing the 2 images
    
        detector = cv2.FeatureDetector_create("SIFT")  # Create a SIFT feature detector   
        descriptor = cv2.DescriptorExtractor_create("SIFT") # Create a SIFT feature descriptor
    
        # Obtain the keypoints and descriptors 
        # skp => original or source image keypoints and sd => source image descriptors
        skp = detector.detect(img)
        skp, sd = descriptor.compute(img, skp)
    
        # tkp => template image keypoints and td => template image descriptors    
        tkp = detector.detect(template)
        tkp, td = descriptor.compute(template, tkp)
    
        # FLANN stands for Fast Library for Approximate Nearest Neighbors. 
        # It contains a collection of algorithms optimized for fast nearest neighbor search in large datasets and for high dimensional features. 
        # For FLANN based matcher, two dictionaries are passed : algorithm to be used, number of trees. 
        # For various algorithms, the information to be passed is explained in FLANN docs. (http://docs.opencv.org/trunk/doc/py_tutorials/py_feature2d/py_matcher/py_matcher.html#matcher)
        # algorithm=1 => FLANN_INDEX_KDTREE 
    
        # For original image
        flann_params = dict(algorithm=1, trees=4)
        flann = cv2.flann_Index(sd, flann_params)
        idx, dist = flann.knnSearch(td, 1, params={}) # Distances between the descriptors detected and the corresponding indices in the descriptor list.
        del flann

        dist = dist[:,0]/2500.0 # Converting the distance values to small value by multiplying with a large number.
        dist = dist.reshape(-1,).tolist()
        idx = idx.reshape(-1).tolist()
    
        indices = range(len(dist))
        indices.sort(key=lambda i: dist[i])
        dist = [dist[i] for i in indices]
        idx = [idx[i] for i in indices]
        skp_final = []
    
        for i, dis in itertools.izip(idx, dist):
            if dis < distance:
                skp_final.append(skp[i])

    
        # For template image
        flann = cv2.flann_Index(td, flann_params)
        idx, dist = flann.knnSearch(sd, 1, params={})
        del flann

        dist = dist[:,0]/2500.0
        dist = dist.reshape(-1,).tolist()
        idx = idx.reshape(-1).tolist()
    
    
        indices = range(len(dist))
        indices.sort(key=lambda i: dist[i])
        dist = [dist[i] for i in indices]
        idx = [idx[i] for i in indices]
        tkp_final = []
    
        for i, dis in itertools.izip(idx, dist):
            if dis < distance:
                tkp_final.append(tkp[i])
    
    
        return skp_final, tkp_final


    def _getregion(self,img, template, skp, tkp , display_match='OFF' , region_detect='OFF'):
    
        # Preapring a combined image by concatenating source and template image for display
        h1, w1 = img.shape[:2]
        h2, w2 = template.shape[:2]
        nWidth = w1+w2
        nHeight = max(h1, h2)
        hdif = (h1-h2)/2
    
        if np.size(np.shape(img))>2 :
            newimg = np.zeros((nHeight, nWidth), np.uint8)
            newimg[hdif:hdif+h2, :w2] = template[:,:,0]
            newimg[:h1, w2:w1+w2] = img[:,:,0]
        else:
            newimg = np.zeros((nHeight, nWidth), np.uint8)
            newimg[hdif:hdif+h2, :w2] = template
            newimg[:h1, w2:w1+w2] = img

        maxlen = min(len(skp), len(tkp))
        
        plist=[]
        qlist=[]
    
        for i in range(maxlen): # Extracting the keypoint coordinates 
            
            pt_a = (int(skp[i].pt[0]+w2), int(skp[i].pt[1]))            
            pt_b = (int(tkp[i].pt[0]), int(tkp[i].pt[1]+hdif))
        
            cv2.line(newimg, pt_a, pt_b, (255, 0, 0))
            
            plist.append([pt_a]) # matching points detected in original image           
            qlist.append([pt_b]) # matching points detected in template image 
    
        
        
        if display_match.upper()=='ON': # Displays the 2 images with lines connecting the feature points    
            plt.figure()            
            plt.gray()
            plt.imshow(newimg)
            plt.savefig('points'+self.tag+'.png')
    
        # px and py => x and y coordinates of points detected in source/original image 
        px=np.transpose(np.array(plist))[0][0]
        py=np.transpose(np.array(plist))[1][0]
        
        # qx and qy => x and y coordinates of points detected in template image (not so important in this context of template matching)    
        qx=np.transpose(np.array(qlist))[0][0]
        qy=np.transpose(np.array(qlist))[1][0] 

    
        if region_detect.upper()=='ON': # Please observe that the density of points in high at the region of interest detected.  
            plt.figure()            
            plt.gray()
            plt.scatter(px,py,s=80)
            plt.imshow(newimg)
            plt.savefig('region'+self.tag+'.png')    
           
    
        avgx=np.average(px)
        avgy=np.average(py)    
        
        #-------This block is just a measure to filter out the wrong points based on their distance from the average value of the points 
        #X coordinate start and end block for the detected template in the original image   
        minx=np.min(px[(avgx-px)>=10])-2048.0 
        maxx=np.max(px[(px-avgx)<=(600)])-2048.0   # The values 10 and 600 are just selected randomly and can be varied.
    
        #Y coordinate start and end block for the detected template  in the original image    
        miny=np.min(py[(avgy-py)>=10])
        maxy=np.max(py[(py-avgy)<=(400)])          # The values 15 and 200 are just selected randomly and can be varied.
         
        orig_pts=plist
        temp_pts=qlist
        
        return newimg , minx ,maxx ,miny ,maxy , orig_pts , temp_pts



    