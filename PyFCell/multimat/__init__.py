# Author: Andreas Putz
# Copyright (c) 2013, OpenPNM
# License: TBD.
r"""
This module contains all functianlity for the cruncher backend of the MAT
postprocessing tool. It supplies the API for the pyramids web frontend

"""

#from dirhandler import find_mat_files
from dirhandler import MultiMatCrawler, default_cruncher_dict, generate_pattern_from_dict
from non_ftp_cruncher import process_non_ftp_data
