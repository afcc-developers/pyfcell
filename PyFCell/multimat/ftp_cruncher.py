# -*- coding: utf-8 -*-
"""
Created on Thu Apr 10 13:41:48 2014

@author: mohhus
"""

import numpy as np
import scipy as sp
from Analyse_MAT_FTP_Data import Analyse_MAT_FTP_Data

class read_ftp_data():
    def __init__(self, file_name, delimiter = ',', skip_header = 0,
                 names = True, dtype = None,  **kwargs):
                     """
                     Constructor of the class 'data_reader'
                     Reads and parses ftp.csv files generated by M-MAT DTS
                     """
                     self.file_name = file_name
                     self.ftp_data = sp.genfromtxt(file_name, delimiter = ',',
                                                   skip_header = 2,
                                                   deletechars = '',
                                                   dtype = dtype,
                                                   names = names)
                     
    def get_ftp_header_tags(self):
        print "=" *35
        for name in self.ftp_data.dtype.names:
            print name
        print "=" *35
    
    def get_ftp_row_size(self):
        return self.ftp_data.size
        
        
    def get_ftp_data(self):
        return self.ftp_data
        
ftp_header_fields={
        "Time": "Time_(s)",        
        "ActiveAreaCurrent":"Active_Area_Current",
        "TotalCurrent":"Total_Current",
        "ActiveAreaVoltage":"Active_Area_Voltage",
        "GuardRingVoltage": "Guard_Ring_Voltage"          
    }


class process_ftp_data(read_ftp_data):
    def __init__(self, file_name, ftp_header_fields = ftp_header_fields, **kwargs):
        read_ftp_data.__init__(self, file_name, **kwargs)    
        self.ftp_header_fields = ftp_header_fields
        
    def get_ftp_data(self):
        time = self.ftp_data[self.ftp_header_fields["Time"]]
        current = self.ftp_data[self.ftp_header_fields["ActiveAreaCurrent"]]
        current = [current[ind]/np.pi for ind in range(0,len(current))]
        voltage = self.ftp_data[self.ftp_header_fields["ActiveAreaVoltage"]]
        voltage = [voltage[ind]*1000 for ind in range(0,len(voltage))]
        return [time, current, voltage]
        
    def perform_ftp_analysis(self):
        Points = [0.05,0.1,0.5,1]
        Tolerance = [point*0.4 for point in Points]
        time = self.ftp_data[self.ftp_header_fields["Time"]]
        current = self.ftp_data[self.ftp_header_fields["ActiveAreaCurrent"]]
        current = [current[ind]/np.pi for ind in range(0,len(current))]
        voltage = self.ftp_data[self.ftp_header_fields["ActiveAreaVoltage"]]
        voltage = [voltage[ind]*1000 for ind in range(0,len(voltage))]
        totalCurrent = self.ftp_data[self.ftp_header_fields["TotalCurrent"]]
        filteredTime,filteredCurrent,filteredVoltage,numBlackList,closestCurrent,closestVoltage,fitVoltage,numFitPoints,logMessage = Analyse_MAT_FTP_Data(time,current,voltage,totalCurrent,0.98*1000,0.6*1000,20,Points,Tolerance)    
        return Points, fitVoltage, current, voltage, logMessage, time
        
    def output_ftp_data(self):
        FTP_Points = {}
        FTP_Points_Labels = ['FTP_0p05Apercm2', 'FTP_0p1Apercm2', 'FTP_0p5Apercm2', 'FTP_1p0Apercm2']
        ftp_data = self.perform_ftp_analysis()
        for ii in range(0,len(FTP_Points_Labels)):
            FTP_Points[FTP_Points_Labels[ii]] = round(ftp_data[1][ii],2)
        return {"data":FTP_Points, "log":ftp_data[4], "aaCurrDens":ftp_data[2], "aaVoltage":ftp_data[3], "time":ftp_data[5]}
    
    def output_full_ftp_data(self):
        full_ftp_data = {}
        data_full = self.perform_ftp_analysis()
        full_ftp_data["ActiveAreaCurrentDensity"] = [round(elem, 2) for elem in data_full[2]] 
        full_ftp_data["ActiveAreaVoltage"] = [round(elem, 2) for elem in data_full[3]] 
        return full_ftp_data
    
if __name__  == "__main__":
    file1 = input('Enter the file path: ')
    read_file = process_ftp_data(file1)
    tmp = read_file.output_ftp_data()
    tmp = read_file.output_ftp_data()
    print tmp.keys()
    #tmp = read_file.output_ftp_data()
    #tmp1 = read_file.perform_ftp_analysis()
    #print tmp1
    print tmp["FTP_0p05Apercm2"]
    print tmp["FTP_0p1Apercm2"]
    print tmp["FTP_0p5Apercm2"]
    print tmp["FTP_1p0Apercm2"]
    tmp2 = read_file.output_full_ftp_data()
    #print tmp2["ActiveAreaCurrentDensity"]
    #print tmp2["ActiveAreaVoltage"]