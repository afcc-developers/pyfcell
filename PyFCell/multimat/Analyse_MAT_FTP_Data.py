import time
import numpy as np

def Analyse_MAT_FTP_Data(rawTime,rawCurrent,rawVoltage,totalCurrent,upperVoltage,lowerVoltage,upperCurrent,Points,Tolerance,blackListTol=0.005,blackListBand=15,pointTol=0.005):
#
#  Function to run through current,voltage data series and locate current points closest to desired points
#  for extracting from data series. Local cubic fits are done of a sub-set of the series to get estimated
#  voltage values at the exact desired current points


    # For code timing purposes
    displayTimes = False

    # Initialize Parameters    
    closestCurrent = []
    closestVoltage = []
    fitVoltage = []
    numFitPoints = []

    startInd = 0
    endInd = 0
    
    time_d = np.array(rawTime)
    current = np.array(rawCurrent)
    totalCurrent = np.array(totalCurrent)
    voltage = np.array(rawVoltage)
    
    startInd = np.where(voltage>upperVoltage)[0][0]
    startInd = np.where(voltage[startInd:len(voltage)]<upperVoltage)[0][0]+startInd
    if len(np.where(totalCurrent[startInd:len(totalCurrent)]>upperCurrent)[0])>0:
	endInd = np.where(totalCurrent[startInd:len(totalCurrent)]>upperCurrent)[0][0]+startInd
    elif len(np.where(voltage[startInd:len(voltage)]<lowerVoltage)[0])>0:
	endInd = np.where(voltage[startInd:len(voltage)]<lowerVoltage)[0][0]+startInd
    else:
	endInd = len(voltage)

    time_d = time_d[startInd:endInd]
    current = current[startInd:endInd]
    voltage = voltage[startInd:endInd]
    
    
    #continuity check
    maxCurrentJump = max(abs(current[1:len(current)]-current[0:len(current)-1]))
    maxVoltageJump = max(abs(voltage[1:len(voltage)]-voltage[0:len(voltage)-1]))
    
    
    logMessage = []
    
    if maxCurrentJump > 0.1:
	logMessage.append("Warning: max current jump exceeded, ")
	print logMessage
    if maxVoltageJump > (0.05*1000):
	logMessage.append("Warning: max voltage jump exceeded, ")
    
    
    #print 'Initial Filtering Length:',len(current)
    
    blackList = (np.ones(current.shape)==0)
    
    
    # For code timing purposes
    t0 = time.time()
    
    # Forward and Backward Fit Black-List
    # Performs linear fit looking forward and backward from each current data point
    # and determines if linearity keep data point (rms error < blackListTol).
    # Additionally, if present point is too far off the fitted line (pointTol)
    # the data point is rejected.
    # Both criteria have to be met in ONLY one direction to keep point.
    
    # For fitting generic equally spaced X vector is used (assumes rawTime vector
    # is sufficiently close to equally spaced)
    X = np.array(range(0,blackListBand))
    Y = np.zeros(blackListBand)
    
    
    # Loop through data
    for M in range(0,len(current)):
        # Perform forward fitting if sufficient data left in current
        if M<len(current)-blackListBand:
            # load current data into Y vector
            for N in range(0,blackListBand):
                Y[N] = current[M+N]
            # perform linear fit using numpy polyfit
            p = np.polyfit(X,Y,1)
            # calculate RMS error of fit
            rsqerr_fwd = np.sqrt(np.mean((Y - np.polyval(p,X))**2))
            # calculate data point error
            pointErr_fwd = np.abs(Y[0] - np.polyval(p,X[0]))
        else:
            # if there is insufficient data to perform fit set both RMS error
            # and point error above tolerance so that point is not admitted
            # based on lack of information
            rsqerr_fwd = blackListTol*10
            pointErr_fwd = pointTol*10
            
        # Perform backward fitting if sufficient data before index in current
        if M>=blackListBand-1:
            # load current data into Y vector
            for N in range(0,blackListBand):
                Y[N] = current[M + N -(blackListBand-1)]
            # perform linear fit using numpy polyfit
            p = np.polyfit(X,Y,1)
            # calculate RMS error of fit
            rsqerr_bkwd = np.sqrt(np.mean((Y - np.polyval(p,X))**2))
            # calculate data point error
            pointErr_bkwd = np.abs(Y[-1] - np.polyval(p,X[-1]))
        else:
            # if there is insufficient data to perform fit set both RMS error
            # and point error above tolerance so that point is not admitted
            # based on lack of information
            rsqerr_bkwd = blackListTol*10
            pointErr_bkwd = pointTol*10
                
        # check if BOTH point tolerance and RMS error tolerance requirements
        # are met in EITHER forward or backward linear fitting
        # if conditions are not met remove point from further analysis
        if (rsqerr_fwd < blackListTol and pointErr_fwd < pointTol) or (rsqerr_bkwd < blackListTol and pointErr_bkwd < pointTol):
            blackList[M] = False
        else:
            blackList[M] = True
          
    numBlackList = np.sum(blackList)
    # For code timing purposes
    t1 = time.time()
    if displayTimes:
        print 'Blacklist Creation: %10.8gs' %(t1-t0)
        
        
    # construct vectors of filtered time, current, and voltage data based on blacklist
    filteredTime = time_d[blackList==False]
    filteredCurrent = current[blackList==False]
    filteredVoltage = voltage[blackList==False]



                
    # For code timing purposes
    t2 = time.time()
    if displayTimes:
        print 'Filtered Assignment and Index Search: %10.8gs' %(t2-t1)  


    # iterate over the number of FTP points to evaluate at
    for M in range(0,len(Points)):
        # for each FTP point clear the local current and voltage vectors used
        # for localized fitting
        current = []
        voltage = []
        
        # initiate the closest variables
        minPointInd = 1
        minPointDist = abs(filteredCurrent[1] - Points[M])
    
        # iterate over the established region of interest in the FTP data
        for N in range(0,len(filteredCurrent)):
            # check to see if present current point is closer than previous
            # closest current point
            # if so set as new closest point
            if minPointDist > abs(filteredCurrent[N] - Points[M]):
                minPointInd = N
                minPointDist = abs(filteredCurrent[N] - Points[M])
            
            # if present current point is within specified range of desired
            # FTP evaluation point (specified in Tolerance) add point to
            # current and voltage vectors for localized fitting
            if filteredCurrent[N] > (Points[M]-Tolerance[M]) and filteredCurrent[N] < (Points[M] + Tolerance[M]):
                current.append(filteredCurrent[N])
                voltage.append(filteredVoltage[N])
            
        # add closest point found to vector of closest points returned
        closestCurrent.append(filteredCurrent[minPointInd])
        closestVoltage.append(filteredVoltage[minPointInd])
    
        # if number of data points within specified range of evaluation points
        # is sufficient for polynomial fit then perform fit
        # if not then set output for that FTP point to zero
        if len(current)>3:
            # quadratic polynomial fit using numpy polyfit
            p = np.polyfit(current, voltage,2)
            # evaluate fitted curve at desired FTP point
            fitVoltage.append(np.polyval(p,Points[M]))
        else:
            fitVoltage.append(0)
        
        # record number of points used in the fitting
        numFitPoints.append(len(current))
        
        
    # For code timing purposes
    t3 = time.time()
    if displayTimes:
        print 'Closest Point and Fitting: %10.8gs' %(t3-t2)  
    
    # return values of interest
    return filteredTime,filteredCurrent,filteredVoltage,numBlackList,closestCurrent,closestVoltage,fitVoltage,numFitPoints,logMessage