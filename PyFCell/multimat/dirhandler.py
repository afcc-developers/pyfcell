# -*- coding: utf-8 -*-
#! /usr/bin/env python

# Author: AFCC
# License: TBD
# Copyright (c) 2013
r"""
*************************************************************************
:mod:`PyFCell.multimat.dirhandler`: Parsers for MultiMat files
*************************************************************************


Contents
--------
The PythonFCST package imports all the functions from the top level modules. 
 
Import
------
>>> import PythonFCST as fcst


Submodules
----------
::

 None                            --- No subpackages at the moment


"""


#from __future__ import print_function


import re
import pandas as pd
import scipy as sp
import PyFCell as pfc
import os
from PyFCell.util.filetree import filtered_walk
from non_ftp_cruncher import process_non_ftp_data
from ftp_cruncher import process_ftp_data
from cv_cruncher import process_cv_data

pattern={}        
pattern["dir"]   = r".*TR(?P<TR>\d*)_?(?P<TRnotes>.*)/.*(?P<MAT_ID>\d\dMS\d\d\d\d)"
pattern["file1"] = r"(?P<year>\d\d\d\d)_(?P<month>\d\d)_(?P<day>\d\d)_(?P<hr>\d\d)(?P<min>\d\d)_(?P<sec>\d\d)_(?P<MAT_HEAD>..)_(?P<MAT_ID>\d\dMS\d\d\d\d)_(?P<phase>MyPattern3141)(\s\d\d\d)?.csv"
pattern["file2"] = r"(?P<year>\d\d\d\d)_(?P<month>\d\d)_(?P<day>\d\d)_(?P<hr>\d\d)(?P<min>\d\d)_(?P<sec>\d\d)_(?P<MAT_HEAD>..)_(?P<phase>MyPattern3141)_(?P<MAT_ID>\d\dMS\d\d\d\d)(\s\d\d\d)?.csv"


default_cruncher_dict = {
    1:{'Phase':"Steady_state_\d*mV",'MatchOrder':'MatID_Phase','Cruncher':'Steady State'},
    2:{'Phase':"Normal",'MatchOrder':'MatID_Phase','Cruncher':'Steady State'},
    3:{'Phase':"Hot_and_dry",'MatchOrder':'MatID_Phase','Cruncher':'Steady State'},
    4:{'Phase':"iLim","MatchOrder":'MatID_Phase','Cruncher':'Steady State'},
    5:{'Phase':"FTP",'MatchOrder':'Phase_MatID','Cruncher':'FTP'},
    6:{'Phase':"FTP",'MatchOrder':'Phase_MatID','Cruncher':'CV'}
}


def generate_pattern_from_dict(mydict=default_cruncher_dict):
    r"""
    Generate a valid pattern list from a list of lists
    
    Parameters
    ----------
    phases : list of lists
        (phase, filenametype, cruncher)
        * phase: regular expression
        * filenamtype: 'MatID_Phase' or 'Phase_MatID'
        * Cruncher: "Steady State", ""
    """
    mydata=[]
    for key in mydict.keys():
        row=mydict[key].copy()
        row['DirPattern']=pattern['dir']
        row['DirPatternC']=re.compile(row['DirPattern'])
        if row["MatchOrder"]=='MatID_Phase':
            row['FilePattern']=pattern["file1"].replace("MyPattern3141",row['Phase'])
            row['FilePatternC']=re.compile(row['FilePattern'])
        elif row["MatchOrder"]=='Phase_MatID':
            row['FilePattern']=pattern["file2"].replace("MyPattern3141",row['Phase'])
            row['FilePatternC']=re.compile(row['FilePattern'])
        mydata.append(row)
    return pd.DataFrame.from_dict(mydata)
    

class MultiMatCrawler(pfc.util.baseobject):
    r"""
    Crawler to find files to postprocess with the MultiMAT analysis tools.
    
    The following patterns are hardecoded in the __init__ method:
    
    "dir"
        r".*TR(?P<TR>\d\d\d)_(?P<TRnotes>.*)/.*(?P<MAT_ID>\d\dMS\d\d\d\d)"
    

    Attributes
    ----------
        
    """    
    
    def __init__(self,patterns=None, **kwargs):
        super(MultiMatCrawler,self).__init__(**kwargs)
        
        #print patterns
        self.set_cruncher_pattern(patterns=patterns)
        
        
        # Declare file storage directory
        self.set_empty_filedict()
        
            
    def set_empty_filedict(self):
        self._filedict = {
                'MAT_ID'    :   [],
                'MAT_HEAD'  :   [],
                'timestamp' :   [],
                'TR'        :   [],
                'TR_notes'  :   [],
                'phase'     :   [],
                'cruncher'  :   [],
                'file_path' :   [],
                'file_name' :   [],
                'file_notes':   []
            }
            
    def set_cruncher_pattern(self,patterns=None):
        """
        This function defines the cruncher patterns.
        
        Parameters
        ----------
        
        patterns : list of lists or None (default)
            If None, a default pattern is created.
            Otherwise a list of lists of the following format for each row is expected:
                (dirpattern, filepattern, cruncher)
                * dirpattern:  regular expression or "default"
                * filepattern: regular expression or "MatID_Phase" or "Phase_MatID"
                * cruncher: "FTP", "CV", "steady state"
        """
        
        if patterns==None:
            self._cruncher_patterns = generate_pattern_from_dict()
        elif isinstance(patterns, pd.DataFrame):
            self._cruncher_patterns = patterns
        elif isinstance(patterns, dict):
            self._cruncher_patterns = generate_pattern_from_dict(patterns)
        else:
            self._logger.error('Incorrect pattern list')
            self._cruncher_patterns = generate_pattern_from_dict()
    
    
    def walk(self,top='.', depth=None, mask=True, debug=False):
        r"""
        Walk directory path or list of directory paths
        
        Parameters
        ----------    
    
        top : string (default: '.') or list of strings
            Top level directory to crawl through
        depth : int (default=None)
            Depth of directories to be crawled (None defaults to all)
        mask  : Bool (default=True)
            Filter 'undefined' phases from the filelist
        """
        
        self.set_empty_filedict()
        
        if isinstance(top,str):
            self._logger.info('Walk directory '+top)
            self.walk_single_dir(top=top,depth=depth,mask=mask,debug=debug)
        elif isinstance(top,list) or isinstance(top,tuple):
            for dirs in top:
                self._logger.info('Walk directory '+dirs)
                self.walk_single_dir(top=dirs,depth=depth,mask=mask,debug=debug)
        else:
            self._logger.error('Incorrect type for path list')
            return
        return self._fileframe
    
    def walk_single_dir(self,top='.', depth=None, mask=True, debug=False):
        r"""
        Walk directory path
        
        Parameters
        ----------    
    
        top : string (default: '.')
            Top level directory to crawl through
        depth : int (default=None)
            Depth of directories to be crawled (None defaults to all)
        mask  : Bool (default=True)
            Filter 'undefined' phases from the filelist
        """
        for dirpath, subdirs, files in filtered_walk(
                    top,depth=depth,
                    included_files=["*.csv"],
                    excluded_dirs=['.svn']):
	    
            if files:
		
                for index, row in self._cruncher_patterns.iterrows():
                    print dirpath
                    matchdir = row['DirPatternC'].match(dirpath)
                    if matchdir:
                        for myfile in files:
                            matchfile = row['FilePatternC'].match(myfile)
                            if matchfile:
                                date = matchfile.group('year')+'-'+matchfile.group('month')+matchfile.group('day')
                                date = date + ' ' + matchfile.group('hr') + ':' + matchfile.group('min') + ':' + matchfile.group('sec')
                                #date = t(date)
                                self._filedict['MAT_ID'].append(matchfile.group('MAT_ID'))
                                self._filedict['MAT_HEAD'].append(matchfile.group('MAT_HEAD'))
                                self._filedict['TR'].append(matchdir.group('TR'))
                                self._filedict['TR_notes'].append(matchdir.group('TRnotes'))
                                self._filedict['timestamp'].append(date)
                                self._filedict['file_path'].append(dirpath)
                                self._filedict['file_name'].append(myfile)
                                self._filedict['phase'].append(matchfile.group('phase'))
                                self._filedict['cruncher'].append(row["Cruncher"])
                                self._filedict['file_notes'].append('Notes')
                            else:
                                if debug:
                                    self._logger.debug('No file match: ' + myfile)
                    elif debug:
                        self._logger.debug('No directory match: ' + dirpath)
                    
        self._fileframe = pd.DataFrame.from_dict( self._filedict )
        
        if mask and len(self._fileframe !=0):
            self._fileframe = self._fileframe[self._fileframe.phase != 'undefined']

        self._fileframe=self._fileframe.sort(['TR','MAT_ID','timestamp'])
        self._fileframe=self._fileframe.reset_index(drop=True)
        return self._fileframe
        

        
    def get_json(self,selection=['TR','MAT_ID','timestamp','phase','cruncher','file_path','file_name'],rows=sp.s_[:]):
        r"""
        Returns a json structure to display with Pyramids
        
        Parameters:
        -----------
        
        selection : list of strings or None
            Selects the headers of the DataFrame to be exported. 
            Defaults to the basic headers for the Pyramids display
            
        """
        if selection !=None:
            tmp=self._fileframe[selection]
        else:
            tmp=self._fileframe
        return tmp.iloc[rows].to_json(date_format='iso',date_unit='s')
        
    def get_file_frame(self):
        return self._fileframe
        
    def append_col(self,columns=['Hallo'],value=sp.NaN):
        for col in columns:
            if col not in self._fileframe.keys():
                self._fileframe[col]=value
                
                
                
    def crunch(self,rows=sp.s_[:]):
        r"""
        Crunch the lines corresponding to the selection parameter.
        
        Parameters:
        -----------
        
        selection : integer, list of integers, scipy slice
            This defines the rows in the lis which will be crunched.
        """
        self.append_col(['CruncherFlag'],'')      
        
        # Append column names for "Non-ftp", "ftp" and "CV"
        colnames=['ActiveAreaCurrentDensity', 'ActiveAreaVoltage', 'GuardRingCurrent', 'GuardRingVoltage', 'FuelFlowRate', 'OxidantFlowRate', 'FxdP_mbard', 'FxInP_barg', 'FxOutP_barg', 'OxdP_mbard', 'OxInP_barg', 'OxOutP_barg', 'CxInSP_C', 'CxIn_C', 'FxHotTube_C', 'FxHead_C', 'OxPlate_C', 'FxHumPmp_mLpmin', 'OxHumPmp_mLpmin']
        colnamesstd = ['ActiveAreaCurrentDensity_std', 'ActiveAreaVoltage_std', 'GuardRingCurrent_std', 'GuardRingVoltage_std', 'FuelFlowRate_std', 'OxidantFlowRate_std', 'FxdP_mbard_std', 'FxInP_barg_std', 'FxOutP_barg_std', 'OxdP_mbard_std', 'OxInP_barg_std', 'OxOutP_barg_std', 'CxInSP_C_std', 'CxIn_C_std', 'FxHotTube_C_std', 'FxHead_C_std',  'OxPlate_C_std', 'FxHumPmp_mLpmin_std', 'OxHumPmp_mLpmin_std']
        colnamesftp = ['FTP_0p05Apercm2', 'FTP_0p1Apercm2', 'FTP_0p5Apercm2', 'FTP_1p0Apercm2']        
        self.append_col(colnames)   
        self.append_col(colnamesstd)
        self.append_col(colnamesftp)
        self.append_col(['ECSA'])

	plotData={}
        for index, row in self._fileframe.iloc[rows].iterrows():
            self._logger.info("\n\nSelecting file\n  index " + str(index) + '\n  ' + row['file_name']) 
            row=self._crunch_file(row)		
            self._fileframe.iloc[index]=row
	    if 'CurrDens' in row.keys():
		print len(row['CurrDens'])
		plotData[index]={}
		plotData[index]['processFileName']=row.file_name
		plotData[index]['fileNotes']=row.file_notes
		plotData[index]['Cruncher']=self._fileframe.iloc[index]['cruncher']
		plotData[index]['CurrDens']=row['CurrDens']
		plotData[index]['Voltage']=row['Voltage']
		plotData[index]['Time']=row['Time']
	    #else:
		#self._fileframe.iloc[index]['plotAvail']=0
	return plotData
            
    
        
    def _crunch_file(self,row):
        r"""
        Crunches a single flile. Build factory method for the crunchers.
        """        
        if row.cruncher=="Steady State":
            self._logger.info('\tSelect "Steady State" cruncher' )
            try:
                              
                cruncher = process_non_ftp_data(row.file_path + os.sep + row.file_name)
                avg_data = cruncher.avg_data_out()
                row[avg_data.keys()] = avg_data.values()
                row['CruncherFlag'] = cruncher.check_data_out_string()  
                std_data = cruncher.std_data_out()
                row[std_data.keys()] = std_data.values()
                row.file_notes = "'Steady State' cruncher completed successfully"
            except:
                self._logger.error('\tCruncher Crashed')
                row.file_notes = "'Steady State' cruncher failed"        
        elif row.cruncher=="FTP":
            self._logger.info('\tSelect "ftp" cruncher')
            try:
                cruncher = process_ftp_data(row.file_path + os.sep + row.file_name)
                ftp_data = cruncher.output_ftp_data()                
                row[ftp_data['data'].keys()] = ftp_data['data'].values()
                row['CurrDens']=ftp_data['aaCurrDens']
                row['Voltage']=ftp_data['aaVoltage']
                row['Time']=ftp_data['time']
                ftp_data['log'].append("ftp cruncher completed successfully")
                row.file_notes = ''.join(ftp_data['log'])
            except Exception as ex:
                errTemplate = "An exception of type {0} occured. Arguments:\n{1!r}"
                errMessage = errTemplate.format(type(ex).__name__, ex.args)
                print errMessage
                self._logger.error('\tCruncher Crashed')
                row.file_notes = "ftp cruncher failed"
        elif row.cruncher=="CV":
            self._logger.info('\tSelect "cv" cruncher')
            try:
                cruncher = process_cv_data(row.file_path + os.sep + row.file_name)
                cv_data = cruncher.get_ecsa()
                row['ECSA'] = cv_data
                row['CurrDens']=ftp_data['aaCurrDens']
                row['Voltage']=ftp_data['aaVoltage']
                row['Time']=ftp_data['time']
                row.file_notes = "cv cruncher completed successfully"
            except:
                self._logger.error('\tCruncher Crashed')
                row.file_notes = "cv cruncher failed"
        else:
            self._logger.warning('\tNo cruncher available')
            row.file_notes = "No cruncher available"
        return row
        
        
    
   
        


if __name__    =="__main__":
    print "Little script to demonstrate the MAT module"
    
    crawler=MultiMatCrawler()
    topdirs = (
        './data/MultiMAT/TR952_perf_eval_hot_check/14MS0106',
        './data/MultiMAT/TR952_perf_eval_hot_check/14MS0107')
    filelist=crawler.walk(top=topdirs[1],mask=True,debug=True)
    myjson = crawler.get_json()
    crawler.crunch()
    
    
    
   
    
    
   
    

    