=======================================================================
PyFCell - Image Processing & Data Crunchers for fuel cell applications
=======================================================================

PyFCell consists of python libraries to perform image processing operations on Microscopy Data.
Post processing functions for analyzing 3D reconstructions of microscopy data are included in the image processing library.
Data crunchers for different porosity and rheology measurements are available.
Binaries for segmenting & postprocessing FIB-SEM data, image comparisons and analysis of rheology data.

Installation
==================
 - Extract the tar ball
 - Open Konsole/CMD/Terminal & go to the PyFCell-*.*.* folder
 - Execute: sudo python setup.py install
   This will install all the files into the appropriate python distribution folder which makes the package available from the python konsole.
   Few binaries which are specifically developed for certain applications would be installed into the /usr/local/bin folder on Linux.

Build Instructions
==================

To work from the source directory 

Linux
 - Go to the PyFCell-*.*.* folder on a Konsole/Terminal
 - Type: source pyfcell.env

Windows
 -  Add the PyFCell-*.*.* to the PYTHONPATH variable


Build documentation
-------------------

> cd docs
> make html 
> firefox _build/html/index.html


Usage Examples
==============