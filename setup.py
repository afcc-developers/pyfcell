from setuptools import find_packages,setup
import os

#from distutils.core import setup


def _getDirs(path):
    r"""
    Paths to be specified with assuming you are
    one level above the PyFCell package (folder which contains the __init__.py for PyFCell')
    Trailing '/' should be ommitted.    
    For development trunk, paths should be defined according to:
    ~/PyFCell/trunk/YOUR_PATH
    So if you want to reference ~/PyFCell/trunk/docs, variable path should be path='docs'
    """
    tmp=[x[0] for x in os.walk(path)]
    tmp2=[i for i in tmp if 'svn' not in i]
    tmp=['../'+i+'/*.*' for i in tmp2]
    return tmp


def _getFiles(path,extension):
    r"""
    Paths to be specified with assuming you are
    one level above the PyFCell package (folder which contains the __init__.py for PyFCell')
    Trailing '/' should be ommitted.
    For development trunk, paths should be defined according to:
    ~/PyFCell/trunk/YOUR_PATH/files.EXTENSION
    So if you want to search ~/PyFCell/trunk/bin for *.py files,
    the arguments to _getFiles should be path='bin', extension='.py'
    """
    tmp=[i for i in os.listdir(path)]
    tmp1=[i for i in tmp if i.endswith(extension)]
    tmp=[path+'/'+i for i in tmp1]
    return tmp
#try:
#  os.system("bash installer.bash")    
#except:
#  print "Sudo Rights required for installing packages."
#  print "Run installer.bash with sudo rights."
  
setup(
    name='PyFCell',
    version='0.1.0',
    author='AFCC Modelling Team',
    author_email='mayank.sabharwal@afcc-auto.com',
    
    include_package_data=True,
    exclude_package_data={'':['README.txt']},
    packages=find_packages(),
    package_dir={'PyFCell':'PyFCell'},
    package_data={      
    '':_getDirs('docs')+_getDirs('examples')},     
    #scripts=['bin/sem.py','bin/fibsem.py','bin/analysis.py','bin/match_image.py','bin/pfc_rheo.py'],
    scripts=_getFiles('bin','.py'),
    url='http://pypi.python.org/pypi/TowelStuff/',
    license='LICENSE.txt',
    description='Python scientific code for fuel cell development',
    long_description=open('README.txt').read(),
    install_requires=[
        "scipy >= 0.9.0",
        "numpy >= 1.6.1",
        "Cython >= 0.17",
        "mahotas >= 1.0.3",
        "pyqtgraph",
        "ipython",
        "xlwt",
        "xlrd",
        "scikit-image",
        "python-pptx",
        "simpleitk",

    ],
#    setup_requires=[
#        "scipy >= 0.9.0",
#        "numpy >= 1.6.1",
#        "Cython",
#        "scikit-image",
#        "mahotas >= 1.0.3",
#        "pyqtgraph",
#        "ipython",
#    ],
)
