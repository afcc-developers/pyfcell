#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2012, TBD
#==============================================================================
# STXM Gui
#   This Gui is developed for the purpose of Stxm image processsing
#   Input: Raw STXM images in the form of .hdr header file
#   Outputs: OD, Delta OD, Sample Thickness, I0, linescans, Material ratios
#==============================================================================

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph.console
import numpy as np
import PyFCell.ips.stxm.stxmtools as stxm
import PyFCell.ips.stxm.Register as Register
import pyqtgraph.dockarea as pda
import scipy as scp
import os
import PyFCell.ips.inout as io


filename_I0 = str()
filename_sample = str()
im_roi_isActive = False
im_roi_s_isActive = False


class dock_area(QtGui.QMainWindow):
    
    
    def __init__(self):
        super(dock_area, self).__init__()
        self.initUI()
        
    def initUI(self):
        global x_win_size
        x_win_size = 900
        y_win_size = 700 
        if x_win_size > width:
            x_win_size = width
            y_win_size = height//2
            
        self.legendexists = False
        self.I0exists = False
        self.sampleexists = False
        self.stack, self.stack_energy, self.stack_name = [], [], []
        self.first_open = True
        self.legendoffset = 20
        self.legend_roiexists = False
        
        self.first = False
        self.second = False
        self.I0isopen = False
        self.sampleisopen = False
        
        self.imageroiexists = False
        self.showpoint = True
        self.vlinescan = True
        
        self.Thickness = 0
        self.CompThickness = 0
        self.ChosenRoi = 0
        self.firstI0 = 1
        self.secondI0 = 1        
        
        self.masterI0 = []
        self.masterEnergy = []
        self.masterI0_spec = []
        self.masterOD = []
        self.masterDOD = []
        self.masterroi = []
        self.masterODspectrum = []
        self.masterSpectrumMap = []
        
        self.textlist = []
        self.combo = []
        
        self.xmouse, self.ymouse, self.deltax, self.deltay = 0., 0., 0., 0.
        self.DX, self.DY = [], []
        
        # this parameter is used to record the plot data shown on the plot
        self.plot_list = [{'x' , 'y'}]
        
        area = pda.DockArea()
        self.setCentralWidget(area)
        self.resize(x_win_size,y_win_size)
        self.setWindowTitle('XFCell STXM Data Processing Tool')
        
        #Creating docks
        ## Button section
        d_btn = pda.Dock("I0", size=(x_win_size//4, y_win_size))  
        self.d_btn_calculator = pda.Dock("Calculator", size=(x_win_size//4, y_win_size))      
        d_btn_sample = pda.Dock("Sample", size=(x_win_size//4, y_win_size))
        self.d_btn_buffer = pda.Dock("Fitting", size=(x_win_size//4, y_win_size))
        self.d_btn_calculator.setOrientation(o = 'vertical')
        
        self.d_I0 = pda.Dock("Image", size=(x_win_size//3,y_win_size//2))
        self.d_roi = pda.Dock("ROI", size=(x_win_size//3,y_win_size//2))
        self.d_slicetree = pda.Dock("Slice Tree", size=(x_win_size//3,y_win_size//2))
        self.d_makestack = pda.Dock("Make Stack", size=(x_win_size//3,y_win_size//2))
        self.d_plot = pda.Dock("Plots", size=(x_win_size//2,y_win_size//2))
        self.d_plot_save = pda.Dock("Save plot", size=(x_win_size//2,5))
        self.d_plot_save.hideTitleBar() # hides the title of the dock
        d_restart = pda.Dock("Restart", size=(x_win_size//4,5))
        d_restart.hideTitleBar() # hides the title of the dock
        d_energylabel = pda.Dock("Energy Label", size=(x_win_size//3,5))
        d_energylabel.hideTitleBar() # hides the title of the dock
        d_slice_move = pda.Dock("Slice Move", size=(x_win_size//2,5))
        d_slice_move.hideTitleBar() # hides the title of the dock
        d_AddRoi = pda.Dock("Add ROI", size=(x_win_size//3,5))
        d_AddRoi.hideTitleBar() # hides the title of the dock
        d_console = pda.Dock("Console", size=(x_win_size//2,y_win_size//2))
        self.d_histogram = pda.Dock("Histogram", size=(x_win_size//2,y_win_size//2))
        
        # Placement of the docks
        ## I0 Section
        area.addDock(self.d_btn_calculator , 'left')      ## place d_btn at left edge of dock area (it will fill the whole space since there are no other docks yet)
        area.addDock(self.d_I0, 'right')## place d_I0 at bottom edge of d_btn
        self.d_I0.setOrientation(o = 'vertical', force = True)
        area.addDock(d_energylabel, 'right')## place d_I0 at bottom edge of d_btn
        area.addDock(self.d_plot, 'bottom', self.d_I0)## place d_plot at bottom edge of d_btn
        area.addDock(d_console, 'bottom', self.d_I0)## place d_plot at bottom edge of d_btn        
        area.addDock(self.d_histogram, 'bottom', self.d_I0)## place d_plot at bottom edge of d_btn
        area.addDock(self.d_makestack, 'right', self.d_I0)        
        area.addDock(self.d_plot_save, 'bottom', d_console)
        area.addDock(d_slice_move, 'bottom', self.d_I0)
        area.addDock(d_AddRoi, 'bottom', self.d_makestack)        
        area.addDock(self.d_btn_buffer, 'above', self.d_btn_calculator)
        area.addDock(d_btn_sample, 'above', self.d_btn_buffer)
        area.addDock(d_btn, 'above', d_btn_sample)     
        area.addDock(d_restart, 'bottom', self.d_btn_calculator)
        
        area.moveDock(self.d_histogram, 'above', d_console)        
        area.moveDock(self.d_plot, 'above', self.d_histogram)        
        area.moveDock(d_energylabel, 'top', self.d_I0)
        area.addDock(self.d_slicetree, 'above', self.d_makestack)
        area.addDock(self.d_roi, 'above', self.d_slicetree)

        # Adding widgets to docks
        ## Button dock
            ## I0 Section
        w_btn = pg.LayoutWidget()
        self.openBtn = QtGui.QPushButton('Open I0 Header file')
        self.I0Btn = QtGui.QPushButton('I0')
        self.SaveI0Btn = QtGui.QPushButton('Save I0 to .txt')
        self.LoadI0Btn = QtGui.QPushButton('Load I0 from .txt')
        self.RegisterBtn = QtGui.QPushButton('Stack Registration')
        self.RejectBtn = QtGui.QPushButton('Reject Reg.')
        self.SelectI0Btn = QtGui.QPushButton('Select I0')
        
        self.MultI0Btn = QtGui.QPushButton('Open Multiple I0')
        self.NumericI0Btn = QtGui.QPushButton('Numeric')
        self.SpecI0Btn = QtGui.QPushButton('Spectroscopic')
        self.RatioI0Btn = QtGui.QPushButton('ratio')
        self.ClearI0Btn = QtGui.QPushButton('Clear')
        self.CompareI0Btn = QtGui.QPushButton('Compare I0s')
#        self.RemoveBadPixelsBtn = QtGui.QPushButton('Remove Bad Pixels')
        

        w_btn.addWidget(self.openBtn, row = 0, col = 0, colspan = 2)
        w_btn.addWidget(self.RegisterBtn, row = 1, col = 0)
        w_btn.addWidget(self.RejectBtn, row = 1, col = 1)
        w_btn.addWidget(self.I0Btn, row = 2, col = 0, colspan = 2)
        w_btn.addWidget(self.SelectI0Btn, row = 3, col = 0)
        w_btn.addWidget(self.SaveI0Btn, row = 4, col = 0)
        w_btn.addWidget(self.LoadI0Btn, row = 4, col = 1)
        
        w_btn.addWidget(self.MultI0Btn, row = 5, col = 0, colspan = 2)
        w_btn.addWidget(self.NumericI0Btn, row = 6, col = 0)
        w_btn.addWidget(self.SpecI0Btn, row = 6, col = 1)

        label_I0Ratio = QtGui.QLabel(" / ")
        label_I0Ratio.setAlignment(QtCore.Qt.AlignCenter)
        w_btn.addWidget(label_I0Ratio, row = 7, col = 0, colspan = 2)

        w_btn.addWidget(self.RatioI0Btn, row = 8, col = 0) 
        w_btn.addWidget(self.ClearI0Btn, row = 8, col = 1) 
        w_btn.addWidget(self.CompareI0Btn, row = 9, col = 0, colspan = 2) 
        
#        w_btn.addWidget(self.RemoveBadPixelsBtn, row = 10, col = 0, colspan = 2)
        
        self.spin2 = pg.SpinBox(value=0, siPrefix=False, dec=False, step=1, bounds=[0, None], minStep=1)
        self.spin2.setMaximumWidth(x_win_size//7)
        w_btn.addWidget(self.spin2, row = 3, col = 1) 
        self.spin2.sigValueChanged.connect(self.valueChanged2)
        
        self.spin3 = pg.SpinBox(value=0, siPrefix=False, dec=False, step=1, bounds=[1, None], minStep=1)
        self.spin4 = pg.SpinBox(value=0, siPrefix=False, dec=False, step=1, bounds=[1, None], minStep=1)
        self.spin3.setMaximumWidth(x_win_size//7)
        self.spin4.setMaximumWidth(x_win_size//7)
        self.spin3.sigValueChanged.connect(self.valueChanged3)
        self.spin4.sigValueChanged.connect(self.valueChanged4)
        w_btn.addWidget(self.spin3, row = 7, col = 0) 
        w_btn.addWidget(self.spin4, row = 7, col = 1)        
        w_btn.layout.setAlignment(QtCore.Qt.AlignTop) 
         
        d_btn.addWidget(w_btn)

            ## Sample Section
        w_btn_sample = pg.LayoutWidget()
        self.openBtn_s = QtGui.QPushButton('Open Sample Header file')
        self.ApplyI0Btn = QtGui.QPushButton('Convert To OD')
        self.LinescanBtn = QtGui.QPushButton('Ver. Linescan')
        self.LinescanDirectionBtn = QtGui.QPushButton('Chng LS Dir.')             
        self.ThicknessBtn = QtGui.QPushButton('Sample Tickness')
        self.RegisterBtn_s = QtGui.QPushButton('Stack Registration')
        self.RejectBtn_s = QtGui.QPushButton('Reject Reg.')
        self.DODBtn = QtGui.QPushButton('Delta OD')
        self.ODspectrumBtn = QtGui.QPushButton('Show OD Sepctrum')
        self.SaveSpectrumBtn = QtGui.QPushButton('Save Sepctrum')

        w_btn_sample.addWidget(self.openBtn_s, row = 0, col = 0, colspan = 2)
        w_btn_sample.addWidget(self.RegisterBtn_s, row = 1, col = 0)
        w_btn_sample.addWidget(self.RejectBtn_s, row = 1, col = 1)
        w_btn_sample.addWidget(self.ApplyI0Btn, row = 2, col = 0, colspan = 2)
        w_btn_sample.addWidget(self.ODspectrumBtn, row = 3, col = 0, colspan = 2)
        w_btn_sample.addWidget(self.SaveSpectrumBtn, row = 4, col = 0, colspan = 2)        
        w_btn_sample.addWidget(self.DODBtn, row = 5, col = 0, colspan = 2)
        w_btn_sample.addWidget(self.LinescanBtn, row = 6, col = 0)
        w_btn_sample.addWidget(self.LinescanDirectionBtn, row = 6, col = 1)
        w_btn_sample.addWidget(self.ThicknessBtn, row = 7, col = 0)
        
        self.spin = pg.SpinBox(value=0.0, suffix=' TF', siPrefix=False, dec=True, step=0.01, bounds=[0, None], minStep=0.001)
        self.spin.setMaximumWidth(x_win_size//8)
        w_btn_sample.addWidget(self.spin, row = 7, col = 1)
        self.spin.sigValueChanged.connect(self.valueChanged)
        w_btn_sample.layout.setAlignment(QtCore.Qt.AlignTop)
        d_btn_sample.addWidget(w_btn_sample) 
        
        # restart section
        w_restart = pg.LayoutWidget()
        self.PrintLabel = QtGui.QLabel() 
        font = self.PrintLabel.font()
        font.setBold(True)
        font.setPointSize(10)
        self.PrintLabel.setFont(font)
        w_restart.addWidget(self.PrintLabel, row = 0, col = 0, colspan = 2)
        self.PrintLabel.setText("Status: ")
        self.PrintLabel.setIndent(10)
        # Restart
        self.RestartBtn = QtGui.QPushButton('Restart')
        w_restart.addWidget(self.RestartBtn, row = 1, col = 0, colspan = 2)
        d_restart.addWidget(w_restart)
                
        # Buffer Section      
        self.buffer_LinescanBtn = QtGui.QPushButton('Ver. Linescan')
        self.buffer_LinescanDirectionBtn = QtGui.QPushButton('Chng LS Dir.')          
        self.bufferspin = pg.SpinBox(value=1, int=True, dec=False, minStep=1, step=1)
        self.num_comp = 1
        self.num_roi = 0
        self.bufferspin.sigValueChanged.connect(self.bufferspin_valueChanged)
        self.bufferspin.setMaximumWidth(x_win_size//8)
        self.CreateBtn = QtGui.QPushButton('Create')
        self.PlotAllBtn = QtGui.QPushButton('Plot All')
        self.MapFitBtn = QtGui.QPushButton('Create Map/Fit')
        self.RGBBtn = QtGui.QPushButton('Create RGB')
        self.BufferLabel1 = QtGui.QLabel()
        self.BufferLabel2 = QtGui.QLabel()
        self.BufferLabel3 = QtGui.QLabel()
        self.BufferLabel4 = QtGui.QLabel()
        self.BufferLabel1.setText("Number of Components:")
        self.BufferLabel2.setText("Name")
        self.BufferLabel3.setText("Data")
        self.BufferLabel4.setText("Plot Linescans of:")
        self.BufferLabel4.setIndent(10)
        self.BufferLabel2.setIndent(5)
        self.BufferLabel3.setIndent(5)
        self.Compspin = pg.SpinBox(value=0.0, suffix=' TF', siPrefix=False, dec=True, step=0.01, bounds=[0, None], minStep=0.001)
        self.Component_thicknessBtn = QtGui.QPushButton('Thickness')
        self.Compspin.sigValueChanged.connect(self.valueChanged5)
        self.Compspin.setMaximumWidth(x_win_size//7)
        self.createbuffer()
        
                
        # Calculator
        w_btn_calculator = pg.LayoutWidget()
        self.Buffer1_Btn = QtGui.QPushButton('Buffer 1')
        self.ShowBuffer1_Btn = QtGui.QPushButton('Show')
        self.Buffer2_Btn = QtGui.QPushButton('Buffer 2')
        self.ShowBuffer2_Btn = QtGui.QPushButton('Show')
        self.Buffer3_Btn = QtGui.QPushButton('Buffer 3')
        self.ShowBuffer3_Btn = QtGui.QPushButton('Show')
        self.Buffer4_Btn = QtGui.QPushButton('Buffer 4')
        self.ShowBuffer4_Btn = QtGui.QPushButton('Show')
        self.calLabel1 = QtGui.QLabel()
        self.calLabel1.setText("Calculator")
        self.calLabel1.setIndent(5)
        self.cal1 = QtGui.QComboBox()
        self.cal2 = QtGui.QComboBox()
        self.cal1.addItem('Buffer1')
        self.cal1.addItem('Buffer2')
        self.cal1.addItem('Buffer3')
        self.cal1.addItem('Buffer4')
        self.cal2.addItem('Buffer1')
        self.cal2.addItem('Buffer2')
        self.cal2.addItem('Buffer3')
        self.cal2.addItem('Buffer4')        
        self.operand = QtGui.QComboBox()
        self.operand.addItem('+')
        self.operand.addItem('-')
        self.operand.addItem('/')
        self.operand.addItem('x')
        
#        self.IBtn = QtGui.QPushButton('I')
#        self.CBtn = QtGui.QPushButton('C')
#        self.EBtn = QtGui.QPushButton('E')
        self.CalcBtn = QtGui.QPushButton('Calculate')
        self.ClearBtn = QtGui.QPushButton('Clear Buffer')
                
        self.CalcLabel = QtGui.QLabel() 
        font_2 = self.CalcLabel.font()
        font_2.setBold(True)
        font_2.setPointSize(12)
        self.CalcLabel.setFont(font_2)
        self.CalcLabel.setText("")
        self.CalcLabel.setAlignment(QtCore.Qt.AlignCenter)
        
        w_btn_calculator.addWidget(self.Buffer1_Btn, col = 0)
        w_btn_calculator.addWidget(self.ShowBuffer1_Btn, col = 1)
        w_btn_calculator.nextRow()
        w_btn_calculator.addWidget(self.Buffer2_Btn, col = 0)
        w_btn_calculator.addWidget(self.ShowBuffer2_Btn, col = 1)
        w_btn_calculator.nextRow()
        w_btn_calculator.addWidget(self.Buffer3_Btn, col = 0)
        w_btn_calculator.addWidget(self.ShowBuffer3_Btn, col = 1) 
        w_btn_calculator.nextRow()
        w_btn_calculator.addWidget(self.Buffer4_Btn, col = 0)
        w_btn_calculator.addWidget(self.ShowBuffer4_Btn, col = 1)
        w_btn_calculator.nextRow()
        w_btn_calculator.addWidget(self.calLabel1, col = 0)
        w_btn_calculator.nextRow()
        w_btn_calculator.addWidget(self.cal1, col = 0)
        w_btn_calculator.addWidget(self.cal2, col = 1)
        w_btn_calculator.nextRow()
        w_btn_calculator.addWidget(self.operand, col = 0)
        w_btn_calculator.nextRow()
        w_btn_calculator.addWidget(self.CalcLabel, col = 0, colspan = 2)
        w_btn_calculator.nextRow()
        w_btn_calculator.addWidget(self.CalcBtn, col = 0)
        w_btn_calculator.addWidget(self.ClearBtn, col = 1)

        w_btn_calculator.layout.setAlignment(QtCore.Qt.AlignTop)
        self.d_btn_calculator.addWidget(w_btn_calculator)

        w_slice = pg.LayoutWidget()
        self.NextBtn = QtGui.QPushButton('Next')
        self.PrevBtn = QtGui.QPushButton('Prev.')
        self.FirstBtn = QtGui.QPushButton('First')
        self.LastBtn = QtGui.QPushButton('End')
        w_slice.addWidget(self.NextBtn, row = 0, col = 2)   
        w_slice.addWidget(self.PrevBtn, row = 0, col = 1)       
        w_slice.addWidget(self.FirstBtn, row = 0, col = 0)
        w_slice.addWidget(self.LastBtn, row = 0, col = 3)

        d_slice_move.addWidget(w_slice)

        w_AddRoi = pg.LayoutWidget()
        self.AddRoiBtn = QtGui.QPushButton('Add ROI')
        self.AcceptRoiBtn = QtGui.QPushButton('Accpt ROI')
        self.CompareRoiBtn = QtGui.QPushButton('Cmp OD')
        self.PlayBtn = QtGui.QPushButton('Play')
        w_AddRoi.addWidget(self.AddRoiBtn, row = 0, col = 0)
        w_AddRoi.addWidget(self.AcceptRoiBtn, row = 0, col = 1)
        w_AddRoi.addWidget(self.CompareRoiBtn, row = 0, col = 2)
        d_AddRoi.addWidget(w_AddRoi)
        
        ## I0 dock  
#        self.v_I0 = pg.ImageView()
        self.v_I0 = pg.ImageView(view=pg.PlotItem(labels = {'left':('y', 'px'), 'bottom':('x', 'px')})) # prepares an images container with left and bottom axes
        self.d_I0.addWidget(self.v_I0)
        
        ## ROI dock
            ## ROI
        self.w_ROI = pg.GraphicsWindow(border=True)
        self.v_roi = self.w_ROI.addViewBox(0,0)
        self.v_roi.setAspectLocked(True)
        self.v_roi.invertY()
        self.im_roi = pg.ImageItem()
        self.v_roi.addItem(self.im_roi)
        self.d_roi.addWidget(self.w_ROI)
        self.lastRoi = None
        self.rois = []
        self.points = []
        
            ## Slice Tree
        self.w_slicetree = pg.TreeWidget()
        self.w_slicetree.setColumnCount(2)
        self.d_slicetree.addWidget(self.w_slicetree)
        
            ## Stack Maker Tree
        self.w_makestack = pg.LayoutWidget()
        self.w_stacktree = pg.TreeWidget()
        self.AddBtn = QtGui.QPushButton('Add to Stack')
        self.MakeBtn = QtGui.QPushButton('Make Stack')
        self.RemoveBtn = QtGui.QPushButton('Remove')
        self.ClearStackBtn = QtGui.QPushButton('Clear Stack')
        self.w_makestack.addWidget(self.w_stacktree, row = 0, col = 0, colspan = 2)
        self.w_makestack.addWidget(self.AddBtn, row = 1, col = 0)
        self.w_makestack.addWidget(self.RemoveBtn, row = 1, col = 1)
        self.w_makestack.addWidget(self.MakeBtn, row = 2, col = 0, colspan = 2)
        self.w_makestack.addWidget(self.ClearStackBtn, row = 3, col = 0, colspan = 2)
        self.w_stacktree.setColumnCount(2)
        self.d_makestack.addWidget(self.w_makestack)


        self.lastRoi_s = None
        self.points_s = []
                       
        self.EnergyLabel = QtGui.QLabel()
        self.EnergyLabel.setText("Energy: ")
        self.EnergyLabel.setIndent(10)
        d_energylabel.addWidget(self.EnergyLabel)
         
        
            #Save plot botton
        w_Saveplotbtn = pg.LayoutWidget()
        self.SaveplotBtn = QtGui.QPushButton('Save Snapshot')
        w_Saveplotbtn.addWidget(self.SaveplotBtn, row=1, col=0)
            #Clear plot botton
        self.ClearplotBtn = QtGui.QPushButton('Clear Plot')
        self.SaveDataBtn = QtGui.QPushButton('Save Data')
        self.ShowPointsBtn = QtGui.QPushButton('Show Points')
                
        self.MouseLocation = QtGui.QLabel() 
        w_Saveplotbtn.addWidget(self.MouseLocation, row = 0, col = 0, colspan = 3)
        self.mouselabel_Handler(self.xmouse, self.ymouse, self.deltax, self.deltay)        
        w_Saveplotbtn.addWidget(self.ClearplotBtn, row=1, col=1)
        w_Saveplotbtn.addWidget(self.SaveDataBtn, row=1, col=2)
        w_Saveplotbtn.addWidget(self.ShowPointsBtn, row=1, col=3)

        self.d_plot_save.addWidget(w_Saveplotbtn)
        
        self.w_console = pg.console.ConsoleWidget()
        d_console.addWidget(self.w_console) 
        self.w_console.catchAllExceptions()
        self.w_console.show()
#        self.w_console.editor

        self.Botton_enable(section = 'All')
        self.LoadI0Btn.setEnabled(True)
         
        ## Plot dock
        pg.mkQApp()
        self.w_plot = pg.PlotWidget()
        self.w_plot.show()
        self.p1 = self.w_plot.plotItem
        self.p2 = pg.ViewBox()
        self.p1.showAxis('right')
        self.p1.scene().addItem(self.p2)
        self.p2.setGeometry(self.p1.vb.sceneBoundingRect())
        self.p1.getAxis('right').linkToView(self.p2)
        self.p2.setXLink(self.p1)
        
        self.legend_roi = self.w_plot.addLegend(offset=(self.legendoffset,10))
        
        self.legend_roiexists = True
        self.d_plot.addWidget(self.w_plot)         
         
        self.w_plot.show() 
             
        ## Histogram Dock
#        self.w_histogram = pg.GraphicsLayoutWidget()
##        self.w_histogram = pg.LayoutWidget()
#        vb = self.w_histogram.addViewBox()
#        vb.setAspectLocked()
#        grad = pg.GradientEditorItem(orientation = 'bottom')
#        self.w_histogram.addItem(grad, 1, 0)
#        self.histplot = pg.PlotItem()
#        self.w_histogram.show()
#        vb.addItem(self.histplot)
             
        self.w_histogram = pg.LayoutWidget()
#        self.gradient = pg.GraphicsLayoutWidget()
#        self.gradient = self.v_I0.getHistogramWidget()
#        self.w_histogram.addWidget(self.gradient, row=3, col=0)
#        
        self.histplot = pg.PlotWidget()
        self.w_histogram.addWidget(self.histplot, row=0, col=0)
#        
#        self.grad = pg.GradientEditorItem(orientation = 'bottom')
#        self.gradient.addItem(self.grad)
        
        self.MaskBtn = QtGui.QPushButton('Mask')
        self.ShowMaskBtn = QtGui.QPushButton('Show Masked Image')

        self.w_histogram.addWidget(self.MaskBtn, row = 1, col = 0)
        self.w_histogram.addWidget(self.ShowMaskBtn, row = 2, col = 0)
        self.connect(self.MaskBtn, QtCore.SIGNAL('clicked()'), self.Mask_Handler) # Mask button event connector
        self.connect(self.ShowMaskBtn, QtCore.SIGNAL('clicked()'), self.ShowMask_Handler) # SHow Mask button event connector
        
#        self.histplot = pg.PlotItem()
##        self.plotwidget.show()
#        self.plotwidget.addItem(self.histplot)        
        
#        self.grad.sigGradientChanged.connect(self.gradupdate)
        
#        self.histplot = self.w_histogram.plotItem
#        l = QtGui.QGridLayout()
#        self.w_histogram.setLayout(l)
#        self.hist = pg.HistogramLUTWidget()
#        
##        self.hist.setAlignment(QtCore.Qt.AlignHCenter)
#        l.addWidget(self.hist, 0, 0)
#        
        self.d_histogram.addWidget(self.w_histogram)
#        self.hist.rotate(90)
             
        ## Event handler for buttons
            ## I0 Section
        self.connect(self.openBtn, QtCore.SIGNAL('clicked()'), self.openfile) # Open button event connector
        self.connect(self.I0Btn, QtCore.SIGNAL('clicked()'), self.I0_Handler) # I0 button event connector
        self.connect(self.SaveI0Btn, QtCore.SIGNAL('clicked()'), self.SaveI0_Handler) # Save I0 button event connector
        self.connect(self.LoadI0Btn, QtCore.SIGNAL('clicked()'), self.LoadI0_Handler) # Load I0 button event connector     
        self.connect(self.SaveplotBtn, QtCore.SIGNAL('clicked()'), self.SavePlot_Handler) # Save plot button event connector 
        self.connect(self.ClearplotBtn, QtCore.SIGNAL('clicked()'), self.ClearPlot_Handler) # Clear plot button event connector 
        self.connect(self.RegisterBtn, QtCore.SIGNAL('clicked()'), self.Registration_Handler) # registration button event connector 
        self.connect(self.SelectI0Btn, QtCore.SIGNAL('clicked()'), self.SelectI0_Handler) # Select I0 button event connector         
        self.connect(self.RejectBtn, QtCore.SIGNAL('clicked()'), self.Reject_Handler) # reject button event connector         
        self.connect(self.MultI0Btn, QtCore.SIGNAL('clicked()'), self.MultI0_Handler) # Multiple I0 button event connector 
        self.connect(self.NumericI0Btn, QtCore.SIGNAL('clicked()'), self.NumericI0_Handler) # Numeric I0 button event connector 
        self.connect(self.SpecI0Btn, QtCore.SIGNAL('clicked()'), self.SpecI0_Handler) # Spectroscopic I0 button event connector 
        self.connect(self.RatioI0Btn, QtCore.SIGNAL('clicked()'), self.RatioI0_Handler) # Ratio I0 button event connector         
        self.connect(self.ClearI0Btn, QtCore.SIGNAL('clicked()'), self.ClearI0_Handler) # Clear I0 button event connector
        self.connect(self.CompareI0Btn, QtCore.SIGNAL('clicked()'), self.CompareI0_Handler) # Compare I0 button event connector 
#        self.connect(self.RemoveBadPixelsBtn, QtCore.SIGNAL('clicked()'), self.RemoveBadPixels_Handler) # Compare I0 button event connector 

            ## plot section
        self.connect(self.SaveplotBtn, QtCore.SIGNAL('clicked()'), self.SavePlot_Handler) # Save plot button event connector 
        self.connect(self.ClearplotBtn, QtCore.SIGNAL('clicked()'), self.ClearPlot_Handler) # Clear plot button event connector               
        self.connect(self.SaveDataBtn, QtCore.SIGNAL('clicked()'), self.SaveData_Handler) # save active data button event connector 
        self.connect(self.ShowPointsBtn, QtCore.SIGNAL('clicked()'), self.ShowPoints_Handler) # Show Points button event connector 
               
            ## Sample Section
        self.connect(self.openBtn_s, QtCore.SIGNAL('clicked()'), self.openfile_s) # Open button event connector
        self.connect(self.ApplyI0Btn, QtCore.SIGNAL('clicked()'), self.ApplyI0_Handler) # Apply I0 button event connector
        self.connect(self.LinescanBtn, QtCore.SIGNAL('clicked()'), self.Linescan_Handler) # V linescan button event connector       
        self.connect(self.LinescanDirectionBtn, QtCore.SIGNAL('clicked()'), self.LinescanDirection_Handler) # H linescan button event connector
        self.connect(self.RegisterBtn_s, QtCore.SIGNAL('clicked()'), self.Registration_Handler) # registration button event connector 
        self.connect(self.RejectBtn_s, QtCore.SIGNAL('clicked()'), self.Reject_Handler) # reject button event connector 
        self.connect(self.DODBtn, QtCore.SIGNAL('clicked()'), self.DOD_Handler) # DeltaOD button event connector 
        self.connect(self.ThicknessBtn, QtCore.SIGNAL('clicked()'), self.Thickness_Handler) # sample thickness button event connector 
        self.connect(self.ODspectrumBtn, QtCore.SIGNAL('clicked()'), self.AcceptRoi_Handler) # show od spectrum button event connector 
        self.connect(self.SaveSpectrumBtn, QtCore.SIGNAL('clicked()'), self.SaveSpectrum_Handler) # save spectrum button event connector 
        
            ## Buffer Section
        self.connect(self.CreateBtn, QtCore.SIGNAL('clicked()'), self.Create_Handler) # Create button event connector  
        self.connect(self.PlotAllBtn, QtCore.SIGNAL('clicked()'), self.PlotAll_Handler) # Plot All button event connector         
        self.connect(self.MapFitBtn, QtCore.SIGNAL('clicked()'), self.MapFit_Handler) # MapFit button event connector 
        self.connect(self.RGBBtn, QtCore.SIGNAL('clicked()'), self.RGB_Handler) # RGB button event connector 
        self.connect(self.buffer_LinescanBtn, QtCore.SIGNAL('clicked()'), self.bufferLS_Handler) # MapFit button event connector 
        self.connect(self.buffer_LinescanDirectionBtn, QtCore.SIGNAL('clicked()'), self.LinescanDirection_Handler) # RGB button event connector  
        self.connect(self.Component_thicknessBtn, QtCore.SIGNAL('clicked()'), self.Comp_Thickness_Handler) # sample thickness button event connector 

            ## Slice Handler        
        self.connect(self.NextBtn, QtCore.SIGNAL('clicked()'), self.Next_Handler)
        self.connect(self.PrevBtn, QtCore.SIGNAL('clicked()'), self.Prev_Handler)
        self.connect(self.FirstBtn, QtCore.SIGNAL('clicked()'), self.First_Handler)
        self.connect(self.LastBtn, QtCore.SIGNAL('clicked()'), self.Last_Handler)

            ## ROI Handler        
        self.connect(self.AddRoiBtn, QtCore.SIGNAL('clicked()'), self.AddRoi_Handler)
        self.connect(self.AcceptRoiBtn, QtCore.SIGNAL('clicked()'), self.AcceptRoi_Handler)
        self.connect(self.CompareRoiBtn, QtCore.SIGNAL('clicked()'), self.CompareRoi_Handler)
        
            ## Slice Tree
        self.connect(self.PlayBtn, QtCore.SIGNAL('clicked()'), self.Play_Handler)   
        
            ## Stack Tree
        self.connect(self.AddBtn, QtCore.SIGNAL('clicked()'), self.AddtoStack_Handler)  
        self.connect(self.MakeBtn, QtCore.SIGNAL('clicked()'), self.MakeStack_Handler) 
        self.connect(self.RemoveBtn, QtCore.SIGNAL('clicked()'), self.RemoveStack_Handler) 
        self.connect(self.ClearStackBtn, QtCore.SIGNAL('clicked()'), self.ClearStack_Handler) 
        
            ## Calculation Section
        self.connect(self.Buffer1_Btn, QtCore.SIGNAL('clicked()'), self.Buffer1_Handler) # Open button event connector
        self.connect(self.ShowBuffer1_Btn, QtCore.SIGNAL('clicked()'), self.ShowBuffer1_Handler) # Open button event connector
        self.connect(self.Buffer2_Btn, QtCore.SIGNAL('clicked()'), self.Buffer2_Handler) # Open button event connector
        self.connect(self.ShowBuffer2_Btn, QtCore.SIGNAL('clicked()'), self.ShowBuffer2_Handler) # Open button event connector
        self.connect(self.Buffer3_Btn, QtCore.SIGNAL('clicked()'), self.Buffer3_Handler) # Open button event connector
        self.connect(self.ShowBuffer3_Btn, QtCore.SIGNAL('clicked()'), self.ShowBuffer3_Handler) # Open button event connector   
        self.connect(self.Buffer4_Btn, QtCore.SIGNAL('clicked()'), self.Buffer4_Handler) # Open button event connector
        self.connect(self.ShowBuffer4_Btn, QtCore.SIGNAL('clicked()'), self.ShowBuffer4_Handler) # Open button event connector  
        
#        self.connect(self.IBtn, QtCore.SIGNAL('clicked()'), self.I_Handler) # Open button event connector
#        self.connect(self.CBtn, QtCore.SIGNAL('clicked()'), self.C_Handler) # Open button event connector
#        self.connect(self.EBtn, QtCore.SIGNAL('clicked()'), self.E_Handler) # Open button event connector
        self.connect(self.CalcBtn, QtCore.SIGNAL('clicked()'), self.Calc_Handler) # Open button event connector
        self.connect(self.ClearBtn, QtCore.SIGNAL('clicked()'), self.Clear_Handler) # Open button event connector
        
            ## Restart
        self.connect(self.RestartBtn, QtCore.SIGNAL('clicked()'), self.initializer) # Open button event connector
        self.connect(self.RestartBtn, QtCore.SIGNAL('clicked()'), self.clearview) # Open button event connector
        
            ## Slice tree signal generator
        self.connect(self.w_slicetree, QtCore.SIGNAL("itemSelectionChanged()"), self.SliceSelector) # Slice Tree Slice selection signal

            ## STack tree signal generator
        self.connect(self.w_stacktree, QtCore.SIGNAL("itemSelectionChanged()"), self.StackSelector) # Slice Tree Slice selection signal
        
        
        #cross hair
        self.w_plot.scene().sigMouseClicked.connect(self.mouseClicked) 
        self.vLine = pg.InfiniteLine(angle=90, movable=False)        
        self.hLine = pg.InfiniteLine(angle=0, movable=False)
        
        self.mousevLine1 = pg.InfiniteLine(angle=90, movable=False, pen = (0,9))  
        self.mousevLine2 = pg.InfiniteLine(angle=90, movable=False, pen = (0,9))
        self.mousehLine1 = pg.InfiniteLine(angle=0, movable=False, pen = (3,9))  
        self.mousehLine2 = pg.InfiniteLine(angle=0, movable=False, pen = (3,9))
        self.mousevLine1.setPos(-1000)
        self.mousehLine1.setPos(-1000)
        self.mousevLine2.setPos(-1000)
        self.mousehLine2.setPos(-1000)
#        self.w_plot.addItem(self.vLine, ignoreBounds=True)
#        self.w_plot.addItem(self.hLine, ignoreBounds=True)
#        
#        self.vb = self.w_plot.plotItem.vb
#        self.proxy = pg.SignalProxy(self.w_plot.scene().sigMouseMoved, rateLimit=100, slot=self.mouseMoved)

    def Botton_enable(self, section = 'All', enable = False):

        def I0_Btns():            
            self.I0Btn.setEnabled(enable)
            self.SaveI0Btn.setEnabled(enable)
            self.SelectI0Btn.setEnabled(enable)
            self.RegisterBtn.setEnabled(enable)
            self.RejectBtn.setEnabled(enable)
            self.LoadI0Btn.setEnabled(enable)
#            self.RemoveBadPixelsBtn.setEnabled(enable)
        
        # Sample
        def Sample_Btns():
            self.ApplyI0Btn.setEnabled(enable)
            self.ThicknessBtn.setEnabled(enable)
            self.LinescanBtn.setEnabled(enable)
            self.LinescanDirectionBtn.setEnabled(enable)
            self.RegisterBtn_s.setEnabled(enable)
            self.DODBtn.setEnabled(enable)
            self.RejectBtn_s.setEnabled(enable)
            self.ODspectrumBtn.setEnabled(enable)
            self.SaveSpectrumBtn.setEnabled(enable)
        
        # Calculator
        def Buffer_Btns():
            self.ShowBuffer1_Btn.setEnabled(enable)
            self.ShowBuffer2_Btn.setEnabled(enable)
            self.ShowBuffer3_Btn.setEnabled(enable)
            self.ShowBuffer4_Btn.setEnabled(enable)
#            self.IBtn.setEnabled(enable)
#            self.CBtn.setEnabled(enable)
#            self.EBtn.setEnabled(enable)
#            self.CalcBtn.setEnabled(enable)
#            self.ClearBtn.setEnabled(enable)
        
        # Slice Move
        def SliceMove_Btns():
            self.NextBtn.setEnabled(enable)
            self.PrevBtn.setEnabled(enable)
            self.FirstBtn.setEnabled(enable)
            self.LastBtn.setEnabled(enable)
        
        # ROI
        def ROI_Btns():
            self.AddRoiBtn.setEnabled(enable)
            self.AcceptRoiBtn.setEnabled(enable)
            self.CompareRoiBtn.setEnabled(enable)
            
        # Plot
        def Plot_Btns():
            self.ClearplotBtn.setEnabled(enable)
            self.SaveDataBtn.setEnabled(enable)
            
        # Multi I0
        def MultiI0_Btns():
            self.NumericI0Btn.setEnabled(enable)
            self.SpecI0Btn.setEnabled(enable)
            self.RatioI0Btn.setEnabled(enable)
            self.CompareI0Btn.setEnabled(enable)
            
        self.dict = {'I0': I0_Btns, 'Sample': Sample_Btns, 'Buffer': Buffer_Btns, 'SliceMove': SliceMove_Btns, 'ROI': ROI_Btns, 'Plot': Plot_Btns, 'MultiI0' : MultiI0_Btns}
        if section == 'All':
            for key in self.dict:
                self.dict[key]()
        else:
            self.dict[section]()

    def createbuffer(self):
        self.w_btn_buffer = pg.LayoutWidget()
        self.w_btn_buffer.addWidget(self.BufferLabel1, colspan = 2)
        self.w_btn_buffer.nextRow()
        self.w_btn_buffer.addWidget(self.bufferspin, col = 0)
        self.w_btn_buffer.addWidget(self.CreateBtn, col = 1)

        self.w_btn_buffer.layout.setAlignment(QtCore.Qt.AlignTop)
        self.d_btn_buffer.addWidget(self.w_btn_buffer) 

    def bufferspin_valueChanged(self, sb):
        self.num_comp = sb.value()
        
    def arrange_buffer(self):
        
        self.w_btn_buffer.nextRow()
        self.w_btn_buffer.addWidget(self.buffer_LinescanBtn, col = 0)
        self.w_btn_buffer.addWidget(self.buffer_LinescanDirectionBtn, col = 1)
        self.w_btn_buffer.nextRow()
        self.w_btn_buffer.addWidget(self.BufferLabel4, col = 0)
        self.w_btn_buffer.addWidget(self.showcomp, col = 1)  
        self.w_btn_buffer.nextRow()
        self.w_btn_buffer.addWidget(self.Component_thicknessBtn, col = 0)
        self.w_btn_buffer.addWidget(self.Compspin, col = 1)
        self.w_btn_buffer.nextRow()
        
        self.w_btn_buffer.update()
        self.w_btn_buffer.layout.setAlignment(QtCore.Qt.AlignTop)
        
    def Create_Handler(self):
        self.w_btn_buffer.close()
        self.createbuffer()        
        self.textlist = []
        self.combo = []
        self.w_btn_buffer.nextRow()
        self.w_btn_buffer.addWidget(self.BufferLabel2, col = 0)
        self.w_btn_buffer.addWidget(self.BufferLabel3, col = 1)
        self.w_btn_buffer.nextRow()        
        self.showcomp = QtGui.QComboBox()                
        for i in range(self.num_comp):            
            self.textlist.append(self.TextBoxes())
            if len(self.masterODspectrum) == 0:
                self.combo.append(self.Combo(self.num_roi))
            else:
                self.combo.append(self.Combo(self.num_roi + 1))

            self.w_btn_buffer.addWidget(self.textlist[i].textbox, col = 0)
            self.w_btn_buffer.addWidget(self.combo[i].box, col = 1)             
            self.w_btn_buffer.nextRow()
            self.showcomp.addItem('comp '+ str(i+1))
        
        self.showcomp.activated.connect(self.showcomp_Handler)
        
        self.w_btn_buffer.addWidget(self.PlotAllBtn, colspan = 2)
        self.w_btn_buffer.nextRow()
        self.w_btn_buffer.addWidget(self.MapFitBtn, colspan = 2)
        self.w_btn_buffer.nextRow()
        self.w_btn_buffer.addWidget(self.RGBBtn, colspan = 2)        
        self.w_btn_buffer.nextRow()
        self.arrange_buffer()

    def MapFit_Handler(self):
        self.masterSpectrumMap = []
        self.roi_added = False
        for i in range(len(self.combo)):
            if self.combo[i].Value[0] == 'R':
                self.masterSpectrumMap.append(self.masterODspectrum[np.int(self.combo[i].Value[4]) - 1])  
            elif self.combo[i].Value[0] == 'L':
#                filename = str(QtGui.QFileDialog.getOpenFileName(self, "Open Spectrum Text File","", "Spectrum Files (*.txt)"))
#                path,name = os.path.split(filename)
                tmp = np.genfromtxt(self.combo[i].filename)
#                energy_range = tmp[:,0]
                self.masterSpectrumMap.append(tmp[:,1])
#                self.pen = 'w'
#                self.ploter(self.I0_spectrum, pname = 'I0 Spectrum', x = self.energy_range, y2 = self.stdvI0, pname2 = 'Stn. Dev.')                              
            
        self.comp_map = stxm.fit_map(self.sample_od, self.masterSpectrumMap, self.num_comp)
        self.PrintLabel.setText('Comp Map Created!')
        self.plot_data = self.comp_map
        self.plotdata_name = self.name_sample + '_CompMap'
        self.plotdata_path = self.path_sample
        
        self.plot_energy = self.energy_range_s
        
        self.updatedocks(self.plot_data[:,:,0])
        energylist = [0 for i in range(self.num_comp)]
        self.SliceTree('Component Map', self.comp_map, energylist)
#        self.ploter(np.average(self.sample_od[:,:,0], axis = 0), pname = 'OD Spectrum of Imaging Area')
        self.plot_name = 'ComponentMap'
        self.w_plot.setLabel('left', text = 'OD')
        self.w_plot.setLabel('right', text = '')
        self.w_plot.setLabel('bottom', text = 'Pixel')  

    def RGB_Handler(self):
        self.RGBmap = stxm.RGB_Map(self.comp_map)
        self.RGBimage = np.zeros((self.RGBmap.shape[1], self.RGBmap.shape[0], self.RGBmap.shape[2]))
        for i in range(self.RGBmap.shape[2]):
            self.RGBimage[:,:,i] = self.RGBmap[:,:,i].transpose()
        self.v_I0.imageItem.setImage(self.RGBimage)
        
        ## Create text object, use HTML tags to specify color/size
        text = pg.TextItem(html='<div style="text-align: center"><span style="color: red;">R=%s</span>, <br><span style="color: green;">G=%s</span>, <br><span style="color: blue">B=%s</span></div>' % (str(self.textlist[2].Value), str(self.textlist[0].Value), str(self.textlist[1].Value)),anchor=(0,0), border='w', fill=(0, 0, 255, 100))
        self.v_I0.addItem(text)
        text.setPos(-1.1*self.RGBmap.shape[1], 0)
             
    def PlotAll_Handler(self):
        self.masterSpectrumMap = []
        self.roi_added = False
        for i in range(len(self.combo)):
            if self.combo[i].Value[0] == 'R':
                self.masterSpectrumMap.append(self.masterODspectrum[np.int(self.combo[i].Value[4]) - 1]) 
                energy = self.energy_range_s
            elif self.combo[i].Value[0] == 'L':
#                filename = str(QtGui.QFileDialog.getOpenFileName(self, "Open Spectrum Text File","", "Spectrum Files (*.txt)"))
#                path,name = os.path.split(filename)
                tmp = np.genfromtxt(self.combo[i].filename)
#                energy_range = tmp[:,0]
                self.masterSpectrumMap.append(tmp[:,1])
                energy = tmp[:,0]
                
        for i in range(len(self.masterSpectrumMap)):        
            if i == 0:
                self.pen = (i,9)
                self.ploter(self.masterSpectrumMap[i], pname = str(self.textlist[i].Value), x = energy, ppen = (i,9))
            else:
                self.ploter(self.masterSpectrumMap[i], pname = str(self.textlist[i].Value), x = energy, ppen = (i,9), c = False) 
        
        self.plotterdata_y = self.masterSpectrumMap[-1]
        self.plotterdata_x = energy 
                
    class TextBoxes(object):
        def __init__(self):
            self.textbox = QtGui.QLineEdit()
            self.Value = self.textbox.text()
            self.textbox.textChanged.connect(self.textchanged)
            
        def getText(self):
            return self.textbox.text()
            
        def textchanged(self):
            self.Value = self.textbox.text()
            
    
    class Combo(object):
        def __init__(self, value):
            self.box = QtGui.QComboBox()
            for j in range(value):
                self.box.addItem('ROI '+ str(j+1))
            self.box.addItem('Load Data')
            self.box.activated.connect(self.combo_changed) 
            self.Value = self.box.currentText()
            self.filename = None
      
        def getValue(self):
            return self.Value
    #        self.box.activated.connect(self.combo_changed)
    
        def combo_changed(self):
            self.Value = self.box.currentText()
            if self.Value[0] == 'L':
                self.filename = str(QtGui.QFileDialog.getOpenFileName(win, "Open Spectrum Text File","", "Spectrum Files (*.txt)"))
          

    def Mask_Handler(self):
        self.masker = np.zeros((self.plot_data.shape[0],self.plot_data.shape[1]))
        self.maskexists = True
#        image = self.v_I0.imageItem.image
        for i in range(self.plot_data.shape[0]):
            for j in range(self.plot_data.shape[1]):
                if self.plot_data[i,j,self.slice_counter] >= self.minHist and self.plot_data[i,j,self.slice_counter] <= self.maxHist:
                    self.masker[i,j] = 1.0

        self.masked_image = np.multiply(self.masker, self.plot_data[:,:,self.slice_counter])
        self.PrintLabel.setText("Status: Mask Created!")
#        self.v_I0.imageItem.setImage(self.masked_image.transpose())
#        self.v_I0.imageItem.setLookupTable(self.lut)            

    def ShowMask_Handler(self):
        self.updatedocks(self.masked_image)
        self.histplot.setXRange(self.minHist, self.maxHist)

    def gradupdate(self):
        self.lut = self.grad.getLookupTable(512)
        
#        self.v_I0.imageItem.setLookupTable(self.lut)

    def histogram_plot(self):
        hist = self.v_I0.imageItem.getHistogram()
        self.histplot.clear()
        
        self.region = pg.LinearRegionItem()
        self.region.setZValue(10)
        self.histplot.addItem(self.region)
        
        if self.maskexists:
            self.histplot.plot(hist[0][self.minHist:self.maxHist], hist[1][self.minHist:self.maxHist])
            self.region.setRegion([self.minHist, self.maxHist])
        else:
            self.histplot.plot(hist[0], hist[1])
            self.region.setRegion([np.min(hist[0]), np.max(hist[0])])

        self.region.sigRegionChanged.connect(self.updateRegion)
        

    def updateRegion(self):
        self.region.setZValue(10)
        self.minHist, self.maxHist = self.region.getRegion()
        self.v_I0.setLevels(self.minHist, self.maxHist)

    def ClearStack_Handler(self):
        self.stack, self.stack_energy, self.stack_name = [],[],[]
        self.w_stacktree.clear()
        self.PrintLabel.setText('Stack Cleared!')        

    def RemoveStack_Handler(self):
        if len(self.stack) > 0:        
            self.stack.pop(self.selected_stacktree - 1)
            self.stack_name.pop(self.selected_stacktree - 1)
            self.stack_energy.pop(self.selected_stacktree - 1)
            if len(self.stack) > 0:
                self.StackTree(self.stack)
                self.PrintLabel.setText('Slice Removed!')
        
    def MakeStack_Handler(self):
        n = []
        m = []
        for i in range(len(self.stack)):
            n.append(self.stack[i].shape[0])
            m.append(self.stack[i].shape[1])
        
        right = np.min(n)
        bottom = np.min(m)
        
        for i in range(len(self.stack)):
            self.stack[i] = self.stack[i][0:right, 0:bottom]
            
        self.stack_final = np.dstack(self.stack)
        self.PrintLabel.setText('Stack Created!')
        
    def AddtoStack_Handler(self):
        
        self.stack.append(self.plot_data[:,:,self.slice_counter])
         
        if self.I0isopen:             
            self.stack_energy.append(self.energy_range[self.slice_counter])
            self.stack_name.append(self.name_I0) 

        if self.sampleisopen:            
            self.stack_energy.append(self.energy_range_s[self.slice_counter])
            self.stack_name.append(self.name_sample) 
            
        self.StackTree(self.stack) 
        self.PrintLabel.setText('Status: Slice Added!')
                             
    def StackTree(self, data):
        
        item_list = []
        energy_list = []           
        item  = QtGui.QTreeWidgetItem(['Stack'])        
        self.w_stacktree.clear()
        self.w_stacktree.addTopLevelItem(item)

        for i in range(len(self.stack)):
            item_list.append(QtGui.QTreeWidgetItem([str(i+1)+'-'+self.stack_name[i]]))
            item.addChild(item_list[i])
            energy_list.append(QtGui.QLabel())
            energy_list[i].setText(str(self.stack_energy[i]) + " KeV")
            self.w_stacktree.setItemWidget(item_list[i], 1, energy_list[i])

        self.w_stacktree.expandAll()
        
    def StackSelector(self):
        sn = self.w_stacktree.selectedItems()[0].text(0)[0]
        if sn == 'S':
            pass
        else:
            self.v_I0.setImage(self.stack[np.int(sn) - 1].transpose())
            self.histogram_plot()
            self.selected_stacktree = np.int(sn)
            self.selected_stacktree_name = self.w_stacktree.selectedItems()[0].text(0)[2:]

    def SliceTree(self, Sample_Name, data, energy):
        global TreeName
        TreeName = Sample_Name
        item_list = []
        energy_list = []
        item  = QtGui.QTreeWidgetItem([Sample_Name])
        
        self.w_slicetree.clear()
        self.w_slicetree.addTopLevelItem(item)
        self.w_slicetree.setItemWidget(item, 1, self.PlayBtn)

        n, m, nslice = data.shape
        for i in range(nslice):
            item_list.append(QtGui.QTreeWidgetItem([str(i+1)]))
            item.addChild(item_list[i])
            energy_list.append(QtGui.QLabel())
            energy_list[i].setText(str(energy[i]) + " KeV")
            self.w_slicetree.setItemWidget(item_list[i], 1, energy_list[i])
            
        self.w_slicetree.expandAll()
            
    def SliceSelector(self):
        global TreeName
        sn = self.w_slicetree.selectedItems()[0].text(0)
        if sn == TreeName:
            pass
        else:
            self.v_I0.setImage(self.plot_data[:,:,np.int(sn) - 1].transpose())
            self.histogram_plot()
        
    def Play_Handler(self):
#        import time
#        n,m,ns = self.plot_data.shape
#        
#        for i in range(ns):
#            
#            time.sleep(0.02)
#            self.v_I0.setImage(self.plot_data[:,:,i].transpose())
#            print i
        self.v_I0.play(10)
        
    def clearview(self):
        self.v_I0.close()
        self.v_I0 = pg.ImageView(view=pg.PlotItem(labels = {'left':('y', 'px'), 'bottom':('x', 'px')}))
        self.d_I0.addWidget(self.v_I0)
        self.AddRoiBtn.setEnabled(True)
        if self.imageroiexists:
            self.im_roi.image.fill(0)
            self.im_roi.updateImage()

    def initializer(self):
        self.masterDOD = []
        self.masterEnergy = []
        self.masterI0 = []
        self.masterroi = []
        self.masterI0_spec = []
        self.I0_spectrum = []
        self.points = []
        self.points_s = []
        self.raw_data = []
        self.raw_data_sample = []
        self.sample_od = []
        self.sample_thickness = []
        self.od_diff = []
        self.plot_list = []
        self.textlist = []
        self.combo = []        
        self.I0exists = False
        self.I0isopen = False
        self.sampleexists = False
        self.roi_added = False
        self.maskexists = False
        self.ClearPlot_Handler()  
        self.w_slicetree.clear()
        self.ClearplotBtn.setEnabled(True)
        self.LoadI0Btn.setEnabled(True)
        self.slice_counter = 0
        self.vlinescan = True
        self.showpoint = True
        self.imageroiexists = False
        self.pen = (0,9)
        self.xmouse, self.ymouse, self.deltax, self.deltay = 0., 0., 0., 0.
        if self.legendexists:
            self.legend.close()
        if self.legend_roiexists:
            self.legend_roi.close()

    def mouselabel_Handler(self, x, y, dx, dy):
        self.MouseLocation.setText("<span style='font-size: 9pt'>x=%0.1f,   <span style='font-size: 9pt'>y=%0.1f</span>, <span style='color: red'>Dx=%0.1f</span>, <span style='color: green'>Dy=%0.1f</span>" % (x, y, dx, dy))
        
    def mouseMoved(self,evt):
        self.pos = evt[0]  ## using signal proxy turns original arguments into a tuple
        if self.w_plot.sceneBoundingRect().contains(self.pos):
            mousePoint = self.vb.mapSceneToView(self.pos)
            self.vLine.setPos(mousePoint.x())
            self.hLine.setPos(mousePoint.y())
            self.xmouse = mousePoint.x()
            self.ymouse = mousePoint.y()
            self.mouselabel_Handler(self.xmouse, self.ymouse, self.deltax, self.deltay)
        
    def mouseClicked(self, evt):
        if len(self.xplot) > 0:
            pos = self.pos  ## using signal proxy turns original arguments into a tuple
            if self.w_plot.sceneBoundingRect().contains(pos):
                mousePoint = self.vb.mapSceneToView(pos)
                index = int(mousePoint.x())
                if len(self.xplot) > 0:
                    if index >= np.min(self.xplot) and index <= np.max(self.xplot):
                        selected_energy = min(self.xplot, key = lambda x:abs(x-mousePoint.x()))
                        if self.I0isopen or self.sampleisopen:
                            self.slice_counter = self.plot_energy.index(selected_energy)
                            self.LabelChange(self.plot_energy[self.slice_counter])
                            self.v_I0.setImage(self.plot_data[:,:,self.slice_counter].transpose())
                            self.histogram_plot()
                        self.delta_calculator(mousePoint, evt)

    def delta_calculator(self, MP, evt):
        if evt.button() == 1:
            self.DX.append(MP.x())
            self.DY.append(MP.y())
#            self.mousevLine2.setPos(0)
#            self.mousehLine2.setPos(0)
        elif evt.button() == 4:
            self.DX, self.DY = [], []
            self.deltax, self.deltay = 0., 0.
            self.mousevLine1.setPos(-1000)
            self.mousehLine1.setPos(-1000)
            self.mousevLine2.setPos(-1000)
            self.mousehLine2.setPos(-1000)
            self.mouselabel_Handler(self.xmouse, self.ymouse, self.deltax, self.deltay)            
            
        if len(self.DX) == 1:
            self.mousevLine1.setPos(MP.x())
            self.mousehLine1.setPos(MP.y())
            self.deltax, self.deltay = 0., 0.
            self.mouselabel_Handler(self.xmouse, self.ymouse, self.deltax, self.deltay)
        if len(self.DX) == 2:
            self.deltax = np.abs(self.DX[0] - self.DX[1])
            self.deltay = np.abs(self.DY[0] - self.DY[1])
            self.mouselabel_Handler(self.xmouse, self.ymouse, self.deltax, self.deltay)            
            self.mousevLine2.setPos(MP.x())
            self.mousehLine2.setPos(MP.y())
            self.DX, self.DY = [], []

    def ShowPoints_Handler(self):
        if self.showpoint:
            self.showpoint = False
            self.ShowPointsBtn.setText('Hide Points')
            for i in range(len(self.plot_list)):
                if i == 0:
                    self.ploter(self.plot_list[i]['y'], x = self.plot_list[i]['x'], ppen = self.pen, record = False, symbol = 'o', symbolSize = 4)
                else:
                    if self.num_roi > 0:
                        self.ploter(self.plot_list[i]['y'], x = self.plot_list[i]['x'], ppen = (i,9), c = False, record = False, symbol = 'o', symbolSize = 4)
                    else:
                        self.ploter(self.plot_list[i]['y'], x = self.plot_list[i]['x'], ppen = (self.pen[0] + i,9), c = False, record = False, symbol = 'o', symbolSize = 4)
        else:
            self.ShowPointsBtn.setText('Show Points')
            self.showpoint = True
            for i in range(len(self.plot_list)):
                if i == 0:
                    self.ploter(self.plot_list[i]['y'], x = self.plot_list[i]['x'], ppen = self.pen, record = False)
                else:
                    if self.num_roi > 0:
                        self.ploter(self.plot_list[i]['y'], x = self.plot_list[i]['x'], ppen = (i,9), c = False, record = False)
                    else:
                        self.ploter(self.plot_list[i]['y'], x = self.plot_list[i]['x'], ppen = (self.pen[0] + i,9), c = False, record = False)   

    def ploter(self, y, pname = None, x=None, y2 = None, pname2 = None, ppen = 'w',c = True, record = True, **kwargs):
    # plots data on the plot widget

        if c:
            self.w_plot.plotItem.clear()
            # reset mouse click delta values
            self.deltax, self.deltay = 0., 0.
            self.mouselabel_Handler(self.xmouse, self.ymouse, self.deltax, self.deltay)
            if pname != None:
                if self.legendexists:
                    self.legend.close()
                if self.legend_roiexists:
                    self.legend_roi.close()
                self.legend = self.w_plot.addLegend(offset=(self.legendoffset,10))
                self.legendexists = True
                self.legend_roiexists = False
        
        
        self.w_plot.addItem(self.vLine, ignoreBounds=True)
        self.w_plot.addItem(self.hLine, ignoreBounds=True)
        self.w_plot.addItem(self.mousevLine1, ignoreBounds=True)
        self.w_plot.addItem(self.mousevLine2, ignoreBounds=True)
        self.w_plot.addItem(self.mousehLine1, ignoreBounds=True)
        self.w_plot.addItem(self.mousehLine2, ignoreBounds=True)
        
        self.vb = self.w_plot.plotItem.vb
        self.proxy = pg.SignalProxy(self.w_plot.scene().sigMouseMoved, rateLimit=100, slot=self.mouseMoved)
#        self.proxy1 = pg.SignalProxy(self.w_plot.scene().sigMouseClicked, slot=self.mouseClicked) 
        
        
        self.p1.setYRange(min(y), max(y)) 
        if x == None:
            self.p1.plot(y ,name = pname, pen = ppen, **kwargs)
            self.xplot = []
        else:
            self.p1.setXRange(min(x), max(x))
            self.p1.plot(x, y ,name = pname, pen = ppen, **kwargs)
            self.yplot = y
            self.xplot = x
        if y2 != None:
            if x == None:
                self.p2.addItem(self.p1.plot(y2, pen = 'b', name = pname2))
                self.p2.setYRange(min(y2), max(y2))
                self.p2.setGeometry(self.p1.vb.sceneBoundingRect())                
            else:
                self.p2.addItem(self.p1.plot(x, y2, pen = 'b', name = pname2))
                self.p2.setYRange(min(y2), max(y2))
                self.p2.setXRange(min(x), max(x))
                self.p2.setGeometry(self.p1.vb.sceneBoundingRect())
                
        if record:
            if c:
                if x != None:
                    self.plot_list = [{'x': x, 'y': y}]
                if x == None:
                    self.plot_list = [{'x': np.arange(len(y)), 'y': y}]
            else:
                if x != None:
                    self.plot_list.append({'x': x, 'y': y})
                if x == None:
                    self.plot_list.append({'x': np.arange(len(y)), 'y': y})                
                    
        
        self.w_plot.autoRange()
#        if len(self.xplot) > 0:
#            s1 = pg.ScatterPlotItem(size=10, pen=pg.mkPen(None), brush=pg.mkBrush(255, 255, 255, 120))
#            pos = numpy.random.normal(size=(2,n), scale=1e-5)
#            spots = [{'pos': pos[:,i], 'data': 1} for i in range(n)] + [{'pos': [0,0], 'data': 1}]
#            s1.addPoints(spots)
#            w1.addItem(s1)
#     

    def MultI0_Handler(self):
        # Opens multiple I0 spectrums for camparison  
        self.showpoint = True
        self.first_open = False
        self.SelectI0Btn.setEnabled(True)
        self.clearview()
        self.num_roi = 0
        self.filename_I0 = str(QtGui.QFileDialog.getOpenFileName(self, "Open I0 Text File","", "Header Files (*.txt)"))
        self.path_I0,self.name_I0 = os.path.split(self.filename_I0)
        tmp = np.genfromtxt(self.filename_I0)
        self.masterEnergy.append(tmp[:,0])
        self.masterI0.append(tmp[:,1]) 
               
        if len(self.masterI0) == 1:
            self.pen = (0,9)
            self.ploter(self.masterI0[0], pname = 'I0 1', x = self.masterEnergy[0], ppen = (0,9))
        else:
            self.ploter(self.masterI0[len(self.masterI0) - 1], pname = 'I0 ' + str(len(self.masterI0)), x = self.masterEnergy[len(self.masterI0) - 1], ppen = (len(self.masterI0) - 1,9), c = False)
        self.plotterdata_y = self.masterI0[-1]
        self.plotterdata_x = self.masterEnergy[-1] 
        
        self.NumericI0Btn.setEnabled(True)
        self.SpecI0Btn.setEnabled(True)
        if len(self.masterI0) > 1:
            self.RatioI0Btn.setEnabled(True)
            if self.sampleexists:
                self.CompareI0Btn.setEnabled(True)
        self.NumericI0 = True

    def NumericI0_Handler(self):
        # Plots I0 plots numerically 
        self.NumericI0 = True
        for i in range(len(self.masterI0)):
            if i == 0:
                self.pen = (0,9)
                self.ploter(self.masterI0[0], pname = 'I0 1', x = self.masterEnergy[0], ppen = (0,9))
            else:
                self.ploter(self.masterI0[i], pname = 'I0 ' + str(i + 1), x = self.masterEnergy[i], ppen = (i,9), c = False)
                
        self.plotterdata_y = self.masterI0[-1]
        self.plotterdata_x = self.masterEnergy[-1] 

    def SpecI0_Handler(self):
        # Plots I0 plot spectroscopicaly
        self.NumericI0 = False
        for i in range(len(self.masterI0)):
            self.masterI0_spec.append(np.divide(self.masterI0[i], np.max(self.masterI0[i])))
            if i == 0:
                self.pen = (0,9)
                self.ploter(self.masterI0_spec[0], pname = 'I0 1', x = self.masterEnergy[0], ppen = (0,9))
            else:
                self.ploter(self.masterI0_spec[i], pname = 'I0 ' + str(i + 1), x = self.masterEnergy[i], ppen = (i,9), c = False)
                
        self.plotterdata_y = self.masterI0_spec[-1]
        self.plotterdata_x = self.masterEnergy[-1] 

    def RatioI0_Handler(self):
        # Opens multiple I0 spectrums for camparison
        if self.firstI0 and self.secondI0 > 0:
            self.ClearPlot_Handler()
            if self.NumericI0:
                ratio = np.divide(self.masterI0[np.int(self.firstI0) - 1], self.masterI0[np.int(self.secondI0) - 1])
            else:
                ratio = np.divide(self.masterI0_spec[np.int(self.firstI0) - 1], self.masterI0_spec[np.int(self.secondI0) - 1])
            self.pen = (9,9)
            self.ploter(ratio, pname = 'I0 ' + str(np.int(self.firstI0)) + '/' + str(np.int(self.secondI0)) , x = self.masterEnergy[0], ppen = (9,9))
            self.plotterdata_y = ratio
            self.plotterdata_x = self.masterEnergy[-1] 
        else:
            self.PrintLabel.setText('Status: Plz enter a valid nmbr')

    def ClearI0_Handler(self):
        # Clears multiple I0 array 
        if len(self.masterI0) > 0:
            self.masterI0 = []
        if len(self.masterI0_spec) > 0:
            self.masterI0_spec = []
        self.masterDOD = []
        self.masterOD =[]
        self.ClearPlot_Handler()

    def CompareI0_Handler(self):
        # Compares od's of multiple I0s

        if self.sampleexists:
            for i in range(len(self.masterI0)):
                sample_od, od_diff = stxm.OD(self.raw_data_sample, self.energy_range_s, self.masterEnergy[i], self.masterI0[i])
                self.masterOD.append(sample_od)
                self.masterDOD.append(od_diff)
                if i == 0:
                    self.pen = (0,9)
                    self.ploter(np.average(self.masterDOD[0][:,:,0], axis = 0), pname = 'OD 1', ppen = (0,9))
                else:
                    self.ploter(np.average(self.masterDOD[i][:,:,0], axis = 0), pname = 'OD '+ str(i + 1), ppen = (i,9), c = False)
            self.plotterdata_y = np.average(self.masterDOD[-1][:,:,0], axis = 0)
            self.plotterdata_x = None
        else:
            self.PrintLabel.setText('Status: Open Sample Data!')
        self.SelectI0Btn.setEnabled(True)

    def Buffer1_Handler(self):
        self.Buffer1 = self.material()   
        self.PrintLabel.setText("Status: Buffer1 Saved!")
        self.ShowBuffer1_Btn.setEnabled(True)
        self.cal = {'1': self.Buffer1.ploty}
        
    def ShowBuffer1_Handler(self):
        self.ploter(self.Buffer1.ploty, pname = 'buffer1',x = self.Buffer1.plotx, ppen = 'y')

    def Buffer2_Handler(self):
        self.Buffer2 = self.material()   
        self.PrintLabel.setText("Status: Buffer2 Saved!")
        self.ShowBuffer2_Btn.setEnabled(True)
        self.cal = {'1': self.Buffer1.ploty, '2': self.Buffer2.ploty}

    
    def ShowBuffer2_Handler(self):
        self.ploter(self.Buffer2.ploty, pname = 'buffer2',x = self.Buffer2.plotx, ppen = 'g')
        
    def Buffer3_Handler(self):
        self.Buffer3 = self.material()   
        self.PrintLabel.setText("Status: Buffer3 Saved!")
        self.ShowBuffer3_Btn.setEnabled(True)
        self.cal = {'1': self.Buffer1.ploty, '2': self.Buffer2.ploty, '3': self.Buffer3.ploty}

    
    def ShowBuffer3_Handler(self):
        self.ploter(self.Buffer3.ploty, pname = 'buffer3',x = self.Buffer3.plotx, ppen = 'r')
   
    def Buffer4_Handler(self):
        self.Buffer4 = self.material()   
        self.PrintLabel.setText("Status: Buffer4 Saved!")
        self.ShowBuffer4_Btn.setEnabled(True)
        self.cal = {'1': self.Buffer1.ploty, '2': self.Buffer2.ploty, '3': self.Buffer3.ploty, '4': self.Buffer4.ploty}

    
    def ShowBuffer4_Handler(self):
        self.ploter(self.Buffer4.ploty, pname = 'buffer3',x = self.Buffer4.plotx, ppen = 'm')
#    def plot_sample(self, x, sname , c = True):
#        if c:        
#            self.w_plot.plot(clear = 1)
#            
#        self.w_plot.setLabel('left', text = 'Delta OD')
#        self.w_plot.setLabel('bottom', text = 'Pixels')
#        self.w_plot.setLabel('right', text = '')
#        self.w_plot.plot(x, name = sname)
#        self.w_plot.autoRange() 
    
    class material():
        def __init__(self):
            self.ini()
        def ini(self):
            self.ploty = win.plotterdata_y
            self.plotx = win.plotterdata_x
#            self.sample_od = win.sample_od
#            self.od_diff = win.od_diff
#            self.energy_range = win.energy_range_s
#            self.linescan = win.linescans
#            self.linescan_area = win.linescan_area
#            self.raw_data = win.raw_data_sample
#            self.I0 = win.I0_spectrum
            if win.maskexists:
                self.Masked_Image = win.masked_image
            
    def Calc_Handler(self):  
        x = {'+': np.add, '-': np.subtract, '/': np.divide, 'x': np.multiply}        
        self.first_sample = self.cal[str(self.cal1.currentText()[6])]
        self.second_sample = self.cal[str(self.cal2.currentText()[6])]
        op = x[str(self.operand.currentText())]
        self.result = op(self.first_sample, self.second_sample)
        self.ploter(self.first_sample,pname=str(self.cal1.currentText()), ppen = 'w')
        self.ploter(self.second_sample,pname=str(self.cal2.currentText()), ppen = 'r', c = False)
        self.ploter(self.result,pname=str(self.cal1.currentText())+str(self.operand.currentText())+str(self.cal2.currentText()), ppen = 'g', c = False)
        
#        if self.first and self.second:
#            self.result = np.divide(self.first_sample, self.second_sample)
#            self.pen = 'g'
#            self.ploter(self.result,pname=self.first_sample_text+'/'+self.second_sample_text, ppen = 'g')
#            self.plotterdata_y = self.result
#            self.plotterdata_x = None
#            self.plot_name = self.first_sample_text+'/'+self.second_sample_text+'_plot'
#            self.CalcLabel.setText(self.first_sample_text + " / " + self.second_sample_text)
#            self.ClearBtn.setEnabled(True)
#        else:
#            if not self.first:
#                self.PrintLabel.setText("Status: Select 1st Sample!")
#            else:
#                self.PrintLabel.setText("Status: Select 2nd Sample!") 

    def Clear_Handler(self):  
        self.Buffer1 = []
        self.Buffer2 = []
        self.Buffer3 = []
        self.Buffer4 = []
        self.ShowBuffer1_Btn.setEnabled(False)
        self.ShowBuffer2_Btn.setEnabled(False)
        self.ShowBuffer3_Btn.setEnabled(False)
        self.ShowBuffer4_Btn.setEnabled(False)
#        self.first = False
#        self.second = False
#        self.first_sample_text = ''
#        self.second_sample_text = ''
#        self.IBtn.setEnabled(True)
#        self.CBtn.setEnabled(True)
#        self.EBtn.setEnabled(True)
#        self.PrintLabel.setText("Status: Cleared!") 
#        self.CalcLabel.setText("")
    
    def valueChanged(self, sb):
            self.Thickness= sb.value()
            
    def valueChanged2(self, sb):
            self.ChosenRoi= sb.value()
            
    def valueChanged3(self, sb):
            self.firstI0= sb.value()
            
    def valueChanged4(self, sb):
            self.secondI0= sb.value()
            
    def valueChanged5(self, sb):
            self.CompThickness= sb.value()

    def SelectI0_Handler(self):
        
        if len(self.masterroi) > 0:
            self.I0_spectrum = self.masterroi[np.int(self.ChosenRoi) - 1]
            self.PrintLabel.setText("Status: I0 of ROI # " + str(np.int(self.ChosenRoi) + 1) + " Selected!")
            self.masterroi = []
        if len(self.masterI0) > 0:
            self.I0_spectrum = self.masterI0[np.int(self.ChosenRoi) - 1]
            self.energy_range = self.masterEnergy[np.int(self.ChosenRoi) - 1]
            self.PrintLabel.setText("Status: I0 # " + str(np.int(self.ChosenRoi)) + " Selected!")
            self.masterI0 = []
        self.I0exists = True

    def LabelChange(self, label):
        self.EnergyLabel.setText("Energy: " + str(label) + "KeV")

    def Registration_Handler(self):   
        
        if self.I0isopen:
            self.RejectBtn.setEnabled(True)
#            self.registered_data, self.x_trans, self.y_trans = Register.registration(self.raw_data)
            self.raw_data_reserved = self.raw_data
            n,m,ns = self.raw_data.shape
#            self.registered_data_cropped = self.registered_data[0+np.max(self.y_trans):n+np.min(self.y_trans),0+np.max(self.x_trans):m+np.min(self.x_trans),:] 
#            self.raw_data = self.registered_data_cropped
                                  
            xdata = np.arange(ns) 
            self.reg_fit, self.x_trans, self.y_trans, self.x_trans_fit, self.y_trans_fit, self.xfit_param, self.yfit_param = Register.registration(self.raw_data, Method = 'Sobel') 
            # for rounded pixel wise shift
#            self.reg_fit_cropped = self.reg_fit[0+np.max(np.around(self.y_trans_fit)):n+np.min(np.around(self.y_trans_fit)),0+np.max(np.around(self.x_trans_fit)):m+np.min(np.around(self.x_trans_fit)),:]
           
           # Cropping criterion check 
           
            if np.max(np.ceil(self.y_trans_fit)) > 0.:
                up = np.max(np.ceil(self.y_trans_fit))
            else:
                up = 0.
            if np.min(np.floor(self.y_trans_fit)) < 0.:
                bottom = np.min(np.floor(self.y_trans_fit))
            else:
                bottom = n
            if np.max(np.ceil(self.x_trans_fit)) > 0.:
                left = np.max(np.ceil(self.x_trans_fit))
            else:
                left = 0.
            if np.min(np.floor(self.x_trans_fit)) < 0.:
                right = np.min(np.floor(self.x_trans_fit))
            else:
                right = m
                    
            self.reg_fit_cropped = self.reg_fit[up:(n + bottom),left:(m + right),:]
            
            self.raw_data = self.reg_fit_cropped
#            self.raw_data = self.reg_fit
            
            self.plot_data = self.raw_data
            self.plotdata_name = self.name_I0 + '_reg'
            self.plotdata_path = self.path_I0
            self.updatedocks(self.plot_data[:,:,0])
            
        if self.sampleisopen:
            self.RejectBtn_s.setEnabled(True)
#            self.registered_data, self.x_trans, self.y_trans = Register.registration(self.raw_data_sample)
            self.raw_data_sample_reserved = self.raw_data_sample
            n,m,ns = self.raw_data_sample.shape
#            self.registered_data_cropped = self.registered_data[0+np.max(self.y_trans):n+np.min(self.y_trans),0+np.max(self.x_trans):m+np.min(self.x_trans),:] 
            

            xdata = np.arange(ns) 
            self.reg_fit, self.x_trans, self.y_trans, self.x_trans_fit, self.y_trans_fit, self.xfit_param, self.yfit_param = Register.registration(self.raw_data_sample, Method = 'Sobel')            

            self.reg_fit_cropped = self.reg_fit[0+np.max(np.round(self.y_trans_fit)):n+np.min(np.round(self.y_trans_fit)),0+np.max(np.round(self.x_trans_fit)):m+np.min(np.round(self.x_trans_fit)),:]            
            
            self.raw_data_sample = self.reg_fit_cropped
            
            self.plot_data = self.raw_data_sample
            self.plotdata_name = self.name_sample + '_reg'
            self.plotdata_path = self.path_sample            
            self.updatedocks(self.plot_data[:,:,0])

       
        self.ploter(self.x_trans, pname='X translation', ppen = 'c', symbol = 'o', symbolSize = 4)
        self.ploter(self.y_trans, pname = 'Y translation', ppen = 'm', c = False, symbol = '+', symbolSize = 4)
        self.ploter(self.x_trans_fit, pname = 'fit', x = xdata, ppen = 'r', c = False)
        self.ploter(self.y_trans_fit, pname = 'fit', x = xdata, ppen = 'g', c = False)
        self.plot_name = 'Alignment_plot'
        self.w_plot.setLabel('left', text = 'Translation in Pixels')
        self.w_plot.setLabel('right', text = '')
        self.w_plot.setLabel('bottom', text = 'Slice Number')


    def Reject_Handler(self):
        if self.I0isopen:
            self.RejectBtn.setEnabled(False)
            self.raw_data = self.raw_data_reserved            
            self.plot_data = self.raw_data
            self.plotdata_name = self.name_I0+'_I0'
            self.plotdata_path = self.path_I0
            
        if self.sampleisopen:
            self.RejectBtn_s.setEnabled(False)
            self.raw_data_sample = self.raw_data_sample_reserved            
            self.plot_data = self.raw_data_sample
            self.plotdata_name = self.name_sample+'_RawData'
            self.plotdata_path = self.path_sample
        
        self.updatedocks(self.plot_data[:,:,0])            
        self.PrintLabel.setText("Status: Images Restored!")
 
    def Next_Handler(self):
        self.PrevBtn.setEnabled(True)
        self.FirstBtn.setEnabled(True)
        self.maskexists = False

        n,m,nslice = self.plot_data.shape
        if self.slice_counter < (nslice - 1):
            self.slice_counter = self.slice_counter + 1
        
        if self.roi_added:
            self.v_I0.setImage(self.plot_data[:,:,self.slice_counter].transpose())
            self.histogram_plot()
        else:
            self.updatedocks(self.plot_data[:,:,self.slice_counter], Slice_Move = True) 
           
        if self.slice_counter == nslice-1:
            self.NextBtn.setEnabled(False)
            self.LastBtn.setEnabled(False)
            self.PrevBtn.setEnabled(True)
            self.FirstBtn.setEnabled(True)
        self.LabelChange(self.plot_energy[self.slice_counter])
                    
    def Prev_Handler(self):
        self.NextBtn.setEnabled(True)
        self.LastBtn.setEnabled(True)   
        self.maskexists = False
        
        n,m,nslice = self.plot_data.shape        
        if self.slice_counter > 0:
            self.slice_counter = self.slice_counter - 1
        
        if self.roi_added:
            self.v_I0.setImage(self.plot_data[:,:,self.slice_counter].transpose())
            self.histogram_plot()
        else:                
            self.updatedocks(self.plot_data[:,:,self.slice_counter], Slice_Move = True)
        
        if self.slice_counter == 0:
            self.PrevBtn.setEnabled(False)
            self.FirstBtn.setEnabled(False)
            self.NextBtn.setEnabled(True)
            self.LastBtn.setEnabled(True)
            
        self.LabelChange(self.plot_energy[self.slice_counter])
        
    def First_Handler(self):
        self.slice_counter = 0 
        self.maskexists = False
              
        if self.roi_added:
            self.v_I0.setImage(self.plot_data[:,:,self.slice_counter].transpose())
            self.histogram_plot()
        else:
            self.updatedocks(self.plot_data[:,:,self.slice_counter], Slice_Move = True)
        
        self.PrevBtn.setEnabled(False)
        self.FirstBtn.setEnabled(False)
        self.NextBtn.setEnabled(True)
        self.LastBtn.setEnabled(True)
        
        self.LabelChange(self.plot_energy[self.slice_counter])
        
    def Last_Handler(self):
        
        n,m,nslice = self.plot_data.shape       
        self.slice_counter = nslice - 1   
        self.maskexists = False

        if self.roi_added:
            self.v_I0.setImage(self.plot_data[:,:,self.slice_counter].transpose())
            self.histogram_plot()
        else:
            self.updatedocks(self.plot_data[:,:,self.slice_counter],Slice_Move = True) 
           
        self.NextBtn.setEnabled(False)
        self.LastBtn.setEnabled(False)
        self.PrevBtn.setEnabled(True)
        self.FirstBtn.setEnabled(True)
        
        self.LabelChange(self.plot_energy[self.slice_counter])
                            
    def SavePlot_Handler(self):
        self.counter = self.counter + 1
        exporter = pg.exporters.ImageExporter.ImageExporter(self.w_plot.plotItem)  
        if self.sampleisopen:
            exporter.export(self.path_sample + '/' + 'plot'+ str(self.counter) +'.png')
        if self.I0isopen:
            exporter.export(self.path_I0 + '/' + 'plot'+ str(self.counter) +'.png')
            
        self.PrintLabel.setText("Status: Plot Saved!")
            
    def ClearPlot_Handler(self):
        if not self.first_open:
            self.w_plot.plotItem.clear()
            if self.legendexists:
                self.legend.close()
                self.legend_roiexists = False
            if self.legend_roiexists:
                self.legend_roi.close()
                self.legend_roiexists = False
                        
    def SaveData_Handler(self):  
            
        exporter  = pg.exporters.CSVExporter.CSVExporter(self.w_plot.plotItem)
        exporter.export(self.plotdata_path+'/'+self.plotdata_name[0:13]+'_'+self.plot_name+'.csv')
        self.PrintLabel.setText("Status: Plot & Data Saved!")
#        
        outname = self.plotdata_path+'/'+self.plotdata_name + '.tiff'
        res= self.plot_data
        data={}
        data["MaterialID"]=res
        io.writer(res.astype(scp.int16),outname)
        
        outname = self.plotdata_path+'/'+self.plotdata_name + '.vti'
        res= self.plot_data
        data={}
        data["MaterialID"]=res
        io.writer(data,outname)
        
    def SaveI0_Handler(self):
        # SaveI0Btn event handler
        self.I0counter = self.I0counter + 1
        basename,extension = os.path.splitext(self.name_I0)
        s = (np.size(self.I0_spectrum),3)
        tmp = np.zeros(s)
        tmp[:,0] = self.energy_range
        tmp[:,1] = self.I0_spectrum
        tmp[:,2] = self.stdvI0
        fname = self.path_I0 + '/' + basename + '_I0_' + str(self.I0counter)
        np.savetxt(fname+'.txt',tmp)
        self.PrintLabel.setText("Status: I0 Saved!")

    def LoadI0_Handler(self):
        # SaveI0Btn event handler
        self.clearview()
        self.filename_I0 = str(QtGui.QFileDialog.getOpenFileName(self, "Open I0 Text File","", "Header Files (*.txt)"))
        self.path_I0,self.name_I0 = os.path.split(self.filename_I0)
        tmp = np.genfromtxt(self.filename_I0)
        self.energy_range = tmp[:,0]
        self.I0_spectrum = tmp[:,1]
        self.stdvI0 = tmp[:,2]
        self.pen = 'w'
        self.ploter(self.I0_spectrum, pname = 'I0 Spectrum', x = self.energy_range, y2 = self.stdvI0, pname2 = 'Stn. Dev.')  
        self.plotterdata_y = self.I0_spectrum
        self.plotterdata_x = self.energy_range           
        self.I0exists = True
        if self.sampleexists:
            self.ApplyI0Btn.setEnabled(True) 
        self.first_open = False 
        
    def openfile(self):
        # OpenBtn event handler
        self.initializer()
        
        self.I0counter = 0
        self.NextBtn.setEnabled(True)
        self.LastBtn.setEnabled(True)        
        self.RegisterBtn.setEnabled(True)
        self.RegisterBtn_s.setEnabled(False)
        self.SaveI0Btn.setEnabled(False)
        self.SelectI0Btn.setEnabled(False)
        self.SaveDataBtn.setEnabled(False)
        self.AddRoiBtn.setEnabled(False)
        self.maskexists = False
        self.I0isopen = True
        self.sampleisopen = False
        self.slice_counter = 0
        self.counter = 0
        self.ChosenRoi = 0
        self.roi_added = False
        self.filename_I0 = str(QtGui.QFileDialog.getOpenFileName(self, "Open Header File","", "Header Files (*.hdr)"))
        self.path_I0,self.name_I0 = os.path.split(self.filename_I0)
        hdr_I0 = open(self.filename_I0)
        header_I0 = hdr_I0.read()
        self.raw_data, self.image_I0, self.x_axis, self.y_axis, self.x_step, self.y_step, self.energy_range =self.OFile(header_I0, self.filename_I0)
        self.I0Btn.setEnabled(True)
        
        self.SliceTree(self.name_I0, self.raw_data, self.energy_range)   
        
        self.plot_data = self.raw_data
        self.plotdata_name = self.name_I0 + '_I0'
        self.plotdata_path = self.path_I0
        

        self.updatedocks(self.plot_data[:,:,0])

        
        self.plot_energy = self.energy_range
        self.LabelChange(self.plot_energy[self.slice_counter])
        if self.first_open:
            self.first_open = False
        
        self.ApplyI0Btn.setEnabled(False)
        self.ThicknessBtn.setEnabled(False)
        self.LinescanBtn.setEnabled(False)
        self.LinescanDirectionBtn.setEnabled(False)
        self.DODBtn.setEnabled(False)
        self.RejectBtn_s.setEnabled(False)
    
    def openfile_s(self):
        # OpenBtn_s event handler
        if len(self.masterI0) > 0:
            self.ClearPlot_Handler()
            self.CompareI0Btn.setEnabled(True)
            
        self.NextBtn.setEnabled(True)
        self.LastBtn.setEnabled(True)
        self.ClearplotBtn.setEnabled(True)
        self.RegisterBtn.setEnabled(False)
        self.RegisterBtn_s.setEnabled(True)
        self.SaveDataBtn.setEnabled(False)
        self.ApplyI0Btn.setEnabled(False)
        self.ThicknessBtn.setEnabled(False)
        self.LinescanBtn.setEnabled(False)
        self.LinescanDirectionBtn.setEnabled(False)
        self.DODBtn.setEnabled(False)
        self.RejectBtn_s.setEnabled(False)
        self.SelectI0Btn.setEnabled(False)
        self.maskexists = False
        self.plot_list = []
        self.vlinescan = True
#        self.AddRoiBtn.setEnabled(True)
        
        self.slice_counter = 0
        self.counter = 0
        self.ChosenRoi = 0
        self.sampleisopen = True
        self.roi_added = False
        self.I0isopen = False
        self.sampleexists = True
        self.filename_sample = str(QtGui.QFileDialog.getOpenFileName(self, "Open Header File","", "Header Files (*.hdr)"))
        self.path_sample,self.name_sample = os.path.split(self.filename_sample)
        hdr_sample = open(self.filename_sample)
        header_sample = hdr_sample.read()
        self.raw_data_sample, self.image_sample, self.x_axis_s, self.y_axis_s, self.x_step_s, self.y_step_s, self.energy_range_s =self.OFile(header_sample, self.filename_sample)
        if self.I0exists:        
            self.ApplyI0Btn.setEnabled(True)  

        self.SliceTree(self.name_sample, self.raw_data_sample, self.energy_range_s)        
        
        self.plot_data = self.raw_data_sample
        self.plotdata_name = self.name_sample + '_RawData'
        self.plotdata_path = self.path_sample
        
        self.updatedocks(self.plot_data[:,:,0])
        
        self.plot_energy = self.energy_range_s
        self.LabelChange(self.plot_energy[self.slice_counter])
        if self.first_open:
            self.first_open = False
            
    def OFile(self, header, filename):
        # Opens and reads data and header files            
        raw_data, image_data = stxm.read_data(filename)
        x_axis, y_axis, x_step, y_step, energy_range = stxm.read_header(header, image_data)
        return raw_data, image_data, x_axis, y_axis, x_step, y_step, energy_range

    def AddRoi_Handler(self):
        self.AcceptRoiBtn.setEnabled(True)
        self.AddRoiBtn.setEnabled(False)
        self.Botton_enable(section = 'I0', enable = False)
        self.SaveSpectrumBtn.setEnabled(True)
        
        self.roi_added = True
        self.num_roi = self.num_roi + 1
        p = (self.num_roi,9)
        self.rois.append(pg.TestROI([0, 0], [self.shape[1]//3, self.shape[0]//3], maxBounds=QtCore.QRectF(0, 0, self.shape[1], self.shape[0]), pen= p))   
        self.v_I0.addItem(self.rois[self.num_roi])     
        
        c = self.w_plot.plot(pen=self.rois[self.num_roi].pen)
        self.rois[self.num_roi].curve = c
        self.rois[self.num_roi].sigRegionChanged.connect(self.updateRoi)
        
        # Adding ROI plot
#        self.w_plot.plot(pen=self.rois[self.num_roi].pen, name = 'ROI '+str(self.num_roi+1))
    def AcceptRoi_Handler(self):
        # Adds the I0 of the selected ROI to the plot and masterroi list
        if self.I0isopen:
            self.I0_Handler()
            self.Botton_enable(section = 'I0', enable = True)
            self.CompareRoiBtn.setEnabled(True)
        if self.sampleexists and self.sample_od.shape[2] > 0:
            self.ODspectrum_Handler()
            self.AddRoiBtn.setEnabled(True)
            self.SaveSpectrumBtn.setEnabled(True)
            
        
        self.AcceptRoiBtn.setEnabled(False)
        self.CompareI0Btn.setEnabled(True)

    def ODspectrum_Handler(self):
        
        self.w_plot.setLabel('bottom', text = 'KeV')
        if len(self.points_s) > 0: 
            # od selection based on roi
            self.OD = self.sample_od[self.points_s[0][1][0]:self.points_s[0][1][1],self.points_s[0][0][0]:self.points_s[0][0][1],:]
        else:
            self.OD = self.sample_od  
#        if scp.size(self.masterODspectrum)/scp.size(self.energy_range_s) <= self.num_roi:    
        self.masterODspectrum.append(stxm.spectrum(self.OD,self.energy_range_s))   
        if self.num_roi == 0:  
            self.pen = (5,9)
            self.ploter(self.masterODspectrum[self.num_roi], pname = 'ROI '+str(self.num_roi + 1), x = self.energy_range_s, ppen = self.rois[self.num_roi].pen)
        else:
            self.ploter(self.masterODspectrum[self.num_roi], pname = 'ROI '+str(self.num_roi + 1), x = self.energy_range_s, ppen = self.rois[self.num_roi].pen, c = False)
        self.plotterdata_y = self.masterODspectrum[self.num_roi]
        self.plotterdata_x = self.energy_range_s 

    def SaveSpectrum_Handler(self):
#        filename = QtGui.QFileDialog.getSaveFileName(self, 'Save File')
        filename = str(QtGui.QFileDialog.getSaveFileName(self, "Save Spectrum As...","", "Text Files (*.txt)"))
        path,name = os.path.split(filename)
        print path, name
        s = (np.size(self.masterODspectrum[0]),2)
        tmp = np.zeros(s)
        tmp[:,0] = self.energy_range_s
        tmp[:,1] = self.masterODspectrum[-1]
        fname = path + '/' + name
        np.savetxt(fname,tmp)
        self.PrintLabel.setText("Status: Spectrum Saved!")
#        tmp = np.genfromtxt(self.filename_I0)         
        
    def updatedocks(self, slice_image, Slice_Move = False):
        
        global im_roi_isActive
        self.num_roi = 0
        # updates I0 and ROI docks
        if not self.first_open:
            self.rois = []
            self.lastRoi = None
            # Reset I0 dock
            self.v_I0.close()
            self.v_I0 = pg.ImageView(view=pg.PlotItem(labels = {'left':('y', 'px'), 'bottom':('x', 'px')}))
            self.d_I0.addWidget(self.v_I0)
            
            # Reset Plot Widget
            self.w_plot.plotItem.clear()
            if self.legendexists:
                self.legend.close()
                self.legend_roiexists = False
            self.legend_roi.close()
            self.legend_roi = self.w_plot.addLegend(offset=(self.legendoffset,10))
            self.legend_roiexists = True
            
            # Reset ROI dock
            if im_roi_isActive:
                self.im_roi.image.fill(0)
                self.im_roi.updateImage()
        
        self.shape = slice_image.shape
        self.arr = slice_image
        
        if self.I0isopen:
            p = (0,9)
        if self.sampleisopen:
            p = (5,9)
            
        self.v_I0.setImage(self.arr.transpose())
        
        self.histogram_plot()        
        
        self.rois.append(pg.TestROI([0, 0], [self.shape[1], self.shape[0]], maxBounds=QtCore.QRectF(0, 0, self.shape[1], self.shape[0]), pen= p))  
#        self.rois.append(pg.PolyLineROI([[0, 0], [0, self.shape[0]], [self.shape[1], self.shape[0]], [self.shape[1], 0]], closed = True, pen= p)) 

        self.v_I0.addItem(self.rois[0])
               
        # updating the ROI region
#        c = self.w_plot.plot(pen=self.rois[0].pen)
        c = self.p1.plot(pen=self.rois[0].pen)
        self.rois[0].curve = c
        self.rois[0].sigRegionChanged.connect(self.updateRoi)
        
        # Adding ROI plot
        self.w_plot.plot(pen=self.rois[0].pen, name = 'ROI')
        
#        self.p1.plot(pen=self.rois[0].pen, name = 'ROI')
              
        #adding total image and standard deviation
        self.w_plot.plot(np.average(self.arr, axis = 0), name = 'Imaging Area')
        
        # xplot and yplot will used for crosshair selection
#        self.xplot = np.arange(self.arr.shape[1])
        self.xplot = []
        self.yplot = np.average(self.arr, axis = 0)      
#        self.p1.plot(np.average(self.arr, axis = 0), name = 'Imaging Area')
                        
        if not Slice_Move:        
            self.w_plot.setLabel('left', text = 'I')
            self.w_plot.setLabel('bottom', text = 'Pixels')
            self.w_plot.setLabel('right', text = 'Stn. Dev.')
        
        self.w_plot.autoRange()

    def updateRoi(self, roi):
        global im_roi_isActive
        if roi is None:
            return
        self.w_plot.autoRange()
        self.v_roi.autoRange()
        self.imageroiexists = True
        self.lastRoi = roi
        self.selected_roi = roi.getArrayRegion(self.v_I0.imageItem.image, img=self.v_I0.imageItem)
        self.im_roi.setImage(self.selected_roi)

        im_roi_isActive = True
#        if not self.roi_added:
#            self.updateRoiPlot(roi, self.selected_roi)
        """
        Important Note: self.selected_roi is transpose of data array
        """
#        print 'roi shape', self.selected_roi.shape
        if self.I0isopen:
            self.points = roi.getArraySlice(self.v_I0.imageItem.image, img=self.v_I0.imageItem,returnSlice = False)
            self.roipoints = self.points
        if self.sampleisopen:
            self.points_s = roi.getArraySlice(self.v_I0.imageItem.image, img=self.v_I0.imageItem,returnSlice = False)            
            self.roipoints = self.points_s
        if not self.roi_added:
            self.updateRoiPlot(roi, self.selected_roi)

        self.v_roi.enableAutoRange()
 
    def updateRoiPlot(self, roi, data=None):

        if data is None:
            data = roi.getArrayRegion(self.v_I0.imageItem.image, img=self.v_I0.imageItem)
        if data is not None:
#            roi.curve.setData(data.mean(axis = 1)) 
            x1 =  np.round(np.linspace(self.roipoints[0][0][0],self.roipoints[0][0][1] - 1,self.roipoints[0][0][1] - self.roipoints[0][0][0]))
            y1 = data.mean(axis = 1)
            if len(x1) > len(y1):
                x1 =  np.round(np.linspace(self.roipoints[0][0][0] + 1,self.roipoints[0][0][1] - 1,self.roipoints[0][0][1] - self.roipoints[0][0][0] - 1))
            roi.curve.setData(x=x1, y=y1)
        self.w_plot.plotItem.autoRange()         
                
    def CompareRoi_Handler(self):
        if self.sampleexists:
            for i in range(len(self.masterroi)):
                sample_od, od_diff = stxm.OD(self.raw_data_sample, self.energy_range_s, self.energy_range, self.masterroi[i])
                self.masterOD.append(sample_od)
                self.masterDOD.append(od_diff)
                if i == 0:
                    self.pen = (0,9)
                    self.ploter(np.average(self.masterDOD[0][:,:,0], axis = 0), pname = 'OD 1', ppen = (0,9))
                else:
                    self.ploter(np.average(self.masterDOD[i][:,:,0], axis = 0), pname = 'OD '+ str(i), ppen = (i,9), c = False)
            self.plotterdata_y = np.average(self.masterDOD[-1][:,:,0], axis = 0)
            self.plotterdata_x = None
        else:
            self.PrintLabel.setText('Status: Open Sample Data!')
            
    def I0_Handler(self):
        # I0Btn event handler
        # if ROI is not selected, all imaging area will be used as the I0
        global stdv
        self.SaveDataBtn.setEnabled(True)
        self.SaveI0Btn.setEnabled(True)
        self.AddRoiBtn.setEnabled(True)
        if len(self.points) > 0: 
            # I0 selection based on roi
            self.I0 = self.raw_data[self.points[0][1][0]:self.points[0][1][1],self.points[0][0][0]:self.points[0][0][1],:]
        else:
            self.I0 = self.raw_data  
        if self.roi_added:
            if self.num_roi == 1:
                self.pen = (0,9)
                self.ploter(self.I0_spectrum, pname = 'ROI 1', x = self.energy_range, ppen = self.rois[0].pen)
                self.plotterdata_y = self.I0_spectrum
                self.plotterdata_x = self.energy_range
            if scp.size(self.masterroi)/scp.size(self.energy_range) <= self.num_roi:
                self.masterroi.append(stxm.spectrum(self.I0,self.energy_range))
                self.ploter(self.masterroi[self.num_roi], pname = 'ROI '+str(self.num_roi + 1), x = self.energy_range, ppen = self.rois[self.num_roi].pen, c = False)
                self.plotterdata_y = self.masterroi[self.num_roi]
                self.plotterdata_x = self.energy_range
        else:
#            self.masterroi = []
            self.I0_spectrum = stxm.spectrum(self.I0,self.energy_range)
            self.masterroi.append(self.I0_spectrum)
            stdv = np.std(self.I0, axis = 0)
            self.stdvI0 = np.std(stdv, axis = 0)  
            self.pen = (0,9)
            self.ploter(self.I0_spectrum, pname = 'I0 Spectrum', x = self.energy_range, y2 = self.stdvI0, pname2 = 'Stn. Dev.')
            self.plotterdata_y = self.I0_spectrum
            self.plotterdata_x = self.energy_range
        
        
        self.plot_name = 'I0_plot'
        
        self.w_plot.setLabel('left', text = 'I')
        self.w_plot.setLabel('bottom', text = 'KeV')
        self.w_plot.setLabel('right', text = 'Stn. Dev.')
        self.w_plot.autoRange()
        self.I0exists = True

    def ApplyI0_Handler(self):
        # I0Btn event handler
        # if ROI is not selected, all imaging area will be used as the I0 
        self.SaveDataBtn.setEnabled(True)
        self.ThicknessBtn.setEnabled(True)
        self.LinescanBtn.setEnabled(True)
        self.LinescanDirectionBtn.setEnabled(True)
        self.DODBtn.setEnabled(True)
        self.AddRoiBtn.setEnabled(True)
        self.AcceptRoiBtn.setEnabled(True)
        self.ODspectrumBtn.setEnabled(True)
        
        self.masterODspectrum = []
        self.sample_od, self.od_diff = stxm.OD(self.raw_data_sample, self.energy_range_s, self.energy_range, self.I0_spectrum)
        # updating images  
        
        self.plot_data = self.sample_od
        self.plotdata_name = self.name_sample + '_OD'
        self.plotdata_path = self.path_sample
        
        self.plot_energy = self.energy_range_s
        
        self.updatedocks(self.plot_data[:,:,0])
        
#        self.ploter(np.average(self.sample_od[:,:,0], axis = 0), pname = 'OD Spectrum of Imaging Area')
        self.plot_name = 'OD_plot'
        self.w_plot.setLabel('left', text = 'OD')
        self.w_plot.setLabel('right', text = '')
        self.w_plot.setLabel('bottom', text = 'Pixel')  

    def LinescanDirection_Handler(self):
        if self.vlinescan:
            self.vlinescan = False
            self.PrintLabel.setText("Status: Horizontal Linescan!")
            self.buffer_LinescanBtn.setText('Hor. Linescan')
            self.LinescanBtn.setText('Hor. Linescan')
        else:
            self.vlinescan = True
            self.PrintLabel.setText("Status: Vertical Linescan!")
            self.buffer_LinescanBtn.setText('Ver. Linescan')
            self.LinescanBtn.setText('Ver. Linescan')

    def bufferLS_Handler(self):
        global linescans, linescan_area, linescans_1
        self.comp_linescans = []
        data = self.comp_map
        for i in range(self.num_comp):
            if len(self.points_s) > 0: 
                # performing horizontal linescan based on selected roi area
                linescan_area = data[self.points_s[0][1][0]:self.points_s[0][1][1],:,i]
            else:
                # performing horizontal linescan on the whole data
                linescan_area = data[:,:,i]              
            if self.vlinescan:
                linescans = scp.average(linescan_area, axis = 0)
#                linescans_1 = linescans 
            else:
                linescans = scp.average(linescan_area, axis = 1)
#                linescans_1 = linescans
#                linescans = scp.average(linescans_1, axis = 1)
    
            self.comp_linescans.append(linescans)
        self.ploter(self.comp_linescans[0], pname = 'Comp 1')
        self.plotterdata_y = self.comp_linescans[0]
        self.plotterdata_x = None
        
    def Comp_Thickness_Handler(self):
        tickness = self.CompThickness
        Value = self.showcomp.currentText()
        self.comp_thickness_plot = np.divide(self.comp_linescans[np.int(Value[5]) - 1], tickness)
        self.ploter(self.comp_thickness_plot, pname = 'Tickness')
        self.plotterdata_y = self.comp_thickness_plot
        self.plotterdata_x = None

    def showcomp_Handler(self):
        Value = self.showcomp.currentText()       
        
        self.ploter(self.comp_linescans[np.int(Value[5]) - 1], pname = 'OD linescans')
        self.plotterdata_y = self.comp_linescans[np.int(Value[5]) - 1]
        self.plotterdata_x = None
       
    def Linescan_Handler(self, data = None, plot = True):        
        # Calculating the linescans
        global stdv, bb, linescans, linescans_std, n,m,nslice, linescans_1
        
        if data == None:
            data = self.od_diff
        
        if len(self.points_s) > 0: 
            # performing horizontal linescan based on selected roi area
            self.linescan_area = data[self.points_s[0][1][0]:self.points_s[0][1][1],:,:]
        else:
            # performing horizontal linescan on the whole data
            self.linescan_area = data[:,:,:]       
#        bb = self.linescan_area 

        n,m,nslice = self.linescan_area.shape
       
        if self.vlinescan:
            self.linescans = scp.average(self.linescan_area, axis = 0)
            linescans_std = scp.std(self.linescan_area, axis = 0) 
            linescans_1 = self.linescans 

        else:
            self.linescans = scp.average(self.linescan_area, axis = 1)
            linescans_std = scp.std(self.linescan_area, axis = 1)
            linescans_1 = self.linescans
            self.linescans = scp.average(linescans_1, axis = 1)

        self.linescans = scp.average(linescans_1, axis = 1)
        if nslice > 1:
            linescans_std = scp.std(linescans_1, axis = 1)             
        linescans = self.linescans
        
        if nslice > 1:
            self.ploter(self.linescans, pname = 'OD linescans', y2 = linescans_std, pname2 = 'Stn. Dev.')
        else:
            self.ploter(self.linescans, pname = 'OD linescans', y2 = linescans_std[:,0], pname2 = 'Stn. Dev.')
          
        self.plotterdata_y = self.linescans
        self.plotterdata_x = None
        
        self.plot_name = 'Linescan_plot'
        self.w_plot.setLabel('left', text = 'OD')
        self.w_plot.setLabel('bottom', text = 'Pixel')
        self.w_plot.setLabel('right', text = 'Stn. Dev.')     
        
    def DOD_Handler(self):
        # I0Btn event handler
        # if ROI is not selected, all imaging area will be used as the I0                   
        self.plot_data = self.od_diff
        self.plotdata_name = self.name_sample + 'DeltaOD'
        self.plotdata_path = self.path_sample
        
        if self.plot_data.shape[2] ==1:
            self.Botton_enable('SliceMove', enable  = False)
            
        self.updatedocks(self.plot_data[:,:,0]) 
        
        self.ploter(np.average(self.od_diff[:,:,0], axis = 0), pname = 'Delta OD Spectrum of Imaging Area')
        self.plotterdata_y = np.average(self.od_diff[:,:,0], axis = 0)
        self.plotterdata_x = None        
        
        self.plot_name = 'DeltaOD_plot'
        self.w_plot.setLabel('left', text = 'Delta OD')
        self.w_plot.setLabel('right', text = '')
        self.w_plot.setLabel('bottom', text = 'Pixel')       

    def Thickness_Handler(self):
        # I0Btn event handler
        # if ROI is not selected, all imaging area will be used as the I0           
        if self.Thickness != 0:
            self.sample_thickness = np.divide(self.od_diff, self.Thickness)
        else: 
            self.PrintLabel.setText("Status: Error! Please Enter Thickness Factor")
        
        self.plot_data = self.sample_thickness
        self.plotdata_name = self.name_sample + '_Thickness'
        self.plotdata_path = self.path_sample
        
        self.updatedocks(self.plot_data[:,:,0]) 
        
        self.w_plot.setLabel('left', text = 'Thickness (nm)')
        self.w_plot.setLabel('right', text = '')
        self.w_plot.setLabel('bottom', text = 'Pixel')


if __name__ == '__main__':  
    print "Execute Main Window Look"
    import sys
    global width, height
    app = QtGui.QApplication([])
    screen_rect = app.desktop().screenGeometry()
    width, height = screen_rect.width(), screen_rect.height()
    
    win = dock_area()
    win.show()

    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
