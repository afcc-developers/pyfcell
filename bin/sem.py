#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2012, TBD
#==============================================================================
# Segmentation & Analysis of Raw SEM Data
#   This code is developed for the purpose of segmentation of raw SEM Images
#   Input: Raw SEM images in the form of .tif image sequences
#   Output: Segmented binary Images (Solid phase = 1, Pores = 0)
#==============================================================================
import sys,os
import numpy as np
import scipy as sp
import PyFCell.ips.util.ImageTools as imtool
import PyFCell.ips.seg as seg
import pylab as pl
import PyFCell.ips.util.clahe as clahe
from PyFCell.ips.util.ImageSequence import ImageSequence
from optparse import OptionParser,OptionGroup
from PIL import Image
from PyFCell.ips.analysis import areaSD
#==============================================================================
#Initializing the different options for the application
#any new options should be added in this section
#==============================================================================
parser=OptionParser()
parser.add_option("-f","--file",dest="filename",action="store",help="path and filename of the FIB raw data to be segmented",metavar="FILE",type="string")
parser.add_option("-t","--thresh",dest="algname",action="store",help="thresholding algorithm to be used:{'fastsauvola','sauvola','adaptive','otsu','riddler_calvard',hysteresis','rat'}",metavar="ALGORITHM",type="string",default="fastsauvola")
parser.add_option("-b", "--block",action="store",dest="blocksize",help="optional;block or window size used for sauvola and adaptive",default=20,type="int")
parser.add_option("-v","--voxel",dest="voxel",action="store",help="voxel size as x y",metavar="VOXEL",type="float",nargs=2)
parser.add_option("-s", "--step",action="store",dest="step",help="step size for the area size distribution: default=20",default=20,type="int")
parser.add_option("-i", "--interval",action="store",dest="intervals",help="no. of intervals for the area size distribution: default=10",default=10,type="int")
group = OptionGroup(parser,"Sauvola",
                  "Additional parameters for sauvola thresholding algorithm. "
                  "Caution:Too much fidgeting is not good!")

group.add_option("-r", action="store",dest="R",help="range of Std. Deviation which is usually chosen as 128 for 8 bit images",default=128,type="float")
group.add_option("-k", action="store",dest="k",help=" adjustable scaling constant (usually 0.4 - 0.6)",default=0.5,type="float")
parser.add_option_group(group)


group = OptionGroup(parser,"Adaptive",
                  "Additional parameters for adaptive thresholding algorithm. "
                  "Caution:Too much fidgeting is not good!")
group.add_option("-m","--method", action="store",dest="method",help="adaptive thresholding method {'gaussian', 'mean', 'generic', 'median'}. Default is 'gaussian'",default="gaussian")
group.add_option("-o","--offset", action="store",dest="offset",help="constant subtracted from weighted mean, default = 0",default=0,type="float")
group.add_option("--mode", action="store",dest="mode",help="determines how array borders are handled: 'reflect', 'constant', 'nearest', 'mirror', 'wrap'",default="reflect")
#group.add_option("-p","--param", action="store",dest="param",help="specifies sigma for gaussian method",default=None,type="float")
parser.add_option_group(group)


(options,args)=parser.parse_args()

#==============================================================================
#intializing the algorithms available for segmentation
#any new algorithms entered into the thresholding.py file 
#should be added to the dictionary below
#==============================================================================
thresh=seg.threshold
alg={"fastsauvola":thresh.fastsauvola,"sauvola":thresh.sauvola,"adaptive":thresh.adaptive,"hysteresis":thresh.hysteresis,"otsu":thresh.otsu,"riddler_calvard":thresh.riddler_calvard,"rat":thresh.rat}

#==============================================================================
#Error Checking !!
#==============================================================================
if options.filename == None:
    parser.error("Please enter the raw data location and filename using the option -f or --file; For more help options check -h or --help")
if options.algname not in alg:
    parser.error("Invalid thresholding algorithm, For more help options check -h or --help.")
if options.voxel == None:
    parser.error("Voxel Size is required; For more help options check -h or --help")

#==============================================================================
#Getting the options from the command line
#==============================================================================

file_name = options.filename
algname = options.algname
block = options.blocksize
r = options.R
k = options.k
mode = options.mode
method = options.method
offset = options.offset
voxel = options.voxel
step=options.step
intervals=options.intervals
#==============================================================================
# FIB-SEM Segmentation Chunck Begins Here
#==============================================================================   
print "#############################################################################"
print "##                                                                         ##"
print "##          Welcome to the SEM Segmentation & Analysis Application         ##"
print "##                                                                         ##"
print "##                       PyFCell version 0.1                               ##"
print "##                                                                         ##"
print "##         Developed by : Mayank Sabharwal,                                ##"
print "##                        Andreas Putz, Arash Ash, Amit Bhatkal            ##"
print "##                                                                         ##"
print "#############################################################################"
indent='\t'
print indent,"="*50
print indent,"= Read image in sequence"
tmp=[]
f=Image.open(file_name)
for frame in ImageSequence(f):
    tmp.append(sp.asarray(frame))
print indent,"= Image Size     (",tmp[0].shape,")"
print indent,"=  Number of Images", len(tmp)

image=sp.dstack(tmp)
n,m, nslice = image.shape
theta = 1
im_eq = []
im_blr = []
im_seg = []
print indent,"="*50
print indent, "=========  The request is being analyed   ========"
print indent, "Operations being performed are :       "
print indent, "1. Histogram Equalization "
print indent, "2. Gaussian blurring "
print indent, "3. Thresholding using ", algname, " algorithm "
print ""   
print indent, "Processing  Slices :-"
print indent, "*"*50
for i in range(nslice):
    im_eq.append(np.asarray(clahe.equalize_adapthist(image[:,:,i],6,6,clip_limit = 0.05), dtype=np.uint8)) # 1
    im_blr.append(np.asarray(imtool.gaussian_blur(im_eq[i],theta),dtype=np.uint8)) # 2
    im_seg.append(np.asarray(alg[algname](im_blr[i],block_size=block,R=r,k=k,mode=mode,offset=offset,method=method), dtype = np.uint8))# 3
    sys.stdout.write("\r        Slice " + str(i+1) + " out of " + str(nslice) + " is done")
    sys.stdout.flush()
    sp.misc.imsave("Segmented_Slice_%04d.tiff" %i, im_seg[i])

print indent
print indent, "Segmentation done  !!!!"
res= np.dstack(im_seg)

#==============================================================================
#Output Writer
#==============================================================================

crunch={}
v=[voxel[0],voxel[1],0]
for i in range(nslice):
    print indent,"="*50
    print indent," Void Area of Slice ", i, " = ", sp.mean(im_seg[i]==0) 
    crunch["area"+str(i+1)]=areaSD(im_seg[i],label='Void',voxelsize=v,material=0)
    crunch["area"+str(i+1)].calcPSD()
    ax_cpsd=crunch["area"+str(i+1)].plot_distn(datatype='cpsd',label='Slice '+str(i+1))
    pl.savefig('Cumulative PSD_Slice%02d.png' %i)
    ax_psd=crunch["area"+str(i+1)].plot_distn(datatype='psd',label='Slice '+str(i+1))
    pl.savefig('Discrete PSD_Slice%02d.png' %i)
    sp.misc.imsave("Area_Slice_%02d.tiff" %i, crunch["area"+str(i+1)]._temp[i])
    pl.figure()
    pl.imshow(crunch["area"+str(i+1)]._temp[i])
    pl.colorbar()
    pl.savefig('Colored Area Distribution_Slice'+str(i+1)+'.png')
    
    
    hist=[]
    span=[]
    psd=np.asarray(crunch["area"+str(i+1)].psd)
    s=(np.asarray(crunch["area"+str(i+1)].span)).copy()

    for jj in range(intervals):
        hist.append(sp.sum(psd[s<step*(jj+1)])-sp.sum(psd[s<step*jj]))
        span.append(str(step*1000*jj)+"-"+str(step*1000*(jj+1)))
            
    import xlwt
    wbk=xlwt.Workbook()
    sheet=wbk.add_sheet('Sheet 1')
    sheet.write(0,0,'Area Range (nm2)')
    for j in range(len(span)):
        sheet.write(j+1,0,span[j])
    name='Slice '+str(i+1)
    sheet.write(0,1,name)
    for j in range(len(hist)):
        sheet.write(j+1,1,str(hist[j]*100))
    wbk.save('Sllice'+str(i+1)+'.xls')


        
        
print indent," Area Based Size Distribution Computed"
print indent,"="*50
print indent
print indent
pl.show()
