#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2012, TBD
#==============================================================================
# Segmentation Code
#   This code is developed for the purpose of segmentation of raw FIB-SEM Images
#   Input: Raw FIB-SEM images in the form of .tif image sequences
#   Output: Segmented binary Images (Solid phase = 1, Pores = 0)
#==============================================================================
"""
Created on Fri Jan 31 07:38:55 2014

@author: andput

This python executable script is designed to postprocess rheology data.
"""

from optparse import OptionParser,OptionGroup
import scipy as sp
import PyFCell
from PyFCell import rhe
from PyFCell.util.opt import fit
import pylab as pl
from itertools import cycle

g_style1=cycle([ 'bo','b-', "b*", 'b--','b.','r-' ])
g_style2=cycle([ 'bo','b-', "b*", 'b--','b.','r-' ])

#==============================================================================
#Initializing the different options for the application
#any new options should be added in this section
#==============================================================================

parser=OptionParser()
parser.add_option("-f","--file",dest="filename",action="store",help="path and filename of the FIB raw data to be segmented",metavar="FILE",type="string")

(options,args)=parser.parse_args()
file_name = options.filename

#==============================================================================
# Flowcruve Analysis
#==============================================================================
HB=rhe.scalarlaws.HershelBulkley(tau0=1,n=0.5)

dreader=rhe.flowcurves.Reader(fname=file_name)
ramp_up = dreader.get_filtered_data(column="Action_Name",criterion="Shear stress ramp up")
ramp_down = dreader.get_filtered_data(column="Action_Name",criterion="Shear stress ramp down")

crunch_up  = rhe.flowcurves.Cruncher(ramp_up)
crunch_down  = rhe.flowcurves.Cruncher(ramp_down)
crunch_combined = rhe.flowcurves.Cruncher(sp.hstack([ramp_up,ramp_down]))

crunchers = [crunch_up, crunch_down, crunch_combined]

for crunch in crunchers:
    flowdata=crunch.get_flowcurve()
    fitresult=fit(HB.gammadot,[HB.tau0,HB.K,HB.n],flowdata[1],x=flowdata[0])
    label = 'HB fit'
    label = label + ' ($ \\tau_0 = ' + str(HB.tau0()) + '$'
    label = label + ' , $K = ' + str(HB.K()) + '$'
    label = label + ' , $n = ' + str(HB.n()) + '$'
    label = label + ')'
    print "tau_0 =", HB.tau0(), "K = ", HB.K(), "n = ", HB.n()
    pl.figure(1)
    pl.plot(flowdata[0],flowdata[1],g_style1.next())
    pl.plot(sp.unique(flowdata[0]),HB.gammadot(sp.unique(flowdata[0])),g_style1.next(),label=label)
    pl.axvline(x=HB.tau0())
    fig=pl.gca()
    handles, labels = fig.get_legend_handles_labels()
    fig.legend(handles,labels,loc=2)
    #HB.plot_tau(flowdata[0],ax=plot1,label='HB fit')
    pl.figure(2)
    pl.loglog(flowdata[0],flowdata[1],g_style2.next())
    pl.loglog(sp.unique(flowdata[0]),HB.gammadot(sp.unique(flowdata[0])),g_style2.next(),label=label)
    pl.axvline(x=HB.tau0())
    fig=pl.gca()
    handles, labels = fig.get_legend_handles_labels()
    fig.legend(handles,labels,loc=2)
pl.show()