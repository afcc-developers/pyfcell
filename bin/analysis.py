#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2012, TBD
#==============================================================================
# Segmentation Code
#   This code is developed for the purpose of segmentation of raw FIB-SEM Images
#   Input: Raw FIB-SEM images in the form of .tif image sequences
#   Output: Segmented binary Images (Solid phase = 1, Pores = 0)
#==============================================================================
import sys,os
import numpy as np
import PyFCell.ips.analysis as a
import PyFCell.ips.inout as io
import pylab as pl
from optparse import OptionParser,OptionGroup

#==============================================================================
#Initializing the different options for the application
#any new options should be added in this section
#==============================================================================
parser = OptionParser()
parser.add_option("-f","--file",dest="filename",action="store",help="path and filename of the segmented data to be analyzed",metavar="FILE",type="string")
parser.add_option("-t","--tag",dest="tag",action="store",help="Tag name for the sample. Default: filename",metavar="TAG",type="string")
parser.add_option("-v","--voxel",dest="voxel",action="store",help="voxel size as x y z",metavar="VOXEL",type="float",nargs=3)
parser.add_option("-m","--mat",dest="material",action="store",help="material id to be analyzed; default=0",metavar="MATERIAL",type="int",default=0)
parser.add_option("-l","--label",dest="label",action="store",help="Material Description: eg. Pore",metavar="LABEL",type="string",default="Pore")

group = OptionGroup(parser,"Tortuosity Parameter")
group.add_option("-n","--numb",dest="tracernum",action="store",help="Tracer Numbers to be used. default=500",metavar="NUMBER",type="int",default=500)
parser.add_option_group(group)

(options,args)=parser.parse_args()

#==============================================================================
#Error Checking !!
#==============================================================================
if options.filename == None:
    parser.error("Please enter the raw data location and filename using the option -f or --file; For more help options check -h or --help")
if options.voxel == None:
    parser.error("Voxel Size is required; For more help options check -h or --help")
if options.tag == None:
    tmp = os.path.split(options.filename)[1]
    tag = os.path.splitext(tmp)[0]
else:
    tag = options.tag
    
#==============================================================================
#Getting the options from the command line
#==============================================================================
filename = options.filename
material = options.material
voxel = [options.voxel[0],options.voxel[1],options.voxel[2]]
label = options.label
tracernum = options.tracernum

#==============================================================================
#Analysis Application Definition
#==============================================================================
    
print "#############################################################################"
print "##                                                                         ##"
print "##           Welcome to the Data Analysis Application                      ##"
print "##                                                                         ##"
print "##                       PyFCell version 0.1                               ##"
print "##                                                                         ##"
print "##         Developed by : Mayank Sabharwal,Andreas Putz                    ##"
print "##                        Arash Ash, Amit Bhatkal                          ##"
print "##                                                                         ##"
print "#############################################################################"


indent = '\t'
print indent
print indent,"*"*50
print indent, "The application performs the following calculations:"
print indent, "1. Basic Statistics and Deviations"
print indent, "2. Tortuosity in X,Y,Z Directions"
print indent, "3. Area Based Size Distribution"
print indent, "4. Fitted Sphere Size Distribution"
print indent, "5. Output 3D structures with distributions"
print indent, "*"*50
print indent

image=io.reader(filename).getarray()
crunch={}

print indent,"="*50
print indent," Computing Basic Statistcs"
crunch["basic"] =  a.basicStats(image,voxelsize=voxel,material=material,label=label)
crunch["basic"].makeStats()
ax_bar=crunch["basic"].plot_bar(label=tag)
ax_stat=crunch["basic"].plot_stat(label=tag)
print indent
print indent

print indent,"="*50
print indent," Computing Tortuosity in X,Y,Z directions"
crunch["tortX"]=a.tortuosity(image,voxelsize=voxel,tracernum=tracernum,material=material,label=label,direction='X')
crunch["tortX"].calc()
crunch["tortY"]=a.tortuosity(image,voxelsize=voxel,tracernum=tracernum,material=material,label=label,direction='Y')
crunch["tortY"].calc()
crunch["tortZ"]=a.tortuosity(image,voxelsize=voxel,tracernum=tracernum,material=material,label=label,direction='Z')
crunch["tortZ"].calc()
ax_bar=crunch["tortX"].plot_tracer(label=tag)
ax_bar=crunch["tortY"].plot_tracer(label=tag)
ax_bar=crunch["tortZ"].plot_tracer(label=tag)
print indent
print indent


crunch["area"]=a.areaSD(image,label=label,voxelsize=voxel,material=material)
crunch["area"].calcPSD()
ax_cpsd=crunch["area"].plot_distn(datatype='cpsd',label=tag)
ax_psd=crunch["area"].plot_distn(datatype='psd',label=tag)
print indent," Area Based Size Distribution Computed"
print indent,"="*50
print indent
print indent

crunch["sphere"]=a.distanceSD(image,label=label,voxelsize=voxel,material=material)
crunch["sphere"].calcPSD()
ax_cpsd=crunch["sphere"].plot_distn(datatype='cpsd',label=tag)
ax_psd=crunch["sphere"].plot_distn(datatype='psd',label=tag,ax=ax_cpsd)
print indent," Fitted Sphere Size Distribution Computed"
print indent,"="*50
print indent

#==============================================================================
#Writing 3D Geometry Output
#==============================================================================

data={}
data["Solid-Pore"] = image.astype('uint8')
data["AreaSD"] = crunch["area"]._imAreaSD.astype('float32')
data["SphereSD"] = (crunch["sphere"].imPSD).astype('float32')
data["DistanceTransform"] = (crunch["sphere"].imDistanceTransform*crunch["sphere"]._rangeX[1]).astype('float32')

io.writer(data,"Analysed_"+tag+".vti",voxelsize=voxel)

pl.show()