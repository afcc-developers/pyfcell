#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2012, TBD
#==============================================================================
# Segmentation Code
#   This code is developed for the purpose of segmentation of raw FIB-SEM Images
#   Input: Raw FIB-SEM images in the form of .tif image sequences
#   Output: Segmented binary Images (Solid phase = 1, Pores = 0)
#==============================================================================
import sys,os
import numpy as np
import PyFCell.ips.util.ImageTools as imtool
import PyFCell.ips.seg as seg
import PyFCell.ips.inout as io
import PyFCell.ips.util.clahe as clahe
from optparse import OptionParser,OptionGroup


#==============================================================================
#Initializing the different options for the application
#any new options should be added in this section
#==============================================================================
parser=OptionParser()
parser.add_option("-f","--file",dest="filename",action="store",help="path and filename of the FIB raw data to be segmented",metavar="FILE",type="string")
parser.add_option("-t","--thresh",dest="algname",action="store",help="thresholding algorithm to be used:{'fastsauvola','sauvola','adaptive','otsu','riddler_calvard',hysteresis','rat'}",metavar="ALGORITHM",type="string",default="fastsauvola")
parser.add_option("-b", "--block",action="store",dest="blocksize",help="optional;block or window size used for sauvola and adaptive",default=20,type="int")

group = OptionGroup(parser,"Sauvola",
                  "Additional parameters for sauvola thresholding algorithm. "
                  "Caution:Too much fidgeting is not good!")

group.add_option("-r", action="store",dest="R",help="range of Std. Deviation which is usually chosen as 128 for 8 bit images",default=128,type="float")
group.add_option("-k", action="store",dest="k",help=" adjustable scaling constant (usually 0.4 - 0.6)",default=0.5,type="float")
parser.add_option_group(group)


group = OptionGroup(parser,"Adaptive",
                  "Additional parameters for adaptive thresholding algorithm. "
                  "Caution:Too much fidgeting is not good!")
group.add_option("-m","--method", action="store",dest="method",help="adaptive thresholding method {'gaussian', 'mean', 'generic', 'median'}. Default is 'gaussian'",default="gaussian")
group.add_option("-o","--offset", action="store",dest="offset",help="constant subtracted from weighted mean, default = 0",default=0,type="float")
group.add_option("--mode", action="store",dest="mode",help="determines how array borders are handled: 'reflect', 'constant', 'nearest', 'mirror', 'wrap'",default="reflect")
#group.add_option("-p","--param", action="store",dest="param",help="specifies sigma for gaussian method",default=None,type="float")
parser.add_option_group(group)


(options,args)=parser.parse_args()

#==============================================================================
#intializing the algorithms available for segmentation
#any new algorithms entered into the thresholding.py file 
#should be added to the dictionary below
#==============================================================================
thresh=seg.threshold
alg={"fastsauvola":thresh.fastsauvola,"sauvola":thresh.sauvola,"adaptive":thresh.adaptive,"hysteresis":thresh.hysteresis,"otsu":thresh.otsu,"riddler_calvard":thresh.riddler_calvard,"rat":thresh.rat}

#==============================================================================
#Error Checking !!
#==============================================================================
if options.filename == None:
    parser.error("Please enter the raw data location and filename using the option -f or --file; For more help options check -h or --help")
if options.algname not in alg:
    parser.error("Invalid thresholding algorithm, For more help options check -h or --help.")

#==============================================================================
#Getting the options from the command line
#==============================================================================

file_name = options.filename
algname = options.algname
block = options.blocksize
r = options.R
k = options.k
mode = options.mode
method = options.method
offset = options.offset

#==============================================================================
# FIB-SEM Segmentation Chunck Begins Here
#==============================================================================   
print "#############################################################################"
print "##                                                                         ##"
print "##          Welcome to the FIB-SEM Segmentation Application                ##"
print "##                                                                         ##"
print "##                       PyFCell version 0.1                               ##"
print "##                                                                         ##"
print "##         Developed by : Arash Ash, Mayank Sabharwal,                     ##"
print "##                        Andreas Putz, Amit Bhatkal                       ##"
print "##                                                                         ##"
print "#############################################################################"

image=io.reader(file_name).getarray()
n,m, nslice = image.shape
indent='\t'
theta = 1
im_eq = []
im_blr = []
im_seg = []
print indent,"="*50
print indent, "=========  The request is being analyed   ========"
print indent, "Operations being performed are :       "
print indent, "1. Histogram Equalization "
print indent, "2. Gaussian blurring "
print indent, "3. Thresholding using ", algname, " algorithm "
print ""   
print indent, "Processing  Slices :-"
print indent, "*"*50
for i in range(nslice):
    im_eq.append(np.asarray(clahe.equalize_adapthist(image[:,:,i],6,6,clip_limit = 0.05), dtype=np.uint8)) # 1
    im_blr.append(np.asarray(imtool.gaussian_blur(im_eq[i],theta),dtype=np.uint8)) # 2
    im_seg.append(np.asarray(alg[algname](im_blr[i],block_size=block,R=r,k=k,mode=mode,offset=offset,method=method), dtype = np.uint8))# 3
    sys.stdout.write("\r        Slice " + str(i+1) + " out of " + str(nslice) + " is done")
    sys.stdout.flush()
print indent
print indent, "Segmentation done  !!!!"
res= np.dstack(im_seg)

#==============================================================================
#Output Writer
#==============================================================================
print indent, "*"*50
print indent, "Writing output"
(path,filename) = os.path.split(file_name)
outname = 'Segmented_'+filename
io.writer(res,outname)
data={}
data["MaterialID"]=res
data["original"]=image
outname = os.path.splitext(outname)[0]+'.vti'
print indent, "Solid = 255"
print indent, "Void = 0"
io.writer(data,outname)
print indent,"="*50