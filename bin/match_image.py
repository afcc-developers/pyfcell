#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2012, TBD
#=======================================================================================
# Image Pattern Matching Code
#   This code is developed to find a template image within a source image
#   Input: Image to be searched & template image to be searched
#   Output: Figures indicating the location of the region with the maximum confidence
#=======================================================================================
import cv2
import argparse
import PyFCell.ips.inout as io
from PyFCell.ips.util import sift_matching
from matplotlib import pyplot as plt
parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('image', metavar='image', 
                   help='image to be searched for the pattern')
parser.add_argument('template', metavar='template',  
                   help='template image to be searched')
parser.add_argument('--sift',dest='sift_switch',action='store_true',help='use SIFT alogirthm for matching instead of conventional correlations')
parser.set_defaults(sift_switch=False)
indent='\t'
args = parser.parse_args()
im = io.reader(args.image).getarray()
template = io.reader(args.template).getarray()
im=im.reshape([im.shape[0],im.shape[1]])
template=template.reshape([template.shape[0],template.shape[1]])
if args.sift_switch:
    o=sift_matching(im,template,display_match='ON',region_detect='ON')
else:
        
    img2 = im.copy()
    w, h = template.shape[::-1]
    w=int(w/2.5)
    h=int(h/2.5)
    
    # All the 6 methods for comparison in a list
    methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
                'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']
    data={}
    for meth in methods:
        img = img2.copy()
        method = eval(meth)
    
        # Apply template Matching
        res = cv2.matchTemplate(img,template,method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    
        # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
        if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
            top_left = min_loc
        else:
            top_left = max_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)
        cv2.rectangle(img,top_left, bottom_right, 255, 20)
        print indent,"="*80
        print indent, "Comparing the images using "+str(meth)
        print indent, "Statistics for the Method"
        print indent, "_"*80
        print indent, "Minimum Correlation Value: ", min_val, indent, "Coordinates: ", min_loc 
        print indent, "Maximum Correlation Value: ", max_val, indent, "Coordinates: ", max_loc
        print ""   
        print indent, "*"*80        
        data[meth]=(min_val,max_val,min_loc,max_loc)
        
        plt.figure()
        plt.subplot(131),plt.imshow(template,cmap = 'gray')
        plt.title('Query Image'), plt.xticks([]), plt.yticks([]) 
        
        plt.subplot(132),plt.imshow(res,cmap = 'gray')
        plt.title('Matching Result'), plt.xticks([]), plt.yticks([]) 
        
        plt.subplot(133),plt.imshow(img,cmap = 'gray')
        plt.title('Detected Region'), plt.xticks([]), plt.yticks([])
        plt.suptitle(str(meth))
        plt.savefig(args.image+'_'+meth+'.png')
    import pickle
    f=open('data','w')
    pickle.dump(data,f)
    f.close()
    plt.show()