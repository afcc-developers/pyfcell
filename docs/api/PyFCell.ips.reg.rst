reg Package
===========

:mod:`reg` Package
------------------

.. automodule:: PyFCell.ips.reg
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`feature` Module
---------------------

.. automodule:: PyFCell.ips.reg.feature
    :members:
    :undoc-members:
    :show-inheritance:

