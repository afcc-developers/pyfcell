.. FCSTpython documentation master file, created by
   sphinx-quickstart on Thu Nov 29 11:31:33 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
*****************************************
PyFCell: Python for fuel cell development
*****************************************


PyFCell is a selection of python classes, scripts used at AFCC.

Documentation
=============

Contents:

.. toctree::
   :maxdepth: 1

   getting_started.rst
   stxm/introduction.rst
   api/PyFCell.rst
   developer_guide.rst
   api.rst
   docSandbox.rst

.. todolist::

Documentation is available in the docstrings and in ths sphinx documentation.


Contents
--------
The OpenPNM package imports all the functions from the top level modules.

 
Import
------
>>> import OpenPNM as PNM


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

