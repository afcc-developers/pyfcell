.. _stxm_intro:


********************************************
XFCell - The Stxm graphical user interface
********************************************

.. image:: images/XFCellLogo.png
	:width: 200px
	:height: 200px
	:align: left
	:alt: XFCell Logo

XFCell is a python based image processing package for Scanning Transmission X-ray Microscopy(STXM). The objective of 
this package is to provide a user friendly interface for mass processing of STXM data. The Graphical User Interface (GUI) 
is designed based on the natural flow of STXM data analysis. It provides a semi-automated platform which starts by
opening raw data to creating quantitative profiles of different components.

The uniquness of XFCell lies in its user-friendlyness and full access to undelying algorithms. It is user-friendly 
in the sense that it is tailored in close collaboration with STXM experts to minimize the complexity of data processing. The 
whole package together with underlying source code is developed at Automotive Fuel Cell Corporation (AFCC) which gives the
development team full knowledge and access to the source code for further alteration if needed. 

This page presents some background information together with tutorials on how to operate XFCell. 

Structure and Flowchart
=======================

Structure
---------

Explorer file tree design is used in development of XFCell. This design provides separate property window for each
process which results a cleaner overall look. The overall software view is comprised of:
	#. **Workflow Tree**: Data processing functions designed based on STXM data processing workflow. 
	#. **Property window**: Available properties of the functions selected in the workflow tree.
	#. **Graphics Viewbox**: Canvas to display images and ROIs capable of zooming in/out, adjust brightness, change color-map and movement.
	#. **Plot dialog**: Plot display unit capable of zooming in/out, auto-scale and flipping through data slices.
	#. **Slice list**: List of images in the data stack marked with corresponding energy.
	#. **Region of Interest (ROI) list**: List of ROIs added by user.

.. image:: images/structure.png
	:width: 600px
	:align: center
	:height: 400px
	:alt: Structure

Flowchart
----------

A simplistic flowchart of the software is presented below.

.. image:: images/flowchart.png
	:width: 600px
	:align: center
	:height: 500px
	:alt: flowchart

Data Processing
================

STXM data processing is explained in this section. This section presents a step-by-step analysis from loading raw data to 
producing component maps. This process involves:
	#. Opening raw data file
	#. Processing: Registration, I0 selection, OD calculation, Spectra selection, component mapping and etc.
	#. Postprocessing: Thresholding, line-scans and etc.
 
Data Loading
------------

XFCell is capable of loading image data in following formats: hdr, ncb, axb, xfc(XFCell file format)
2D and 3D data arrays can be opened using **Open File** function. 
 
.. image:: images/open.png
	:width: 600px
	:align: center
	:height: 400px
	:alt: Loading Data

In some instances multiple stacks are saved in a form of the *super-stack* in a single HDR file. In these cases a window pop-up 
will prompt the user to select the desired stack number.

After loading the data file it will be presented in the graphics viewbox. Note that energy of the selected slice together with min and max 
pixel value is shown in the graphics viewbox. Opening a new data set resets the slice list and ROI list.

.. image:: images/gv.png
        :width: 600px
        :align: center
        :height: 400px
        :alt: Opened Data



Processing
----------

Registration
^^^^^^^^^^^^

Acquiring images at a range of energies often lead to image shifts due to error in stage adjustment. These mis-alignments 
occur in a random fashion which makes the registration algorithm complex. Image registration is one of the most important 
aspects of STXM data analysis. Alignment in XFCell is handled through **Registration** function. This function offers 
both quick and a more thorough registration process. To introduce the registration algorithm a quick introduction to the 
available options is needed. Brief description of these options are presented in the following table.

.. list-table:: Registration Parameters.
	:widths: 20 20 60
	:header-rows: 1

	* - Parameter
	  - Value
	  - Description
	* - Method
	  - fft, sift
	  - Registration Algorithm, Fourier cross-correlation and feature detection algorithm respectively
	* - Data
	  - Whole Image, ROI Data
	  - Data input to the registration algorithm. Either whole image or a portion(ROI) could be used.
	* - Filter
	  - No Filter, Sobel
	  - Filters data input using an edge sharpening algorithm. A Gaussian of Laplacian algorithm is used followed by a sobel edge detection algorithm.
	* - Crop
	  - Auto Crop, No Crop
	  - Crop On/Off, if Auto Crop, the output image will be automatically cropped
	* - Anchor
	  - Selected Image
	  - Image with the best focus and sharp featured should be selected as the reference image for the registration algorithm
	* - Stack Head/Tail Treatment
	  - Stack Interpolation, Unchanged, Linear anchored on first/last image, Extrapolated
	  - To be used when images from the beginning or end of the stack are removed. for more details refer to user manual.
	* - Ignore Shift in
	  - None, x axis, y axis
	  - Ignores image translation in one direction, if None, translation in both directions will be taken into account.
	* - Curve Degree
	  - value between 1 to 5
	  - Degree of smoothing spline used in curve fitting process. if 0, the fitting curve will go through all data points. For more info refer to SciPy.interpolate.UnivariateSpline package.
	* - Smoothing Factor
	  - Positive Value
	  - Positive smoothing factor used to choose number of knots, number of knots should be increased until smoothing is satisfied. if >10e4, linear fit will be resulted.
	* - Distance
	  - value in pixel
	  - For Sift algorithm only. Maximum translation allowed by the Sift algorithm in pixels.

A snap-shot of the **Registration** function is illustrated in the image above. **Quick Register** offers a quick alignment using the following default parameters:

.. list-table:: Quick Register Parameters
        :widths: 50 50
        :header-rows: 1

        * - Parameter
          - Value
        * - Method
          - fft
        * - Data  
          - Whole Image
        * - Filter
          - No Filter  
        * - Crop
          - Auto Crop
        * - Anchor
          - First Image
        * - Stack Head/Tail Treatment
          - Not Applicable, since all images are used.
        * - Ignore Shift in
          - None
        * - Curve Degree
          - 3
        * - Smoothing Factor
          - 50
        * - Distance
          - Not Applicable, since FFT is used.

As mentioned before alignment could be a tricky task. If for any reason the *Quick Register* option wasn't satisfactory the *most recent* registration step may be ignored by **Reject** button. 
This reverts the *most recent* alignment process and restores the data stack. For a more thorough registration **Registration Assistant** method may be used. A snap-shot of the **Registration Assistant** 
is shown in image below.

.. image:: images/reg.png
	:width: 600px
	:align: center
	:height: 400px
	:alt: Registration Assistant

**Registration Assistant** consists of two graphical viewboxes, slice list, plot dialog and property window. The upper viewbox is the illustration of the data set to be fed into the registration algorithm. 
The lower viewbox illustrates the outcome of the registration algorithm. Both viewboxes offer animation cabapilities. When activated a timer flips through the stack of images in a form of a movie. It is the 
best practice to remove images with features with no distinct border. The rule of thumb is if human eye can't identify the featured and edges, alignment algorithm won't pick them up either. Removed images will 
be removed from the slice list and upper view box consequently. For more information about the registration assistant refer to the manual.

I0 Selection
^^^^^^^^^^^^

After aligning the stack, the I0 spectrum should be selected for OD normalization. For the test sample provided here, the hole in the bottom part of the sample will be used for the I0 spectrum. A new ROI 
is added and the corresponding spectrum is automatically shown in the plot dialog. Once the user is satisfied with I0 spectrum, he or she should tell the software to use that spectrum for OD calculation. 
This is done through **I0 Selection** function which is shown in the image below.

.. image:: images/I0.png
        :width: 600px
        :align: center
        :height: 400px
        :alt: I0 Selection

In the properties section the user has the options of pulling the spectrum from the ROI list(Spectrum loaded from files will also appear in this list) by *Select Spectrum for I0* or do a *Line I0*. *Select 
Spectrum for I0* will use the average of the selected ROI for each image as the reference I0 value for OD normalization. However, *Line I0* calculates OD of each *line* of the image, either horizontal or 
vertical, with respect to the average I0 value calculated for each line of the ROI. Obviously for this mode the height of the ROI should be equal to the height of the image if horizontal line I0 is considered 
and vice versa for vertical line I0. If line I0 is selected a new window pops-up prompting the user to pick the direction(horizontal or vertical) and the range of pixels to be considered. for more information 
on *Line I0* refer to the manual. for the test sample being considered in the illustration the line I0 is not applicable and an average value is used by selecting an ROI and *Select Spectrum for I0* method. It 
should be noted that after I0 is selected by the user, a check mark sign will appear beside the I0 section in the *workflow tree* meaning that the software has all information to calculate OD.

Once the *Select Spectrum for I0* button in pressed a new window will pop-up with the selected spectrum and its corresponding standard deviation. The spectrum and std for the test sample is shown below.

.. image:: images/I0Spec.png
        :width: 600px
        :align: center
        :height: 400px  
        :alt: I0 Spectrum   

Other functions available in the I0 section such as **Energy Shift** and **Compare I0** are explained in the manual.

OD Calculation
^^^^^^^^^^^^^^^

In some cases the I0 stack is different from the Sample stack. For these cases once the I0 spectrum is identified, the Sample stack should be loaded into the software separately in the Sample section. For such
cases similar Registration function is available for aligning the Sample stack. However, in other cases, like the example illustrated here, in which I0 is extracted from a part of the data stack, the same data 
stack which is already aligned can be used in the *Sample Tree*. For this purpose the **Open File** function of the *Sample Tree* offers the option of *Use I0 Stack* which transfers the I0 stack from the *I0 Tree* 
to the *Sample Tree*. Once stack is transferred to the *Sample Tree*, OD normalization may be performed using the **Optical Density** function as shown below. This method normalizes the Sample Stack from Intensity
to OD units.

.. image:: images/OD.png
        :width: 600px
        :align: center
        :height: 400px
        :alt: I0 Spectrum
 
*Sample Tree* like the *I0 Tree* offers more functions such as **Energy Shift** and **Spectrum Calculator** which are explained in the user manual.


Spectra Selection
^^^^^^^^^^^^^^^^^^
Once the sample is normalized, different components present in the sample can be identified based on their spectral finger print. To get quantitative information from the sample, pure material spectra is required. 
At the time, XFCell is not capable of producing pure material spectra and they should be loaded to the software in the form of a txt file. However, for illustration purposes, material spectra of components available 
in the sample may be pulled from the sample for qualitative analysis. For this purpose, spectrums of the materials 1,2,3 present in the test sample are pulled by creating 3 ROIs. This is shown in the figure below.

.. image:: images/SpecSelect.png
        :width: 600px
        :align: center
        :height: 400px
        :alt: I0 Spectrum
 

It should be noted that the ROI list is editable. By default added ROIs are names by the sample filename and number of ROI. These ROIs can be then removed from the list or renamed as needed.

Component Mapping
^^^^^^^^^^^^^^^^^^

*Component Mapping* window looks a little different comparing to those of *I0* and *Sample Tree*. in the graphical section it is comprised of a viewbox for image illustration, two sectional plot dialogs which 
plot the pixel values along horizontal and vertical cross-hairs shown on the viewbox(shown in yellow) and a histogram for thresholding purposes. *Slide list* contains the list of component maps and any added image 
from image manipulation. ROI list is also replaced with a plot dialog which illustrates the measured linescans. A snap-shot of the *Component Mapping* window for the example sample are shown below.

.. list-table:: Component Mapping
	:widths: 10 90
	:header-rows: 1

	* - Material
	  - Image 
	* - Material 1
	  - .. image:: images/Mat1.png
		:width: 400px
		:height: 300px
		:align: center
	* - Material 2	
	  - .. image:: images/Mat2.png
		:width: 400px
		:height: 300px
		:align: center
	* - Material 3
	  - .. image:: images/Mat3.png
		:width: 400px
		:height: 300px
		:align: center

After calculating the component maps, some post processing might be required based on user requirements. In this example we are going to create linescans of the Material 2 divided by Material 3 which is 
explained in the following section.

Post-Processing
----------------

Thresholding
^^^^^^^^^^^^^

For this illustration  we would like to know what the ratio of the Material 3 and Material 2 is in the region were Material 2 is more dominant. For this task first we should mask the region where Material 2 
is more dominant. Histogram thresholding is used for this purpose. Min and Max level of the histogram is manualy adjusted so that the area of interest is highlighted in red. After adjusting the levels, the area 
is thresholded to create a mask. The newly created mask is automatically added to the *Slide List* as shown in the following figure.

.. image:: images/mask.png
	:width: 600px
	:align: center
	:height: 400px
	:alt: Mask

Map Manipulation
^^^^^^^^^^^^^^^^^

The next step is to cut the required area(area in which Material 2 is more dominant) from Material 2 and 3 maps. This is done by multiplying the created mask and Material 2 and 3 maps. This is performed by 
**Image - Image Manipulation** function. The required images and the operation may be selected from the drop-down lists. for this example *Material 2_masked* image is multiplied by Material 2 and 3.  It 
should be noted that the white area in the mask image represents the area of interest and the rest of the images will be discarded after multiplication. Masked images of Material 2 and 3 are shown below. It 
should be also noted that the resultant images are automatically added to the *slide list* instantly.

.. list-table:: Material Masks
	:widths: 10 90
	:header-rows: 1

	* - Mask
	  - Image
	* - Material 2
	  - .. image:: images/MatMask.png
		:width: 400px
		:align: center
		:height: 300px
	* - Material 3
	  - .. image:: images/MatMask1.png
		:width: 400px
		:align: center
		:height: 300px


The next step is to divide masked image of the material 3 by that of material 2. This can be also done using the **Image - Image Manipulation** function.


Line-scan
^^^^^^^^^^

In order to illustrated the ratio of Material 3 and Material 2, the horizontal *Line-scan* is performed which integrates all the horizontal lines of the selected region (shown by green lines in the image). 
For this example horizontal lines-scans of the area shown in the image was calculated on the resultant image of Material 3/Material 2. The resultant plot is automatically generated and added to the *Plot Dialog*. 
The details of the lines scan calculation is presented in the Manual. 

.. image:: images/lineScan.png
	:width: 600px
	:align: center
	:height: 400px
	:alt: Line Scans

RGB
^^^^

The other interesting way of presenting the data is to create an RGB image in which each component is represented by Red, Blue or Green. RGB images of the components can be created using **RGB** function in the 
*Component Mapping Tree*.

.. image:: images/RGB.png
	:width: 600px
	:align: center
	:height: 400px
	:alt: RGB Image

