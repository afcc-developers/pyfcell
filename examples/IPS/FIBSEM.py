#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Author: AFCC Development Team
# License: TBD
# Copyright (c) 2012, TBD
#==============================================================================
# Segmentation Code
#   This code is developed for the purpose of segmentation of raw FIB-SEM Images
#   Input: Raw FIB-SEM images in the form of .tif image sequences
#   Output: Segmented binary Images (Solid phase = 1, Pores = 0)
#==============================================================================
r"""
*****************************************************************************************
:mod:`PyFCell.ips.util.fibsem` -- FIBSEM segmentation sequence for Image Processing Suite
*****************************************************************************************


Contents
========
The PyFCell package imports all the functions from the top level modules. Module for FIBSEM segmentation sequence.
 
Import
======
>>> import PyFCell as pfc

Submodules
==========

Classes
=======

"""


import numpy as np
import PyFCell.ips.util.ImageTools as imtool
import PyFCell.ips.seg as seg
import PyFCell.ips.inout as io
import PyFCell.ips.util.clahe as clahe

thresh = seg.threshold()

def fibsem_sequence(file_name):
    
    r"""
    returns segmented images in a file called Segmented.tif
    
    file_name
    
    Parameters
    ----------
    file_name : string of location and name of the file
    
    Examples
    --------
    >>> import PyFCell.ips.util.ImageTools as imtool
    >>> import PyFCell.ips.inout as io
    >>> file_name = "../../data/FIBSEM/Carmen/DH-22.tif"
    >>> fibsem_sequence(file_name)
        
    .. plot::
                
        import pylab as pl
        import numpy as np
        file_name = "../../data/FIBSEM/Carmen/DH-22.tif"
        fibsem_sequence(file_name)
        
    
    """
#    file_name = "../../data/FIBSEM/Carmen/DH-22.tif"
    
    image=io.reader(file_name).getarray()
    n,m, nslice = image.shape
    
    theta = 1
    im_eq = []
    im_blr = []
    im_seg = []
    
    print "The request is being analyed..."
    for i in range(nslice):
        im_eq.append(np.asarray(clahe.equalize_adapthist(image[:,:,i],6,6,clip_limit = 0.05), dtype=np.uint8)) # 1
        im_blr.append(np.asarray(imtool.gaussian_blur(im_eq[i],theta),dtype=np.uint8)) # 2
        im_seg.append(np.asarray(thresh.fastsauvola(im_blr[i],1), dtype = np.uint8))# 3
        print "Slice " + str(i+1) + " out of " + str(nslice) + " is done"
    
    res= np.dstack(im_seg)
    io.writer(res,'Segmented.tif')
